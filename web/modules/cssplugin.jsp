<%-- 
    Document   : plugins
    Created on : Jul 31, 2017, 3:00:01 PM
    Author     : RubySenpaii
--%>

<!-- Bootstrap Core CSS -->
<link href="/TCP/vendor/bootstrap/css/bootstrap.min.css" rel="stylesheet">
<!-- MetisMenu CSS -->
<link href="/TCP/vendor/metisMenu/metisMenu.min.css" rel="stylesheet">
<!-- DataTables CSS -->
<link href="/TCP/vendor/datatables-plugins/dataTables.bootstrap.css" rel="stylesheet">
<!-- DataTables Responsive CSS -->
<link href="/TCP/vendor/datatables-responsive/dataTables.responsive.css" rel="stylesheet">
<!-- Custom CSS -->
<link href="/TCP/dist/css/sb-admin-2.css" rel="stylesheet">
<link href="/TCP/assets/css/custom.css" rel="stylesheet">
<!-- Custom Fonts -->
<link href="/TCP/vendor/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css">
