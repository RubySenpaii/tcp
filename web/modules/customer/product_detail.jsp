<%-- 
    Document   : product_detail
    Created on : Aug 10, 2017, 5:31:51 PM
    Author     : RubySenpaii
--%>

<%@page import="object.Product"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">

        <jsp:include page="../cssplugin.jsp"/>
        <title>TCP - Customer Home Page</title>
    </head>
    <body>
        <div id="wrapper">

            <!-- Navigation -->
            <nav class="navbar navbar-default navbar-static-top" role="navigation" style="margin-bottom: 0">
                <jsp:include page="_navbar.jsp"/>
            </nav>

            <div class="container-fluid customer-content">
                <jsp:include page="_header.jsp"/>

                <div class="row">
                    <%
                        Product product = (Product) session.getAttribute("selectedProduct");
                        String userType = (String) session.getAttribute("userType");
                    %>
                    <div class="col-md-5 col-md-offset-1">
                        <img src="assets/img/products/<%=product.getImage()%>" class="product-detail-image">
                    </div>
                    <form action="CustomerAddCart">
                        <div class="col-md-5 product-detail-description">

                            <h3>Product Name: <text class="product-detail"><%=product.getProductName()%></text></h3>
                            <h3>Category: <text class="product-detail"><%=product.getCategory()%></text></h3>
                            <h3>Description: <text class="product-detail"><%=product.getDescription()%></text></h3>

                            <%
                                if (userType.equals("customer")) {
                            %>
                            <label style="font-size: 24px; font-weight: 500; margin-top: 40px;">Quantity</label>
                            <input style="width: 50px;" type="number" name="orderQuantity" value="1" min="1" max="<%=product.getStockQuantity()%>">

                            <button class="pull-right btn btn-success" style="margin-top: 40px;" name="action" value="addToCart">Add to Cart</button>
                            <%
                                }
                            %>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </body>

    <jsp:include page="../scriptplugin.jsp"/>
</html>
