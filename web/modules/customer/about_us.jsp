<%-- 
    Document   : about_us
    Created on : Nov 3, 2017, 5:26:16 PM
    Author     : RubySenpaii
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">

        <jsp:include page="../cssplugin.jsp"/>
        <title>TCP - About Us</title>
    </head>
    <body>
        <div id="wrapper">

            <!-- Navigation -->
            <nav class="navbar navbar-default navbar-static-top" role="navigation" style="margin-bottom: 0">
                <jsp:include page="_navbar.jsp"/>
            </nav>

            <div class="container-fluid">
                <div class="row">
                    <div class="col-md-6 col-md-offset-3">
                        <div class="panel-body">
                            <img src="/TCP/assets/img/resource/TCP Store.png" height="350px" width="100%">
                            <br>
                            <h3>About TCP</h3>
                            <p>TCP Enterprises, Inc. started off as PIC Enterprises established in 1933 by its sole proprietor, Mr. Ong Sue.  
                                In 2002, Mr. Sue decided to pass the business on to his nephew, Mr. Fernando Tiong, who established the name, TCP Enterprises, Inc. and formed a corporation. 
                                As the demand for construction supplies increased, Mr. Tiong incorporated additional products, which consists of retailing of electrical supplies, hardware, lightings, and other construction needs. 
                                Mr.Tiong expanded the business not only to walk-in customers and residential construction, 
                                but also to high rise buildings, catering to clients from Baguio and to neighboring provinces.</p>
                            <br>
                            <h3>Mission</h3>
                            <p>TCP Enterprises is committed to building long term relationships based on integrity, performance value, and client satisfaction. 
                                We always strive to provide the highest value to our customers and community with a critical eye towards quality. 
                                We will continue to meet the changing needs of our clients with our quality service delivered by the most qualified people, 
                                satisfying them on their desired needs with our quality products. </p>
                            <br>
                            <h3>Vision</h3>
                            <p>“We strive to grow our organization through investigating advancing technology, industry changes, and the varying needs of our clients as our industry evolves. 
                                TCP Enterprises is not just a retail company, for we are dedicated team striving to bring growth to our community 
                                and assisting our clients in making their dreams become a reality.” </p>
                            <br>
                            <h3>Store Information</h3>
                            <p>Located at: No. 118 Upper Bonifacio Street, Corner Sumulong Street, Baguio City, Benguet</p>
                            <p>Tel Nos: (074) 443 – 3609, 446 – 0113, 304 – 8989</p>
                            <p>Telefax No: 424 – 4670 </p>
                            <p>Email: tcpenterprises@gmail.com</p>
                            <p>Google Map: TCP Enterprisses Incorporated</p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </body>

    <jsp:include page="../scriptplugin.jsp"/>
</html>
