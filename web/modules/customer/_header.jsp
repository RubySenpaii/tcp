<%-- 
    Document   : _header
    Created on : Aug 2, 2017, 7:07:34 PM
    Author     : RubySenpaii
--%>

<%@page import="java.util.ArrayList"%>
<div class="row customer-header">
    <div class="col-md-3 col-header">
        <a class="btn btn-default btn-header" href="/TCP/CustomerHomepage?action=homepage">All Products</a>
    </div>
    <div class="col-md-6 col-header">
        <form action="/TCP/CustomerHomepage">
            <div class="form-group input-group">

                <input type="text" class="form-control btn-header" placeholder="Search..." name="keyword">
                <span class="input-group-btn">
                    <button class="btn btn-default btn-header" type="submit" name="action" value="filter-keyword"><i class="fa fa-search"></i>
                    </button>
                </span>
            </div>
        </form>
    </div>
    <div class="col-md-3 col-header">
        <button class="btn btn-header dropdown-toggle" data-toggle="dropdown">Categories</button>
        <ul class="dropdown-menu pull-right" role="menu">
            <%
                ArrayList<String> categories = (ArrayList<String>) session.getAttribute("categories");
                for (int a = 0; a < categories.size(); a++) {
            %>
            <li>
                <a href="/TCP/CustomerHomepage?action=filter-category&category=<%=categories.get(a)%>"><%=categories.get(a)%></a>
            </li>
            <%
                }
            %>
        </ul>
    </div>
</div>