<%-- 
    Document   : order_review
    Created on : Aug 2, 2017, 9:51:11 PM
    Author     : RubySenpaii
--%>

<%@page import="extra.Reference"%>
<%@page import="object.Customer"%>
<%@page import="object.Product"%>
<%@page import="java.util.ArrayList"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">

        <jsp:include page="../cssplugin.jsp"/>
        <title>TCP - Customer Order</title>
    </head>
    <body>
        <div id="wrapper">

            <!-- Navigation -->
            <nav class="navbar navbar-default navbar-static-top" role="navigation" style="margin-bottom: 0">
                <jsp:include page="_navbar.jsp"/>
            </nav>

            <div class="container-fluid customer-content">

                <div class="row customer-content" style="margin-top: 0px">
                    <div class="col-lg-12">
                        <h2 class="page-header">Order Review Page</h2>
                    </div>

                    <div class="col-md-12">
                        <h4>Customer Details</h4>
                        <address>
                            <%
                                Customer userLogged = (Customer) session.getAttribute("userLogged");
                                String fullName = userLogged.getFirstName() + " " + userLogged.getMiddleName() + " " + userLogged.getLastName();
                            %>
                            <strong><%=fullName%></strong>
                            <br>
                            Billing Address: <%=userLogged.getAddress()%>
                            <br>
                            Delivery Address: <%=(String) session.getAttribute("billingAddress")%>
                            <br>
                            Payment: <%=(String) session.getAttribute("paymentOption")%>
                            <br>
                            <abbr title="Contact Number">CN:</abbr> <%=(String) session.getAttribute("billingContactNo")%>
                            <br>
                            <abbr title="Requested Delivery Date">RD:</abbr> <%=(String) session.getAttribute("billingDelivery")%>
                        </address>
                    </div>

                    <div class="col-md-12">
                        <%
                            ArrayList<Product> cart = (ArrayList<Product>) session.getAttribute("cart");
                        %>
                        <table class="table table-bordered table-hover table-review">
                            <thead>
                                <tr>
                                    <th style="width: 10%">Product Name</th>
                                    <th style="width: 15%">Image</th>
                                    <th style="width: 25%">Description</th>
                                    <th style="width: 10%">Quantity</th>
                                    <th style="width: 10%">Selling Price</th>
                                    <th style="width: 10%">Total Selling Price</th>
                                </tr>
                            </thead>
                            <tbody>
                                <%
                                    double total = 0;
                                    for (int a = 0; a < cart.size(); a++) {
                                        double price = cart.get(a).getOrderQuantity() * cart.get(a).getUnitPrice();
                                        total += price;
                                %>
                                <tr>
                                    <td><%=cart.get(a).getProductName()%></td>
                                    <td><img src="assets/img/products/<%=cart.get(a).getImage()%>" width="150px" height="150px"></td>
                                    <td><%=cart.get(a).getDescription()%></td>
                                    <td><%=cart.get(a).getOrderQuantity()%> <%=cart.get(a).getUnit()%></td>
                                    <td style="text-align: right">Php <%=cart.get(a).getUnitPrice()%></td>
                                    <td style="text-align: right">Php <%=price%></td>
                                </tr>
                                <%
                                    }
                                %>
                            </tbody>
                            <tfoot>
                                <tr>
                                    <th colspan="5">Delivery Charge: Free</th>
                                    <th style="text-align: right">Php 0.00</th>
                                </tr>
                                <tr>
                                    <th colspan="5">Sub-total</th>
                                    <th style="text-align: right">Php <%=Reference.doubleToString(total / 1.12)%></th>
                                </tr>
                                <tr>
                                    <th colspan="5">VAT</th>
                                    <th style="text-align: right">Php <%=Reference.doubleToString(total - (total / 1.12))%></th>
                                </tr>
                                <tr>
                                    <th colspan="5">Total</th>
                                    <th style="text-align: right">Php <%=total%></th>
                                </tr>
                            </tfoot>
                        </table>

                        <form action="/TCP/CustomerSubmitOrder">
                            <div class="row">
                                <div class="col-md-12">
                                    <input type="checkbox" required> <b>NOTE: Order is subject for approval by TPC.</b>
                                </div>
                            </div>

                            <!--a class="btn btn-warning pull-right" href="/TCP/CustomerSubmitOrder?action=submitOrder">Submit Order</a-->
                            <button class="btn btn-warning pull-right" name="action" value="submitOrder">Submit Order</button>
                        </form>
                        <!--button class="btn btn-warning pull-right" type="button" data-toggle="modal" data-target="#myModal">
                            Submit Order
                        </button>
                        <!-- Modal >
                        <div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
                            <div class="modal-dialog">
                                <div class="modal-content">
                                    <div class="modal-header">
                                        <h4 class="modal-title" id="myModalLabel">Order Confirmation</h4>
                                    </div>
                                    <div class="modal-body">
                                        Order is subject for approval by TPC.
                                    </div>
                                    <div class="modal-footer">
                                        <a class="btn btn-warning pull-right" href="/TCP/CustomerSubmitOrder?action=submitOrder">Confirm</a>
                                    </div>
                                </div>
                        <!-- /.modal-content>
                    </div>
                        <!-- /.modal-dialog >
                    </div>
                        <!-- /.modal -->
                    </div>
                </div>
            </div>
        </div>
    </body>

    <jsp:include page="../scriptplugin.jsp"/>
</html>
