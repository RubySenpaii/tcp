<%-- 
    Document   : user_detail
    Created on : Oct 2, 2017, 3:02:20 AM
    Author     : RubySenpaii
--%>

<%@page import="object.Customer"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">

        <jsp:include page="../cssplugin.jsp"/>
        <title>TCP - Customer Template</title>
    </head>
    <body>
        <div id="wrapper">

            <!-- Navigation -->
            <nav class="navbar navbar-default navbar-static-top" role="navigation" style="margin-bottom: 0">
                <jsp:include page="_navbar.jsp"/>
            </nav>

            <div class="container-fluid">
                <div class="row">
                    <div class="col-md-8 col-md-offset-2">
                        <%
                            Customer customer = (Customer) session.getAttribute("customer");
                        %>
                        <h2 class="page-header">User Details</h2>
                    </div>
                </div>

                <div class="row">
                    <div class="col-md-8 col-md-offset-2">
                        <form action="/TCP/CustomerDetail">
                            <div class="form-group">
                                <label>Last Name</label>
                                <input class="form-control" name="lastName" <%=(String) session.getAttribute("enabled")%> value="<%=customer.getLastName()%>">
                            </div>
                            <div class="form-group">
                                <label>First Name</label>
                                <input class="form-control" name="firstName" disabled value="<%=customer.getFirstName()%>">
                            </div>
                            <div class="form-group">
                                <label>Middle Name</label>
                                <input class="form-control" name="middleName" <%=(String) session.getAttribute("enabled")%> value="<%=customer.getMiddleName()%>">
                            </div>
                            <div class="form-group">
                                <label>Billing Address</label>
                                <input class="form-control" name="address" <%=(String) session.getAttribute("enabled")%> value="<%=customer.getAddress()%>">
                            </div>
                            <div class="form-group">
                                <label>Contact Number</label>
                                <input class="form-control" name="contactNumber" <%=(String) session.getAttribute("enabled")%> value="<%=customer.getContactNo()%>">
                            </div>
                            <div class="form-group">
                                <label>Email</label>
                                <input class="form-control" name="email" disabled value="<%=customer.getEmail()%>">
                            </div>
                            <div class="form-group">
                                <label>Mother's Maiden Name</label>
                                <input class="form-control" name="motherMaiden" disabled value="<%=customer.getMotherMaiden()%>">
                            </div>
                            <div class="form-group">
                                <label>Birthday</label>
                                <input class="form-control" name="birthday" type="date" disabled value="<%=customer.getBirthday()%>">
                            </div>
                            <div class="text-center">
                                <button class="btn btn-success" type="submit" name="action" value="submit">Submit</button>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </body>

    <jsp:include page="../scriptplugin.jsp"/>
</html>
