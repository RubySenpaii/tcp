<%-- 
    Document   : order_payment
    Created on : Sep 11, 2017, 3:13:37 AM
    Author     : RubySenpaii
--%>

<%@page import="object.SalesOrder"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">

        <jsp:include page="../cssplugin.jsp"/>
        <title>TCP - Customer Template</title>
    </head>
    <body>
        <div id="wrapper">

            <!-- Navigation -->
            <nav class="navbar navbar-default navbar-static-top" role="navigation" style="margin-bottom: 0">
                <jsp:include page="_navbar.jsp"/>
            </nav>

            <div class="container-fluid customer-content">
                <div class="row customer-content" style="margin-top: 0px">
                    <div class="col-lg-12">
                        <h2 class="page-header">Order Payment</h2>
                    </div>
                    <div class="col-md-2 col-md-offset-1">
                        <div class="panel panel-default">
                            <div class="panel-heading">
                                Payment Instructions
                            </div>
                            <div class="panel-body">
                                <p>Step 1: Deposit 50% of the amount to BDO Savings Account Number: 58784656597 Account Name: TCP Enterprises</p>
                                <p>Step 2: Fill up the amount and date paid.</p>
                                <p>Step 3: Upload a scanned copy of the deposit slip.</p>
                                <p>Step 4: Wait for verification of payment.</p>
                            </div>
                            <!-- /.panel-body -->
                        </div>
                    </div>
                    <div class="col-md-6 ">
                        <%
                            SalesOrder salesOrder = (SalesOrder) session.getAttribute("salesOrder");
                            int min = (int) (salesOrder.getOrderTotal() * 0.5);
                            String placeholder = "Min: " + min + " Max: " + salesOrder.getOrderTotal();
                        %>
                        <form role="form" enctype="multipart/form-data" method="POST" action="/TCP/CustomerPayment">
                            <div class="form-group">
                                <label>Sales Order Number</label>
                                <input class="form-control" disabled value="<%=salesOrder.getSalesOrderNumber()%>">
                                <input type="hidden" name="salesOrderNumber" value="<%=salesOrder.getSalesOrderNumber()%>">
                            </div>
                            <div class="form-group">
                                <label>Amount Paid</label>
                                <input class="form-control" type="number" name="amountPaid" placeholder="<%=placeholder%>" min="<%=min%>" max="<%=salesOrder.getOrderTotal()%>" step="0.01" value="" required>
                            </div>
                            <div class="form-group">
                                <label>Date Paid</label>
                                <input class="form-control" name="datePaid" type="datetime-local" required>
                            </div>
                            <div class="form-group">
                                <label>Proof of Payment</label>
                                <input class="form-control" name="image" type="file" required>
                            </div>
                            <br/>
                            <div class="form-group">
                                <input type="checkbox" required> <b>NOTE: Payment is subject for approval by TPC.</b>
                            </div>
                            <div class="text-center">
                                <button class="btn btn-success" type="submit" name="action" value="submitPayment">Confirm</button>
                                <!--button class="btn btn-success" type="button" data-toggle="modal" data-target="#myModal">Submit</button-->
                            </div>

                            <!-- Modal >
                            <div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
                                <div class="modal-dialog">
                                    <div class="modal-content">
                                        <div class="modal-header">
                                            <h4 class="modal-title" id="myModalLabel">Payment Confirmation</h4>
                                        </div>
                                        <div class="modal-body">
                                            Payment submitted is subject for approval.
                                        </div>
                                        <div class="modal-footer">
                                            <button class="btn btn-success" type="submit" name="action" value="submitPayment">Confirm</button>
                                        </div>
                                    </div>
                            <!-- /.modal-content >
                        </div>
                            <!-- /.modal-dialog >
                        </div>
                            <!-- /.modal -->
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </body>

    <jsp:include page="../scriptplugin.jsp"/>
</html>
