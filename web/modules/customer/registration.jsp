<%-- 
    Document   : registration
    Created on : Aug 17, 2017, 2:26:17 PM
    Author     : RubySenpaii
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">

        <jsp:include page="../cssplugin.jsp"/>
        <title>TCP - Customer Login</title>
    </head>
    <body>
        <div class="container">
            <div class="row">
                <div class="col-md-6 col-md-offset-3">
                    <div class="registration-panel panel panel-default">
                        <div class="panel-heading">
                            <h3 class="panel-title">TCP - Customer Registration</h3>
                        </div>
                        <div class="panel-body">
                            <form role="form" action="/TCP/CustomerRegistration">
                                <fieldset>
                                    <%
                                        String dangerAlert = "none";
                                        String errorMessage = "";
                                        if (session.getAttribute("dangerAlert") != null) {
                                            dangerAlert = (String) session.getAttribute("dangerAlert");
                                            errorMessage = (String) session.getAttribute("errorMessage");
                                        }
                                    %>
                                    <div class="alert alert-danger" style="display: <%=dangerAlert%>; text-align: center">
                                        <%=errorMessage%>
                                    </div>
                                    <div class="form-group">
                                        <label>Last Name</label>
                                        <input class="form-control login-input" placeholder="LAST NAME" name="last_name" type="text" required autofocus autocomplete="off" pattern="[a-zA-Z0-9]+[a-zA-Z0-9 ]+" title="Alphanumeric with Spaces ONLY">
                                    </div>
                                    <div class="form-group login-form">
                                        <label>First Name</label>
                                        <input class="form-control login-input" placeholder="FIRST NAME" name="first_name" type="text" required autocomplete="off" pattern="[a-zA-Z0-9]+[a-zA-Z0-9 ]+" title="Alphanumeric with Spaces ONLY">
                                    </div>
                                    <div class="form-group login-form">
                                        <label>Middle Name</label>
                                        <input class="form-control login-input" placeholder="MIDDLE NAME" name="middle_name" type="text" required autocomplete="off" pattern="[a-zA-Z0-9]+[a-zA-Z0-9 ]+" title="Alphanumeric with Spaces ONLY">
                                    </div>
                                    <div class="form-group login-form">
                                        <label>Billing Address</label>
                                        <input class="form-control login-input" placeholder="ADDRESS" name="address" type="text" required autocomplete="off" pattern="[a-zA-Z0-9,.-]+[a-zA-Z0-9,.- ]+" title="Alphanumeric with Spaces ONLY">
                                    </div>
                                    <div class="form-group login-form">
                                        <label>Contact Number</label>
                                        <input class="form-control login-input" placeholder="CONTACT NO" name="contact_no" type="text" required autocomplete="off" pattern="[a-zA-Z0-9]+[a-zA-Z0-9 ]+" title="Alphanumeric with Spaces ONLY">
                                    </div>
                                    <div class="form-group login-form">
                                        <label>Email</label>
                                        <input class="form-control login-input" placeholder="EMAIL" name="email" type="email" required autocomplete="off">
                                    </div>
                                    <div class="form-group login-form">
                                        <label>Mother's Maiden Name</label>
                                        <input class="form-control login-input" placeholder="MOTHER'S MAIDEN NAME" name="motherMaiden" type="text" required autocomplete="off" pattern="[a-zA-Z0-9]+[a-zA-Z0-9 ]+" title="Alphanumeric with Spaces ONLY">
                                    </div>
                                    <div class="form-group login-form">
                                        <label>Birthday</label>
                                        <input class="form-control login-input" placeholder="BIRTHDAY" name="birthday" type="date" required autocomplete="off">
                                    </div>
                                    <div class="form-group login-form">
                                        <label>Username</label>
                                        <input class="form-control login-input" placeholder="USERNAME" name="username" type="text" required autocomplete="off" pattern="[a-zA-Z0-9]+" title="Alphanumeric with Spaces ONLY">
                                    </div>
                                    <div class="form-group login-form">
                                        <label>Password</label>
                                        <input class="form-control login-input" placeholder="PASSWORD" name="password" type="password" required autocomplete="off"  pattern="[a-zA-Z0-9]+" title="Alphanumeric with Spaces ONLY">
                                    </div>

                                    <!-- Change this to a button or input when using this as a form -->
                                    <button type="submit" class="btn btn-lg btn-primary btn-block login-form" name="action" value="new_customer">Register</button>
                                </fieldset>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </body>

    <jsp:include page="../scriptplugin.jsp"/>
</html>
