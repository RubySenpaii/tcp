<%-- 
    Document   : order_list
    Created on : Aug 2, 2017, 9:51:48 PM
    Author     : RubySenpaii
--%>

<%@page import="object.SalesOrder"%>
<%@page import="java.util.ArrayList"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">

        <jsp:include page="../cssplugin.jsp"/>
        <title>TCP - Customer Order History</title>
    </head>
    <body>
        <div id="wrapper">

            <!-- Navigation -->
            <nav class="navbar navbar-default navbar-static-top" role="navigation" style="margin-bottom: 0">
                <jsp:include page="_navbar.jsp"/>
            </nav>

            <div class="container-fluid customer-content">
                <div class="row customer-content" style="margin-top: 0px">
                    <div class="col-lg-12">
                        <h2 class="page-header">Order History</h2>
                    </div>
                    <div class="col-md-12">
                        <%
                            String dangerAlert = "none";
                            String errorMessage = "";
                            if (session.getAttribute("dangerAlert") != null) {
                                dangerAlert = (String) session.getAttribute("dangerAlert");
                                errorMessage = (String) session.getAttribute("errorMessage");
                            }
                        %>
                        <div class="alert alert-danger" style="display: <%=dangerAlert%>; text-align: center">
                            <%=errorMessage%>
                        </div>
                        <%
                            String successAlert = "none";
                            String successMessage = "";
                            if (session.getAttribute("successAlert") != null) {
                                successAlert = (String) session.getAttribute("successAlert");
                                successMessage = (String) session.getAttribute("successMessage");
                            }
                        %>
                        <div class="alert alert-success" style="display: <%=successAlert%>; text-align: center">
                            <%=successMessage%>
                        </div>
                        <table class="table table-bordered table-hover table-review" id="dataTables-example">
                            <thead>
                                <tr>
                                    <th style="width: 10%">Order Number</th>
                                    <th style="width: 10%">Order Date</th>
                                    <th style="width: 10%">Total Price</th>
                                    <th style="width: 10%">Status</th>
                                    <th style="width: 15%">Remarks</th>
                                    <th style="width: 15%">Delivery Date</th>
                                    <th style="width: 10%">Action</th>
                                </tr>
                            </thead>
                            <tbody>
                                <%
                                    ArrayList<SalesOrder> orders = (ArrayList<SalesOrder>) session.getAttribute("orders");
                                    for (int a = 0; a < orders.size(); a++) {
                                %>
                                <tr>
                                    <td><%=orders.get(a).getSalesOrderNumber()%></td>
                                    <td><%=orders.get(a).getOrderDate()%></td>
                                    <td style="text-align: right">Php <%=orders.get(a).getOrderTotal()%></td>
                                    <td><%=orders.get(a).getOrderStatus()%></td>
                                    <td><%=orders.get(a).getRemarks()%></td>
                                    <td><%=orders.get(a).getRequestDelivery()%></td>
                                    <td>
                                        <form action="CustomerViewOrder">
                                            <a href="/TCP/CustomerViewOrder?action=viewDetails&salesOrderNumber=<%=orders.get(a).getSalesOrderNumber()%>" class="btn btn-info">View</a>
                                            <%
                                                if (orders.get(a).getOrderStatus().equals("Approved")) {
                                            %>
                                            <a href="/TCP/CustomerPayment?action=pay&salesOrderNumber=<%=orders.get(a).getSalesOrderNumber()%>" class="btn btn-primary">Pay</a>
                                            <%
                                            } else if (orders.get(a).getOrderStatus().equals("Delivery") || orders.get(a).getOrderStatus().equals("Completed")) {
                                            %>
                                            <a href="/TCP/CustomerDeliveryUpdates?action=trackOrder&salesOrderNumber=<%=orders.get(a).getSalesOrderNumber()%>" class="btn btn-info">Track</a>
                                            <%
                                                }
                                            %>
                                        </form>
                                    </td>
                                </tr>
                                <%
                                    }
                                %>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </body>

    <jsp:include page="../scriptplugin.jsp"/>
</html>
