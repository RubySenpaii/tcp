<%-- 
    Document   : _navbar
    Created on : Jul 31, 2017, 3:21:28 PM
    Author     : RubySenpaii
--%>


<%@page import="object.Customer"%>
<%@page import="object.Product"%>
<%@page import="java.util.ArrayList"%>
<div class="navbar-header">
    <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
        <span class="sr-only">Toggle navigation</span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
    </button>
    <a class="navbar-brand" href="/TCP/CustomerHomepage?action=homepage" style="padding: 0">
        <img src="/TCP/assets/img/resource/TCP Logo.png" class="panel-title" width="200px" height="100%">
    </a>
</div>

<!--div class="col-md-5 col-md-offset-3" style="vertical-align: middle">
    <h4 style="padding-top: 10px">
        <a href="/TCP/modules/customer/about_us.jsp">About Us</a>
        &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
        &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
        <a href="/TCP/modules/customer/contact_us.jsp">Contact Us</a>
    </h4>
</div-->

<ul class="nav navbar-top-links navbar-right">
    <li class="dropdown">
        <a class="dropdown-toggle" href="/TCP/modules/customer/about_us.jsp"> About Us
        </a>
    </li>
    <li class="dropdown">
        <a class="dropdown-toggle" href="/TCP/CustomerContact?action=view"> Contact Us
        </a>
    </li>
    <%
        String userType = (String) session.getAttribute("userType");
        if (userType.equals("customer")) {
            int approved = (Integer) session.getAttribute("approved");
            int delivery = (Integer) session.getAttribute("delivery");
    %>
    <li class="dropdown">
        <a class="dropdown-toggle" data-toggle="dropdown" href="#">
            <i class="fa fa-bell fa-fw"><span class="badge"><%=approved + delivery%></span></i> <i class="fa fa-caret-down"></i>
        </a>
        <ul class="dropdown-menu dropdown-alerts">
            <li>
                <a href="/TCP/CustomerOrders?action=viewOrder">
                    <div>
                        Approved Sales
                        <span class="red pull-right"><%=approved%></span>
                    </div>
                </a>
            </li>
            <li class="divider"></li>
            <li>
                <a href="/TCP/CustomerOrders?action=viewOrder">
                    <div>
                        Orders for Delivery
                        <span class="red pull-right"><%=delivery%></span>
                    </div>
                </a>
            </li>
        </ul>
    </li>

    <li class="dropdown">
        <a class="dropdown-toggle" data-toggle="dropdown" href="#">
            <i class="fa fa-shopping-cart fa-fw"></i> <i class="fa fa-caret-down"></i>
        </a>
        <ul class="dropdown-menu dropdown-tasks">
            <%
                ArrayList<Product> cart = (ArrayList<Product>) session.getAttribute("cart");
                for (int a = 0; a < cart.size(); a++) {
            %>
            <li>
                <a href="#">
                    <div>
                        <p>
                            <strong><%=cart.get(a).getProductName()%> x <%=cart.get(a).getOrderQuantity()%><%=cart.get(a).getUnit()%></strong>
                            <span class="pull-right text-muted">Php <%=cart.get(a).getUnitPrice()%></span>
                        </p>
                    </div>
                </a>
            </li>
            <li class="divider"></li>
                <%
                    }
                %>
            <li>
                <a class="text-center" href="/TCP/CustomerViewCart?action=view_cart">
                    <strong>See All Products in Cart</strong>
                    <i class="fa fa-angle-right"></i>
                </a>
            </li>
        </ul>
    </li>

    <li class="dropdown">
        <a class="dropdown-toggle" data-toggle="dropdown" href="#">
            <%
                Customer user = (Customer) session.getAttribute("userLogged");
                String name = user.getLastName() + ", " + user.getFirstName();
            %>
            <i class="fa fa-user fa-fw"></i> <%=name%> <i class="fa fa-caret-down"></i>
        </a>
        <ul class="dropdown-menu dropdown-user">
            <li><a href="/TCP/CustomerDetail?action=view&customerID=<%=user.getCustomerID()%>"><i class="fa fa-user fa-fw"></i> User Profile</a>
            </li>
            <li><a href="/TCP/CustomerOrders?action=viewOrder"><i class="fa fa-gear fa-fw"></i> Order History</a>
            </li>
            <li class="divider"></li>
            <li><a href="/TCP/CustomerLogout"><i class="fa fa-sign-out fa-fw"></i> Logout</a>
            </li>
        </ul>
        <!-- /.dropdown-user -->
    </li>
    <%
        } else {
    %>
    
    <li class="dropdown">
        <a href="/TCP/modules/customer/login.jsp">
            <i class="fa fa-user fa-fw"></i> Register / Log In
        </a>
    </li>
    <%
        }
    %>
</ul>
