<%-- 
    Document   : billing
    Created on : Aug 2, 2017, 9:39:11 PM
    Author     : RubySenpaii
--%>

<%@page import="extra.Reference"%>
<%@page import="object.Product"%>
<%@page import="java.util.ArrayList"%>
<%@page import="object.Customer"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">

        <jsp:include page="../cssplugin.jsp"/>
        <title>TCP - Customer Shopping Cart</title>
    </head>
    <body>
        <div id="wrapper">

            <!-- Navigation -->
            <nav class="navbar navbar-default navbar-static-top" role="navigation" style="margin-bottom: 0">
                <jsp:include page="_navbar.jsp"/>
            </nav>

            <div class="container-fluid customer-content">

                <div class="row customer-content" style="margin-top: 0px">
                    <div class="col-lg-12">
                        <h2 class="page-header">Billing Information</h2>
                    </div>

                    <div class="col-md-4">
                        <form role="form" action="/TCP/CustomerOrderReview">
                            <%
                                Customer customer = (Customer) session.getAttribute("userLogged");
                            %>
                            <div class="form-group">
                                <label>Billing Address</label>
                                <input class="form-control" disabled placeholder="Full Address" name="billingAddress" value="<%=customer.getAddress()%>" required pattern="[a-zA-Z0-9,]+[a-zA-Z0-9 ]+" title="Alphanumeric with Spaces ONLY">
                            </div>
                            <div class="form-group">
                                <label>Delivery Address</label>
                                <input class="form-control" placeholder="Full Address" name="deliveryAddress" value="<%=customer.getAddress()%>" required pattern="[a-zA-Z0-9,.-]+[a-zA-Z0-9,.- ]+" title="Alphanumeric with Spaces ONLY">
                            </div>
                            <div class="form-group">
                                <label>Contact Person</label>
                                <input class="form-control" placeholder="Contact Person" name="contactPerson" value="<%=customer.getLastName() + ", " + customer.getFirstName()%>" required pattern="[a-zA-Z0-9,.-]+[a-zA-Z0-9,.- ]+" title="Alphanumeric with Spaces ONLY">
                            </div>
                            <div class="form-group">
                                <label>Contact Number</label>
                                <input class="form-control" placeholder="Contact Number" name="contact_no" value="<%=customer.getContactNo()%>" required pattern="[a-zA-Z0-9]+[a-zA-Z0-9 ]+" title="Alphanumeric with Spaces ONLY">
                            </div>
                            <div class="form-group">
                                <label>Payment</label>
                                <select class="form-control" name="paymentOption">
                                    <option>50% Initial Deposit and Cash on Delivery</option>
                                    <option>50% Initial Deposit and Check on Delivery</option>
                                </select>
                            </div>
                            <div class="form-group">
                                <label>Request Delivery Date</label>
                                <input id="datefield" type="date" class="form-control" placeholder="Delivery Date" name="request_delivery" required>
                            </div>
                            <button class="btn btn-primary pull-right" name="action" value="review_order">Continue</button>
                        </form>
                    </div>

                    <div class="col-md-8">
                        <table class="table table-bordered table-review">
                            <thead>
                                <tr>
                                    <th>Product Name</th>
                                    <th>Image</th>
                                    <th>Quantity</th>
                                    <th>Selling Price</th>
                                    <th>Total Selling Price</th>
                                </tr>
                            </thead>
                            <tbody>
                                <%
                                    ArrayList<Product> cart = (ArrayList<Product>) session.getAttribute("cart");
                                    double total = 0;
                                    for (int a = 0; a < cart.size(); a++) {
                                        double price = cart.get(a).getOrderQuantity() * cart.get(a).getUnitPrice();
                                        total += price;
                                %>
                                <tr>
                                    <td><%=cart.get(a).getProductName()%></td>
                                    <td><img src="assets/img/products/<%=cart.get(a).getImage()%>" width="150px" height="150px"></td>
                                    <td><%=cart.get(a).getOrderQuantity()%> <%=cart.get(a).getUnit()%></td>
                                    <td style="text-align: right"><%=cart.get(a).getUnitPrice()%></td>
                                    <td style="text-align: right">Php <%=price%></td>
                                </tr>
                                <%
                                    }
                                %>
                            </tbody>
                            <tfoot>
                                <tr>
                                    <th colspan="4">Delivery Charge: Free</th>
                                    <th style="text-align: right">Php 0.00</th>
                                </tr>
                                <tr>
                                    <th colspan="4">Sub-total</th>
                                    <th style="text-align: right">Php <%=Reference.doubleToString(total / 1.12)%></th>
                                </tr>
                                <tr>
                                    <th colspan="4">VAT</th>
                                    <th style="text-align: right">Php <%=Reference.doubleToString(total - (total / 1.12))%></th>
                                </tr>
                                <tr>
                                    <th colspan="4">Total</th>
                                    <th style="text-align: right">Php <%=total%></th>
                                </tr>
                            </tfoot>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </body>

    <jsp:include page="../scriptplugin.jsp"/>
</html>
