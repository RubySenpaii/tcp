<%-- 
    Document   : order_delivery
    Created on : Sep 20, 2017, 8:16:03 PM
    Author     : RubySenpaii
--%>

<%@page import="object.SalesDelivery"%>
<%@page import="java.util.ArrayList"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">

        <jsp:include page="../cssplugin.jsp"/>
        <title>TCP - Customer Order</title>
    </head>
    <body>
        <div id="wrapper">

            <!-- Navigation -->
            <nav class="navbar navbar-default navbar-static-top" role="navigation" style="margin-bottom: 0">
                <jsp:include page="_navbar.jsp"/>
            </nav>

            <div class="container-fluid customer-content">

                <div class="row customer-content" style="margin-top: 0px">
                    <div class="col-lg-12">
                        <h2 class="page-header">Delivery Status</h2>
                    </div>

                    <div class="col-md-6 col-md-offset-3">
                        <%
                            ArrayList<SalesDelivery> deliveryUpdates = (ArrayList<SalesDelivery>) session.getAttribute("deliveryUpdates");
                        %>
                        <table class="table table-bordered table-hover table-review">
                            <thead>
                                <tr>
                                    <th style="width: 25%">Date Updated</th>
                                    <th style="width: 25%">Status</th>
                                    <th style="width: 25%">Updated By</th>
                                    <th style="width: 25%">Remarks</th>
                                </tr>
                            </thead>
                            <tbody>
                                <%
                                    for (int a = 0; a < deliveryUpdates.size(); a++) {
                                %>
                                <tr>
                                    <td><%=deliveryUpdates.get(a).getDateUpdated()%></td>
                                    <td><%=deliveryUpdates.get(a).getDeliveryStatus()%></td>
                                    <td><%=deliveryUpdates.get(a).getUpdatedName()%></td>
                                    <td><%=deliveryUpdates.get(a).getRemarks()%></td>
                                </tr>
                                <%
                                    }
                                %>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </body>

    <jsp:include page="../scriptplugin.jsp"/>
</html>
