<%-- 
    Document   : homepage
    Created on : Jul 30, 2017, 5:21:29 PM
    Author     : RubySenpaii
--%>

<%@page import="object.Product"%>
<%@page import="java.util.ArrayList"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">

        <jsp:include page="../cssplugin.jsp"/>
        <title>TCP - Customer Home Page</title>
    </head>
    <body>
        <div id="wrapper">

            <!-- Navigation -->
            <nav class="navbar navbar-default navbar-static-top" role="navigation" style="margin-bottom: 0">
                <jsp:include page="_navbar.jsp"/>
            </nav>

            <div class="container-fluid customer-content">
                <jsp:include page="_header.jsp"/>

                <%
                    String dangerAlert = "none";
                    String errorMessage = "";
                    if (session.getAttribute("dangerAlert") != null) {
                        dangerAlert = (String) session.getAttribute("dangerAlert");
                        errorMessage = (String) session.getAttribute("errorMessage");
                    }
                %>
                <div class="alert alert-danger" style="display: <%=dangerAlert%>; text-align: center">
                    <%=errorMessage%>
                </div>
                <%
                    String successAlert = "none";
                    String successMessage = "";
                    if (session.getAttribute("successAlert") != null) {
                        successAlert = (String) session.getAttribute("successAlert");
                        successMessage = (String) session.getAttribute("successMessage");
                    }
                %>
                <div class="alert alert-success" style="display: <%=successAlert%>; text-align: center">
                    <%=successMessage%>
                </div>

                <div class="row product-layout">
                    <%
                        ArrayList<Product> products = (ArrayList<Product>) session.getAttribute("products");
                        for (int a = 0; a < products.size(); a++) {
                    %>
                    <form action="CustomerProductDetail">
                        <input type="hidden" name="product" value="<%=a%>">
                        <button class="col-md-2 product-box" type="submit" name="action" value="viewProductDetails">
                            <img src="assets/img/products/<%=products.get(a).getImage()%>" class="product-image">
                            <div class="product-info">
                                <p>Product Name: <%=products.get(a).getProductName()%></p>
                                <p><%=products.get(a).getCategory()%> - Php <%=products.get(a).getUnitPrice()%></p>
                            </div>
                        </button>
                    </form>
                    <%
                        }
                        if (products.size() == 0) {
                    %>
                    <div class="alert alert-danger" style="text-align: center">
                        No products found!
                    </div>
                    <%
                        }
                    %>
                </div>
            </div>
        </div>
    </body>

    <jsp:include page="../scriptplugin.jsp"/>
</html>
