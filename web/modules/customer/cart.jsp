<%-- 
    Document   : cart
    Created on : Aug 2, 2017, 9:13:12 PM
    Author     : RubySenpaii
--%>

<%@page import="object.Product"%>
<%@page import="java.util.ArrayList"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">

        <jsp:include page="../cssplugin.jsp"/>
        <title>TCP - Customer Shopping Cart</title>
    </head>
    <body>
        <div id="wrapper">

            <!-- Navigation -->
            <nav class="navbar navbar-default navbar-static-top" role="navigation" style="margin-bottom: 0">
                <jsp:include page="_navbar.jsp"/>
            </nav>

            <div class="container-fluid customer-content">

                <div class="row customer-content" style="margin-top: 0px">
                    <div class="col-lg-12">
                        <h2 class="page-header">Shopping Cart</h2>
                    </div>
                    <div class="col-md-12">
                        <%
                            String dangerAlert = "none";
                            String errorMessage = "";
                            if (session.getAttribute("dangerAlert") != null) {
                                dangerAlert = (String) session.getAttribute("dangerAlert");
                                errorMessage = (String) session.getAttribute("errorMessage");
                            }
                        %>
                        <div class="alert alert-danger" style="display: <%=dangerAlert%>; text-align: center">
                            <%=errorMessage%>
                        </div>
                        <%
                            String successAlert = "none";
                            String successMessage = "";
                            if (session.getAttribute("successAlert") != null) {
                                successAlert = (String) session.getAttribute("successAlert");
                                successMessage = (String) session.getAttribute("successMessage");
                            }
                        %>
                        <div class="alert alert-success" style="display: <%=successAlert%>; text-align: center">
                            <%=successMessage%>
                        </div>
                        <%
                            ArrayList<Product> cart = (ArrayList<Product>) session.getAttribute("cart");
                        %>
                        <form action="/TCP/CustomerViewCart">
                            <table class="table table-bordered table-hover table-review">
                                <thead>
                                    <tr>
                                        <th style="width: 10%">Product Name</th>
                                        <th style="width: 15%">Image</th>
                                        <th style="width: 25%">Description</th>
                                        <th style="width: 10%">Quantity</th>
                                        <th style="width: 10%">Selling Price</th>
                                        <th style="width: 10%">Total Selling Price</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <%
                                        double total = 0;
                                        for (int a = 0; a < cart.size(); a++) {
                                            double price = cart.get(a).getOrderQuantity() * cart.get(a).getUnitPrice();
                                            total += price;
                                    %>
                                    <tr>
                                        <td><%=cart.get(a).getProductName()%></td>
                                        <td><img src="assets/img/products/<%=cart.get(a).getImage()%>" width="150px" height="150px"></td>
                                        <td><%=cart.get(a).getDescription()%></td>
                                        <td><input type="number" name="quantity" value="<%=cart.get(a).getOrderQuantity()%>" min="0" max="<%=cart.get(a).getStockQuantity()%>" required> <%=cart.get(a).getUnit()%></td>
                                        <td style="text-align: right">Php <%=cart.get(a).getUnitPrice()%></td>
                                        <td style="text-align: right">Php <%=price%></td>
                                    </tr>
                                    <%
                                        }
                                    %>
                                </tbody>
                                <tfoot>
                                    <tr>
                                        <th colspan="5">Total</th>
                                        <th style="text-align: right">Php <%=total%></th>
                                    </tr>
                                </tfoot>
                            </table>

                            <div class="col-md-2 col-md-offset-10">
                                <button class="btn btn-primary" name="action" value="update_cart">Update</button>
                                <%
                                    if (total > 0) {
                                %>
                                <a class="btn btn-primary " href="/TCP/CustomerBilling?action=billing">Checkout</a>
                                <%
                                    }
                                %>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </body>

    <jsp:include page="../scriptplugin.jsp"/>
</html>
