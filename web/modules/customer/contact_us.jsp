<%-- 
    Document   : contact_us
    Created on : Nov 3, 2017, 5:51:40 PM
    Author     : RubySenpaii
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">

        <jsp:include page="../cssplugin.jsp"/>
        <title>TCP - Contact Us</title>
    </head>
    <body>
        <div id="wrapper">

            <!-- Navigation -->
            <nav class="navbar navbar-default navbar-static-top" role="navigation" style="margin-bottom: 0">
                <jsp:include page="_navbar.jsp"/>
            </nav>

            <div class="container-fluid">
                <div class="row">
                    <div class="col-md-6 col-md-offset-3">
                        <h2 class="page-header">Contact Us</h2>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-6 col-md-offset-3">
                        <%
                            String dangerAlert = "none";
                            String errorMessage = "";
                            if (session.getAttribute("dangerAlert") != null) {
                                dangerAlert = (String) session.getAttribute("dangerAlert");
                                errorMessage = (String) session.getAttribute("errorMessage");
                            }
                        %>
                        <div class="alert alert-danger" style="display: <%=dangerAlert%>; text-align: center">
                            <%=errorMessage%>
                        </div>
                        <%
                            String successAlert = "none";
                            String successMessage = "";
                            if (session.getAttribute("successAlert") != null) {
                                successAlert = (String) session.getAttribute("successAlert");
                                successMessage = (String) session.getAttribute("successMessage");
                            }
                        %>
                        <div class="alert alert-success" style="display: <%=successAlert%>; text-align: center">
                            <%=successMessage%>
                        </div>
                        <div class="panel-body">
                            <form action="/TCP/CustomerContact">
                                <br>
                                <div class="form-group">
                                    <label>Full Name</label>
                                    <input class="form-control" name="fullName" placeholder="FULL NAME">
                                </div>
                                <br>
                                <div class="form-group">
                                    <label>Email</label>
                                    <input class="form-control" name="email" placeholder="EMAIL" type="email">
                                </div>
                                <br>
                                <div class="form-group">
                                    <label>Contact Number</label>
                                    <input class="form-control" name="contactNumber" placeholder="CONTACT NUMBER">
                                </div><br>
                                <div class="form-group">
                                    <label>Message</label>
                                    <textarea class="form-control" placeholder="Message" name="message"></textarea>
                                </div>

                                <div class="text-center">
                                    <button class="btn btn-success" type="submit" name="action" value="submit">Submit</button>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </body>

    <jsp:include page="../scriptplugin.jsp"/>
</html>
