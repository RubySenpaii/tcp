<%-- 
    Document   : inventory_add
    Created on : Aug 17, 2017, 3:53:19 PM
    Author     : RubySenpaii
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">

        <jsp:include page="../cssplugin.jsp"/>
        <title>TCP - Administrator Template</title>
    </head>
    <body>
        <div id="wrapper">

            <!-- Navigation -->
            <nav class="navbar navbar-default navbar-static-top" role="navigation" style="margin-bottom: 0">
                <jsp:include page="_navbar.jsp"/>
                <jsp:include page="_sidebar.jsp"/>
            </nav>

            <div id="page-wrapper">
                <div class="container-fluid">
                    <div class="row">
                        <div class="col-lg-12">
                            <h2 class="page-header">Add Inventory</h2>
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-lg-6 col-md-offset-3">
                            <form role="form" action="/TCP/AdminAddProduct" enctype="multipart/form-data" method="POST">
                                <%
                                    String dangerAlert = "none";
                                    String errorMessage = "";
                                    if (session.getAttribute("dangerAlert") != null) {
                                        dangerAlert = (String) session.getAttribute("dangerAlert");
                                        errorMessage = (String) session.getAttribute("errorMessage");
                                    }
                                %>
                                <div class="alert alert-danger" style="display: <%=dangerAlert%>; text-align: center">
                                    <%=errorMessage%>
                                </div>
                                <%
                                    String successAlert = "none";
                                    String successMessage = "";
                                    if (session.getAttribute("successAlert") != null) {
                                        successAlert = (String) session.getAttribute("successAlert");
                                        successMessage = (String) session.getAttribute("successMessage");
                                    }
                                %>
                                <div class="alert alert-success" style="display: <%=successAlert%>; text-align: center">
                                    <%=successMessage%>
                                </div>
                                <div class="form-group">
                                    <label>Product Name</label>
                                    <input class="form-control" name="product_name" placeholder="Enter Product Name" autocomplete="off" required pattern="[a-zA-Z0-9]+[a-zA-Z0-9 ]+" title="Alphanumeric with Spaces ONLY">
                                </div>
                                <div class="form-group">
                                    <label>Category</label>
                                    <input class="form-control" name="category" placeholder="Enter Category" autocomplete="off" required pattern="[a-zA-Z0-9]+[a-zA-Z0-9 ]+" title="Alphanumeric with Spaces ONLY">
                                </div>
                                <div class="form-group">
                                    <label>Description</label>
                                    <input class="form-control" name="description" placeholder="Enter Description" autocomplete="off" required pattern="[a-zA-Z0-9]+[a-zA-Z0-9 ]+" title="Alphanumeric with Spaces ONLY">
                                </div>
                                <div class="form-group">
                                    <label>Unit Price</label>
                                    <input class="form-control" name="unit_price" placeholder="Enter Unit Price" type="number" autocomplete="off" required>
                                </div>
                                <div class="form-group">
                                    <label>Cost Price</label>
                                    <input class="form-control" name="cost_price" placeholder="Enter Cost Price" type="number" autocomplete="off" required>
                                </div>
                                <div class="form-group">
                                    <label>Unit</label>
                                    <input class="form-control" name="unit" placeholder="Enter Unit" autocomplete="off" required pattern="[a-zA-Z0-9]+[a-zA-Z0-9 ]+" title="Alphanumeric with Spaces ONLY">
                                </div>
                                <div class="form-group">
                                    <label>Image</label>
                                    <input class="form-control" name="image" placeholder="Enter text" type="file" autocomplete="off" required>
                                </div>
                                <div class="form-group">
                                    <label>Critical Level</label>
                                    <input class="form-control" name="critical_level" placeholder="Enter Critical Level" type="number" autocomplete="off" required>
                                </div>
                                <button class="btn btn-success pull-right" type="submit" name="action" value="add_product">Add</button>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </body>

    <jsp:include page="../scriptplugin.jsp"/>
</html>
