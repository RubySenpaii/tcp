<%-- 
    Document   : purchase_return_status
    Created on : Oct 29, 2017, 8:45:13 PM
    Author     : RubySenpaii
--%>

<%@page import="object.PurchaseReturn"%>
<%@page import="java.util.ArrayList"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">

        <jsp:include page="../cssplugin.jsp"/>
        <title>TCP - Administrator Purchase Return Status</title>
    </head>
    <body>
        <div id="wrapper">

            <!-- Navigation -->
            <nav class="navbar navbar-default navbar-static-top" role="navigation" style="margin-bottom: 0">
                <jsp:include page="_navbar.jsp"/>
                <jsp:include page="_sidebar.jsp"/>
            </nav>

            <div id="page-wrapper">
                <div class="container-fluid">
                    <div class="row">
                        <div class="col-lg-12">
                            <h2 class="page-header">Purchase Return Status</h2>
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-lg-12">
                            <%
                                String dangerAlert = "none";
                                String errorMessage = "";
                                if (session.getAttribute("dangerAlert") != null) {
                                    dangerAlert = (String) session.getAttribute("dangerAlert");
                                    errorMessage = (String) session.getAttribute("errorMessage");
                                }
                            %>
                            <div class="alert alert-danger" style="display: <%=dangerAlert%>; text-align: center">
                                <%=errorMessage%>
                            </div>
                            <%
                                String successAlert = "none";
                                String successMessage = "";
                                if (session.getAttribute("successAlert") != null) {
                                    successAlert = (String) session.getAttribute("successAlert");
                                    successMessage = (String) session.getAttribute("successMessage");
                                }
                            %>
                            <div class="alert alert-success" style="display: <%=successAlert%>; text-align: center">
                                <%=successMessage%>
                            </div>
                            <table width="100%" class="table table-striped table-bordered table-hover" id="dataTables-example">
                                <thead>
                                    <tr>
                                        <th style="width: 9%">Purchase Order Number</th>
                                        <th style="width: 10%">Product</th>
                                        <th style="width: 10%">Quantity</th>
                                        <th style="width: 14%">Status</th>
                                        <th style="width: 12%">Action</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <%
                                        ArrayList<PurchaseReturn> returns = (ArrayList<PurchaseReturn>) session.getAttribute("purchaseReturns");
                                        for (int a = 0; a < returns.size(); a++) {
                                    %>
                                    <tr>
                                        <td><%=returns.get(a).getPurchaseOrderNumber()%></td>
                                        <td><%=returns.get(a).getProductName()%></td>
                                        <td><%=returns.get(a).getQuantity()%></td>
                                        <td><%=returns.get(a).getReturnStatus()%></td>
                                        <td>
                                            <%
                                                if (returns.get(a).getReturnStatus().equals("Returned")) {
                                            %>
                                            <a href="/TCP/AdminPurchaseReturnStatus?action=receive&purchaseOrderNumber=<%=returns.get(a).getPurchaseOrderNumber()%>&productName=<%=returns.get(a).getProductName()%>&quantity=<%=returns.get(a).getQuantity()%>" class="btn btn-success">
                                                Receive
                                            </a>
                                            <%
                                                }
                                            %>
                                        </td>
                                    </tr>
                                    <%
                                        }
                                    %>
                                </tbody>
                            </table>
                            <!-- /.table-responsive -->
                        </div>
                    </div>                    
                </div>
            </div>
        </div>
    </body>

    <jsp:include page="../scriptplugin.jsp"/>
</html>
