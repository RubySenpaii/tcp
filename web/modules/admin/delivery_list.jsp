<%-- 
    Document   : delivery_list
    Created on : Sep 15, 2017, 2:29:04 AM
    Author     : RubySenpaii
--%>

<%@page import="object.SalesOrder"%>
<%@page import="java.util.ArrayList"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">

        <jsp:include page="../cssplugin.jsp"/>
        <title>TCP - Administrator Sales Orders For Delivery</title>
    </head>
    <body>
        <div id="wrapper">

            <!-- Navigation -->
            <nav class="navbar navbar-default navbar-static-top" role="navigation" style="margin-bottom: 0">
                <jsp:include page="_navbar.jsp"/>
                <jsp:include page="_sidebar.jsp"/>
            </nav>

            <div id="page-wrapper">
                <div class="container-fluid">
                    <div class="row">
                        <div class="col-lg-12">
                            <h2 class="page-header">Sales Orders For Delivery</h2>
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-md-2 col-md-offset-10" style="border: 1px solid black; padding: 10px; margin-bottom: 10px">
                            <p><span class="pending-box">Ongoing</span> - Delivery is in transit</p>
                            <p><span class="hold-box">Hold</span> - Delivery is on hold</p>
                            <p><span class="delivered-box">Delivered</span> - Items have been delivered</p>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-lg-12">
                            <table width="100%" class="table table-striped table-bordered table-hover" id="dataTables-example">
                                <thead>
                                    <tr>
                                        <th style="width: 12%">Sales Order Number</th>
                                        <th style="width: 13%">Customer Name</th>
                                        <th style="width: 9%">Delivery Status</th>
                                        <th style="width: 14%">Last Updated</th>
                                        <th style="width: 12%">Expected Delivery</th>
                                        <th style="width: 15%">Action</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <%
                                        ArrayList<SalesOrder> salesOrders = (ArrayList<SalesOrder>) session.getAttribute("salesOrders");
                                        for (int a = 0; a < salesOrders.size(); a++) {
                                    %>
                                    <tr>
                                        <td><%=salesOrders.get(a).getSalesOrderNumber()%></td>
                                        <td><%=salesOrders.get(a).getCustomerName()%></td>
                                        <td><%=salesOrders.get(a).getDeliveryStatus()%></td>
                                        <td><%=salesOrders.get(a).getDeliveryUpdate()%></td>
                                        <td>
                                            <%=salesOrders.get(a).getRequestDelivery()%>
                                        </td>
                                        <td>
                                            <%
                                                if (salesOrders.get(a).getDeliveryStatus().equals("Delivered")) {
                                            %>
                                            <%
                                            } else if (salesOrders.get(a).getDeliveryStatus().equals("Ongoing")) {
                                            %>
                                            <button class="btn btn-danger" data-toggle="modal" data-target="#holdmodal<%=a%>">Hold</button>
                                            <button class="btn btn-success" data-toggle="modal" data-target="#deliveredmodal<%=a%>">Delivered</button>
                                            <%
                                            } else if (salesOrders.get(a).getDeliveryStatus().equals("Hold")) {
                                            %>
                                            <button class="btn btn-warning" data-toggle="modal" data-target="#ongoingmodal<%=a%>">Ongoing</button>
                                            <button class="btn btn-success" data-toggle="modal" data-target="#deliveredmodal<%=a%>">Delivered</button>
                                            <%
                                            } else {
                                            %>
                                            <button class="btn btn-warning" data-toggle="modal" data-target="#ongoingmodal<%=a%>">Ongoing</button>
                                            <button class="btn btn-danger" data-toggle="modal" data-target="#holdmodal<%=a%>">Hold</button>
                                            <%
                                                }
                                            %>

                                            <!--Hold Modal -->
                                            <div class="modal fade" id="holdmodal<%=a%>" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
                                                <div class="modal-dialog">
                                                    <div class="modal-content">
                                                        <form action="/TCP/AdminDeliveryUpdate">
                                                            <div class="modal-header">
                                                                <h4 class="modal-title" id="myModalLabel">Hold Remarks</h4>
                                                            </div>
                                                            <div class="modal-body">
                                                                Sales Order#<%=salesOrders.get(a).getSalesOrderNumber()%>
                                                                <br>
                                                                Remarks
                                                                <textarea name="remarks" required></textarea>
                                                            </div>
                                                            <div class="modal-footer">
                                                                <input type="hidden" name="salesOrderNumber" value="<%=salesOrders.get(a).getSalesOrderNumber()%>">
                                                                <button class="btn btn-success" name="action" value="hold">Submit</button>
                                                            </div>
                                                        </form>
                                                    </div>
                                                </div>
                                            </div>
                                            <!-- /.hold modal -->

                                            <!--Ongoing Modal -->
                                            <div class="modal fade" id="ongoingmodal<%=a%>" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
                                                <div class="modal-dialog">
                                                    <div class="modal-content">
                                                        <form action="/TCP/AdminDeliveryUpdate">
                                                            <div class="modal-header">
                                                                <h4 class="modal-title" id="myModalLabel">Ongoing Remarks</h4>
                                                            </div>
                                                            <div class="modal-body">
                                                                Sales Order#<%=salesOrders.get(a).getSalesOrderNumber()%>
                                                                <br>
                                                                Remarks
                                                                <textarea name="remarks" required></textarea>
                                                            </div>
                                                            <div class="modal-footer">
                                                                <input type="hidden" name="salesOrderNumber" value="<%=salesOrders.get(a).getSalesOrderNumber()%>">
                                                                <button class="btn btn-success" name="action" value="ongoing">Submit</button>
                                                            </div>
                                                        </form>
                                                    </div>
                                                </div>
                                            </div>
                                            <!-- /.ongoing modal -->

                                            <!--Delivered Modal -->
                                            <div class="modal fade" id="deliveredmodal<%=a%>" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
                                                <div class="modal-dialog">
                                                    <div class="modal-content">
                                                        <form action="/TCP/AdminDeliveryUpdate">
                                                            <div class="modal-header">
                                                                <h4 class="modal-title" id="myModalLabel">Delivered Details</h4>
                                                            </div>
                                                            <div class="modal-body">
                                                                Sales Order#<%=salesOrders.get(a).getSalesOrderNumber()%>
                                                                <br><br>
                                                                Received By
                                                                <input name="receivedBy" required>
                                                                <br><br>
                                                                Date Received
                                                                <input type="datetime-local" name="receivedDate" required>
                                                                <br><br>
                                                                Remarks
                                                                <textarea name="remarks" required></textarea>
                                                            </div>
                                                            <div class="modal-footer">
                                                                <input type="hidden" name="salesOrderNumber" value="<%=salesOrders.get(a).getSalesOrderNumber()%>">
                                                                <button class="btn btn-success" name="action" value="delivered">Submit</button>
                                                            </div>
                                                        </form>
                                                    </div>
                                                </div>
                                            </div>
                                            <!-- /.delivered modal -->
                                        </td>
                                    </tr>
                                    <%
                                        }
                                    %>
                                </tbody>
                            </table>
                            <!-- /.table-responsive -->
                        </div>
                    </div>                    
                </div>
            </div>
        </div>
    </body>


    <jsp:include page="../scriptplugin.jsp"/>
</html>
