<%-- 
    Document   : backup
    Created on : Nov 2, 2017, 12:13:32 AM
    Author     : RubySenpaii
--%>

<%@page import="java.util.ArrayList"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">

        <jsp:include page="../cssplugin.jsp"/>
        <title>TCP - Administrator Backups</title>
    </head>
    <body>
        <div id="wrapper">

            <!-- Navigation -->
            <nav class="navbar navbar-default navbar-static-top" role="navigation" style="margin-bottom: 0">
                <jsp:include page="_navbar.jsp"/>
                <jsp:include page="_sidebar.jsp"/>
            </nav>

            <div id="page-wrapper">
                <div class="container-fluid">
                    <div class="row">
                        <div class="col-lg-12">
                            <h2 class="page-header">Backups</h2>
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-md-4 col-md-offset-4" style="text-align: center">
                            <a href="/TCP/AdminBackup?action=createBackup" class="btn btn-info">Create Backup Now</a>
                        </div>
                    </div>
                    <br><br>
                    <div class="row">
                        <div class="col-md-4 col-md-offset-4">
                            <table class="table table-bordered">
                                <thead>
                                    <tr>
                                        <th>Date of Backup</th>
                                        <th>Time of Backup</th>
                                        <th>Backup Files</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <%
                                        ArrayList<String> files = (ArrayList<String>) session.getAttribute("fileList");
                                        for (int a = 0; a < files.size(); a++) {
                                            String dateTime = files.get(a).split("f")[1];
                                            String date = dateTime.substring(0, 4) + "-" + dateTime.substring(4, 6) + "-" + dateTime.substring(6, 8);
                                            String time = dateTime.substring(8, 10) + ":" + dateTime.substring(10, 12);
                                    %>
                                    <tr>
                                        <td><%=date%></td>
                                        <td><%=time%></td>
                                        <td>
                                            <a href="/TCP/AdminBackup?action=restoreBackup&fileName=<%=files.get(a)%>" class="btn btn-primary"><%=files.get(a)%></a>
                                        </td>
                                    </tr>
                                    <%
                                        }
                                    %>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </body>

    <jsp:include page="../scriptplugin.jsp"/>
</html>
