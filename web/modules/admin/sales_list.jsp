<%-- 
    Document   : list
    Created on : Aug 1, 2017, 2:15:58 PM
    Author     : RubySenpaii
--%>

<%@page import="object.SalesOrder"%>
<%@page import="java.util.ArrayList"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">

        <jsp:include page="../cssplugin.jsp"/>
        <title>TCP - Administrator Sales Order List</title>
    </head>
    <body>
        <div id="wrapper">

            <!-- Navigation -->
            <nav class="navbar navbar-default navbar-static-top" role="navigation" style="margin-bottom: 0">
                <jsp:include page="_navbar.jsp"/>
                <jsp:include page="_sidebar.jsp"/>
            </nav>

            <div id="page-wrapper">
                <div class="container-fluid">
                    <div class="row">
                        <div class="col-lg-12">
                            <h2 class="page-header">Sales Order List</h2>
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-lg-12">
                            <%
                                String dangerAlert = "none";
                                String errorMessage = "";
                                if (session.getAttribute("dangerAlert") != null) {
                                    dangerAlert = (String) session.getAttribute("dangerAlert");
                                    errorMessage = (String) session.getAttribute("errorMessage");
                                }
                            %>
                            <div class="alert alert-danger" style="display: <%=dangerAlert%>; text-align: center">
                                <%=errorMessage%>
                            </div>
                            <%
                                String successAlert = "none";
                                String successMessage = "";
                                if (session.getAttribute("successAlert") != null) {
                                    successAlert = (String) session.getAttribute("successAlert");
                                    successMessage = (String) session.getAttribute("successMessage");
                                }
                            %>
                            <div class="alert alert-success" style="display: <%=successAlert%>; text-align: center">
                                <%=successMessage%>
                            </div>
                            <table width="100%" class="table table-striped table-bordered table-hover" id="dataTables-example">
                                <thead>
                                    <tr>
                                        <th style="width: 12%">Sales Order Number</th>
                                        <th>Order Date</th>
                                        <th>Customer Name</th>
                                        <th>Order Status</th>
                                        <th style="width: 10%">Received By</th>
                                        <th style="width: 10%">Date Received</th>
                                        <th style="width: 10%">Approved By</th>
                                        <th style="width: 10%">Total Selling Price</th>
                                        <th style="width: 12%">Remarks</th>
                                        <th style="width: 8%">Action</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <%
                                        ArrayList<SalesOrder> salesOrders = (ArrayList<SalesOrder>) session.getAttribute("salesOrders");
                                        for (int a = 0; a < salesOrders.size(); a++) {
                                    %>
                                    <tr>
                                        <td><%=salesOrders.get(a).getSalesOrderNumber()%></td>
                                        <td><%=salesOrders.get(a).getOrderDate()%></td>
                                        <td><%=salesOrders.get(a).getCustomerName()%></td>
                                        <td><%=salesOrders.get(a).getOrderStatus()%></td>
                                        <td><%=salesOrders.get(a).getReceivedBy()%></td>
                                        <td><%=salesOrders.get(a).getReceivedDate()%></td>
                                        <td>Smith, John</td>
                                        <td style="text-align: right">Php <%=salesOrders.get(a).getOrderTotal()%></td>
                                        <td>
                                            <%=salesOrders.get(a).getRemarks()%>
                                        </td>
                                        <td>
                                            <form action="/TCP/AdminSalesDetail">
                                                <input type="hidden" name="salesOrderNumber" value="<%=salesOrders.get(a).getSalesOrderNumber()%>">
                                                <button class="btn btn-success" name="action" value="view_detail">View</button>
                                            </form>
                                            <form action="/TCP/AdminReport" target="_blank">
                                                <input type="hidden" name="salesOrderNumber" value="<%=salesOrders.get(a).getSalesOrderNumber()%>">
                                                <button class="btn btn-success" name="action" value="createReceipt">Create Receipt</button>
                                            </form>
                                        </td>
                                    </tr>
                                    <%
                                        }
                                    %>
                                </tbody>
                            </table>
                            <!-- /.table-responsive -->
                        </div>
                    </div>                    
                </div>
            </div>
        </div>
    </body>

    <jsp:include page="../scriptplugin.jsp"/>
</html>
