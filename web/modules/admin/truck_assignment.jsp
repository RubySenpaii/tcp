<%-- 
    Document   : truck_assignment
    Created on : Oct 29, 2017, 8:17:01 PM
    Author     : RubySenpaii
--%>

<%@page import="object.TruckReference"%>
<%@page import="java.util.ArrayList"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">

        <jsp:include page="../cssplugin.jsp"/>
        <title>TCP - Administrator Truck Assignment</title>
    </head>
    <body>
        <div id="wrapper">

            <!-- Navigation -->
            <nav class="navbar navbar-default navbar-static-top" role="navigation" style="margin-bottom: 0">
                <jsp:include page="_navbar.jsp"/>
                <jsp:include page="_sidebar.jsp"/>
            </nav>

            <div id="page-wrapper">
                <div class="container-fluid">
                    <div class="row">
                        <div class="col-lg-12">
                            <h2 class="page-header">Truck Assignment <button type="button" class="btn btn-info pull-right"  data-toggle="modal" data-target="#myModal">Add New Truck</button></h2>
                            <!-- Modal -->
                            <div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
                                <div class="modal-dialog">
                                    <div class="modal-content">
                                        <form action="/TCP/AdminTruckAssignment">
                                            <div class="modal-header">
                                                <h4 class="modal-title" id="myModalLabel">Add New Truck</h4>
                                            </div>
                                            <div class="modal-body">
                                                <div class="form-group">
                                                    <label>Plate Number</label>
                                                    <input class="form-control" name="plateNumber" placeholder="Enter Truck's Plate Number Here">
                                                </div>
                                                <div class="form-group">
                                                    <label>Max Capacity</label>
                                                    <input class="form-control" type="number" name="capacity" placeholder="Enter Max Capacity of Truck">
                                                </div>
                                            </div>
                                            <div class="modal-footer">
                                                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                                                <button type="submit" class="btn btn-primary" name="action" value="addTruck">Add New Truck</button>
                                            </div>
                                        </form>
                                    </div>
                                    <!-- /.modal-content -->
                                </div>
                                <!-- /.modal-dialog -->
                            </div>
                            <!-- /.modal -->
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-lg-12">
                            <%
                                String dangerAlert = "none";
                                String errorMessage = "";
                                if (session.getAttribute("dangerAlert") != null) {
                                    dangerAlert = (String) session.getAttribute("dangerAlert");
                                    errorMessage = (String) session.getAttribute("errorMessage");
                                }
                            %>
                            <div class="alert alert-danger" style="display: <%=dangerAlert%>; text-align: center">
                                <%=errorMessage%>
                            </div>
                            <%
                                String successAlert = "none";
                                String successMessage = "";
                                if (session.getAttribute("successAlert") != null) {
                                    successAlert = (String) session.getAttribute("successAlert");
                                    successMessage = (String) session.getAttribute("successMessage");
                                }
                            %>
                            <div class="alert alert-success" style="display: <%=successAlert%>; text-align: center">
                                <%=successMessage%>
                            </div>
                            <table width="100%" class="table table-striped table-bordered table-hover" id="dataTables-example">
                                <thead>
                                    <tr>
                                        <th style="width: 9%">Truck Plate Number</th>
                                        <th style="width: 10%">Assigned Orders</th>
                                        <th style="width: 14%">Total Quantity for Delivery</th>
                                        <th style="width: 10%">Capacity</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <%
                                        ArrayList<TruckReference> trucks = (ArrayList<TruckReference>) session.getAttribute("trucks");
                                        for (int a = 0; a < trucks.size(); a++) {
                                    %>
                                    <tr>
                                        <td><%=trucks.get(a).getPlateNumber()%></td>
                                        <td><%=trucks.get(a).getDetails()%></td>
                                        <td><%=trucks.get(a).getTotalQuantity()%></td>
                                        <td><%=trucks.get(a).getCapacity()%></td>
                                    </tr>
                                    <%
                                        }
                                    %>
                                </tbody>
                            </table>
                            <!-- /.table-responsive -->
                        </div>
                    </div>                    
                </div>
            </div>
        </div>
    </body>

    <jsp:include page="../scriptplugin.jsp"/>
</html>
