<%-- 
    Document   : _sidebar
    Created on : Jul 31, 2017, 3:22:31 PM
    Author     : RubySenpaii
--%>

<%@page import="object.Employee"%>
<div class="navbar-default sidebar" role="navigation">
    <div class="sidebar-nav navbar-collapse">
        <ul class="nav" id="side-menu">

            <li>
                <a href="/TCP/AdminHomepage?action=homepage"><i class="fa fa-home fa-fw"></i> Home Page</a>
            </li>
            <li>
                <a href="#"><i class="fa fa-sticky-note-o fa-fw"></i> Procurement<span class="fa arrow"></span></a>
                <ul class="nav nav-second-level">
                    <li>
                        <a href="/TCP/AdminProcurementCreate?action=procure_page">Create Purchase Order</a>
                    </li>
                    <li>
                        <a href="/TCP/AdminProcurementList?action=procure_list">View Purchase Orders</a>
                    </li>
                    <li>
                        <a href="/TCP/AdminPurchaseDefective?action=goPage">Purchase Returns</a>
                    </li>
                    <li>
                        <a href="/TCP/AdminPurchaseReturnStatus?action=showList">Purchase Return Status</a>
                    </li>
                </ul>
            </li>
            <li>
                <a href="#"><i class="fa fa-list-alt fa-fw"></i> Sales<span class="fa arrow"></span></a>
                <ul class="nav nav-second-level">
                    <li>
                        <a href="/TCP/AdminSalesList?action=approved_sales">Approved Sales Order</a>
                    </li>
                    <li>
                        <a href="/TCP/AdminSalesList?action=rejected_sales">Rejected Sales Order</a>
                    </li>
                    <li>
                        <a href="/TCP/AdminSalesDefective?action=goPage">Sales Returns</a>
                    </li>
                </ul>
            </li>
            <li>
                <a href="#"><i class="fa fa-barcode fa-fw"></i> Inventory<span class="fa arrow"></span></a>
                <ul class="nav nav-second-level">
                    <li>
                        <a href="/TCP/modules/admin/inventory_add.jsp">Add New Product</a>
                    </li>
                    <li>
                        <a href="/TCP/AdminViewInventoryList?action=activeProducts">View Active Products</a>
                    </li>
                    <li>
                        <a href="/TCP/AdminViewInventoryList?action=archivedProducts">Archived Products</a>
                    </li>
                </ul>
            </li>
            <li>
                <a href="/TCP/AdminDeliveryUpdate?action=viewDeliveries"><i class="fa fa-inbox fa-fw"></i> Delivery</a>
            </li>
            <li>
                <a href="/TCP/AdminTruckAssignment?action=viewTrucks"><i class="fa fa-truck fa-fw"></i> Truck Assignments</a>
            </li>
            <%
                Employee userLogged = (Employee) session.getAttribute("userLogged");
                if (userLogged.getUserLevel() > 1) {
            %>
            <li>
                <a href="#"><i class="fa fa-users fa-fw"></i> Users<span class="fa arrow"></span></a>
                <ul class="nav nav-second-level">
                    <li>
                        <a href="/TCP/modules/admin/employee_registration.jsp">Employee Registration</a>
                    </li>
                    <li>
                        <a href="/TCP/AdminEmployeeList?action=view_employee_list">Employee List</a>
                    </li>
                    <li>
                        <a href="/TCP/AdminCustomerList?action=view_customer_list">Customer List</a>
                    </li>
                    <li>
                        <a href="/TCP/modules/admin/supplier_add.jsp">Supplier Registration</a>
                    </li>
                    <li>
                        <a href="/TCP/AdminSupplierList?action=view_supplier_list">Supplier List</a>
                    </li>
                </ul>
            </li>
            <li>
                <a href="/TCP/AdminReport?action=reports"><i class="fa fa-bar-chart-o fa-fw"></i> Reports</a>
            </li>
            <%
                }
            %>
            <li>
                <a href="/TCP/AdminBackup?action=viewBackups"><i class="fa fa-hdd-o fa-fw"></i> Backup (Configuration)</a>
            </li>
            <li>
                <a href="/TCP/AdminContact?action=viewList"><i class="fa fa-file-text-o fa-fw"></i> Messages</a>
            </li>
        </ul>
    </div>
</div>