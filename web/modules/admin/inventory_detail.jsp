<%-- 
    Document   : inventory_detail
    Created on : Sep 22, 2017, 10:35:39 PM
    Author     : RubySenpaii
--%>

<%@page import="object.Employee"%>
<%@page import="object.Inventory"%>
<%@page import="java.util.ArrayList"%>
<%@page import="object.Product"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">

        <jsp:include page="../cssplugin.jsp"/>
        <title>TCP - Administrator Template</title>
    </head>
    <body>
        <div id="wrapper">

            <!-- Navigation -->
            <nav class="navbar navbar-default navbar-static-top" role="navigation" style="margin-bottom: 0">
                <jsp:include page="_navbar.jsp"/>
                <jsp:include page="_sidebar.jsp"/>
            </nav>

            <div id="page-wrapper">
                <div class="container-fluid">
                    <div class="row">
                        <div class="col-lg-12">
                            <%
                                Product product = (Product) session.getAttribute("product");
                            %>
                            <h2 class="page-header">Product #<%=product.getProductID()%></h2>
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-lg-6 col-md-offset-3">
                            <form role="form" action="/TCP/AdminInventoryDetail" enctype="multipart/form-data" method="POST">
                                <div class="form-group">
                                    <label>Product Name</label>
                                    <input class="form-control" name="product_name" value="<%=product.getProductName()%>" <%=(String) session.getAttribute("enabled")%> autocomplete="off" required pattern="[a-zA-Z0-9,./-:;]+[a-zA-Z0-9,./;:- ]+" title="Alphanumeric with Spaces ONLY">
                                </div>
                                <div class="form-group">
                                    <label>Category</label>
                                    <input class="form-control" name="category" value="<%=product.getCategory()%>" <%=(String) session.getAttribute("enabled")%> autocomplete="off" required pattern="[a-zA-Z0-9]+[a-zA-Z0-9 ]+" title="Alphanumeric with Spaces ONLY">
                                </div>
                                <div class="form-group">
                                    <label>Description</label>
                                    <input class="form-control" name="description" value="<%=product.getDescription()%>" <%=(String) session.getAttribute("enabled")%> autocomplete="off" required pattern="[a-zA-Z0-9,./-_()#'<>*%!@]+[a-zA-Z0-9,./-_()#'<>*%!@ ]+" title="Alphanumeric with Spaces ONLY">
                                </div>
                                <div class="form-group">
                                    <label>Selling Price</label>
                                    <input class="form-control" name="unit_price" value="<%=product.getUnitPrice()%>" <%=(String) session.getAttribute("enabled")%> autocomplete="off" required>
                                </div>
                                <%
                                    Employee userLogged = (Employee) session.getAttribute("userLogged");
                                    if (userLogged.getUserLevel() > 1) {
                                %>
                                <div class="form-group">
                                    <label>Cost Price</label>
                                    <input class="form-control" name="cost_price" value="<%=product.getCostPrice()%>" <%=(String) session.getAttribute("enabled")%> autocomplete="off" required>
                                </div>
                                <%
                                    }
                                %>
                                <div class="form-group">
                                    <label>Unit</label>
                                    <input class="form-control" name="unit" value="<%=product.getUnit()%>" <%=(String) session.getAttribute("enabled")%> autocomplete="off" required pattern="[a-zA-Z0-9]+[a-zA-Z0-9 ]+" title="Alphanumeric with Spaces ONLY">
                                </div>
                                <div class="form-group">
                                    <label>Critical Level</label>
                                    <input class="form-control" name="critical_level" value="<%=product.getCriticalLevel()%>" <%=(String) session.getAttribute("enabled")%> autocomplete="off" required>
                                </div>
                                <%
                                    if (((String) session.getAttribute("enabled")).equals("disabled")) {
                                %>
                                <div class="col-md-12">
                                    <table class="table table-bordered">
                                        <thead>
                                            <tr>
                                                <th>Date</th>
                                                <th>Defect</th>
                                                <th>Purchased</th>
                                                <th>Sold</th>
                                                <th>Stock Quantity</th>
                                                <th>Remarks</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            <%
                                                ArrayList<Inventory> inventories = (ArrayList<Inventory>) session.getAttribute("inventories");
                                                for (int a = 0; a < inventories.size(); a++) {
                                            %>
                                            <tr>
                                                <td><%=inventories.get(a).getDate()%></td>
                                                <td><%=inventories.get(a).getDefect()%></td>
                                                <td><%=inventories.get(a).getIn()%></td>
                                                <td><%=inventories.get(a).getOut()%></td>
                                                <td><%=inventories.get(a).getTotal()%></td>
                                                <td><%=inventories.get(a).getReference()%></td>
                                            </tr>
                                            <%
                                                }
                                            %>
                                        </tbody>
                                    </table>
                                </div>
                                <%
                                    }
                                %>
                                <div class="text-center">
                                    <button class="btn btn-success" type="submit" name="action" value="back">Back</button>
                                    <%
                                        if (((String) session.getAttribute("enabled")).equals("enabled")) {
                                    %>
                                    <button class="btn btn-success" type="submit" name="action" value="submit">Submit</button>
                                    <%
                                        }
                                    %>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </body>

    <jsp:include page="../scriptplugin.jsp"/>
</html>
