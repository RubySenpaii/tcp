<%-- 
    Document   : report
    Created on : Oct 31, 2017, 4:15:51 PM
    Author     : RubySenpaii
--%>

<%@page import="object.Employee"%>
<%@page import="java.util.ArrayList"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">

        <jsp:include page="../cssplugin.jsp"/>
        <title>TCP - Administrator Reports</title>
    </head>
    <body>
        <div id="wrapper">

            <!-- Navigation -->
            <nav class="navbar navbar-default navbar-static-top" role="navigation" style="margin-bottom: 0">
                <jsp:include page="_navbar.jsp"/>
                <jsp:include page="_sidebar.jsp"/>
            </nav>

            <div id="page-wrapper">
                <div class="container-fluid">
                    <div class="row">
                        <div class="col-lg-12">
                            <h2 class="page-header">Reports Page</h2>
                        </div>
                    </div>

                    <div class="row">
                        <%
                            String dangerAlert = "none";
                            String errorMessage = "";
                            if (session.getAttribute("dangerAlert") != null) {
                                dangerAlert = (String) session.getAttribute("dangerAlert");
                                errorMessage = (String) session.getAttribute("errorMessage");
                            }
                        %>
                        <div class="alert alert-danger" style="display: <%=dangerAlert%>; text-align: center">
                            <%=errorMessage%>
                        </div>
                        <%
                            String successAlert = "none";
                            String successMessage = "";
                            if (session.getAttribute("successAlert") != null) {
                                successAlert = (String) session.getAttribute("successAlert");
                                successMessage = (String) session.getAttribute("successMessage");
                            }
                        %>
                        <div class="alert alert-success" style="display: <%=successAlert%>; text-align: center">
                            <%=successMessage%>
                        </div>
                    </div>

                    <div class="row panel">
                        <form action="/TCP/AdminReport">
                            <div class="col-md-4">
                                <div class="form-group">
                                    <label>Report</label>
                                    <select name="reportType" class="form-control" required>
                                        <option disaled>-- Select One --</option>
                                        <option>Purchase Report</option>
                                        <option>Sales Report</option>
                                        <option>Sales Return Report</option>
                                        <option>Delivery Report</option>
                                        <option>Top Product</option>
                                    </select>
                                </div>
                                <label>Date Range</label>
                                <div class="form-group input-group">
                                    <input type="date" class="form-control" name="startDate" required>
                                    <span class="input-group-addon">to</span>
                                    <input type="date" class="form-control" name="endDate" required>
                                </div>
                            </div>
                            <div class="col-md-4">
                                <div class="form-group">
                                    <label>Prepared By</label>
                                    <%
                                        Employee logged = (Employee) session.getAttribute("userLogged");
                                        String name = logged.getFirstName() + " " + logged.getLastName();
                                    %>
                                    <input class="form-control" value="<%=name%>" disabled>
                                </div>
                            </div>
                            <div class="col-md-4">
                                <button type="submit" name="action" value="createReport">Create Report</button>
                            </div>
                        </form>
                    </div>

                    <div class="row">
                        <div class="col-md-5">
                            <a class="btn btn-success" href="/TCP/AdminReport?action=sales">Sales</a>
                            <a class="btn btn-danger" href="/TCP/AdminReport?action=purchase">Purchases</a>
                            <a class="btn btn-warning" href="/TCP/AdminReport?action=inventory">Inventory</a>
                            <a class="btn btn-primary" href="/TCP/AdminReport?action=view">All</a>

                            <br><br>
                            <table class="table table-bordered" id="dataTables-example">
                                <thead>
                                    <tr>
                                        <th>Report Type</th>
                                        <th>Coverage Date</th>
                                        <th>Files</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <%
                                        ArrayList<String> files = (ArrayList<String>) session.getAttribute("fileList");
                                        for (int a = 0; a < files.size(); a++) {
                                            String reportType = files.get(a).split("For")[0];
                                            String forDate = files.get(a).split("For")[1].split(".p")[0];
                                    %>
                                    <tr>
                                        <td><%=reportType%></td>
                                        <td><%=forDate%></td>
                                        <td>
                                            <button onclick="document.getElementById('pdfViewer').setAttribute('data', '/TCP/assets/pdf/report/<%=files.get(a)%>')">View</button>
                                        </td>
                                    </tr>
                                    <%
                                        }
                                    %>
                                </tbody>
                            </table>
                        </div>

                        <div class="col-md-7">
                            <object id="pdfViewer" data="" type="application/pdf" width="100%" height="800px" style="padding: 20px">
                                <p>Alternative text - include a link <a href="myfile.pdf">to the PDF!</a></p>
                            </object>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </body>

    <jsp:include page="../scriptplugin.jsp"/>

    <script>
        function changePdf(val) {
            alert(val);
        }
    </script>
</html>
