<%-- 
    Document   : employee_detail
    Created on : Sep 20, 2017, 11:23:07 PM
    Author     : RubySenpaii
--%>

<%@page import="object.Employee"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">

        <jsp:include page="../cssplugin.jsp"/>
        <title>TCP - Administrator Employee Details</title>
    </head>
    <body>
        <div id="wrapper">

            <!-- Navigation -->
            <nav class="navbar navbar-default navbar-static-top" role="navigation" style="margin-bottom: 0">
                <jsp:include page="_navbar.jsp"/>
                <jsp:include page="_sidebar.jsp"/>
            </nav>

            <div id="page-wrapper">
                <div class="container-fluid">
                    <div class="row">
                        <div class="col-lg-12">
                            <%
                                Employee employee = (Employee) session.getAttribute("employee");
                            %>
                            <h2 class="page-header">Employee Details</h2>
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-lg-12">
                            <form action="/TCP/AdminEmployeeDetail">
                                <div class="form-group">
                                    <label>Last Name</label>
                                    <input class="form-control" name="lastName" <%=(String) session.getAttribute("enabled")%> value="<%=employee.getLastName()%>" pattern="[a-zA-Z0-9]+[a-zA-Z0-9 ]+" title="Alphanumeric with Spaces ONLY">
                                </div>
                                <div class="form-group">
                                    <label>First Name</label>
                                    <input class="form-control" name="firstName" disabled value="<%=employee.getFirstName()%>" pattern="[a-zA-Z0-9]+[a-zA-Z0-9 ]+" title="Alphanumeric with Spaces ONLY">
                                </div>
                                <div class="form-group">
                                    <label>Middle Name</label>
                                    <input class="form-control" name="middleName" <%=(String) session.getAttribute("enabled")%> value="<%=employee.getMiddleName()%>" pattern="[a-zA-Z0-9]+[a-zA-Z0-9 ]+" title="Alphanumeric with Spaces ONLY">
                                </div>
                                <div class="form-group">
                                    <label>Address</label>
                                    <input class="form-control" name="address" <%=(String) session.getAttribute("enabled")%> value="<%=employee.getAddress()%>" pattern="[a-zA-Z0-9,./()[]!':;<>?-_#]+[a-zA-Z0-9,./()[]!':;<>?-_# ]+" title="Alphanumeric with Spaces ONLY">
                                </div>
                                <div class="form-group">
                                    <label>Contact Number</label>
                                    <input class="form-control" name="contactNumber" <%=(String) session.getAttribute("enabled")%> value="<%=employee.getContactNo()%>" pattern="[a-zA-Z0-9]+[a-zA-Z0-9 ]+" title="Alphanumeric with Spaces ONLY">
                                </div>
                                <div class="form-group">
                                    <label>Email</label>
                                    <input class="form-control" name="email" disabled value="<%=employee.getEmail()%>">
                                </div>
                                <div class="form-group">
                                    <label>Mother's Maiden Name</label>
                                    <input class="form-control" name="motherMaiden" disabled value="<%=employee.getMotherMaiden()%>">
                                </div>
                                <div class="form-group">
                                    <label>Birthday</label>
                                    <input class="form-control" name="birthday" type="date" disabled value="<%=employee.getBirthday()%>">
                                </div>
                                <div class="text-center">
                                    <button class="btn btn-primary" type="submit" name="action" value="back">Back</button>
                                    <%
                                        if (((String) session.getAttribute("enabled")).equals("enabled")) {
                                    %>
                                    <button class="btn btn-success" type="submit" name="action" value="submit">Submit</button>
                                    <%
                                        }
                                    %>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </body>

    <jsp:include page="../scriptplugin.jsp"/>
</html>
