<%-- 
    Document   : inventory_defective
    Created on : Sep 22, 2017, 9:41:32 PM
    Author     : RubySenpaii
--%>

<%@page import="object.PurchaseOrder"%>
<%@page import="object.PurchaseOrderItem"%>
<%@page import="java.util.ArrayList"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">

        <jsp:include page="../cssplugin.jsp"/>
        <title>TCP - Administrator Template</title>
    </head>
    <body>
        <div id="wrapper">

            <!-- Navigation -->
            <nav class="navbar navbar-default navbar-static-top" role="navigation" style="margin-bottom: 0">
                <jsp:include page="_navbar.jsp"/>
                <jsp:include page="_sidebar.jsp"/>
            </nav>

            <div id="page-wrapper">
                <div class="container-fluid">
                    <div class="row">
                        <div class="col-lg-12">
                            <h2 class="page-header">Return Defective</h2>
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-lg-12">
                            <form action="/TCP/AdminPurchaseDefective">
                                <label>Purchase Order Number</label>
                                <div class="form-group input-group">
                                    <select class="form-control" name="purchaseOrderNumber">
                                        <%
                                            ArrayList<PurchaseOrder> purchaseOrders = (ArrayList<PurchaseOrder>) session.getAttribute("purchaseOrders");
                                            for (int a = 0; a < purchaseOrders.size(); a++) {
                                        %>
                                        <option><%=purchaseOrders.get(a).getPurchaseOrderNumber()%></option>
                                        <%
                                            }
                                        %>
                                    </select>
                                    <span class="input-group-btn">
                                        <button class="btn btn-default" type="submit" name="action" value="view"><i class="fa fa-search"></i>
                                        </button>
                                    </span>
                                </div>

                                <%
                                    PurchaseOrder purchaseOrder = (PurchaseOrder) session.getAttribute("purchaseOrder");
                                    ArrayList<PurchaseOrderItem> purchaseItems = (ArrayList<PurchaseOrderItem>) session.getAttribute("purchaseItems");
                                    if (purchaseItems.size() > 0) {
                                %>
                                 <div class="form-group">
                                    <label>Supplier Name</label>
                                    <input class="form-control" value="<%=purchaseOrder.getSupplierName()%>" disabled>
                                </div>
                                <table class="table table-bordered">
                                    <thead>
                                        <tr>
                                            <th>Product ID</th>
                                            <th>Product Name</th>
                                            <th>Quantity Ordered</th>
                                            <th>Quantity to Return</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <%
                                            for (int a = 0; a < purchaseItems.size(); a++) {
                                        %>
                                        <tr>
                                            <td><%=purchaseItems.get(a).getProductID()%></td>
                                            <td><%=purchaseItems.get(a).getProductName()%></td>
                                            <td><%=purchaseItems.get(a).getQuantity()%></td>
                                            <td>
                                                <input type="number" name="defective" value="0" required>
                                            </td>
                                        </tr>
                                        <%
                                            }
                                        %>
                                    </tbody>
                                </table>

                                <div class="form-group">
                                    <label>Remarks</label>
                                    <textarea class="form-control" name="remarks" required></textarea>
                                </div>

                                <div class="text-center">
                                    <button class="btn btn-success" type="submit" name="action" value="submit">Submit</button>
                                </div>
                                <%
                                    }
                                %>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </body>

    <jsp:include page="../scriptplugin.jsp"/>
</html>
