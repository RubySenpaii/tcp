<%-- 
    Document   : inventory_defective
    Created on : Sep 22, 2017, 9:41:32 PM
    Author     : RubySenpaii
--%>

<%@page import="object.SalesOrder"%>
<%@page import="object.SalesOrderItem"%>
<%@page import="java.util.ArrayList"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">

        <jsp:include page="../cssplugin.jsp"/>
        <title>TCP - Administrator Template</title>
    </head>
    <body>
        <div id="wrapper">

            <!-- Navigation -->
            <nav class="navbar navbar-default navbar-static-top" role="navigation" style="margin-bottom: 0">
                <jsp:include page="_navbar.jsp"/>
                <jsp:include page="_sidebar.jsp"/>
            </nav>

            <div id="page-wrapper">
                <div class="container-fluid">
                    <div class="row">
                        <div class="col-lg-12">
                            <h2 class="page-header">Return Defective</h2>
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-lg-12">
                            <form action="/TCP/AdminSalesDefective">
                                <label>Sales Order Number</label>
                                <div class="form-group input-group">
                                    <select class="form-control" name="salesOrderNumber">
                                        <%
                                            ArrayList<SalesOrder> salesOrders = (ArrayList<SalesOrder>) session.getAttribute("salesOrders");
                                            for (int a = 0; a < salesOrders.size(); a++) {
                                        %>
                                        <option><%=salesOrders.get(a).getSalesOrderNumber()%></option>
                                        <%
                                            }
                                        %>
                                    </select>
                                    <span class="input-group-btn">
                                        <button class="btn btn-default" type="submit" name="action" value="view"><i class="fa fa-search"></i>
                                        </button>
                                    </span>
                                </div>

                                <%
                                    ArrayList<SalesOrderItem> salesItems = (ArrayList<SalesOrderItem>) session.getAttribute("salesItems");
                                    if (salesItems.size() > 0) {
                                        SalesOrder salesOrder = (SalesOrder) session.getAttribute("salesOrder");
                                %>
                                <div class="form-group">
                                    <label>Customer Name</label>
                                    <input class="form-control" value="<%=salesOrder.getCustomerName()%>" disabled>
                                </div>
                                <table class="table table-bordered">
                                    <thead>
                                        <tr>
                                            <th>Product ID</th>
                                            <th>Product Name</th>
                                            <th>Quantity Ordered</th>
                                            <th>Quantity to Return</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <%
                                            for (int a = 0; a < salesItems.size(); a++) {
                                        %>
                                        <tr>
                                            <td><%=salesItems.get(a).getProductID()%></td>
                                            <td><%=salesItems.get(a).getProductName()%></td>
                                            <td><%=salesItems.get(a).getQuantity()%></td>
                                            <td>
                                                <input type="number" name="defective" value="0" required>
                                            </td>
                                        </tr>
                                        <%
                                            }
                                        %>
                                    </tbody>
                                </table>

                                <div class="form-group">
                                    <label>Remarks</label>
                                    <textarea class="form-control" name="remarks" required></textarea>
                                </div>

                                <div class="text-center">
                                    <button class="btn btn-success" type="submit" name="action" value="submit">Submit</button>
                                </div>
                                <%
                                    }
                                %>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </body>

    <jsp:include page="../scriptplugin.jsp"/>
</html>
