<%-- 
    Document   : procurement_create
    Created on : Aug 20, 2017, 5:26:12 PM
    Author     : RubySenpaii
--%>

<%@page import="object.Supplier"%>
<%@page import="object.Product"%>
<%@page import="java.util.ArrayList"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">

        <jsp:include page="../cssplugin.jsp"/>
        <title>TCP - Administrator Create Purchase Order</title>
    </head>
    <body>
        <div id="wrapper">

            <!-- Navigation -->
            <nav class="navbar navbar-default navbar-static-top" role="navigation" style="margin-bottom: 0">
                <jsp:include page="_navbar.jsp"/>
                <jsp:include page="_sidebar.jsp"/>
            </nav>

            <div id="page-wrapper">
                <div class="container-fluid">
                    <div class="row">
                        <div class="col-lg-12">
                            <h2 class="page-header">Create Purchase Order</h2>
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-lg-8 col-md-offset-2">
                            <form role="form" action="/TCP/AdminProcurementCreate">
                                <%
                                    String dangerAlert = "none";
                                    String errorMessage = "";
                                    if (session.getAttribute("dangerAlert") != null) {
                                        dangerAlert = (String) session.getAttribute("dangerAlert");
                                        errorMessage = (String) session.getAttribute("errorMessage");
                                    }
                                %>
                                <div class="alert alert-danger" style="display: <%=dangerAlert%>; text-align: center">
                                    <%=errorMessage%>
                                </div>
                                <%
                                    String successAlert = "none";
                                    String successMessage = "";
                                    if (session.getAttribute("successAlert") != null) {
                                        successAlert = (String) session.getAttribute("successAlert");
                                        successMessage = (String) session.getAttribute("successMessage");
                                    }
                                %>
                                <div class="alert alert-success" style="display: <%=successAlert%>; text-align: center">
                                    <%=successMessage%>
                                </div>
                                <div class="form-group">
                                    <label>Purchase Order Number</label>
                                    <input class="form-control" value="<%=(Integer) session.getAttribute("purchaseOrderNumber")%>" disabled>
                                    <input class="form-control" type="hidden" name="purchaseOrderNumber" value="<%=(Integer) session.getAttribute("purchaseOrderNumber")%>">
                                </div>
                                <div class="form-group">
                                    <label>Product Search</label>
                                    <input class="form-control" list="products" name="productSearch" value="<%=(String) session.getAttribute("productName")%>" id="productSearch" onchange="getSupplier()" autofocus="on" onfocus="getSupplier()">
                                    <datalist id="products">
                                        <%
                                            ArrayList<Product> products = (ArrayList<Product>) session.getAttribute("products");
                                            for (int a = 0; a < products.size(); a++) {
                                        %>
                                        <option value="<%=products.get(a).getProductName()%>"></option>
                                        <%
                                            }
                                        %>
                                    </datalist>
                                </div>
                                <table class="table table-bordered">
                                    <thead>
                                        <tr>
                                            <th></th>
                                            <th>Supplier Name</th>
                                            <th>Punctuality</th>
                                            <th>Volume</th>
                                            <th>Customer Friendly</th>
                                            <th>Payment Terms</th>
                                            <th>Average Rating</th>
                                        </tr>
                                    </thead>
                                    <tbody id="supplierTable">
                                    </tbody>
                                </table>
                                <div class="form-group">
                                    <label>Expected Delivery Date</label>
                                    <input id="datefield" class="form-control" name="expectedDelivery" type="date">
                                </div>
                                <div class="form-group">
                                    <label>Delivery Address</label>
                                    <input class="form-control" name="address" pattern="[a-zA-Z0-9,.]+[a-zA-Z0-9,. ]+" title="Alphanumeric with Spaces ONLY" value="118 Upper Bonifacio Street corner Sumulong Street, Holy Ghost Proper Baguio City" required>
                                </div>
                                <div class="form-group">
                                    <label>Contact Person</label>
                                    <input class="form-control" name="contactPerson"  pattern="[a-zA-Z0-9]+[a-zA-Z0-9 ]+" title="Alphanumeric with Spaces ONLY" required autocomplete="off">
                                </div>
                                <div class="form-group">
                                    <label>Contact No</label>
                                    <input class="form-control" name="contactNo"  pattern="[a-zA-Z0-9]+[a-zA-Z0-9 ]+" title="Alphanumeric with Spaces ONLY" required autocomplete="off">
                                </div>
                                <div class="form-group">
                                    <label>Payment</label>
                                    <select class="form-control" name="payment">
                                        <option>Cash</option>
                                        <option>Credit 30 Days</option>
                                        <option>Credit 60 Days</option>
                                        <option>Credit 90 Days</option>
                                    </select>
                                </div>
                                <table class="table table-bordered">
                                    <thead>
                                        <tr>
                                            <th style="width: 15px">
                                                <input type="checkbox" class="check_all" onclick="select_all()">
                                            </th>
                                            <th>Product Name</th>
                                            <th>Quantity</th>
                                            <th>Cost Price</th>
                                            <th>Total Cost Price</th>
                                        </tr>
                                    </thead>
                                    <tbody id="productTable">
                                        <tr>
                                            <td><input type="checkbox" class="case"></td>
                                            <td><input list="supplierItemList" name="productName" id="productName1" onchange="getPrice(this.id)" required></td>
                                            <td><input type="number" name="quantity" min="1" id="qty1" required onchange="getTotal(this.id)"></td>
                                            <td><input type="number" name="unitPrice" step="0.01" id="unitPrice1" required></td>
                                            <td><input type="number" name="totalUnitPrice" id="total1" disabled required></td>
                                        </tr>
                                    <datalist id="supplierItemList">
                                    </datalist>
                                    <button type="button" onclick="delete_row()">- Delete</button>
                                    <button type="button" onclick="add_row()">+ Add More</button>
                                    </tbody>
                                </table>

                                <div class="col-md-12">
                                    <div class="form-group">
                                        <label>Remarks</label>
                                        <textarea class="form-control" name="remarks" required></textarea>
                                    </div>
                                </div>

                                <div class="row">
                                    <div class="col-md-12">
                                        <input type="checkbox" required> <b>NOTE: Order is subject for approval by TPC.</b>
                                    </div>
                                </div>      

                                <div class="text-center">
                                    <button class="btn btn-success" type="submit" name="action" value="submit">Submit</button>
                                    <!--button class="btn btn-success" type="button" data-toggle="modal" data-target="#myModal">Submit</button-->
                                </div>
                                <!-- Modal >
                                <div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
                                    <div class="modal-dialog">
                                        <div class="modal-content">
                                            <div class="modal-header">
                                                <h4 class="modal-title" id="myModalLabel">Order Confirmation</h4>
                                            </div>
                                            <div class="modal-body">
                                                Order is subject for approval by super admin.
                                            </div>
                                            <div class="modal-footer">
                                                <button class="btn btn-success" type="submit" name="action" value="submit">Submit</button>
                                            </div>
                                        </div>
                                        <!-- /.modal-content >
                                    </div>
                                    <!-- /.modal-dialog >
                                </div>
                                <!-- /.modal -->
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <script>
            function delete_row() {
                $('.case:checkbox:checked').parents("tr").remove();
                $('.check_all').prop("checked", false);
                check();
            }
            var i = 2;
            function add_row() {
                count = $('table tr').length;
                var data = "<tr><td><input type='checkbox' class='case'/></td>";
                data += '<td><input list="supplierItemList" name="productName" id="productName' + i + '" onchange="getPrice(this.id)" required></td>';
                data += '<td><input type="number" name="quantity" min="1" id="qty' + i + '" required onchange="getTotal(this.id)"></td>';
                data += '<td><input type="number" name="unitPrice" step="0.01" id="unitPrice' + i + '" required></td>';
                data += '<td><input type="number" name="totalUnitPrice" disabled id="total' + i + '" required></td></tr>';
                $('#productTable').append(data);
                i++;
            }

            function select_all() {
                $('input[class=case]:checkbox').each(function () {
                    if ($('input[class=check_all]:checkbox:checked').length === 0) {
                        $(this).prop("checked", false);
                    } else {
                        $(this).prop("checked", true);
                    }
                });
            }

            function check() {
                obj = $('table tr').find('span');
                $.each(obj, function (key, value) {
                    id = value.id;
                    $('#' + id).html(key + 1);
                });
            }

            function getSupplier() {
                var productName = document.getElementById('productSearch').value;
                $.ajax({
                    url: 'CreateProcurementSupplierList',
                    dataType: 'json',
                    data: {productName: productName},
                    success: function (data) {
                        $('#supplierTable').html('');
                        if (data.suggestions.length === 0) {
                            $('#supplierTable').append('<tr><td colspan="3">No results found.</td></tr>');
                        }
                        for (var a = 0; a < data.suggestions.length; a++) {
                            $('#supplierTable').append('<tr><td><input type="radio" name="supplierName" onchange="getSupplierItems(this.value)" value="' + data.suggestions[a].supplierName + '"</td><td>' + data.suggestions[a].supplierName + '</td><td>' + data.suggestions[a].punctualityRating + '</td><td>' + data.suggestions[a].volumeRating + '</td><td>' + data.suggestions[a].friendlyRating + '</td><td>' + data.suggestions[a].paymentRating + '</td><td>' + data.suggestions[a].averageRating + '</td></tr>');
                        }
                    }
                });
            }

            var supplierItemsProductName = [];
            var supplierItemsProductPrice = [];
            function getSupplierItems(value) {
                var supplierName = value;
                $.ajax({
                    url: 'CreateProcurementSupplierItem',
                    dataType: 'json',
                    data: {supplierName: supplierName},
                    success: function (data) {
                        supplierItemsProductName = [];
                        supplierItemsProductPrice = [];
                        $('#supplierItemList').html('');
                        for (var a = 0; a < data.suggestions.length; a++) {
                            $('#supplierItemList').append('<option>' + data.suggestions[a].productName + '</option>');
                            supplierItemsProductName.push(data.suggestions[a].productName);
                            supplierItemsProductPrice.push(data.suggestions[a].unitPrice);
                        }
                    }
                });
            }

            function getPrice(value) {
                var productName = document.getElementById(value).value;
                for (var a = 0; a < supplierItemsProductName.length; a++) {
                    if (productName === supplierItemsProductName[a]) {
                        document.getElementById("unitPrice" + value.substr(value.length - 1)).value = supplierItemsProductPrice[a];
                        break;
                    }
                }
            }
            
            function getTotal(value) {
                var id = value.substr(value.length - 1);
                var qty = document.getElementById("qty" + id).value;
                var cost = document.getElementById("unitPrice" + id).value;
                document.getElementById("total" + id).value = qty * cost;
            }
        </script>
    </body>

    <jsp:include page="../scriptplugin.jsp"/>
</html>
