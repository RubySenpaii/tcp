<%-- 
    Document   : homepage
    Created on : Jul 30, 2017, 5:20:13 PM
    Author     : RubySenpaii
--%>


<%@page import="object.PurchaseOrder"%>
<%@page import="object.TruckReference"%>
<%@page import="object.Employee"%>
<%@page import="object.Product"%>
<%@page import="object.SalesPayment"%>
<%@page import="object.SalesOrder"%>
<%@page import="java.util.ArrayList"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">

        <jsp:include page="../cssplugin.jsp"/>
        <title>TCP - Administrator Home Page</title>
    </head>
    <body>
        <div id="wrapper">

            <!-- Navigation -->
            <nav class="navbar navbar-default navbar-static-top" role="navigation" style="margin-bottom: 0">
                <jsp:include page="_navbar.jsp"/>
                <jsp:include page="_sidebar.jsp"/>
            </nav>

            <div id="page-wrapper">
                <div class="container-fluid">
                    <div class="row">
                        <div class="col-lg-12">
                            <h2 class="page-header">Home Page</h2>
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-md-12">
                            <%
                                String dangerAlert = "none";
                                String errorMessage = "";
                                if (session.getAttribute("dangerAlert") != null) {
                                    dangerAlert = (String) session.getAttribute("dangerAlert");
                                    errorMessage = (String) session.getAttribute("errorMessage");
                                }
                            %>
                            <div class="alert alert-danger" style="display: <%=dangerAlert%>; text-align: center">
                                <%=errorMessage%>
                            </div>
                            <%
                                String successAlert = "none";
                                String successMessage = "";
                                if (session.getAttribute("successAlert") != null) {
                                    successAlert = (String) session.getAttribute("successAlert");
                                    successMessage = (String) session.getAttribute("successMessage");
                                }
                            %>
                            <div class="alert alert-success" style="display: <%=successAlert%>; text-align: center">
                                <%=successMessage%>
                            </div>
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-md-7">
                            <div id="container" style="min-width: 310px; max-width: available; height: 440px; margin: 0 auto"></div>
                        </div>

                        <div class="col-md-5">
                            <div id="container1" style="min-width: 310px; max-width: available; height: 440px; margin: 0 auto"></div>
                        </div>
                    </div>

                    <%
                        String focus = (String) session.getAttribute("focus");
                    %>
                    <div class="row">
                        <div class="col-md-6">
                            <%
                                String pendingSalesDiv = "panel-default";
                                if (focus.equals("pendingSales")) {
                                    pendingSalesDiv = "panel-red";
                                }
                            %>
                            <div class="panel <%=pendingSalesDiv%>">
                                <div class="panel-heading">
                                    Pending Sales Order
                                </div>
                                <div class="panel-body">
                                    <table class="table table-bordered">
                                        <thead>
                                            <tr>
                                                <th>Sales Order #</th>
                                                <th>Customer Name</th>
                                                <th>Order Amount</th>
                                                <th></th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            <%
                                                ArrayList<SalesOrder> pendingSales = (ArrayList<SalesOrder>) session.getAttribute("pendingSales");
                                                for (int a = 0; a < pendingSales.size(); a++) {
                                            %>
                                            <tr>
                                                <td><%=pendingSales.get(a).getSalesOrderNumber()%></td>
                                                <td><%=pendingSales.get(a).getCustomerName()%></td>
                                                <td><%=pendingSales.get(a).getOrderTotal()%></td>
                                                <td>
                                                    <form action="/TCP/AdminSalesDetail">
                                                        <input type="hidden" name="salesOrderNumber" value="<%=pendingSales.get(a).getSalesOrderNumber()%>">
                                                        <button class="btn btn-success" name="action" value="review_sales">View</button>
                                                    </form>
                                                </td>
                                            </tr>
                                            <%
                                                }
                                            %>
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>

                        <div class="col-md-6">
                            <%
                                String pendingPaymentDiv = "panel-default";
                                if (focus.equals("pendingPayment")) {
                                    pendingPaymentDiv = "panel-red";
                                }
                            %>
                            <div class="panel <%=pendingPaymentDiv%>">
                                <div class="panel-heading">
                                    Payments for Verification
                                </div>
                                <div class="panel-body">
                                    <table class="table table-bordered">
                                        <thead>
                                            <tr>
                                                <th>Sales Order #</th>
                                                <th>Date Paid</th>
                                                <th>Amount Paid</th>
                                                <th></th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            <%
                                                ArrayList<SalesPayment> pendingSalesPayments = (ArrayList<SalesPayment>) session.getAttribute("pendingSalesPayments");
                                                for (int a = 0; a < pendingSalesPayments.size(); a++) {
                                            %>
                                            <tr>
                                                <td><%=pendingSalesPayments.get(a).getSalesOrderNumber()%></td>
                                                <td><%=pendingSalesPayments.get(a).getDatePaid()%></td>
                                                <td>Php <%=pendingSalesPayments.get(a).getAmountPaid()%></td>
                                                <td>
                                                    <a class="btn btn-primary" href="/TCP/AdminPaymentVerification?action=verify&salesOrderNumber=<%=pendingSalesPayments.get(a).getSalesOrderNumber()%>&paymentID=<%=pendingSalesPayments.get(a).getPaymentID()%>">
                                                        View
                                                    </a>
                                                </td>
                                            </tr>
                                            <%
                                                }
                                            %>
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>

                        <div class="col-md-7">
                            <div class="panel panel-default">
                                <div class="panel-heading">
                                    Expected Order Arrivals
                                </div>
                                <div class="panel-body">
                                    <table class="table table-bordered">
                                        <thead>
                                            <tr>
                                                <th>Purchase Order #</th>
                                                <th>Supplier Name</th>
                                                <th>Requested Delivery</th>
                                                <th></th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            <%
                                                ArrayList<PurchaseOrder> purchaseOrders = (ArrayList<PurchaseOrder>) session.getAttribute("purchaseOrders");
                                                for (int a = 0; a < purchaseOrders.size(); a++) {
                                            %>
                                            <tr>
                                                <td><%=purchaseOrders.get(a).getPurchaseOrderNumber()%></td>
                                                <td><%=purchaseOrders.get(a).getSupplierName()%></td>
                                                <td><%=purchaseOrders.get(a).getRequestedDelivery()%></td>
                                                <td>
                                                    <a href="/TCP/AdminProcurementDetail?action=view_detail&purchaseOrderNumber=<%=purchaseOrders.get(a).getPurchaseOrderNumber()%>" class="btn btn-success">View</a>
                                                </td>
                                            </tr>
                                            <%
                                                }
                                            %>
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>

                        <div class="col-md-5">
                            <%
                                String pendingCritical = "panel-default";
                                if (focus.equals("critical")) {
                                    pendingCritical = "panel-red";
                                }
                            %>
                            <div class="panel <%=pendingCritical%>">
                                <div class="panel-heading">
                                    Inventory at Critical Level
                                </div>
                                <div class="panel-body">
                                    <table class="table table-bordered">
                                        <thead>
                                            <tr>
                                                <th>Product ID</th>
                                                <th>Product Name</th>
                                                <th>Stock Count</th>
                                                <th>Critical Level</th>
                                                <th></th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            <%
                                                ArrayList<Product> criticalProducts = (ArrayList<Product>) session.getAttribute("criticalProducts");
                                                for (int a = 0; a < criticalProducts.size(); a++) {
                                            %>
                                            <tr>
                                                <td><%=criticalProducts.get(a).getProductID()%></td>
                                                <td><%=criticalProducts.get(a).getProductName()%></td>
                                                <td><%=criticalProducts.get(a).getStockQuantity()%> <%=criticalProducts.get(a).getUnit()%>(s) left</td>
                                                <td><%=criticalProducts.get(a).getCriticalLevel()%> <%=criticalProducts.get(a).getUnit()%>(s)</td>
                                                <td>
                                                    <a href="/TCP/AdminProcurementCreate?action=criticalProcurePage&productName=<%=criticalProducts.get(a).getProductName()%>" class="btn btn-success">View</a>
                                                </td>
                                            </tr>
                                            <%
                                                }
                                            %>

                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>

                    <%
                        Employee userLogged = (Employee) session.getAttribute("userLogged");
                        if (userLogged.getUserLevel() > 1) {
                    %>
                    <div class="row">
                        <div class="col-md-7">
                            <%
                                String pendingPurchaseDiv = "panel-default";
                                if (focus.equals("pendingPurchase")) {
                                    pendingPurchaseDiv = "panel-red";
                                }
                            %>
                            <div class="panel <%=pendingPurchaseDiv%>">
                                <div class="panel-heading">
                                    Pending Purchase Orders
                                </div>
                                <div class="panel-body">
                                    <table class="table table-bordered">
                                        <thead>
                                            <tr>
                                                <th>Purchase Order #</th>
                                                <th>Supplier Name</th>
                                                <th>Requested Delivery</th>
                                                <th></th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            <%
                                                ArrayList<PurchaseOrder> pendingOrders = (ArrayList<PurchaseOrder>) session.getAttribute("pendingPurchases");
                                                for (int a = 0; a < pendingOrders.size(); a++) {
                                            %>
                                            <tr>
                                                <td><%=pendingOrders.get(a).getPurchaseOrderNumber()%></td>
                                                <td><%=pendingOrders.get(a).getSupplierName()%></td>
                                                <td><%=pendingOrders.get(a).getRequestedDelivery()%></td>
                                                <td>
                                                    <a href="/TCP/AdminProcurementDetail?action=review_sales&purchaseOrderNumber=<%=pendingOrders.get(a).getPurchaseOrderNumber()%>" class="btn btn-success">View</a>
                                                </td>
                                            </tr>
                                            <%
                                                }
                                            %>
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                    <%
                        }
                    %>
                </div>
            </div>
        </div>
    </body>

    <jsp:include page="../scriptplugin.jsp"/>

    <script>
        $(function () {
            var values;
            $.ajax({
                url: "AdminHomepageCharts",
                type: 'POST',
                dataType: 'JSON',
                success: function (data) {
                    values = data;
                },
                async: false
            });

            var monthNames = ["January", "February", "March", "April", "May", "June",
                "July", "August", "September", "October", "November", "December"
            ];

            var d = new Date();

            var monthSales = [];
            for (var a = 0; a < values[0].monthlySales.length; a++) {
                monthSales.push(values[0].monthlySales[a]);
            }

            Highcharts.chart('container', {
                chart: {
                    type: 'column'
                },
                title: {
                    text: 'Monthly Sales For ' + d.getFullYear()
                },
                xAxis: {
                    categories: ['Jan', 'Feb', 'Mar', 'Apr', 'May', 'Jun', 'Jul', 'Aug', 'Sept', 'Oct', 'Nov', 'Dec']
                },
                yAxis: {
                    min: 0,
                    title: {
                        text: 'Total Sales'
                    }
                },
                legend: {
                    reversed: false
                },
                plotOptions: {
                    series: {
                        stacking: 'normal'
                    }
                },
                series: [{
                        name: 'Sales',
                        data: monthSales
                    }]
            });

            var topProducts = [];
            for (var a = 0; a < values[0].topProducts.length; a++) {
                var topProduct = {};
                topProduct['name'] = values[0].topProducts[a].productName;
                topProduct['y'] = values[0].topProducts[a].orderQuantity;
                topProducts.push(topProduct);
            }

            Highcharts.chart('container1', {
                chart: {
                    plotBackgroundColor: null,
                    plotBorderWidth: null,
                    plotShadow: false,
                    type: 'pie'
                },
                title: {
                    text: 'Top 10 Products for ' + monthNames[d.getMonth()]
                },
                tooltip: {
                    pointFormat: '{series.name}: <b>{point.percentage:.1f}%</b>'
                },
                plotOptions: {
                    pie: {
                        allowPointSelect: true,
                        cursor: 'pointer',
                        dataLabels: {
                            enabled: true,
                            format: '<b>{point.name}</b>: {point.y} ({point.percentage:.1f} %)',
                            style: {
                                color: (Highcharts.theme && Highcharts.theme.contrastTextColor) || 'black'
                            }
                        }
                    }
                },
                series: [{
                        name: 'Brands',
                        colorByPoint: true,
                        data: topProducts
                    }]
            });
        });
    </script>
</html>
