<%-- 
    Document   : message_list
    Created on : Nov 3, 2017, 9:24:17 PM
    Author     : RubySenpaii
--%>

<%@page import="object.Message"%>
<%@page import="java.util.ArrayList"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">

        <jsp:include page="../cssplugin.jsp"/>
        <title>TCP - Administrator Template</title>
    </head>
    <body>
        <div id="wrapper">

            <!-- Navigation -->
            <nav class="navbar navbar-default navbar-static-top" role="navigation" style="margin-bottom: 0">
                <jsp:include page="_navbar.jsp"/>
                <jsp:include page="_sidebar.jsp"/>
            </nav>

            <div id="page-wrapper">
                <div class="container-fluid">
                    <div class="row">
                        <div class="col-lg-12">
                            <h2 class="page-header">Message List</h2>
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-md-12">
                            <table width="100%" class="table table-striped table-bordered table-hover" id="dataTables-example">
                                <thead>
                                    <tr>
                                        <th style="width: 15%">Full Name</th>
                                        <th style="width: 15%">Date</th>
                                        <th style="width: 15%">Email</th>
                                        <th style="width: 18%">Contact Number</th>
                                        <th>Message</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <%
                                        ArrayList<Message> messages = (ArrayList<Message>) session.getAttribute("messages");
                                        for (int a = 0; a < messages.size(); a++) {
                                    %>
                                    <tr>
                                        <td><%=messages.get(a).getName()%></td>
                                        <td><%=messages.get(a).getMessageDate()%></td>
                                        <td><%=messages.get(a).getEmail()%></td>
                                        <td><%=messages.get(a).getContactNo()%></td>
                                        <td><%=messages.get(a).getMessage()%></td>
                                    </tr>
                                    <%
                                        }
                                    %>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </body>

    <jsp:include page="../scriptplugin.jsp"/>
</html>
