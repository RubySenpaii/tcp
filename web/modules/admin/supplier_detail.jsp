<%-- 
    Document   : supplier_detail
    Created on : Sep 20, 2017, 11:35:58 PM
    Author     : RubySenpaii
--%>

<%@page import="object.Supplier"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">

        <jsp:include page="../cssplugin.jsp"/>
        <title>TCP - Administrator Supplier Details</title>
    </head>
    <body>
        <div id="wrapper">

            <!-- Navigation -->
            <nav class="navbar navbar-default navbar-static-top" role="navigation" style="margin-bottom: 0">
                <jsp:include page="_navbar.jsp"/>
                <jsp:include page="_sidebar.jsp"/>
            </nav>

            <div id="page-wrapper">
                <div class="container-fluid">
                    <div class="row">
                        <div class="col-lg-12">
                            <%
                                Supplier supplier = (Supplier) session.getAttribute("supplier");
                            %>
                            <h2 class="page-header">
                                Supplier Details 
                                <div class="pull-right">
                                    <%
                                        if (((String) session.getAttribute("enabled")).equals("disabled")) {
                                    %>
                                    <a class="btn btn-primary" href="/TCP/AdminSupplierItem?action=viewItems&supplierID=<%=supplier.getSupplierID()%>">View Supplier Items</a>
                                    <%
                                        }
                                    %>
                                </div>
                            </h2>

                        </div>
                    </div>

                    <div class="row">
                        <div class="col-lg-12">
                            <form>
                                <div class="form-group">
                                    <label>Supplier Name</label>
                                    <input class="form-control" name="supplierName" <%=(String) session.getAttribute("enabled")%> value="<%=supplier.getSupplierName()%>">
                                </div>
                                <div class="form-group">
                                    <label>Address</label>
                                    <input class="form-control" name="address" <%=(String) session.getAttribute("enabled")%> value="<%=supplier.getAddress()%>">
                                </div>
                                <div class="form-group">
                                    <label>Contact Person</label>
                                    <input class="form-control" name="contactPerson" <%=(String) session.getAttribute("enabled")%> value="<%=supplier.getContactPerson()%>">
                                </div>
                                <div class="form-group">
                                    <label>Contact Number</label>
                                    <input class="form-control" name="contactNumber" <%=(String) session.getAttribute("enabled")%> value="<%=supplier.getContactNo()%>">
                                </div>
                                <div class="form-group">
                                    <label>Email</label>
                                    <input class="form-control" name="email" <%=(String) session.getAttribute("enabled")%> value="<%=supplier.getEmail()%>">
                                </div>
                                <table class="table">
                                    <thead>
                                        <tr>
                                            <th style="width: 40%">Characteristic</th>
                                            <th style="width: 10%; text-align: center">1 - Lowest</th>
                                            <th style="width: 10%; text-align: center">2</th>
                                            <th style="width: 10%; text-align: center">3</th>
                                            <th style="width: 10%; text-align: center">4</th>
                                            <th style="width: 10%; text-align: center">5 - Highest</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <tr>
                                            <td>Punctuality</td>
                                            <%
                                                for (int a = 1; a <= 5; a++) {
                                                    String selected = "";
                                                    if (a == supplier.getPunctualityRating()) {
                                                        selected = "checked";
                                                    }
                                            %>
                                            <td style="text-align: center">
                                                <input type="radio" name="punctuality" value='<%=a%>' <%=selected%> required  <%=(String) session.getAttribute("enabled")%>>
                                            </td>
                                            <%
                                                }
                                            %>
                                        </tr>
                                        <tr>
                                            <td>Volume</td>
                                            <%
                                                for (int a = 1; a <= 5; a++) {
                                                    String selected = "";
                                                    if (a == supplier.getVolumeRating()) {
                                                        selected = "checked";
                                                    }
                                            %>
                                            <td style="text-align: center">
                                                <input type="radio" name="volume" value='<%=a%>' <%=selected%> required  <%=(String) session.getAttribute("enabled")%>>
                                            </td>
                                            <%
                                                }
                                            %>
                                        </tr>
                                        <tr>
                                            <td>Customer Friendly</td>
                                            <%
                                                for (int a = 1; a <= 5; a++) {
                                                    String selected = "";
                                                    if (a == supplier.getFriendlyRating()) {
                                                        selected = "checked";
                                                    }
                                            %>
                                            <td style="text-align: center">
                                                <input type="radio" name="customerFriendly" value='<%=a%>' <%=selected%> required  <%=(String) session.getAttribute("enabled")%>>
                                            </td>
                                            <%
                                                }
                                            %>
                                        </tr>
                                        <tr>
                                            <td>Terms of Payment</td>
                                            <%
                                                for (int a = 1; a <= 5; a++) {
                                                    String selected = "";
                                                    if (a == supplier.getPaymentRating()) {
                                                        selected = "checked";
                                                    }
                                            %>
                                            <td style="text-align: center">
                                                <input type="radio" name="paymentTerms" value='<%=a%>' <%=selected%> required  <%=(String) session.getAttribute("enabled")%>>
                                            </td>
                                            <%
                                                }
                                            %>
                                        </tr>
                                    </tbody>
                                </table>
                                <div class="form-group">
                                    <label>Remarks</label>
                                    <textarea class="form-control" name="remarks" <%=(String) session.getAttribute("enabled")%> required><%=supplier.getRemarks()%></textarea>
                                </div>
                                <div class="text-center">
                                    <button class="btn btn-primary" type="submit" name="action" value="back">Back</button>
                                    <%
                                        if (((String) session.getAttribute("enabled")).equals("enabled")) {
                                    %>
                                    <button class="btn btn-success" type="submit" name="action" value="submit">Submit</button>
                                    <%
                                        }
                                    %>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </body>

    <jsp:include page="../scriptplugin.jsp"/>
</html>
