<%-- 
    Document   : supplier_list
    Created on : Sep 20, 2017, 9:22:46 PM
    Author     : RubySenpaii
--%>

<%@page import="extra.Reference"%>
<%@page import="object.Supplier"%>
<%@page import="java.util.ArrayList"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">

        <jsp:include page="../cssplugin.jsp"/>
        <title>TCP - Administrator Supplier List</title>
    </head>
    <body>
        <div id="wrapper">

            <!-- Navigation -->
            <nav class="navbar navbar-default navbar-static-top" role="navigation" style="margin-bottom: 0">
                <jsp:include page="_navbar.jsp"/>
                <jsp:include page="_sidebar.jsp"/>
            </nav>

            <div id="page-wrapper">
                <div class="container-fluid">
                    <div class="row">
                        <div class="col-lg-12">
                            <form action="/TCP/AdminReport" target="_blank">
                                <h2 class="page-header">
                                    Supplier List
                                    <input type="hidden" name="userList" value="supplier">
                                    <button class="btn btn-primary pull-right" name="action" value="createList">Print List</button>
                                </h2>
                            </form>
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-lg-12">
                            <%
                                String dangerAlert = "none";
                                String errorMessage = "";
                                if (session.getAttribute("dangerAlert") != null) {
                                    dangerAlert = (String) session.getAttribute("dangerAlert");
                                    errorMessage = (String) session.getAttribute("errorMessage");
                                }
                            %>
                            <div class="alert alert-danger" style="display: <%=dangerAlert%>; text-align: center">
                                <%=errorMessage%>
                            </div>
                            <%
                                String successAlert = "none";
                                String successMessage = "";
                                if (session.getAttribute("successAlert") != null) {
                                    successAlert = (String) session.getAttribute("successAlert");
                                    successMessage = (String) session.getAttribute("successMessage");
                                }
                            %>
                            <div class="alert alert-success" style="display: <%=successAlert%>; text-align: center">
                                <%=successMessage%>
                            </div>
                            <table width="100%" class="table table-striped table-bordered table-hover" id="dataTables-example">
                                <thead>
                                    <tr>
                                        <th style="width: 12%">Employee ID</th>
                                        <th>Full Name</th>
                                        <th style="width: 20%">Address</th>
                                        <th style="width: 10%">Contact Person</th>
                                        <th style="width: 10%">Contact</th>
                                        <th style="width: 13%">Email</th>
                                        <th style="width: 10%">Is Active?</th>
                                        <th style="width: 15%">Action</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <%
                                        ArrayList<Supplier> suppliers = (ArrayList<Supplier>) session.getAttribute("suppliers");
                                        for (int a = 0; a < suppliers.size(); a++) {
                                            String value = "Inactive";
                                            if (suppliers.get(a).getFlag() == Reference.ACTIVE) {
                                                value = "Active";
                                            }
                                    %>
                                    <tr>
                                        <td><%=suppliers.get(a).getSupplierID()%></td>
                                        <td><%=suppliers.get(a).getSupplierName()%></td>
                                        <td><%=suppliers.get(a).getAddress()%></td>
                                        <td><%=suppliers.get(a).getContactPerson()%></td>
                                        <td><%=suppliers.get(a).getContactNo()%></td>
                                        <td><%=suppliers.get(a).getEmail()%></td>
                                        <td><%=value%></td>
                                        <td>
                                            <form action="/TCP/AdminSupplierDetail">
                                                <input type="hidden" name="supplierID" value="<%=suppliers.get(a).getSupplierID()%>">
                                                <button class="btn btn-success" type="submit" name="action" value="view">View</button>
                                                <button class="btn btn-warning" type="submit" name="action" value="edit">Edit</button>
                                                <button class="btn btn-danger" type="submit" name="action" value="flag">Flag</button>
                                            </form>
                                        </td>
                                    </tr>
                                    <%
                                        }
                                    %>
                                </tbody>
                            </table>
                            <!-- /.table-responsive -->
                        </div>
                    </div>                    
                </div>
            </div>
        </div>
    </body>

    <jsp:include page="../scriptplugin.jsp"/>
</html>