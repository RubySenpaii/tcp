<%-- 
    Document   : login
    Created on : Jul 30, 2017, 5:19:09 PM
    Author     : RubySenpaii
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">

        <jsp:include page="../cssplugin.jsp"/>
        <title>TCP - Administrator Login</title>
    </head>
    <body>
        <div class="container">
            <div class="row">
                <div class="col-md-6 col-md-offset-3">
                    <div class="login-panel panel panel-default">
                        <div class="panel-heading">
                            <!--h3 class="panel-title">TCP - Administrator Login</h3-->
                            <img src="/TCP/assets/img/resource/TCP Logo.png" class="panel-title" width="100%">
                        </div>
                        <div class="panel-body">
                            <form role="form" action="/TCP/AdminLogin">
                                <fieldset>
                                    <%
                                        String dangerAlert = "none";
                                        String errorMessage = "";
                                        if (session.getAttribute("dangerAlert") != null) {
                                            dangerAlert = (String) session.getAttribute("dangerAlert");
                                            errorMessage = (String) session.getAttribute("errorMessage");
                                        }
                                    %>
                                    <div class="alert alert-danger" style="display: <%=dangerAlert%>; text-align: center">
                                        <%=errorMessage%>
                                    </div>
                                    <%
                                        String successAlert = "none";
                                        String successMessage = "";
                                        if (session.getAttribute("successAlert") != null) {
                                            successAlert = (String) session.getAttribute("successAlert");
                                            successMessage = (String) session.getAttribute("successMessage");
                                        }
                                    %>
                                    <div class="alert alert-success" style="display: <%=successAlert%>; text-align: center">
                                        <%=successMessage%>
                                    </div>
                                    <div class="form-group login-form">
                                        <input class="form-control login-input" placeholder="USERNAME" name="username" type="text" autofocus>
                                    </div>
                                    <div class="form-group login-form">
                                        <input class="form-control login-input" placeholder="PASSWORD" name="password" type="password" value="">
                                    </div>

                                    <!-- Change this to a button or input when using this as a form -->
                                    <button type="submit" class="btn btn-lg btn-success btn-block login-form" name="action" value="login">Login</button>

                                    <!-- Button trigger modal -->
                                    <button type="button" class="btn btn-primary btn-lg btn-block login-form" data-toggle="modal" data-target="#myModal">
                                        Forgot Password
                                    </button>

                                </fieldset>
                            </form>
                        </div>
                        <!-- Modal -->
                        <div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
                            <div class="modal-dialog">
                                <div class="modal-content">
                                    <form action="/TCP/AdminForgetPassword">
                                        <div class="modal-header">
                                            <h4 class="modal-title" id="myModalLabel">Forgot Password</h4>
                                        </div>
                                        <div class="modal-body">
                                            <label>Username</label>
                                            <input class="form-control" name="username" placeholder="Please enter registered username." required>
                                            <br>
                                            <label>Birthday</label>
                                            <input class="form-control" name="birthday" placeholder="Please enter birthday." type="date" required>
                                            <br>
                                            <label>Mother's Maiden Name</label>
                                            <input class="form-control" name="motherMaiden" placeholder="Please enter mother's maiden name." required>
                                            <br>
                                            <label>Password</label>
                                            <input class="form-control" id="pw1" name="password" placeholder="Please enter new password." type="password" required>
                                            <br>
                                            <label>Re-enter Password</label>
                                            <input class="form-control" id="pw2" placeholder="Please re-enter new password." type="password" required onchange="verifySecondPw()">
                                        </div>
                                        <div class="modal-footer">
                                            <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                                            <button type="submit" id="forgetBtn" class="btn btn-primary" name="action" value="forgotPassword" disabled>Forgot Password</button>
                                        </div>
                                    </form>
                                </div>
                                <!-- /.modal-content -->
                            </div>
                            <!-- /.modal-dialog -->
                        </div>
                        <!-- /.modal -->
                    </div>
                </div>
            </div>
        </div>
    </body>

    <jsp:include page="../scriptplugin.jsp"/>

    <script>
        function verifySecondPw() {
            var pw1 = document.getElementById("pw1").value;
            var pw2 = document.getElementById("pw2").value;
            if (pw1 === pw2) {
                document.getElementById("forgetBtn").removeAttribute("disabled");
            } else {
                document.getElementById("forgetBtn").setAttribute("disabled", "disabled");
            }
        }
    </script>
</html>
