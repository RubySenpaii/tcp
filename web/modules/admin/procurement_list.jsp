<%-- 
    Document   : list
    Created on : Aug 1, 2017, 2:15:58 PM
    Author     : RubySenpaii
--%>

<%@page import="object.PurchaseOrder"%>
<%@page import="java.util.ArrayList"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">

        <jsp:include page="../cssplugin.jsp"/>
        <title>TCP - Administrator Inventory List</title>
    </head>
    <body>
        <div id="wrapper">

            <!-- Navigation -->
            <nav class="navbar navbar-default navbar-static-top" role="navigation" style="margin-bottom: 0">
                <jsp:include page="_navbar.jsp"/>
                <jsp:include page="_sidebar.jsp"/>
            </nav>

            <div id="page-wrapper">
                <div class="container-fluid">
                    <div class="row">
                        <div class="col-lg-12">
                            <h2 class="page-header">Purchase Order List</h2>
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-lg-12">
                            <%
                                String dangerAlert = "none";
                                String errorMessage = "";
                                if (session.getAttribute("dangerAlert") != null) {
                                    dangerAlert = (String) session.getAttribute("dangerAlert");
                                    errorMessage = (String) session.getAttribute("errorMessage");
                                }
                            %>
                            <div class="alert alert-danger" style="display: <%=dangerAlert%>; text-align: center">
                                <%=errorMessage%>
                            </div>
                            <%
                                String successAlert = "none";
                                String successMessage = "";
                                if (session.getAttribute("successAlert") != null) {
                                    successAlert = (String) session.getAttribute("successAlert");
                                    successMessage = (String) session.getAttribute("successMessage");
                                }
                            %>
                            <div class="alert alert-success" style="display: <%=successAlert%>; text-align: center">
                                <%=successMessage%>
                            </div>
                            <table width="100%" class="table table-striped table-bordered table-hover" id="dataTables-example">
                                <thead>
                                    <tr>
                                        <th style="width: 17%">Purchase Order Number</th>
                                        <th style="width: 10%">Order Date</th>
                                        <th>Supplier</th>
                                        <th>Order Status</th>
                                        <th style="width: 10%">Prepared By</th>
                                        <th style="width: 10%">Approved By</th>
                                        <th style="width: 10%">Total Price</th>
                                        <th style="width: 15%">Remarks</th>
                                        <th style="width: 12%">Action</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <%
                                        ArrayList<PurchaseOrder> purchaseOrders = (ArrayList<PurchaseOrder>) session.getAttribute("purchaseOrders");
                                        for (int a = 0; a < purchaseOrders.size(); a++) {
                                    %>
                                    <tr>
                                        <td><%=purchaseOrders.get(a).getPurchaseOrderNumber()%></td>
                                        <td><%=purchaseOrders.get(a).getOrderDate()%></td>
                                        <td><%=purchaseOrders.get(a).getSupplierName()%></td>
                                        <td><%=purchaseOrders.get(a).getOrderStatus()%></td>
                                        <td><%=purchaseOrders.get(a).getPreparerName()%></td>
                                        <td><%=purchaseOrders.get(a).getApproverName()%></td>
                                        <td>Php <%=purchaseOrders.get(a).getOrderTotal()%></td>
                                        <td>
                                            <%=purchaseOrders.get(a).getRemarks()%>
                                        </td>
                                        <td>
                                            <a href="/TCP/AdminProcurementDetail?action=view_detail&purchaseOrderNumber=<%=purchaseOrders.get(a).getPurchaseOrderNumber()%>" class="btn btn-success">View</a>
                                            <%
                                                if (purchaseOrders.get(a).getDeliveryDate().equals("9999-12-31")) {
                                            %>
                                            <a href="/TCP/AdminProcurementInventory?action=recordItem&purchaseOrderNumber=<%=purchaseOrders.get(a).getPurchaseOrderNumber()%>" class="btn btn-success">Receive</a>
                                            <%
                                                }
                                            %>
                                        </td>
                                    </tr>
                                    <%
                                        }
                                    %>
                                </tbody>
                            </table>
                            <!-- /.table-responsive -->
                        </div>
                    </div>                    
                </div>
            </div>
        </div>
    </body>

    <jsp:include page="../scriptplugin.jsp"/>
</html>
