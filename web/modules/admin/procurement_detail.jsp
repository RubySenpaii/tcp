<%-- 
    Document   : procurement_detail
    Created on : Aug 24, 2017, 12:52:22 AM
    Author     : RubySenpaii
--%>

<%@page import="object.Product"%>
<%@page import="java.util.ArrayList"%>
<%@page import="object.Supplier"%>
<%@page import="object.PurchaseOrder"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">

        <jsp:include page="../cssplugin.jsp"/>
        <title>TCP - Procurement Order Detail</title>
    </head>
    <body>
        <div id="wrapper">

            <!-- Navigation -->
            <nav class="navbar navbar-default navbar-static-top" role="navigation" style="margin-bottom: 0">
                <jsp:include page="_navbar.jsp"/>
                <jsp:include page="_sidebar.jsp"/>
            </nav>

            <div id="page-wrapper">
                <div class="container-fluid">
                    <%
                        PurchaseOrder purchaseOrder = (PurchaseOrder) session.getAttribute("purchaseOrder");
                    %>
                    <div class="row">
                        <div class="col-lg-12">
                            <h2 class="page-header">Purchase Order# <%=purchaseOrder.getPurchaseOrderNumber()%> 
                                <%
                                    if (!purchaseOrder.getDeliveryDate().equals("9999-12-31")) {
                                %>
                                <font class="pull-right">Delivery Date: <%=purchaseOrder.getDeliveryDate()%></font>
                                <%
                                    }
                                %>
                            </h2>
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-md-12">
                            <%
                                String dangerAlert = "none";
                                String errorMessage = "";
                                if (session.getAttribute("dangerAlert") != null) {
                                    dangerAlert = (String) session.getAttribute("dangerAlert");
                                    errorMessage = (String) session.getAttribute("errorMessage");
                                }
                            %>
                            <div class="alert alert-danger" style="display: <%=dangerAlert%>; text-align: center">
                                <%=errorMessage%>
                            </div>
                            <%
                                String successAlert = "none";
                                String successMessage = "";
                                if (session.getAttribute("successAlert") != null) {
                                    successAlert = (String) session.getAttribute("successAlert");
                                    successMessage = (String) session.getAttribute("successMessage");
                                }
                            %>
                            <div class="alert alert-success" style="display: <%=successAlert%>; text-align: center">
                                <%=successMessage%>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <form action="/TCP/AdminProcurementReview">
                            <div class="col-md-12">
                                <div class="col-md-4">
                                    <h4>Supplier Details</h4>
                                    <%
                                        Supplier supplier = (Supplier) session.getAttribute("supplier");
                                    %>
                                    <address>
                                        <strong><%=supplier.getSupplierName()%></strong>
                                        <br><%=supplier.getAddress()%>
                                        <br>
                                        <abbr title="Contact Number">CN:</abbr> <%=supplier.getContactNo()%>
                                    </address>
                                </div>

                                <%
                                    if (!(Boolean) session.getAttribute("isReview")) {
                                %>
                                <div class="col-md-4">
                                    <h4>Billed To:</h4>
                                    <address>
                                        <strong><%=purchaseOrder.getContactPerson()%></strong>
                                        <br><%=purchaseOrder.getAddress()%>
                                        <br>
                                        <abbr title="Contact Number">CN:</abbr> <%=purchaseOrder.getContactNo()%>
                                    </address>
                                </div>
                                <%
                                    }
                                %>
                            </div>
                            <%
                                if ((Boolean) session.getAttribute("isReview")) {
                            %>
                            <div class="form-group">
                                <label>Delivery Address</label>
                                <input class="form-control" name="address" value="<%=purchaseOrder.getAddress()%>">
                            </div>
                            <div class="form-group">
                                <label>Contact Person</label>
                                <input class="form-control" name="contactPerson" value="<%=purchaseOrder.getContactPerson()%>">
                            </div>
                            <div class="form-group">
                                <label>Contact No</label>
                                <input class="form-control" name="contactNo"  value="<%=purchaseOrder.getContactNo()%>">
                            </div>
                            <div class="form-group">
                                <label>Payment</label>
                                <select class="form-control" name="payment">
                                    <option>Cash</option>
                                    <option>30 days</option>
                                    <option>60 days</option>
                                    <option>90 days</option>
                                </select>
                            </div>
                            <div class="form-group">
                                <label>Expected Delivery Date</label>
                                <input class="form-control" name="expectedDelivery" type="date"  value="<%=purchaseOrder.getRequestedDelivery()%>">
                            </div>
                            <%
                                }
                            %>
                            <div class="col-md-12">
                                <table class="table table-bordered table-hover table-review">
                                    <thead>
                                        <tr>
                                            <%
                                                if ((Boolean) session.getAttribute("isReview")) {
                                            %>
                                            <th></th>
                                                <%
                                                    }
                                                %>
                                            <th style="width: 10%">Product Name</th>
                                            <th style="width: 15%">Image</th>
                                            <th style="width: 25%">Description</th>
                                            <th style="width: 10%">Quantity</th>
                                            <th style="width: 10%">Unit Price</th>
                                            <th style="width: 10%">Total Price</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <%
                                            ArrayList<Product> products = (ArrayList<Product>) session.getAttribute("orderItems");
                                            double total = 0;
                                            for (int a = 0; a < products.size(); a++) {
                                                double price = products.get(a).getOrderQuantity() * products.get(a).getUnitPrice();
                                                total += price;
                                        %>
                                        <tr>
                                            <%
                                                if ((Boolean) session.getAttribute("isReview")) {
                                            %>
                                            <td><input type="checkbox" name="index" value="<%=a%>"></td>
                                                <%
                                                    }
                                                %>
                                            <td><%=products.get(a).getProductName()%></td>
                                            <td><img src="assets/img/products/<%=products.get(a).getImage()%>" width="150px" height="150px"></td>
                                            <td><%=products.get(a).getDescription()%></td>
                                            <td><%=products.get(a).getOrderQuantity()%> <%=products.get(a).getUnit()%></td>
                                            <td style="text-align: right">Php <%=products.get(a).getUnitPrice()%></td>
                                            <td style="text-align: right">Php <%=price%></td>
                                        </tr>
                                        <%
                                            }
                                        %>
                                    </tbody>
                                    <tfoot>
                                        <tr>
                                            <th>Total</th>
                                            <th colspan="6" style="text-align: right">Php <%=total%></th>
                                        </tr>
                                    </tfoot>
                                </table>
                            </div>
                            <%
                                if ((Boolean) session.getAttribute("isReview")) {
                            %>
                            <div class="col-md-12">
                                <div class="form-group">
                                    <label>Remarks</label>
                                    <textarea class="form-control" name="remarks" required><%=purchaseOrder.getRemarks()%></textarea>
                                </div>
                            </div>
                            <%
                                }
                            %>
                            <div class="text-center">
                                <input type="hidden" name="purchaseOrderNumber" value="<%=purchaseOrder.getPurchaseOrderNumber()%>">
                                <%
                                    if ((Boolean) session.getAttribute(
                                            "isReview")) {
                                %>
                                <button class="btn btn-success" type="submit" name="action" value="approveOrder">Approve</button>
                                <button class="btn btn-danger" type="submit" name="action" value="rejectOrder">Reject</button>
                                <%
                                } else {
                                %>
                                <button class="btn btn-warning" type="submit" name="action" value="sendEmail">Send Email</button>
                                <%
                                    }
                                %>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </body>

    <jsp:include page="../scriptplugin.jsp"/>
</html>
