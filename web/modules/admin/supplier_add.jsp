<%-- 
    Document   : supplier_add
    Created on : Oct 19, 2017, 12:42:17 AM
    Author     : RubySenpaii
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">

        <jsp:include page="../cssplugin.jsp"/>
        <title>TCP - Administrator Add Supplier</title>
    </head>
    <body>
        <div id="wrapper">

            <!-- Navigation -->
            <nav class="navbar navbar-default navbar-static-top" role="navigation" style="margin-bottom: 0">
                <jsp:include page="_navbar.jsp"/>
                <jsp:include page="_sidebar.jsp"/>
            </nav>

            <div id="page-wrapper">
                <div class="container-fluid">
                    <div class="row">
                        <div class="col-lg-12">
                            <h2 class="page-header">Add Supplier</h2>
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-lg-12">
                            <form action="/TCP/AdminSupplierAdd">
                                <div class="form-group">
                                    <label>Supplier Name</label>
                                    <input class="form-control" name="supplierName" value="" required  pattern="[a-zA-Z0-9]+[a-zA-Z0-9 ]+" title="Alphanumeric with Spaces ONLY">
                                </div>
                                <div class="form-group">
                                    <label>Address</label>
                                    <input class="form-control" name="address" value="" required  pattern="[a-zA-Z0-9,.]+[a-zA-Z0-9,. ]+" title="Alphanumeric with Spaces ONLY">
                                </div>
                                <div class="form-group">
                                    <label>Contact Person</label>
                                    <input class="form-control" name="contactPerson" value="" required  pattern="[a-zA-Z0-9]+[a-zA-Z0-9 ]+" title="Alphanumeric with Spaces ONLY">
                                </div>
                                <div class="form-group">
                                    <label>Contact Number</label>
                                    <input class="form-control" name="contactNumber" value="" required  pattern="[a-zA-Z0-9]+[a-zA-Z0-9 ]+" title="Alphanumeric with Spaces ONLY">
                                </div>
                                <div class="form-group">
                                    <label>Email</label>
                                    <input class="form-control" name="email" value="" required>
                                </div>
                                <table class="table">
                                    <thead>
                                        <tr>
                                            <th style="width: 40%">Characteristic</th>
                                            <th style="width: 10%; text-align: center">1 - Lowest</th>
                                            <th style="width: 10%; text-align: center">2</th>
                                            <th style="width: 10%; text-align: center">3</th>
                                            <th style="width: 10%; text-align: center">4</th>
                                            <th style="width: 10%; text-align: center">5 - Highest</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <tr>
                                            <td>Punctuality</td>
                                            <td style="text-align: center">
                                                <input type="radio" name="punctuality" value='1' required>
                                            </td>
                                            <td style="text-align: center">
                                                <input type="radio" name="punctuality" value='2' required>
                                            </td>
                                            <td style="text-align: center">
                                                <input type="radio" name="punctuality" value='3' required>
                                            </td>
                                            <td style="text-align: center">
                                                <input type="radio" name="punctuality" value='4' required>
                                            </td>
                                            <td style="text-align: center">
                                                <input type="radio" name="punctuality" value='5' required>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>Volume</td>
                                            <td style="text-align: center">
                                                <input type="radio" name="volume" value='1' required>
                                            </td>
                                            <td style="text-align: center">
                                                <input type="radio" name="volume" value='2' required>
                                            </td>
                                            <td style="text-align: center">
                                                <input type="radio" name="volume" value='3' required>
                                            </td>
                                            <td style="text-align: center">
                                                <input type="radio" name="volume" value='4' required>
                                            </td>
                                            <td style="text-align: center">
                                                <input type="radio" name="volume" value='5' required>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>Customer Friendly</td>
                                            <td style="text-align: center">
                                                <input type="radio" name="customerFriendly" value='1' required>
                                            </td>
                                            <td style="text-align: center">
                                                <input type="radio" name="customerFriendly" value='2' required>
                                            </td>
                                            <td style="text-align: center">
                                                <input type="radio" name="customerFriendly" value='3' required>
                                            </td>
                                            <td style="text-align: center">
                                                <input type="radio" name="customerFriendly" value='4' required>
                                            </td>
                                            <td style="text-align: center">
                                                <input type="radio" name="customerFriendly" value='5' required>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>Terms of Payment</td>
                                            <td style="text-align: center">
                                                <input type="radio" name="paymentTerms" value='1' required>
                                            </td>
                                            <td style="text-align: center">
                                                <input type="radio" name="paymentTerms" value='2' required>
                                            </td>
                                            <td style="text-align: center">
                                                <input type="radio" name="paymentTerms" value='3' required>
                                            </td>
                                            <td style="text-align: center">
                                                <input type="radio" name="paymentTerms" value='4' required>
                                            </td>
                                            <td style="text-align: center">
                                                <input type="radio" name="paymentTerms" value='5' required>
                                            </td>
                                        </tr>
                                    </tbody>
                                </table>
                                <div class="form-group">
                                    <label>Remarks</label>
                                    <textarea class="form-control" name="remarks" value="" required  pattern="[a-zA-Z0-9]+[a-zA-Z0-9 ]+" title="Alphanumeric with Spaces ONLY"></textarea>
                                </div>
                                <div class="text-center">
                                    <button class="btn btn-success" type="submit" name="action" value="submit">Submit</button>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </body>

    <jsp:include page="../scriptplugin.jsp"/>
</html>
