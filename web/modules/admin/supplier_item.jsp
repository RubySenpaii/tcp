<%-- 
    Document   : supplier_item
    Created on : Oct 19, 2017, 1:49:20 AM
    Author     : RubySenpaii
--%>

<%@page import="object.SupplierItem"%>
<%@page import="java.util.ArrayList"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">

        <jsp:include page="../cssplugin.jsp"/>
        <title>TCP - Administrator Add Supplier Item</title>
    </head>
    <body>
        <div id="wrapper">

            <!-- Navigation -->
            <nav class="navbar navbar-default navbar-static-top" role="navigation" style="margin-bottom: 0">
                <jsp:include page="_navbar.jsp"/>
                <jsp:include page="_sidebar.jsp"/>
            </nav>

            <div id="page-wrapper">
                <div class="container-fluid">
                    <div class="row">
                        <div class="col-lg-12">
                            <h2 class="page-header">Add Supplier Item</h2>
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-lg-4 col-md-offset-4">
                            <form action="/TCP/AdminSupplierItem">
                                <table class="table table-bordered table-striped">
                                    <thead>
                                        <tr>
                                            <th style="width: 15px">
                                                <input type="checkbox" class="check_all" onclick="select_all()">
                                            </th>
                                            <th>Product Name</th>
                                            <th>Unit Price</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <%
                                            ArrayList<SupplierItem> supplierItems = (ArrayList<SupplierItem>) session.getAttribute("supplierItems");
                                            for (int a = 0; a < supplierItems.size(); a++) {
                                        %>
                                        <tr>
                                            <td><input type="checkbox" class="case"></td>
                                            <td><input type="text" name="productName" value="<%=supplierItems.get(a).getProductName()%>" required></td>
                                            <td><input type="number" name="unitPrice" step="0.01" value="<%=supplierItems.get(a).getUnitPrice()%>" required></td>
                                        </tr>
                                        <%
                                            }
                                        %>
                                    <button type="button" onclick="delete_row()">- Delete</button>
                                    <button type="button" onclick="add_row()">+ Add More</button>
                                    </tbody>
                                </table>
                                
                                <div class="text-center">
                                    <button class="btn btn-success" type="submit" name="action" value="update">Update Supplier Items</button>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <script>
            function delete_row() {
                $('.case:checkbox:checked').parents("tr").remove();
                $('.check_all').prop("checked", false);
                check();
            }
            var i = 2;
            function add_row() {
                count = $('table tr').length;
                var data = "<tr><td><input type='checkbox' class='case'/></td>";
                data += "<td><input type='text' list='products' name='productName'></td>";
                data += "<td><input type='text' name='unitPrice'></td></tr>";
                $('table').append(data);
                i++;
            }

            function select_all() {
                $('input[class=case]:checkbox').each(function () {
                    if ($('input[class=check_all]:checkbox:checked').length === 0) {
                        $(this).prop("checked", false);
                    } else {
                        $(this).prop("checked", true);
                    }
                });
            }

            function check() {
                obj = $('table tr').find('span');
                $.each(obj, function (key, value) {
                    id = value.id;
                    $('#' + id).html(key + 1);
                });
            }
        </script>
    </body>

    <jsp:include page="../scriptplugin.jsp"/>
</html>
