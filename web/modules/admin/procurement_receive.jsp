<%-- 
    Document   : procurement_receive
    Created on : Oct 9, 2017, 1:58:00 AM
    Author     : RubySenpaii
--%>

<%@page import="object.Product"%>
<%@page import="java.util.ArrayList"%>
<%@page import="object.Supplier"%>
<%@page import="object.PurchaseOrder"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">

        <jsp:include page="../cssplugin.jsp"/>
        <title>TCP - Procurement Order Detail</title>
    </head>
    <body>
        <div id="wrapper">

            <!-- Navigation -->
            <nav class="navbar navbar-default navbar-static-top" role="navigation" style="margin-bottom: 0">
                <jsp:include page="_navbar.jsp"/>
                <jsp:include page="_sidebar.jsp"/>
            </nav>

            <div id="page-wrapper">
                <div class="container-fluid">
                    <%
                        PurchaseOrder purchaseOrder = (PurchaseOrder) session.getAttribute("purchaseOrder");
                    %>
                    <div class="row">
                        <div class="col-lg-12">
                            <h2 class="page-header">Purchase Order# <%=purchaseOrder.getPurchaseOrderNumber()%> 
                                <%
                                    if (!purchaseOrder.getDeliveryDate().equals("9999-12-31")) {
                                %>
                                <font class="pull-right">Delivery Date: <%=purchaseOrder.getDeliveryDate()%></font>
                                <%
                                    }
                                %>
                            </h2>
                        </div>
                    </div>

                    <div class="row">
                        <form action="/TCP/AdminProcurementInventory">
                            <div class="col-md-12">
                                <h4>Supplier Details</h4>
                                <%
                                    Supplier supplier = (Supplier) session.getAttribute("supplier");
                                %>
                                <address>
                                    <strong><%=supplier.getSupplierName()%></strong>
                                    <br><%=supplier.getAddress()%>
                                    <br>
                                    <abbr title="Contact Number">CN:</abbr> <%=supplier.getContactNo()%>
                                </address>
                            </div>
                            <div class="col-md-12">
                                <table class="table table-bordered table-hover table-review">
                                    <thead>
                                        <tr>
                                            <th style="width: 10%">Product Name</th>
                                            <th style="width: 15%">Image</th>
                                            <th style="width: 25%">Description</th>
                                            <th style="width: 10%">Quantity</th>
                                            <th style="width: 10%">Quantity Received</th>
                                            <th style="width: 10%">Amount to Receive</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <%
                                            ArrayList<Product> products = (ArrayList<Product>) session.getAttribute("orderItems");
                                            double total = 0;
                                            for (int a = 0; a < products.size(); a++) {
                                                double max = products.get(a).getOrderQuantity() - products.get(a).getReceivedQuantity();
                                                double price = products.get(a).getOrderQuantity() * products.get(a).getUnitPrice();
                                                total += price;
                                        %>
                                        <tr>
                                            <td><%=products.get(a).getProductName()%></td>
                                            <td><img src="assets/img/products/<%=products.get(a).getImage()%>" width="150px" height="150px"></td>
                                            <td><%=products.get(a).getDescription()%></td>
                                            <td><%=products.get(a).getOrderQuantity()%> <%=products.get(a).getUnit()%></td>
                                            <td><%=products.get(a).getReceivedQuantity()%> <%=products.get(a).getUnit()%></td>
                                            <td><input type="number" name="receivedQty" min="0" max="<%=max%>" required> <%=products.get(a).getUnit()%></td>
                                        </tr>
                                        <%
                                            }
                                        %>
                                    </tbody>
                                </table>
                            </div>
                            <div class="text-center">
                                <input type="hidden" name="purchaseOrderNumber" value="<%=purchaseOrder.getPurchaseOrderNumber()%>">
                                <button class="btn btn-success" type="submit" name="action" value="receiveItems">Receive</button>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </body>

    <jsp:include page="../scriptplugin.jsp"/>
</html>
