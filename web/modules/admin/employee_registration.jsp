<%-- 
    Document   : employee_registration
    Created on : Sep 23, 2017, 12:53:52 AM
    Author     : RubySenpaii
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">

        <jsp:include page="../cssplugin.jsp"/>
        <title>TCP - Administrator Employee Registration</title>
    </head>
    <body>
        <div id="wrapper">

            <!-- Navigation -->
            <nav class="navbar navbar-default navbar-static-top" role="navigation" style="margin-bottom: 0">
                <jsp:include page="_navbar.jsp"/>
                <jsp:include page="_sidebar.jsp"/>
            </nav>

            <div id="page-wrapper">
                <div class="container-fluid">
                    <div class="row">
                        <div class="col-lg-12">
                            <h2 class="page-header">Employee Registration</h2>
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-lg-12">
                            <form action="/TCP/AdminEmployeeRegistration">
                                <div class="form-group">
                                    <label>Last Name</label>
                                    <input class="form-control" name="lastName" value="" required pattern="[a-zA-Z0-9]+[a-zA-Z0-9 ]+" title="Alphanumeric with Spaces ONLY">
                                </div>
                                <div class="form-group">
                                    <label>First Name</label>
                                    <input class="form-control" name="firstName" value="" required pattern="[a-zA-Z0-9]+[a-zA-Z0-9 ]+" title="Alphanumeric with Spaces ONLY">
                                </div>
                                <div class="form-group">
                                    <label>Middle Name</label>
                                    <input class="form-control" name="middleName" value="" required pattern="[a-zA-Z0-9]+[a-zA-Z0-9 ]+" title="Alphanumeric with Spaces ONLY">
                                </div>
                                <div class="form-group">
                                    <label>User Level</label>
                                    <select class="form-control" name="userLevel" required>
                                        <option value="1">Super User</option>
                                        <option value="2">Administrator</option>
                                    </select>
                                </div>
                                <div class="form-group">
                                    <label>Position</label>
                                    <select class="form-control" name="position" required>
                                        <option>General Manager</option>
                                        <option>DIY Manager</option>
                                        <option>DIY Team Member</option>
                                        <option>Inventory Checker</option>
                                        <option>Truck Driver</option>
                                    </select>
                                </div>
                                <div class="form-group">
                                    <label>Address</label>
                                    <input class="form-control" name="address" value="" required pattern="[a-zA-Z0-9,.!#%[]()]+[a-zA-Z0-9,.!#%[]() ]+" title="Alphanumeric with Spaces ONLY">
                                </div>
                                <div class="form-group">
                                    <label>Contact Number</label>
                                    <input class="form-control" name="contactNumber" value="" required pattern="[a-zA-Z0-9]+[a-zA-Z0-9 ]+" title="Alphanumeric with Spaces ONLY">
                                </div>
                                <div class="form-group">
                                    <label>Email</label>
                                    <input class="form-control" name="email" value="" required type="email">
                                </div>
                                <div class="form-group">
                                    <label>Mother's Maiden Name</label>
                                    <input class="form-control" name="motherMaiden" value="" required pattern="[a-zA-Z0-9]+[a-zA-Z0-9 ]+" title="Alphanumeric with Spaces ONLY">
                                </div>
                                <div class="form-group">
                                    <label>Birthday</label>
                                    <input class="form-control" name="birthday" type="date" value="" required>
                                </div>
                                <div class="form-group">
                                    <label>Username</label>
                                    <input class="form-control" name="username" value="" required pattern="[a-zA-Z0-9]+[a-zA-Z0-9 ]+" title="Alphanumeric with Spaces ONLY">
                                </div>
                                <div class="form-group">
                                    <label>Password</label>
                                    <input class="form-control" name="password" value="" required pattern="[a-zA-Z0-9]+[a-zA-Z0-9 ]+" title="Alphanumeric with Spaces ONLY">
                                </div>
                                <div class="text-center">
                                    <button class="btn btn-success" type="submit" name="action" value="submit">Submit</button>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </body>

    <jsp:include page="../scriptplugin.jsp"/>
</html>
