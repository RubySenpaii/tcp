<%-- 
    Document   : list
    Created on : Aug 1, 2017, 2:15:58 PM
    Author     : RubySenpaii
--%>

<%@page import="object.Employee"%>
<%@page import="object.Product"%>
<%@page import="java.util.ArrayList"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">

        <jsp:include page="../cssplugin.jsp"/>
        <title>TCP - Administrator Inventory List</title>
    </head>
    <body>
        <div id="wrapper">

            <!-- Navigation -->
            <nav class="navbar navbar-default navbar-static-top" role="navigation" style="margin-bottom: 0">
                <jsp:include page="_navbar.jsp"/>
                <jsp:include page="_sidebar.jsp"/>
            </nav>

            <div id="page-wrapper">
                <div class="container-fluid">
                    <div class="row">
                        <div class="col-lg-12">
                            <form action="/TCP/AdminReport" target="_blank">
                                <h2 class="page-header">
                                    Inventory List As Of <%=(String) session.getAttribute("timeNow")%>
                                    <input type="hidden" name="userList" value="inventory">
                                    <button class="btn btn-primary pull-right" name="action" value="createList">Print List</button>
                                </h2>
                            </form>
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-lg-12">
                            <%
                                String dangerAlert = "none";
                                String errorMessage = "";
                                if (session.getAttribute("dangerAlert") != null) {
                                    dangerAlert = (String) session.getAttribute("dangerAlert");
                                    errorMessage = (String) session.getAttribute("errorMessage");
                                }
                            %>
                            <div class="alert alert-danger" style="display: <%=dangerAlert%>; text-align: center">
                                <%=errorMessage%>
                            </div>
                            <%
                                String successAlert = "none";
                                String successMessage = "";
                                if (session.getAttribute("successAlert") != null) {
                                    successAlert = (String) session.getAttribute("successAlert");
                                    successMessage = (String) session.getAttribute("successMessage");
                                }
                            %>
                            <div class="alert alert-success" style="display: <%=successAlert%>; text-align: center">
                                <%=successMessage%>
                            </div>
                            <table width="100%" class="table table-striped table-bordered table-hover" id="dataTables-example">
                                <thead>
                                    <tr>
                                        <th style="width: 9%">Product ID</th>
                                        <th>Product Name</th>
                                        <th style="width: 9%">Category</th>
                                        <th style="width: 13%">Quantity On Hand</th>
                                        <th style="width: 13%">Quantity For Delivery</th>
                                            <%
                                                Employee userLogged = (Employee) session.getAttribute("userLogged");
                                                if (userLogged.getUserLevel() > 1) {
                                            %>
                                        <th style="width: 10%">Cost Price</th>
                                            <%
                                                }
                                            %>
                                        <th style="width: 10%">Selling Price</th>
                                        <th style="width: 14%">Image</th>
                                        <th style="width: 12%">Action</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <%
                                        ArrayList<Product> products = (ArrayList<Product>) session.getAttribute("products");
                                        for (int a = 0; a < products.size(); a++) {
                                            String buttonName = "Unflag";
                                            if (products.get(a).getFlag() == 1) {
                                                buttonName = "Flag";
                                            }
                                    %>
                                    <tr>
                                        <td><%=products.get(a).getProductID()%></td>
                                        <td><%=products.get(a).getProductName()%></td>
                                        <td><%=products.get(a).getCategory()%></td>
                                        <td><%=products.get(a).getStockQuantity()%> <%=products.get(a).getUnit()%>(s)</td>
                                        <td><%=products.get(a).getDeliveryQuantity()%> <%=products.get(a).getUnit()%>(s)</td>
                                        <%
                                            if (userLogged.getUserLevel() > 1) {
                                        %>
                                        <td>Php <%=products.get(a).getCostPrice()%></td>
                                        <%
                                            }
                                        %>
                                        <td>Php <%=products.get(a).getUnitPrice()%></td>
                                        <td>
                                            <img src="assets/img/products/<%=products.get(a).getImage()%>" width="100%" height="150px">
                                        </td>
                                        <td>
                                            <form action="/TCP/AdminInventoryDetail">
                                                <input type="hidden" name="productID" value="<%=products.get(a).getProductID()%>">
                                                <button class="btn btn-success" type="submit" name="action" value="view">View</button>
                                                <%
                                                    if (products.get(a).getFlag() == 1) {
                                                %>
                                                <button class="btn btn-warning" type="submit" name="action" value="edit">Edit</button>
                                                <%
                                                    }
                                                    if (products.get(a).getStockQuantity() == 0) {
                                                %>
                                                <button class="btn btn-danger" type="submit" name="action" value="flag"><%=buttonName%></button>
                                                <%
                                                    }
                                                %>
                                            </form>
                                        </td>
                                    </tr>
                                    <%
                                        }
                                    %>
                                </tbody>
                            </table>
                            <!-- /.table-responsive -->
                        </div>
                    </div>                    
                </div>
            </div>
        </div>
    </body>

    <jsp:include page="../scriptplugin.jsp"/>
</html>
