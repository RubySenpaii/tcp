<%-- 
    Document   : customer_list
    Created on : Sep 20, 2017, 9:22:34 PM
    Author     : RubySenpaii
--%>

<%@page import="extra.Reference"%>
<%@page import="object.Customer"%>
<%@page import="java.util.ArrayList"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">

        <jsp:include page="../cssplugin.jsp"/>
        <title>TCP - Administrator Customer List</title>
    </head>
    <body>
        <div id="wrapper">

            <!-- Navigation -->
            <nav class="navbar navbar-default navbar-static-top" role="navigation" style="margin-bottom: 0">
                <jsp:include page="_navbar.jsp"/>
                <jsp:include page="_sidebar.jsp"/>
            </nav>

            <div id="page-wrapper">
                <div class="container-fluid">
                    <div class="row">
                        <div class="col-lg-12">
                            <form action="/TCP/AdminReport" target="_blank">
                                <h2 class="page-header">
                                    Customer List
                                    <input type="hidden" name="userList" value="customer">
                                    <button class="btn btn-primary pull-right" name="action" value="createList">Print List</button>
                                </h2>
                            </form>
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-lg-12">
                            <%
                                String dangerAlert = "none";
                                String errorMessage = "";
                                if (session.getAttribute("dangerAlert") != null) {
                                    dangerAlert = (String) session.getAttribute("dangerAlert");
                                    errorMessage = (String) session.getAttribute("errorMessage");
                                }
                            %>
                            <div class="alert alert-danger" style="display: <%=dangerAlert%>; text-align: center">
                                <%=errorMessage%>
                            </div>
                            <%
                                String successAlert = "none";
                                String successMessage = "";
                                if (session.getAttribute("successAlert") != null) {
                                    successAlert = (String) session.getAttribute("successAlert");
                                    successMessage = (String) session.getAttribute("successMessage");
                                }
                            %>
                            <div class="alert alert-success" style="display: <%=successAlert%>; text-align: center">
                                <%=successMessage%>
                            </div>
                            <table width="100%" class="table table-striped table-bordered table-hover" id="dataTables-example">
                                <thead>
                                    <tr>
                                        <th style="width: 12%">Customer ID</th>
                                        <th>Full Name</th>
                                        <th style="width: 20%">Billing Address</th>
                                        <th style="width: 10%">Contact</th>
                                        <th style="width: 13%">Email</th>
                                        <th style="width: 10%">Username</th>
                                        <th style="width: 10%">Is Active?</th>
                                        <th style="width: 15%">Action</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <%
                                        ArrayList<Customer> customers = (ArrayList<Customer>) session.getAttribute("customers");
                                        for (int a = 0; a < customers.size(); a++) {
                                            String value = "Inactive";
                                            if (customers.get(a).getFlag() == Reference.ACTIVE) {
                                                value = "Active";
                                            }
                                    %>
                                    <tr>
                                        <td><%=customers.get(a).getCustomerID()%></td>
                                        <td><%=customers.get(a).getLastName() + ", " + customers.get(a).getFirstName()%></td>
                                        <td><%=customers.get(a).getAddress()%></td>
                                        <td><%=customers.get(a).getContactNo()%></td>
                                        <td><%=customers.get(a).getEmail()%></td>
                                        <td><%=customers.get(a).getUsername()%></td>
                                        <td><%=value%></td>
                                        <td>
                                            <form action="/TCP/AdminCustomerDetail">
                                                <input type="hidden" name="customerID" value="<%=customers.get(a).getCustomerID()%>">
                                                <button class="btn btn-success" type="submit" name="action" value="view">View</button>
                                                <button class="btn btn-warning" type="submit" name="action" value="edit">Edit</button>
                                                <button class="btn btn-danger" type="submit" name="action" value="flag">Flag</button>
                                            </form>

                                            <button class="btn btn-primary btn-lg" data-toggle="modal" data-target="#myModal<%=a%>">
                                                Sales Report List
                                            </button>
                                            <!-- Modal -->
                                            <div class="modal fade" id="myModal<%=a%>" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
                                                <div class="modal-dialog">
                                                    <div class="modal-content">
                                                        <form action="/TCP/AdminReport" target="_blank">
                                                            <div class="modal-header">
                                                                <h4 class="modal-title" id="myModalLabel">Generate Sales Report List For <%=customers.get(a).getLastName()%></h4>
                                                            </div>
                                                            <div class="modal-body">
                                                                <input type="hidden" name="customerID" value="<%=customers.get(a).getCustomerID()%>">
                                                                <br>
                                                                <label>Start Date</label>
                                                                <input type="date" class="form-control" name="startDate">
                                                                <br>
                                                                <label>End Date</label>
                                                                <input type="date" class="form-control" name="endDate">
                                                            </div>
                                                            <div class="modal-footer">
                                                                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                                                                <button type="submit" class="btn btn-primary" name="action" value="createCustomerSales">Generate</button>
                                                            </div>
                                                        </form>
                                                    </div>
                                                    <!-- /.modal-content -->
                                                </div>
                                                <!-- /.modal-dialog -->
                                            </div>
                                            <!-- /.modal -->
                                        </td>
                                    </tr>
                                    <%
                                        }
                                    %>
                                </tbody>
                            </table>
                            <!-- /.table-responsive -->
                        </div>
                    </div>                    
                </div>
            </div>
        </div>
    </body>

    <jsp:include page="../scriptplugin.jsp"/>
</html>