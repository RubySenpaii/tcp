<%-- 
    Document   : sales_detail
    Created on : Aug 24, 2017, 12:52:22 AM
    Author     : RubySenpaii
--%>

<%@page import="object.Product"%>
<%@page import="java.util.ArrayList"%>
<%@page import="object.Customer"%>
<%@page import="object.SalesOrder"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">

        <jsp:include page="../cssplugin.jsp"/>
        <title>TCP - Sales Order Detail</title>
    </head>
    <body>
        <div id="wrapper">

            <!-- Navigation -->
            <nav class="navbar navbar-default navbar-static-top" role="navigation" style="margin-bottom: 0">
                <jsp:include page="_navbar.jsp"/>
                <jsp:include page="_sidebar.jsp"/>
            </nav>

            <div id="page-wrapper">
                <div class="container-fluid">
                    <div class="row">
                        <%
                            SalesOrder salesOrder = (SalesOrder) session.getAttribute("salesOrder");
                        %>
                        <div class="col-lg-12">
                            <h2 class="page-header">Sales Order# <%=salesOrder.getSalesOrderNumber()%></h2>
                        </div>
                    </div>

                    <div class="row">
                        <form action="/TCP/AdminSalesReview">
                            <div class="col-md-12">
                                <h4>Customer Details</h4>
                                <%
                                    Customer customer = (Customer) session.getAttribute("customer");
                                %>
                                <address>
                                    <strong><%=customer.getLastName()%>, <%=customer.getFirstName()%></strong>
                                    <br><%=salesOrder.getAddress()%>
                                    <br>
                                    Payment: <%=salesOrder.getPaymentOption()%>
                                    <br>
                                    <abbr title="Contact Number">CN:</abbr> <%=customer.getContactNo()%>
                                    <br>
                                    <abbr title="Requested Delivery Date">RD:</abbr> <%=salesOrder.getRequestDelivery()%>
                                </address>
                            </div>
                            <div class="col-md-12">
                                <table class="table table-bordered table-hover table-review">
                                    <thead>
                                        <tr>
                                            <%
                                                if ((Boolean) session.getAttribute("isReview")) {
                                            %>
                                            <th></th>
                                                <%
                                                    }
                                                %>
                                            <th style="width: 10%">Product Name</th>
                                            <th style="width: 15%">Image</th>
                                            <th style="width: 25%">Description</th>
                                            <th style="width: 10%">Quantity</th>
                                            <th style="width: 10%">Selling Price</th>
                                            <th style="width: 10%">Total Selling Price</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <%
                                            ArrayList<Product> products = (ArrayList<Product>) session.getAttribute("orderItems");
                                            double total = 0;
                                            for (int a = 0; a < products.size(); a++) {
                                                double price = products.get(a).getOrderQuantity() * products.get(a).getUnitPrice();
                                                total += price;
                                        %>
                                        <tr>
                                            <%
                                                if ((Boolean) session.getAttribute("isReview")) {
                                            %>
                                            <td><input type="checkbox" name="index" value="<%=a%>"></td>
                                                <%
                                                    }
                                                %>
                                            <td><%=products.get(a).getProductName()%></td>
                                            <td><img src="assets/img/products/<%=products.get(a).getImage()%>"></td>
                                            <td><%=products.get(a).getDescription()%></td>
                                            <td><%=products.get(a).getOrderQuantity()%> <%=products.get(a).getUnit()%></td>
                                            <td style="text-align: right">Php <%=products.get(a).getUnitPrice()%></td>
                                            <td style="text-align: right">Php <%=price%></td>
                                        </tr>
                                        <%
                                            }
                                        %>
                                    </tbody>
                                </table>
                            </div>
                            <%
                                if ((Boolean) session.getAttribute(
                                        "isReview")) {
                            %>
                            <div class="col-md-12">
                                <div class="form-group">
                                    <label>Remarks</label>
                                    <textarea class="form-control" name="remarks" required></textarea>
                                </div>
                            </div>
                            <%
                                }
                            %>
                            <div class="text-center">
                                <%
                                    if ((Boolean) session.getAttribute(
                                            "isReview")) {
                                %>
                                <input type="hidden" name="salesOrderNumber" value="<%=salesOrder.getSalesOrderNumber()%>">
                                <button class="btn btn-success" type="submit" name="action" value="approveOrder">Approve</button>
                                <button class="btn btn-danger" type="submit" name="action" value="rejectOrder">Reject</button>
                                <%
                                    }
                                %>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </body>

    <jsp:include page="../scriptplugin.jsp"/>
</html>
