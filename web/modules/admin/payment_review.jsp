<%-- 
    Document   : payment_review
    Created on : Oct 23, 2017, 9:33:40 PM
    Author     : RubySenpaii
--%>

<%@page import="object.SalesPayment"%>
<%@page import="object.TruckReference"%>
<%@page import="object.Employee"%>
<%@page import="object.Product"%>
<%@page import="java.util.ArrayList"%>
<%@page import="object.Customer"%>
<%@page import="object.SalesOrder"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">

        <jsp:include page="../cssplugin.jsp"/>
        <title>TCP - Payment Verification</title>
    </head>
    <body>
        <div id="wrapper">
            <!-- Navigation -->
            <nav class="navbar navbar-default navbar-static-top" role="navigation" style="margin-bottom: 0">
                <jsp:include page="_navbar.jsp"/>
                <jsp:include page="_sidebar.jsp"/>
            </nav>

            <div id="page-wrapper">
                <div class="container-fluid">
                    <div class="row">
                        <%
                            SalesOrder salesOrder = (SalesOrder) session.getAttribute("salesOrder");
                        %>
                        <div class="col-lg-12">
                            <h2 class="page-header">Sales Order# <%=salesOrder.getSalesOrderNumber()%></h2>
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-md-12">
                            <h4>Customer Details</h4>
                            <%
                                Customer customer = (Customer) session.getAttribute("customer");
                            %>
                            <address>
                                <strong><%=customer.getLastName()%>, <%=customer.getFirstName()%></strong>
                                <br><%=salesOrder.getAddress()%>
                                <br>
                                <abbr title="Contact Number">CN:</abbr> <%=customer.getContactNo()%>
                            </address>
                        </div>
                        <div class="col-md-12">
                            <table class="table table-bordered table-hover table-review">
                                <thead>
                                    <tr>
                                        <th style="width: 10%">Product Name</th>
                                        <th style="width: 15%">Image</th>
                                        <th style="width: 25%">Description</th>
                                        <th style="width: 10%">Quantity</th>
                                        <th style="width: 10%">Selling Price</th>
                                        <th style="width: 10%">Total Selling Price</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <%
                                        ArrayList<Product> products = (ArrayList<Product>) session.getAttribute("orderItems");
                                        double total = 0;
                                        for (int a = 0; a < products.size(); a++) {
                                            double price = products.get(a).getOrderQuantity() * products.get(a).getUnitPrice();
                                            total += price;
                                    %>
                                    <tr>
                                        <td><%=products.get(a).getProductName()%></td>
                                        <td><img src="assets/img/products/<%=products.get(a).getImage()%>"></td>
                                        <td><%=products.get(a).getDescription()%></td>
                                        <td><%=products.get(a).getOrderQuantity()%> <%=products.get(a).getUnit()%></td>
                                        <td style="text-align: right">Php <%=products.get(a).getUnitPrice()%></td>
                                        <td style="text-align: right">Php <%=price%></td>
                                    </tr>
                                    <%
                                        }
                                    %>
                                </tbody>
                            </table>
                        </div>
                    </div>

                    <div class="row">
                        <form action="/TCP/AdminPaymentVerification">
                            <div class="col-md-12">
                                <h2>Delivery Assignment</h2>
                            </div>
                            <div class="col-md-12">
                                <%
                                    SalesPayment salesPayment = (SalesPayment) session.getAttribute("salesPayment");
                                %>
                                Sales Order#<%=salesPayment.getSalesOrderNumber()%>
                                <br>
                                Amount Paid: <%=salesPayment.getAmountPaid()%>
                                <br>
                                Date Paid: <%=salesPayment.getDatePaid()%>
                                <br>
                                <img src="assets/img/payments/<%=salesPayment.getProof()%>" width="600px" height="400px">
                                <br><br>
                                <div class="form-group">
                                    <label>Assign Delivery In Charge</label>
                                    <select class="form-control" name="delivery">
                                        <%
                                            ArrayList<Employee> employees = (ArrayList<Employee>) session.getAttribute("employees");
                                            for (int b = 0; b < employees.size(); b++) {
                                        %>
                                        <option value="<%=employees.get(b).getEmployeeID()%>"><%=employees.get(b).getLastName() + ", " + employees.get(b).getFirstName()%></option>
                                        <%
                                            }
                                        %>
                                    </select>
                                </div>
                            </div>
                            <div class="col-md-12">
                                <div class="form-group">
                                    <label>Assign Truck</label>
                                    <select name="truck" required id="plateNumber" onchange="getDeliveryList()">
                                        <option selected disabled></option>
                                        <%
                                            ArrayList<TruckReference> trucks = (ArrayList<TruckReference>) session.getAttribute("trucks");
                                            for (int b = 0; b < trucks.size(); b++) {
                                        %>
                                        <option value="<%=trucks.get(b).getTruckID()%>"><%=trucks.get(b).getPlateNumber()%></option>
                                        <%
                                            }
                                        %>
                                    </select>
                                </div>
                            </div>
                            <table class="table table-bordered">
                                <thead>
                                    <tr>
                                        <th>Sales Order No</th>
                                        <th>Capacity</th>
                                        <th>Location</th>
                                    </tr>
                                </thead>
                                <tbody id="deliveryList">
                                    
                                </tbody>
                            </table>
                            <div class="text-center">
                                <input type="hidden" name="salesOrderNumber" value="<%=salesOrder.getSalesOrderNumber()%>">
                                <input type="hidden" name="paymentID" value="<%=salesPayment.getPaymentID()%>">
                                <button class="btn btn-success" type="submit" name="action" value="approve">Approve</button>
                                <button class="btn btn-danger" type="submit" name="action" value="reject">Reject</button>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </body>

    <jsp:include page="../scriptplugin.jsp"/>

    <script>
        function getDeliveryList() {
            var plateNumber = document.getElementById('plateNumber').value;
            $.ajax({
                url: 'AssignTruckDeliveryList',
                dataType: 'json',
                data: {truckPlateNumber: plateNumber},
                success: function (data) {
                    $('#deliveryList').html('');
                    if (data.suggestions.length === 0) {
                        $('#deliveryList').append('<tr><td colspan="3">No delivery list</td></tr>');
                    }
                    for (var a = 0; a < data.suggestions.length; a++) {
                        $('#deliveryList').append('<tr><td>' + data.suggestions[a].salesOrderNumber + '</td><td>' + data.suggestions[a].quantity + '</td><td>' + data.suggestions[a].address + '</td></tr>');
                    }
                }
            });
        }
    </script>
</html>

