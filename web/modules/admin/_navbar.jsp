<%-- 
    Document   : _navbar
    Created on : Jul 31, 2017, 3:21:28 PM
    Author     : RubySenpaii
--%>


<%@page import="object.Product"%>
<%@page import="object.PurchaseOrder"%>
<%@page import="object.SalesPayment"%>
<%@page import="object.SalesOrder"%>
<%@page import="java.util.ArrayList"%>
<%@page import="object.Employee"%>
<div class="navbar-header">
    <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
        <span class="sr-only">Toggle navigation</span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
    </button>
    <a class="navbar-brand" href="/TCP/AdminHomepage?action=goHome" style="padding: 0">
        <img src="/TCP/assets/img/resource/TCP Logo.png" class="panel-title" width="200px" height="100%">
    </a>
</div>

<ul class="nav navbar-top-links navbar-right">
    <%
        ArrayList<SalesOrder> headerSales = (ArrayList<SalesOrder>) session.getAttribute("pendingSales");
        ArrayList<SalesPayment> headerPayment = (ArrayList<SalesPayment>) session.getAttribute("pendingSalesPayments");
        ArrayList<Product> headerProducts = (ArrayList<Product>) session.getAttribute("criticalProducts");
        ArrayList<PurchaseOrder> headerPurchases = (ArrayList<PurchaseOrder>) session.getAttribute("pendingPurchases");
        int total = headerSales.size() + headerPayment.size() + headerProducts.size() + headerPurchases.size();
        Employee userLogged = (Employee) session.getAttribute("userLogged");
        if (userLogged.getUserLevel() <= 1) {
            total = headerSales.size() + headerPayment.size() + headerProducts.size();
        }  
    %>
    <li class="dropdown">
        <a class="dropdown-toggle" data-toggle="dropdown" href="#">
            <i class="fa fa-bell fa-fw"><span class="badge"><%=total%></span></i> <i class="fa fa-caret-down"></i>
        </a>
        <ul class="dropdown-menu dropdown-alerts">
            <li>
                <a href="/TCP/AdminHomepage?action=homepage&focus=pendingSales">
                    <div>
                        Sales Order Request
                        <span class="pull-right red"><%=headerSales.size()%></span>
                    </div>
                </a>
            </li>
            <li class="divider"></li>
            <li>
                <a href="/TCP/AdminHomepage?action=homepage&focus=pendingPayment">
                    <div>
                        Payments for Approval
                        <span class="pull-right red"><%=headerPayment.size()%></span>
                    </div>
                </a>
            </li>
            <%
                if (userLogged.getUserLevel() > 1) {
            %>
            <li class="divider"></li>
            <li>
                <a href="/TCP/AdminHomepage?action=homepage&focus=pendingPurchase">
                    <div>
                        Purchase Order Request
                        <span class="pull-right red"><%=headerPurchases.size()%></span>
                    </div>
                </a>
            </li>
            <%
                }
            %>
            <li class="divider"></li>
            <li>
                <a href="/TCP/AdminHomepage?action=homepage&focus=critical">
                    <div>
                        Products at Critical Level
                        <span class="pull-right red"><%=headerProducts.size()%></span>
                    </div>
                </a>
            </li>
        </ul>
        <!-- /.dropdown-alerts -->
    </li>

    <li class="dropdown">
        <a class="dropdown-toggle" data-toggle="dropdown" href="#">
            <i class="fa fa-user fa-fw"></i> <%=userLogged.getLastName() + ", " + userLogged.getFirstName()%><i class="fa fa-caret-down"></i>
        </a>
        <ul class="dropdown-menu dropdown-user">
            <li><a href="/TCP/AdminEmployeeDetail?employeeID=<%=userLogged.getEmployeeID()%>&action=view"><i class="fa fa-user fa-fw"></i> User Profile</a>
            </li>
            <li class="divider"></li>
            <li><a href="/TCP/AdminLogout"><i class="fa fa-sign-out fa-fw"></i> Logout</a>
            </li>
        </ul>
        <!-- /.dropdown-user -->
    </li>
</ul>
