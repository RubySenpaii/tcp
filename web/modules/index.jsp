<%-- 
    Document   : index
    Created on : Jul 30, 2017, 5:22:01 PM
    Author     : RubySenpaii
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">

        <jsp:include page="cssplugin.jsp"/>
        <title>TCP</title>
    </head>
    <body>
        <div class="container">
            <div class="row">
                <div class="col-md-8 col-md-offset-2">
                    <div class="login-panel panel panel-default">
                        <div class="panel-heading portal-heading">
                            <h3 class="panel-title portal-title">TCP Enterprise: Portal</h3>
                        </div>
                        <div class="panel-body">
                            <form role="form">
                                <a href="/TCP/modules/admin/login.jsp" class="btn btn-lg btn-primary btn-block">Administrator</a>
                                <a href="/TCP/modules/customer/login.jsp" class="btn btn-lg btn-info btn-block">Customer</a>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </body>
    <jsp:include page="scriptplugin.jsp"/>
</html>
