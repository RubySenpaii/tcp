<%-- 
    Document   : scriptplugin
    Created on : Jul 31, 2017, 4:10:48 PM
    Author     : RubySenpaii
--%>

<!-- jQuery -->
<script src="/TCP/vendor/jquery/jquery.min.js"></script>
<!-- Bootstrap Core JavaScript -->
<script src="/TCP/vendor/bootstrap/js/bootstrap.min.js"></script>
<!-- Metis Menu Plugin JavaScript -->
<script src="/TCP/vendor/metisMenu/metisMenu.min.js"></script>
<!-- DataTables JavaScript -->
<script src="/TCP/vendor/datatables/js/jquery.dataTables.min.js"></script>
<script src="/TCP/vendor/datatables-plugins/dataTables.bootstrap.min.js"></script>
<script src="/TCP/vendor/datatables-responsive/dataTables.responsive.js"></script>
<!-- Custom Theme JavaScript -->
<script src="/TCP/dist/js/sb-admin-2.js"></script>
<script src="/TCP/assets/js/custom.js"></script>
<!-- Highcharts -->
<script src="/TCP/vendor/highcharts/highcharts.js"></script>
<script src="/TCP/vendor/highcharts/modules/drilldown.js"></script>
<script src="/TCP/vendor/highcharts/modules/exporting.js"></script>

<script>
    $(document).ready(function () {
        $('#dataTables-example').DataTable({
            responsive: true,
            "aaSorting": []
        });
    });

    var today = new Date();
    var dd = today.getDate();
    var mm = today.getMonth() + 1; //January is 0!
    var yyyy = today.getFullYear();
    if (dd < 10) {
        dd = '0' + dd
    }
    if (mm < 10) {
        mm = '0' + mm
    }

    today = yyyy + '-' + mm + '-' + dd;
    document.getElementById("datefield").setAttribute("min", today);
</script>