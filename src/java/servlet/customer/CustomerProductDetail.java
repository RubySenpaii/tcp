/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package servlet.customer;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import object.Product;
import servlet.BaseServlet;

/**
 *
 * @author RubySenpaii
 */
public class CustomerProductDetail extends BaseServlet {

    @Override
    protected String servletAction(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        HttpSession session = request.getSession();
        ArrayList<Product> products = (ArrayList<Product>) session.getAttribute("products");
        int selectedProduct = Integer.parseInt(request.getParameter("product"));
        Product product = products.get(selectedProduct);
        session.setAttribute("selectedProduct", product);
        session.setAttribute("show", "nope");
        return "/modules/customer/product_detail.jsp";
    }
}
