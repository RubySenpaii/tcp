/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package servlet.customer;

import dao.SalesDeliveryDAO;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import object.SalesDelivery;
import servlet.BaseServlet;

/**
 *
 * @author RubySenpaii
 */
public class CustomerDeliveryUpdates extends BaseServlet {

    @Override
    protected String servletAction(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        HttpSession session = request.getSession();
        int salesOrderNumber = Integer.parseInt(request.getParameter("salesOrderNumber"));
        ArrayList<SalesDelivery> deliveryUpdates = new SalesDeliveryDAO().getDeliveryUpdatesForSO(salesOrderNumber);
        session.setAttribute("show", "nope");
        session.setAttribute("deliveryUpdates", deliveryUpdates);
        return "/modules/customer/order_delivery.jsp";
    }
}
