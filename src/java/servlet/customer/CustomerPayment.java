/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package servlet.customer;

import dao.SalesOrderDAO;
import dao.SalesPaymentDAO;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.io.PrintWriter;
import java.nio.file.Paths;
import javax.servlet.ServletException;
import javax.servlet.annotation.MultipartConfig;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import javax.servlet.http.Part;
import object.SalesOrder;
import object.SalesPayment;
import servlet.BaseServlet;

/**
 *
 * @author RubySenpaii
 */
@MultipartConfig
public class CustomerPayment extends BaseServlet {

    @Override
    protected String servletAction(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        HttpSession session = request.getSession();
        String action = request.getParameter("action");

        if (action.equals("pay")) {
            int salesOrderNumber = Integer.parseInt(request.getParameter("salesOrderNumber"));
            SalesOrder salesOrder = new SalesOrderDAO().getSalesOrderDetails(salesOrderNumber);
            session.setAttribute("salesOrder", salesOrder);
            session.setAttribute("show", "nope");
            return "/modules/customer/order_payment.jsp";
        } else if (action.equals("submitPayment")) {
            int salesOrderNumber = Integer.parseInt(request.getParameter("salesOrderNumber"));
            double amountPaid = Double.parseDouble(request.getParameter("amountPaid"));
            String datePaid = request.getParameter("datePaid");
            datePaid = datePaid.replace('T', ' ');
            Part file = request.getPart("image");

            SalesPayment salesPayment = new SalesPayment();
            salesPayment.setPaymentID(new SalesPaymentDAO().getListOfSalesPayments().size() + 1);
            salesPayment.setAmountPaid(amountPaid);
            salesPayment.setApprovedBy(-1);
            salesPayment.setDatePaid(datePaid);
            String dateTime = salesPayment.getDatePaid().replace(" ", "");
            dateTime = dateTime.replace(":", "");
            salesPayment.setProof(salesOrderNumber + dateTime + ".jpg");
            salesPayment.setSalesOrderNumber(salesOrderNumber);

            boolean success = new SalesPaymentDAO().addSalesPayment(salesPayment);
            if (success) {
                System.out.println(salesPayment.getSalesOrderNumber() + " partial payment added...");

                System.out.println("uploading image to server...");
                String appPath = getServletContext().getRealPath("/assets/img/payments");
                String savePath = appPath;
                File fileDir = new File(savePath);
                if (!fileDir.exists()) {
                    System.out.println(savePath + " created: " + fileDir.mkdir());
                }
                InputStream fileContent = null;
                OutputStream out = null;
                out = new FileOutputStream(new File(savePath + File.separator + salesOrderNumber + dateTime + ".jpg"));
                fileContent = file.getInputStream();
                int read = 0;
                final byte[] bytes = new byte[1024];
                while ((read = fileContent.read(bytes)) != -1) {
                    out.write(bytes, 0, read);
                }
                out.flush();
                out.close();
                System.out.println("image upload success...");
                session.setAttribute("successAlert", "block");
                session.setAttribute("successMessage", "Payment submitted. Wait for approval.");
                session.setAttribute("show", "show");
                return "/CustomerHomepage?action=homepage";
            } else {
                System.out.println("upload failed...");
                session.setAttribute("dangerAlert", "block");
                session.setAttribute("errorMessage", "Payment NOT submitted. Please retry.");
                session.setAttribute("show", "show");
                return "/CustomerHomepage?action=homepage";
            }
        } else {
            session.setAttribute("dangerAlert", "block");
            session.setAttribute("errorMessage", "Payment NOT submitted. Please retry.");
            session.setAttribute("show", "show");
            return "/CustomerHomepage";
        }
    }
}
