/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package servlet.customer;

import dao.SalesDeliveryDAO;
import dao.SalesOrderDAO;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import object.Customer;
import object.SalesDelivery;
import object.SalesOrder;
import servlet.BaseServlet;

/**
 *
 * @author RubySenpaii
 */
public class CustomerOrders extends BaseServlet {

    @Override
    protected String servletAction(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        HttpSession session = request.getSession();
        Customer userLogged = (Customer) session.getAttribute("userLogged");
        ArrayList<SalesOrder> salesOrders = new SalesOrderDAO().getListOfSalesOrdersWithFrom(userLogged.getCustomerID());
        for (int a = 0; a < salesOrders.size(); a++) {
            if (salesOrders.get(a).getOrderStatus().equals("Completed")) {
                SalesDelivery salesDelivery = new SalesDeliveryDAO().getCurrentSalesDeliveryStatus(salesOrders.get(a).getSalesOrderNumber());
                salesOrders.get(a).setRemarks(salesDelivery.getRemarks());
            }
        }
        session.setAttribute("show", "nope");
        session.setAttribute("orders", salesOrders);
        return "/modules/customer/order_list.jsp";
    }
}
