/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package servlet.customer;

import java.io.IOException;
import java.io.PrintWriter;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import servlet.BaseServlet;

/**
 *
 * @author RubySenpaii
 */
public class CustomerOrderReview extends BaseServlet {

    @Override
    protected String servletAction(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        HttpSession session = request.getSession();
        String address = request.getParameter("deliveryAddress");
        String contactPerson = request.getParameter("contactPerson");
        String contactNo = request.getParameter("contact_no");
        String requestDelivery = request.getParameter("request_delivery");
        String paymentOption = request.getParameter("paymentOption");
        session.setAttribute("show", "nope");
        session.setAttribute("billingAddress", address);
        session.setAttribute("billingContactPerson", contactPerson);
        session.setAttribute("billingContactNo", contactNo);
        session.setAttribute("billingDelivery", requestDelivery);
        session.setAttribute("paymentOption", paymentOption);
        return "/modules/customer/order_review.jsp";
    }
}
