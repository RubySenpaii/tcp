/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package servlet.customer;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import object.Product;
import servlet.BaseServlet;

/**
 *
 * @author RubySenpaii
 */
public class CustomerViewCart extends BaseServlet {

    @Override
    protected String servletAction(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        HttpSession session = request.getSession();
        String action = request.getParameter("action");
        if (action.equals("view_cart")) {
            System.out.println("viewing cart...");
            session.setAttribute("show", "nope");
            return "/modules/customer/cart.jsp";
        } else if (action.equals("update_cart")) {
            String[] quantities = request.getParameterValues("quantity");
            ArrayList<Product> products = (ArrayList<Product>) session.getAttribute("cart");
            for (int a = 0; a < products.size(); a++) {
                products.get(a).setOrderQuantity(Integer.parseInt(quantities[a]));
            }

            for (int a = 0; a < products.size(); a++) {
                if (products.get(a).getOrderQuantity() == 0) {
                    products.remove(a);
                }
            }
            session.setAttribute("show", "nope");
            session.setAttribute("cart", products);
            return "/modules/customer/cart.jsp";
        } else {
            session.setAttribute("show", "nope");
            return "/modules/customer/cart.jsp";
        }
    }
}
