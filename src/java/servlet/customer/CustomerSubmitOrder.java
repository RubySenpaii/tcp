/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package servlet.customer;

import dao.SalesOrderDAO;
import dao.SalesOrderItemDAO;
import extra.Reference;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Calendar;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import object.Customer;
import object.Product;
import object.SalesOrder;
import object.SalesOrderItem;
import servlet.BaseServlet;

/**
 *
 * @author RubySenpaii
 */
public class CustomerSubmitOrder extends BaseServlet {

    @Override
    protected String servletAction(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        HttpSession session = request.getSession();
        Customer userLogged = (Customer) session.getAttribute("userLogged");

        SalesOrder salesOrder = new SalesOrder();
        salesOrder.setAddress((String) session.getAttribute("billingAddress"));
        salesOrder.setApprovedBy(Reference.UNASSIGNED);
        salesOrder.setContactNo((String) session.getAttribute("billingContactNo"));
        salesOrder.setContactPerson((String) session.getAttribute("billingContactPerson"));
        salesOrder.setCustomerID(userLogged.getCustomerID());
        salesOrder.setDeliveredBy(Reference.UNASSIGNED);
        salesOrder.setOrderDate(Reference.DATABASE_DATETIME_FORMAT.format(Calendar.getInstance().getTime()));
        salesOrder.setOrderStatus("Pending");
        salesOrder.setRemarks("");
        salesOrder.setRequestDelivery((String) session.getAttribute("billingDelivery"));
        salesOrder.setAssignedTruck(-1);
        salesOrder.setSalesOrderNumber(new SalesOrderDAO().getListOfSalesOrders().size() + 1);
        salesOrder.setReceivedDate("9999-12-31");
        salesOrder.setReceivedBy("N/A");
        salesOrder.setPaymentOption((String) session.getAttribute("paymentOption"));

        ArrayList<SalesOrderItem> salesOrderItems = new ArrayList<>();
        double orderTotal = 0;
        ArrayList<Product> cart = (ArrayList<Product>) session.getAttribute("cart");
        for (int a = 0; a < cart.size(); a++) {
            SalesOrderItem salesOrderItem = new SalesOrderItem();
            salesOrderItem.setProductID(cart.get(a).getProductID());
            salesOrderItem.setQuantity(cart.get(a).getOrderQuantity());
            salesOrderItem.setSalesOrderNumber(salesOrder.getSalesOrderNumber());
            salesOrderItem.setUnitPrice(cart.get(a).getUnitPrice());
            orderTotal += (cart.get(a).getOrderQuantity() * cart.get(a).getUnitPrice());
            salesOrderItems.add(salesOrderItem);
        }
        salesOrder.setOrderTotal(orderTotal);

        boolean isRecorded = false;
        if (orderTotal != 0) {
            isRecorded = new SalesOrderDAO().addSalesOrder(salesOrder);
        }

        session.setAttribute("show", "show");
        if (isRecorded) {
            System.out.println("sales order recorded...");
            if (new SalesOrderItemDAO().addSalesOrderItems(salesOrderItems)) {
                System.out.println("sales order items recorded...");
                cart.clear();
                session.setAttribute("cart", cart);
                session.setAttribute("show", "show");
                session.setAttribute("successAlert", "block");
                session.setAttribute("successMessage", "Sales Order Request sent. Subject for approval.");
                return "/CustomerOrders";
            } else {
                System.out.println("deleting sales order recorded due to items not fully recorded...");
                System.out.println("directing back to cart...");
                session.setAttribute("show", "show");
                session.setAttribute("dangerAlert", "block");
                session.setAttribute("errorMessage", "Error recording sales order items.");
                return "/TCP/modules/customer/cart.jsp";
            }
        } else {
            return (String) session.getAttribute("prevPath");
        }
    }
}
