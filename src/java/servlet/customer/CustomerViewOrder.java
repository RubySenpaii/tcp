/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package servlet.customer;

import dao.ProductDAO;
import dao.SalesOrderDAO;
import dao.SalesOrderItemDAO;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import object.Product;
import object.SalesOrder;
import object.SalesOrderItem;
import servlet.BaseServlet;

/**
 *
 * @author RubySenpaii
 */
public class CustomerViewOrder extends BaseServlet {

    @Override
    protected String servletAction(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        HttpSession session = request.getSession();
        int salesOrderNumber = Integer.parseInt(request.getParameter("salesOrderNumber"));
        SalesOrder salesOrder = new SalesOrderDAO().getSalesOrderDetails(salesOrderNumber);
        ArrayList<Product> orderItems = new ArrayList<>();
        ArrayList<SalesOrderItem> salesOrderItems = new SalesOrderItemDAO().getListOfSalesOrderItems(salesOrderNumber);
        for (int a = 0; a < salesOrderItems.size(); a++) {
            Product item = new ProductDAO().getProductDetail(salesOrderItems.get(a).getProductID());
            item.setOrderQuantity(salesOrderItems.get(a).getQuantity());
            item.setUnitPrice(salesOrderItems.get(a).getUnitPrice());
            orderItems.add(item);
        }
        
        session.setAttribute("order", salesOrder);
        session.setAttribute("orderItems", orderItems);
        session.setAttribute("show", "nope");
        return "/modules/customer/order_detail.jsp";
    }
}
