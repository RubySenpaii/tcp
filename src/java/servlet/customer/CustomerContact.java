/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package servlet.customer;

import dao.MessageDAO;
import extra.Reference;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.Calendar;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import object.Message;
import servlet.BaseServlet;

/**
 *
 * @author RubySenpaii
 */
public class CustomerContact extends BaseServlet {

    @Override
    protected String servletAction(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        HttpSession session = request.getSession();
        String action = request.getParameter("action");
        if (action.equals("submit")) {
            String fullName = request.getParameter("fullName");
            String email = request.getParameter("email");
            String contactNumber = request.getParameter("contactNumber");
            String message = request.getParameter("message");

            MessageDAO msgdao = new MessageDAO();
            Message custMessage = new Message();
            custMessage.setContactNo(contactNumber);
            custMessage.setEmail(email);
            custMessage.setMessage(message);
            custMessage.setName(fullName);
            custMessage.setMessageDate(Reference.DATABASE_DATE_FORMAT.format(Calendar.getInstance().getTime()));
            custMessage.setMessageID(msgdao.getMessages().size() + 1);

            boolean recorded = msgdao.addMessage(custMessage);
            System.out.println("messages sent: " + recorded);

            if (recorded) {
                session.setAttribute("show", "show");
                session.setAttribute("successAlert", "block");
                session.setAttribute("successMessage", "message successfully sent");
            } else {
                session.setAttribute("show", "show");
                session.setAttribute("dangerAlert", "block");
                session.setAttribute("errorMessage", "messsage sending failed");
            }
        }

        return "/modules/customer/contact_us.jsp";
    }
}
