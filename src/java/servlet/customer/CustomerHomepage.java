/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package servlet.customer;

import dao.ProductDAO;
import java.io.IOException;
import java.util.ArrayList;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import object.Product;
import servlet.BaseServlet;

/**
 *
 * @author RubySenpaii
 */
public class CustomerHomepage extends BaseServlet {

    @Override
    protected String servletAction(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        HttpSession session = request.getSession();
        String action = request.getParameter("action");

        ArrayList<Product> products = new ProductDAO().getListOfActiveProducts();
        if (action.equals("filter-category")) {
            String category = request.getParameter("category");
            products = new ProductDAO().getListOfProductsForCategory(category);
        } else if (action.equals("filter-keyword")) {
            String keyword = request.getParameter("keyword");
            products = new ProductDAO().getListOfProductsWithKeyword(keyword);
        } else {

        }
        ArrayList<String> categories = new ProductDAO().getProductCategories();
        session.setAttribute("show", "nope");
        session.setAttribute("categories", categories);
        session.setAttribute("products", products);
        return "/modules/customer/homepage.jsp";
    }
}
