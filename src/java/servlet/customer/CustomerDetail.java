/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package servlet.customer;

import dao.CustomerDAO;
import java.io.IOException;
import java.io.PrintWriter;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import object.Customer;
import servlet.BaseServlet;

/**
 *
 * @author RubySenpaii
 */
public class CustomerDetail extends BaseServlet {

    @Override
    protected String servletAction(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        HttpSession session = request.getSession();
        String action = request.getParameter("action");

        if (action.equals("edit") || action.equals("view")) {
            int customerID = Integer.parseInt(request.getParameter("customerID"));
            Customer customer = new CustomerDAO().getCustomerInfo(customerID);
            String enabled = "enabled";

            session.setAttribute("customer", customer);
            session.setAttribute("enabled", enabled);
            session.setAttribute("show", "nope");
            return "/modules/customer/user_detail.jsp";
        } else {
            String address = request.getParameter("address");
            String contactNo = request.getParameter("contactNumber");

            Customer customer = (Customer) session.getAttribute("customer");
            customer.setAddress(address);
            customer.setContactNo(contactNo);
            boolean updatedCustInfo = new CustomerDAO().updateCustomerInfo(customer);
            System.out.println(customer.getLastName() + ", " + customer.getFirstName() + " info updated: " + updatedCustInfo);
            session.setAttribute("show", "nope");

            return "/CustomerDetail?action=view&customerID=" + customer.getCustomerID();
        }
    }
}
