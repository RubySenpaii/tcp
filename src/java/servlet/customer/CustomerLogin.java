/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package servlet.customer;

import dao.CustomerDAO;
import java.io.IOException;
import java.util.ArrayList;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import object.Customer;
import object.Product;
import servlet.BaseServlet;

/**
 *
 * @author RubySenpaii
 */
public class CustomerLogin extends BaseServlet {

    @Override
    protected String servletAction(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        HttpSession session = request.getSession();
        String action = request.getParameter("action");

        if (action.equals("login")) {
            return customerLogin(request, response);
        } else if (action.equals("new_customer")) {
            return "/modules/customer/registration.jsp";
        } else if (action.equals("guest")) {
            session.setAttribute("userType", "guest");
            session.setAttribute("userLogged", "Guest");
            session.setAttribute("show", "nope");
            return "/CustomerHomepage?action=homepage";
        } else {
            session.setAttribute("show", "nope");
            return "/modules/customer/login.jsp";
        }
    }

    private String customerLogin(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        HttpSession session = request.getSession();
        String path = "/modules/customer/login.jsp";

        String username = request.getParameter("username");
        String password = request.getParameter("password");

        boolean doesCustomerExist = new CustomerDAO().doesCustomerExist(username, password);
        System.out.println(username + " exist: " + doesCustomerExist);
        if (doesCustomerExist) {
            Customer customer = new CustomerDAO().getCustomerInfo(username);
            ArrayList<Product> cart = new ArrayList<>();
            System.out.println("user info: " + customer.getUsername());
            session.setAttribute("show", "show");
            session.setAttribute("userType", "customer");
            session.setAttribute("userLogged", customer);
            session.setAttribute("cart", cart);
            path = "/CustomerHomepage?action=homepage";
        } else {
            session.setAttribute("show", "show");
            session.setAttribute("dangerAlert", "block");
            session.setAttribute("errorMessage", "username/password does not exist. please try again");
            System.out.println("no user found...");
        }

        return path;
    }
}
