/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package servlet.customer;

import dao.CustomerDAO;
import java.io.IOException;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import object.Customer;
import servlet.BaseServlet;

/**
 *
 * @author RubySenpaii
 */
public class CustomerRegistration extends BaseServlet {

    @Override
    protected String servletAction(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        HttpSession session = request.getSession();
        Customer customer = new Customer();
        customer.setAddress(request.getParameter("address"));
        customer.setContactNo(request.getParameter("contact_no"));
        customer.setEmail(request.getParameter("email"));
        customer.setFirstName(request.getParameter("first_name"));
        customer.setFlag(1);
        customer.setLastName(request.getParameter("last_name"));
        customer.setMiddleName(request.getParameter("middle_name"));
        customer.setPassword(request.getParameter("password"));
        customer.setUsername(request.getParameter("username"));
        customer.setBirthday(request.getParameter("birthday"));
        customer.setMotherMaiden(request.getParameter("motherMaiden"));
        customer.setCustomerID(new CustomerDAO().getListOfCustomers().size() + 1);

        boolean isFullName = new CustomerDAO().checkCustomerName(customer.getLastName(), customer.getFirstName(), customer.getMiddleName());
        boolean isContactNo = new CustomerDAO().checkCustomerContactNo(customer.getContactNo());
        boolean isEmail = new CustomerDAO().checkCustomerEmail(customer.getEmail());
        boolean isUsername = new CustomerDAO().checkCustomerUsername(customer.getUsername());

        String errorMessage = "";
        if (!isFullName) {
            errorMessage += customer.getLastName() + ", " + customer.getFirstName() + " " + customer.getMiddleName() + " is already registered.<br>";
        }
        if (!isContactNo) {
            errorMessage += customer.getContactNo() + " already registered.<br>";
        }
        if (!isEmail) {
            errorMessage += customer.getEmail() + " already registrered.<br>";
        }
        if (!isUsername) {
            errorMessage += customer.getUsername() + " already taken.<br>";
        }

        session.setAttribute("show", "show");
        if (isFullName && isContactNo && isEmail && isUsername) {
            boolean didCustomerRegister = new CustomerDAO().registerCustomer(customer);
            if (didCustomerRegister) {
                System.out.println(customer.getUsername() + " successfully created...");
                session.setAttribute("successAlert", "block");
                session.setAttribute("successMessage", "Registration Successful");
                return "/modules/customer/login.jsp";
            }
        } else {
            session.setAttribute("dangerAlert", "block");
            session.setAttribute("errorMessage", errorMessage);
        }
        System.out.println(customer.getUsername() + " not created...");
        return "/modules/customer/registration.jsp";
    }

}
