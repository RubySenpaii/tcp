/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package servlet.customer;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import object.Product;
import servlet.BaseServlet;

/**
 *
 * @author RubySenpaii
 */
public class CustomerAddCart extends BaseServlet {

    @Override
    protected String servletAction(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        HttpSession session = request.getSession();
        Product product = (Product) session.getAttribute("selectedProduct");
        int orderQty = Integer.parseInt(request.getParameter("orderQuantity"));

        ArrayList<Product> cart = (ArrayList<Product>) session.getAttribute("cart");
        boolean addProduct = true;
        for (int a = 0; a < cart.size(); a++) {
            if (cart.get(a).getProductID() == product.getProductID()) {
                addProduct = false;
                cart.get(a).setOrderQuantity(cart.get(a).getOrderQuantity() + orderQty);
                break;
            }
        }
        if (addProduct) {
            Product temp = product;
            temp.setOrderQuantity(orderQty);
            System.out.println(temp.getProductName() + " added to cart...");
            cart.add(temp);
        }
        session.setAttribute("show", "nope");
        return "/CustomerHomepage?action=homepage";
    }
}
