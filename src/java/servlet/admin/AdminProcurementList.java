/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package servlet.admin;

import dao.EmployeeDAO;
import dao.PurchaseOrderDAO;
import dao.SupplierDAO;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import object.Employee;
import object.PurchaseOrder;
import object.Supplier;
import servlet.BaseServlet;

/**
 *
 * @author RubySenpaii
 */
public class AdminProcurementList extends BaseServlet {

    @Override
    protected String servletAction(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        HttpSession session = request.getSession();
        ArrayList<PurchaseOrder> purchaseOrders = new PurchaseOrderDAO().getListOfApprovedPurchaseOrders();
        for (int a = 0; a < purchaseOrders.size(); a++) {
            Supplier supplierInfo = new SupplierDAO().getSupplierInfo(purchaseOrders.get(a).getSupplierID());
            Employee preparedByInfo = new EmployeeDAO().getEmployeeInfo(purchaseOrders.get(a).getPreparedBy());
            purchaseOrders.get(a).setSupplierName(supplierInfo.getSupplierName());
            purchaseOrders.get(a).setPreparerName(preparedByInfo.getLastName() + ", " + preparedByInfo.getFirstName());
        }
        session.setAttribute("show", "nope");
        session.setAttribute("purchaseOrders", purchaseOrders);
        return "/modules/admin/procurement_list.jsp";
    }
}
