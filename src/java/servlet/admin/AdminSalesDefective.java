/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package servlet.admin;

import dao.InventoryDAO;
import dao.SalesOrderDAO;
import dao.SalesOrderItemDAO;
import extra.Reference;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.Calendar;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import object.Inventory;
import object.SalesOrder;
import object.SalesOrderItem;
import servlet.BaseServlet;

/**
 *
 * @author RubySenpaii
 */
public class AdminSalesDefective extends BaseServlet {

    @Override
    protected String servletAction(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        HttpSession session = request.getSession();
        String action = request.getParameter("action");
        
        if (action.equals("goPage")) {
            ArrayList<SalesOrder> salesOrders = new SalesOrderDAO().getListOfSalesOrdersWithStatus("Completed");
            ArrayList<SalesOrderItem> salesItems = new ArrayList<>();
            
            session.setAttribute("salesOrders", salesOrders);
            session.setAttribute("salesItems", salesItems);
            session.setAttribute("show", "nope");
            return "/modules/admin/sales_return.jsp";
        } else if (action.equals("view")) {
            int salesOrderNumber = Integer.parseInt(request.getParameter("salesOrderNumber"));
            SalesOrder salesOrder = new SalesOrderDAO().getSalesOrderDetails(salesOrderNumber);
            ArrayList<SalesOrderItem> salesItems = new SalesOrderItemDAO().getListOfSalesOrderItems(salesOrderNumber);
            
            session.setAttribute("salesOrder", salesOrder);
            session.setAttribute("salesItems", salesItems);
            session.setAttribute("show", "nope");
            return "/modules/admin/sales_return.jsp";
        } else {
            //recording of defective sales items
            String[] defective = request.getParameterValues("defective");
            String remarks = request.getParameter("remarks");
            ArrayList<SalesOrderItem> salesItems = (ArrayList<SalesOrderItem>) session.getAttribute("salesItems");
            for (int a = 0; a < defective.length; a++) {
                int defectiveQty = Integer.parseInt(defective[a]);
                if (defectiveQty > 0) {
                    Inventory inventory = new Inventory();
                    inventory.setDate(Reference.DATABASE_DATETIME_FORMAT.format(Calendar.getInstance().getTime()));
                    inventory.setDefect(defectiveQty);
                    inventory.setIn(0);
                    inventory.setOut(-defectiveQty);
                    inventory.setProductID(salesItems.get(a).getProductID());
                    inventory.setReference("DefectiveSales-" + salesItems.get(a).getSalesOrderNumber() + " Remarks: " + remarks);
                    inventory.setTotal(new InventoryDAO().getLatestCountForProduct(salesItems.get(a).getProductID()).getTotal() - defectiveQty);
                    boolean isRecorded = new InventoryDAO().recordInventory(inventory);
                    System.out.println("inventory updated " + isRecorded);
                }
            }
            session.setAttribute("show", "nope");
            return "/AdminSalesList?action=approved_sales";
        }
    }
}
