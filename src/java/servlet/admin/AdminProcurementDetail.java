/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package servlet.admin;

import dao.ProductDAO;
import dao.PurchaseOrderDAO;
import dao.PurchaseOrderItemDAO;
import dao.SupplierDAO;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import object.Product;
import object.PurchaseOrder;
import object.PurchaseOrderItem;
import object.Supplier;
import servlet.BaseServlet;

/**
 *
 * @author RubySenpaii
 */
public class AdminProcurementDetail extends BaseServlet {

    @Override
    protected String servletAction(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        //retrieve info from db to be viewed on purchase order details
        HttpSession session = request.getSession();
        String action = request.getParameter("action");
        int purchaseOrderNumber = Integer.parseInt(request.getParameter("purchaseOrderNumber"));
        PurchaseOrder purchaseOrder = new PurchaseOrderDAO().getPurchaseOrderDetail(purchaseOrderNumber);
        ArrayList<PurchaseOrderItem> purchaseOrderItems = new PurchaseOrderItemDAO().getListOfPurchaseOrderItems(purchaseOrderNumber);
        Supplier supplierInfo = new SupplierDAO().getSupplierInfo(purchaseOrder.getSupplierID());
        ArrayList<Product> orderItems = new ArrayList<>();
        for (int a = 0; a < purchaseOrderItems.size(); a++) {
            Product product = new ProductDAO().getProductDetail(purchaseOrderItems.get(a).getProductID());
            product.setUnitPrice(purchaseOrderItems.get(a).getUnitPrice());
            product.setOrderQuantity(purchaseOrderItems.get(a).getQuantity());
            orderItems.add(product);
        }

        boolean isReview = false;
        if (action.equals("review_sales")) {
            isReview = true;
        }
        session.setAttribute("purchaseOrder", purchaseOrder);
        session.setAttribute("orderItems", orderItems);
        session.setAttribute("purchaseItems", purchaseOrderItems);
        session.setAttribute("supplier", supplierInfo);
        session.setAttribute("isReview", isReview);
        session.setAttribute("show", "nope");
        return "/modules/admin/procurement_detail.jsp";
    }
}
