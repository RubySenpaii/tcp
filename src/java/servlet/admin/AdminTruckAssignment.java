/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package servlet.admin;

import dao.TruckReferenceDAO;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import object.TruckReference;
import servlet.BaseServlet;

/**
 *
 * @author RubySenpaii
 */
public class AdminTruckAssignment extends BaseServlet {

    @Override
    protected String servletAction(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        HttpSession session = request.getSession();
        String action = request.getParameter("action");

        if (action.equals("viewTrucks")) {
            ArrayList<TruckReference> trucks = new TruckReferenceDAO().getListOfTrucksWithFormattedDeliveryDetails();
            session.setAttribute("show", "nope");
            session.setAttribute("trucks", trucks);
        } else if (action.equals("addTruck")) {
            TruckReferenceDAO trdao = new TruckReferenceDAO();
            TruckReference truck = new TruckReference();
            truck.setTruckID(trdao.getListOfTrucks().size() + 1);
            truck.setPlateNumber(request.getParameter("plateNumber"));
            truck.setCapacity(Integer.parseInt(request.getParameter("capacity")));
            boolean isAdded = trdao.registerTruck(truck);
            if (isAdded) {
                session.setAttribute("show", "show");
                session.setAttribute("successAlert", "block");
                session.setAttribute("successMessage", truck.getPlateNumber() + " successfully added.");
            } else {
                session.setAttribute("show", "show");
                session.setAttribute("dangerAlert", "block");
                session.setAttribute("errorMessage", truck.getPlateNumber() + " NOT added.");
            }
        }

        return "/modules/admin/truck_assignment.jsp";
    }
}
