/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package servlet.admin;

import dao.InventoryDAO;
import dao.ProductDAO;
import dao.PurchaseReturnDAO;
import extra.Reference;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.Calendar;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import object.Inventory;
import object.Product;
import object.PurchaseReturn;
import servlet.BaseServlet;

/**
 *
 * @author RubySenpaii
 */
public class AdminPurchaseReturnStatus extends BaseServlet {

    @Override
    protected String servletAction(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        HttpSession session = request.getSession();
        String action = request.getParameter("action");

        if (action.equals("showList")) {
            ArrayList<PurchaseReturn> purchaseReturns = new PurchaseReturnDAO().getListOfPurchaseReturns();
            session.setAttribute("purchaseReturns", purchaseReturns);
        } else if (action.equals("receive")) {
            //receiving of purchase goods
            PurchaseReturn purchaseReturn = new PurchaseReturn();
            purchaseReturn.setPurchaseOrderNumber(Integer.parseInt(request.getParameter("purchaseOrderNumber")));
            purchaseReturn.setProductName(request.getParameter("productName"));
            purchaseReturn.setQuantity(Integer.parseInt(request.getParameter("quantity")));
            purchaseReturn.setReturnStatus("Received");
            boolean isUpdated = new PurchaseReturnDAO().updatePurchaseReturn(purchaseReturn);
            System.out.println("updated: " + isUpdated);
            
            Product product = new ProductDAO().getProductDetail(request.getParameter("productName"));
            
            //updating inventory count
            Inventory inventory = new Inventory();
            inventory.setDate(Reference.DATABASE_DATETIME_FORMAT.format(Calendar.getInstance().getTime()));
            inventory.setDefect(0);
            inventory.setIn(Integer.parseInt(request.getParameter("quantity")));
            inventory.setOut(0);
            inventory.setProductID(product.getProductID());
            inventory.setReference("ReceiveItemFromPurchaseOrder-" + Integer.parseInt(request.getParameter("purchaseOrderNumber")));
            inventory.setTotal(new InventoryDAO().getLatestCountForProduct(product.getProductID()).getTotal() + Integer.parseInt(request.getParameter("quantity")));
            boolean isRecorded = new InventoryDAO().recordInventory(inventory);
            System.out.println("inventory updated " + isRecorded);

            return "/AdminPurchaseReturnStatus?action=showList";
        }

        return "/modules/admin/purchase_return_status.jsp";
    }
}
