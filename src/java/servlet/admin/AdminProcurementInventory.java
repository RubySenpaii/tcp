/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package servlet.admin;

import dao.InventoryDAO;
import dao.ProductDAO;
import dao.PurchaseOrderDAO;
import dao.PurchaseOrderItemDAO;
import dao.SupplierDAO;
import extra.Reference;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.Calendar;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import object.Employee;
import object.Inventory;
import object.Product;
import object.PurchaseOrder;
import object.PurchaseOrderItem;
import object.Supplier;
import servlet.BaseServlet;

/**
 *
 * @author RubySenpaii
 */
public class AdminProcurementInventory extends BaseServlet {

    @Override
    protected String servletAction(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        HttpSession session = request.getSession();
        Employee userLogged = (Employee) session.getAttribute("userLogged");
        String action = request.getParameter("action");

        if (action.equals("recordItem")) {
            int purchaseOrderNumber = Integer.parseInt(request.getParameter("purchaseOrderNumber"));
            PurchaseOrder purchaseOrder = new PurchaseOrderDAO().getPurchaseOrderDetail(purchaseOrderNumber);
            ArrayList<PurchaseOrderItem> purchaseOrderItems = new PurchaseOrderItemDAO().getListOfPurchaseOrderItems(purchaseOrderNumber);
            Supplier supplierInfo = new SupplierDAO().getSupplierInfo(purchaseOrder.getSupplierID());
            ArrayList<Product> orderItems = new ArrayList<>();
            for (int a = 0; a < purchaseOrderItems.size(); a++) {
                Product product = new ProductDAO().getProductDetail(purchaseOrderItems.get(a).getProductID());
                product.setUnitPrice(purchaseOrderItems.get(a).getUnitPrice());
                product.setOrderQuantity(purchaseOrderItems.get(a).getQuantity());
                product.setReceivedQuantity(getReceivedQty(purchaseOrderNumber, purchaseOrderItems.get(a).getProductID()));
                orderItems.add(product);
            }

            session.setAttribute("purchaseOrder", purchaseOrder);
            session.setAttribute("orderItems", orderItems);
            session.setAttribute("supplier", supplierInfo);
            session.setAttribute("show", "nope");
            return "/modules/admin/procurement_receive.jsp";
        } else {
            //viewing of purchase orders for receiving of goods
            int purchaseOrderNumber = Integer.parseInt(request.getParameter("purchaseOrderNumber"));
            PurchaseOrder purchaseOrder = new PurchaseOrderDAO().getPurchaseOrderDetail(purchaseOrderNumber);
            purchaseOrder.setDeliveryDate(Reference.DATABASE_DATE_FORMAT.format(Calendar.getInstance().getTime()));
            ArrayList<PurchaseOrderItem> purchaseItems = new PurchaseOrderItemDAO().getListOfPurchaseOrderItems(purchaseOrderNumber);
            String[] items = request.getParameterValues("receivedQty");
            for (int a = 0; a < purchaseItems.size(); a++) {
                if (!items[a].equals("0")) {
                    Inventory currentInventory = new InventoryDAO().getLatestCountForProduct(purchaseItems.get(a).getProductID());
                    Inventory inventory = new Inventory();
                    inventory.setDate(Reference.DATABASE_DATETIME_FORMAT.format(Calendar.getInstance().getTime()));
                    inventory.setDefect(0);
                    inventory.setIn(Integer.parseInt(items[a]));
                    inventory.setOut(0);
                    inventory.setProductID(purchaseItems.get(a).getProductID());
                    inventory.setReference("PurchaseOrder-" + purchaseOrderNumber);
                    inventory.setTotal(currentInventory.getTotal() + Integer.parseInt(items[a]));

                    boolean updateInventory = new InventoryDAO().recordInventory(inventory);
                    System.out.println("updated productid" + purchaseItems.get(a).getProductID() + " inventory count: " + updateInventory);
                }
            }

            if (checkIfComplete(purchaseOrderNumber)) {
                boolean updateDelivery = new PurchaseOrderDAO().updatePurchaseOrderInfo(purchaseOrder);
                System.out.println("update delivery status: " + updateDelivery);
            }
            session.setAttribute("show", "nope");
            return "/AdminProcurementList?action=procure_list";
        }
    }

    private boolean checkIfComplete(int purchaseOrderNumber) {
        ArrayList<PurchaseOrderItem> orderItems = new PurchaseOrderItemDAO().getListOfPurchaseOrderItems(purchaseOrderNumber);
        for (int a = 0; a < orderItems.size(); a++) {
            ArrayList<Inventory> inventories = new InventoryDAO().getListOfProductReferences(orderItems.get(a).getProductID(), "PurchaseOrder-" + orderItems.get(a).getPurchaseOrderNumber());
            double received = 0;
            for (int b = 0; b < inventories.size(); b++) {
                received += inventories.get(b).getIn();
            }
            if (orderItems.get(a).getQuantity() != received) {
                return false;
            }
        }
        return true;
    }

    private int getReceivedQty(int purchaseOrderNumber, int productID) {
        int received = 0;
        ArrayList<Inventory> inventories = new InventoryDAO().getListOfProductReferences(productID, "PurchaseOrder-" + purchaseOrderNumber);
        for (int b = 0; b < inventories.size(); b++) {
            received += inventories.get(b).getIn();
        }
        return received;
    }
}
