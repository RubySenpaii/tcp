/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package servlet.admin;

import dao.SupplierDAO;
import extra.Reference;
import java.io.IOException;
import java.io.PrintWriter;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import object.Supplier;
import servlet.BaseServlet;

/**
 *
 * @author RubySenpaii
 */
public class AdminSupplierDetail extends BaseServlet {

    @Override
    protected String servletAction(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        HttpSession session = request.getSession();
        String action = request.getParameter("action");

        if (action.equals("edit") || action.equals("view")) {
            int supplierID = Integer.parseInt(request.getParameter("supplierID"));
            Supplier supplier = new SupplierDAO().getSupplierInfo(supplierID);
            String enabled = "enabled";
            if (action.equals("view")) {
                enabled = "disabled";
            }

            session.setAttribute("supplier", supplier);
            session.setAttribute("enabled", enabled);
            session.setAttribute("show", "nope");
            return "/modules/admin/supplier_detail.jsp";
        } else if (action.equals("back")) {
            return "/AdminSupplierList?action=view_supplier_list";
        } else if (action.equals("flag")) {
            int supplierID = Integer.parseInt(request.getParameter("supplierID"));
            Supplier supplier = new SupplierDAO().getSupplierInfo(supplierID);
            if (supplier.getFlag() == Reference.ACTIVE) {
                supplier.setFlag(Reference.ARCHIVED);
            } else {
                supplier.setFlag(Reference.ACTIVE);
            }
            boolean updated = new SupplierDAO().updateSupplierFlag(supplier);
            System.out.println("supplier flag updated: " + updated);

            if (updated) {
                session.setAttribute("successAlert", "block");
                session.setAttribute("successMessage", supplier.getSupplierName() + " flag changed successfully.");
            } else {
                session.setAttribute("dangerAlert", "block");
                session.setAttribute("errorMessage", supplier.getSupplierName() + " flag NOT changed.");
            }

            session.setAttribute("show", "show");
            return "/AdminSupplierList?action=view_supplier_list";
        } else {
            //retrieve new info provided from html
            String address = request.getParameter("address");
            String contactPerson = request.getParameter("contactPerson");
            String contactNo = request.getParameter("contactNumber");
            String email = request.getParameter("email");

            Supplier supplier = (Supplier) session.getAttribute("supplier");
            supplier.setAddress(address);
            supplier.setContactPerson(contactPerson);
            supplier.setContactNo(contactNo);
            supplier.setEmail(email);
            supplier.setFriendlyRating(Integer.parseInt(request.getParameter("customerFriendly")));
            supplier.setPaymentRating(Integer.parseInt(request.getParameter("paymentTerms")));
            supplier.setPunctualityRating(Integer.parseInt(request.getParameter("punctuality")));
            supplier.setVolumeRating(Integer.parseInt(request.getParameter("volume")));
            supplier.setRemarks(request.getParameter("remarks"));
            //updating db info
            boolean updatedSupplierInfo = new SupplierDAO().updateSupplierInfo(supplier);
            System.out.println(supplier.getSupplierName() + " info updated: " + updatedSupplierInfo);

            if (updatedSupplierInfo) {
                session.setAttribute("successAlert", "block");
                session.setAttribute("successMessage", supplier.getSupplierName() + " info successfully changed.");
            } else {
                session.setAttribute("dangerAlert", "block");
                session.setAttribute("errorMessage", supplier.getSupplierName() + " info NOT changed.");
            }

            session.setAttribute("show", "show");
            return "/AdminSupplierDetail?action=view&supplierID=" + supplier.getSupplierID();
        }
    }
}
