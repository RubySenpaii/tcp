/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package servlet.admin;

import dao.SupplierDAO;
import extra.Reference;
import java.io.IOException;
import java.io.PrintWriter;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import object.Supplier;
import servlet.BaseServlet;

/**
 *
 * @author RubySenpaii
 */
public class AdminSupplierAdd extends BaseServlet {

    @Override
    protected String servletAction(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        HttpSession session = request.getSession();
        Supplier supplier = new Supplier();
        supplier.setAddress(request.getParameter("address"));
        supplier.setContactNo(request.getParameter("contactNumber"));
        supplier.setContactPerson(request.getParameter("contactPerson"));
        supplier.setEmail(request.getParameter("email"));
        supplier.setFlag(Reference.ACTIVE);
        supplier.setFriendlyRating(Integer.parseInt(request.getParameter("customerFriendly")));
        supplier.setPaymentRating(Integer.parseInt(request.getParameter("paymentTerms")));
        supplier.setPunctualityRating(Integer.parseInt(request.getParameter("punctuality")));
        supplier.setSupplierID(new SupplierDAO().getListOfSuppliers().size() + 1);
        supplier.setSupplierName(request.getParameter("supplierName"));
        supplier.setVolumeRating(Integer.parseInt(request.getParameter("volume")));
        supplier.setRemarks(request.getParameter("remarks"));
        boolean supplierAdded = new SupplierDAO().registerSupplier(supplier);
        System.out.println("supplier added: " + supplierAdded);

        if (supplierAdded) {
            session.setAttribute("successAlert", "block");
            session.setAttribute("successMessage", "Suppleir successfully added.");
        } else {
            session.setAttribute("dangerAlert", "block");
            session.setAttribute("errorMessage", "Supplier NOT added.");
        }
        session.setAttribute("show", "show");
        return "/AdminSupplierList?action=view_supplier_list";
    }
}
