/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package servlet.admin;

import dao.CustomerDAO;
import dao.EmployeeDAO;
import dao.SalesDeliveryDAO;
import dao.SalesOrderDAO;
import dao.SalesPaymentDAO;
import extra.Reference;
import java.io.IOException;
import java.io.PrintWriter;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import object.Customer;
import object.Employee;
import object.SalesDelivery;
import object.SalesOrder;
import object.SalesPayment;
import servlet.BaseServlet;

/**
 *
 * @author RubySenpaii
 */
public class AdminDeliveryUpdate extends BaseServlet {

    @Override
    protected String servletAction(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        HttpSession session = request.getSession();
        String action = request.getParameter("action");
        Employee employee = (Employee) session.getAttribute("userLogged");

        if (action.equals("viewDeliveries")) {
            ArrayList<SalesOrder> unsortedSalesOrder = new SalesOrderDAO().getListOfSalesOrdersWithStatus("Delivery");
            ArrayList<SalesOrder> completedOrders = new SalesOrderDAO().getListOfSalesOrdersWithStatus("Completed");
            ArrayList<SalesOrder> sortedSalesOrder = new ArrayList<>();
            ArrayList<SalesOrder> temporary = new ArrayList<>();
            
            //retrieving sales delivery info for sales order on delivery
            for (int a = 0; a < unsortedSalesOrder.size(); a++) {
                Employee approverInfo = new EmployeeDAO().getEmployeeInfo(unsortedSalesOrder.get(a).getApprovedBy());
                Customer customerInfo = new CustomerDAO().getCustomerInfo(unsortedSalesOrder.get(a).getCustomerID());
                unsortedSalesOrder.get(a).setApprover(approverInfo.getLastName() + ", " + approverInfo.getFirstName());
                unsortedSalesOrder.get(a).setCustomerName(customerInfo.getLastName() + ", " + customerInfo.getFirstName());
                try {
                    SalesDelivery deliveryInfo = new SalesDeliveryDAO().getCurrentSalesDeliveryStatus(unsortedSalesOrder.get(a).getSalesOrderNumber());
                    unsortedSalesOrder.get(a).setDeliveryStatus(deliveryInfo.getDeliveryStatus());
                    unsortedSalesOrder.get(a).setDeliveryUpdate(deliveryInfo.getDateUpdated());

                    if (unsortedSalesOrder.get(a).getDeliveryStatus().equals("Delivered")) {
                        temporary.add(unsortedSalesOrder.get(a));
                    } else {
                        sortedSalesOrder.add(unsortedSalesOrder.get(a));
                    }
                } catch (IndexOutOfBoundsException ex) {
                    System.out.println("no delivery records found on sales order " + unsortedSalesOrder.get(a).getSalesOrderNumber() + "...");
                    unsortedSalesOrder.get(a).setDeliveryStatus("N/A");
                    sortedSalesOrder.add(unsortedSalesOrder.get(a));
                }
            }

            //retrieving the latest info for completed sales order
            for (int a = 0; a < completedOrders.size(); a++) {
                Employee approverInfo = new EmployeeDAO().getEmployeeInfo(completedOrders.get(a).getApprovedBy());
                Customer customerInfo = new CustomerDAO().getCustomerInfo(completedOrders.get(a).getCustomerID());
                completedOrders.get(a).setApprover(approverInfo.getLastName() + ", " + approverInfo.getFirstName());
                completedOrders.get(a).setCustomerName(customerInfo.getLastName() + ", " + customerInfo.getFirstName());
                SalesDelivery deliveryInfo = new SalesDeliveryDAO().getCurrentSalesDeliveryStatus(completedOrders.get(a).getSalesOrderNumber());
                completedOrders.get(a).setDeliveryStatus(deliveryInfo.getDeliveryStatus());
                completedOrders.get(a).setDeliveryUpdate(deliveryInfo.getDateUpdated());
            }
            sortedSalesOrder.addAll(sortedSalesOrder.size(), temporary);
            sortedSalesOrder.addAll(sortedSalesOrder.size(), completedOrders);
            session.setAttribute("salesOrders", sortedSalesOrder);
            session.setAttribute("show", "nope");
            return "/modules/admin/delivery_list.jsp";
        } else {
            //updating of sales delivery status
            SalesDelivery salesDelivery = new SalesDelivery();
            salesDelivery.setSalesOrderNumber(Integer.parseInt(request.getParameter("salesOrderNumber")));
            salesDelivery.setDateUpdated(Reference.DATABASE_DATETIME_FORMAT.format(Calendar.getInstance().getTime()));
            salesDelivery.setUpdatedBy(employee.getEmployeeID());
            salesDelivery.setRemarks(request.getParameter("remarks"));
            if (action.equals("ongoing")) {
                salesDelivery.setDeliveryStatus("Ongoing");
            } else if (action.equals("hold")) {
                salesDelivery.setDeliveryStatus("Hold");
            } else if (action.equals("delivered")) {
                salesDelivery.setDeliveryStatus("Delivered");

                SalesOrder salesOrder = new SalesOrderDAO().getSalesOrderDetails(salesDelivery.getSalesOrderNumber());
                ArrayList<SalesPayment> deposit = new SalesPaymentDAO().getApprovedPaymentsOnSalesOrder(salesDelivery.getSalesOrderNumber());
                double totalDeposit = 0;
                for (int a = 0; a < deposit.size(); a++) {
                    totalDeposit += deposit.get(a).getAmountPaid();
                }
                SalesPayment payment = new SalesPayment();
                payment.setSalesOrderNumber(salesDelivery.getSalesOrderNumber());
                double outstanding = salesOrder.getOrderTotal() - totalDeposit;
                payment.setAmountPaid(outstanding);
                payment.setApprovedBy(employee.getEmployeeID());
                payment.setDatePaid(Reference.DATABASE_DATE_FORMAT.format(Calendar.getInstance().getTime()));
                payment.setProof("door to door delivery");
                boolean updatePayment = new SalesPaymentDAO().addSalesPayment(payment);
                System.out.println("sales payment collected: " + updatePayment);

                salesOrder.setOrderStatus("Completed");
                salesOrder.setReceivedBy(request.getParameter("receivedBy"));
                salesOrder.setReceivedDate(request.getParameter("receivedDate"));
                boolean receivedSalesOrder = new SalesOrderDAO().updateSalesOrderInfo(salesOrder);
                System.out.println("sales order updated: " + receivedSalesOrder);
            }
            boolean addSalesDelivery = new SalesDeliveryDAO().addSalesDelivery(salesDelivery);
            System.out.println("sales delivery status update to " + salesDelivery.getDeliveryStatus() + ": " + addSalesDelivery);
            session.setAttribute("show", "show");
            session.setAttribute("successAlert", "block");
            session.setAttribute("successMessage", salesDelivery.getSalesOrderNumber() + " status updated to " + salesDelivery.getDeliveryStatus().toUpperCase());
            return "/AdminDeliveryUpdate?action=viewDeliveries";
        }

    }
}
