/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package servlet.admin;

import dao.InventoryDAO;
import dao.SalesOrderDAO;
import dao.SalesOrderItemDAO;
import extra.Reference;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.Calendar;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import object.Employee;
import object.Inventory;
import object.SalesOrder;
import object.SalesOrderItem;
import servlet.BaseServlet;

/**
 *
 * @author RubySenpaii
 */
public class AdminSalesReview extends BaseServlet {

    @Override
    protected String servletAction(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        HttpSession session = request.getSession();
        Employee userLogged = (Employee) session.getAttribute("userLogged");
        String action = request.getParameter("action");

        if (action.equals("approveOrder")) {
            int salesOrderNumber = Integer.parseInt(request.getParameter("salesOrderNumber"));
            double total = 0, newTotal = 0;
            String[] index = request.getParameterValues("index");
            ArrayList<SalesOrderItem> salesItems = new SalesOrderItemDAO().getListOfSalesOrderItems(salesOrderNumber);
            SalesOrder salesOrder = new SalesOrderDAO().getSalesOrderDetails(salesOrderNumber);

            SalesOrder newSalesOrder = new SalesOrderDAO().getSalesOrderDetails(salesOrderNumber);
            newSalesOrder.setSalesOrderNumber(new SalesOrderDAO().getListOfSalesOrders().size() + 1);
            boolean createdNew = false;

            for (int a = 0; a < salesItems.size(); a++) {
                for (int b = 0; b < index.length; b++) {
                    if (a == Integer.parseInt(index[b])) {
                        total += (salesItems.get(a).getQuantity() * salesItems.get(a).getUnitPrice());
                        break;
                    } else if (b == index.length - 1) {
                        if (!createdNew) {
                            createdNew = new SalesOrderDAO().addSalesOrder(newSalesOrder);
                        }
                        newTotal += (salesItems.get(a).getQuantity() * salesItems.get(a).getUnitPrice());
                        salesItems.get(a).setSalesOrderNumber(newSalesOrder.getSalesOrderNumber());
                        boolean isUpdated = new SalesOrderItemDAO().updateSalesOrderNumber(salesItems.get(a), salesOrderNumber);
                        System.out.println("updated: " + isUpdated);
                    }
                }
                if (salesItems.get(a).getQuantity() > 0) {
                    Inventory existingStocks = new InventoryDAO().getLatestCountForProduct(salesItems.get(a).getProductID());
                    Inventory inventory = new Inventory();
                    inventory.setDate(Reference.DATABASE_DATETIME_FORMAT.format(Calendar.getInstance().getTime()));
                    inventory.setIn(0);
                    inventory.setOut(salesItems.get(a).getQuantity());
                    inventory.setDefect(0);
                    inventory.setProductID(salesItems.get(a).getProductID());
                    inventory.setTotal(existingStocks.getTotal() - salesItems.get(a).getQuantity());
                    inventory.setReference("SalesOrder-" + salesItems.get(a).getSalesOrderNumber());
                    boolean inventoryUpdate = new InventoryDAO().recordInventory(inventory);
                    System.out.println("inventory count recorded: " + inventoryUpdate);
                }
            }

            salesOrder.setApprovedBy(userLogged.getEmployeeID());
            salesOrder.setOrderStatus("Approved");
            salesOrder.setRemarks(request.getParameter("remarks"));
            salesOrder.setOrderTotal(total);
            boolean updated = new SalesOrderDAO().updateSalesOrderInfo(salesOrder);
            
            newSalesOrder.setOrderTotal(newTotal);
            System.out.println("new order updated: " + new SalesOrderDAO().updateSalesOrderInfo(newSalesOrder));
            System.out.println("sales order " + salesOrderNumber + " approved: " + updated);
            session.setAttribute("show", "show");
            session.setAttribute("successAlert", "block");
            session.setAttribute("successMessage", "Sales Order#" + salesOrder.getSalesOrderNumber()+ " successfully " + salesOrder.getOrderStatus().toUpperCase() + ".");
        } else {
            //rejecting of order
            int salesOrderNumber = Integer.parseInt(request.getParameter("salesOrderNumber"));
            SalesOrder salesOrder = new SalesOrderDAO().getSalesOrderDetails(salesOrderNumber);
            salesOrder.setApprovedBy(userLogged.getEmployeeID());
            salesOrder.setOrderStatus("Rejected");
            salesOrder.setRemarks(request.getParameter("remarks"));
            boolean updated = new SalesOrderDAO().updateSalesOrderInfo(salesOrder);
            System.out.println("sales order " + salesOrderNumber + " rejected: " + updated);
            session.setAttribute("show", "show");
            session.setAttribute("successAlert", "block");
            session.setAttribute("successMessage", "Sales Order#" + salesOrder.getSalesOrderNumber()+ " successfully " + salesOrder.getOrderStatus().toUpperCase() + ".");
        }
        return "/AdminHomepage";
    }
}
