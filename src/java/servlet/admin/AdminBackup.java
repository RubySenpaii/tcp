/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package servlet.admin;

import db.Backups;
import java.io.File;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import servlet.BaseServlet;

/**
 *
 * @author RubySenpaii
 */
public class AdminBackup extends BaseServlet {

    @Override
    protected String servletAction(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        HttpSession session = request.getSession();
        String action = request.getParameter("action");
        
        String backupPath = getServletContext().getRealPath("/assets/backup");
        Backups backupClass = new Backups();
        
        if (action.equals("createBackup")) {
            boolean backupCreated = backupClass.createBackup(backupPath);
            System.out.println("backup created: " + backupCreated);
            return "/AdminBackup?action=viewBackups";
        } else if (action.equals("restoreBackup")) {
            String fileName = request.getParameter("fileName");
            String filePath = backupPath + File.separator + fileName;
            boolean backupRestored = backupClass.restoreBackup(filePath);
            System.out.println("backup resoted: " + backupRestored);
            return "/AdminBackup?action=viewBackups";
        } else {
            File file = new File(backupPath);
            String fileNames[] = file.list();
            ArrayList<String> filtered = new ArrayList<>();
            for (int a = 0; a < fileNames.length; a++) {
                filtered.add(fileNames[a]);
            }
            session.setAttribute("fileList", filtered);
        }
        
        return "/modules/admin/backup.jsp";
    }
}
