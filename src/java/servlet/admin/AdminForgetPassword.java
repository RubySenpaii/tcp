/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package servlet.admin;

import dao.EmployeeDAO;
import extra.EmailSender;
import java.io.IOException;
import java.io.PrintWriter;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import object.Employee;

/**
 *
 * @author RubySenpaii
 */
public class AdminForgetPassword extends HttpServlet {

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        HttpSession session = request.getSession();
        ServletContext context = getServletContext();
        String path = "/modules/admin/login.jsp";
        
        String username = request.getParameter("username");
        String birthday = request.getParameter("birthday");
        String password = request.getParameter("password");
        String motherMaiden = request.getParameter("motherMaiden");

        try {
            EmployeeDAO empdao = new EmployeeDAO();
            Employee employee = empdao.getEmployeeInfo(username);
//            EmailSender sender = new EmailSender();
//            sender.sendCustomerPassword(customer);

            if (employee.getBirthday().equals(birthday) && employee.getMotherMaiden().equals(motherMaiden)) {
                employee.setPassword(password);
                boolean updateCompleted = empdao.updateEmployeeInfo(employee);
                if (updateCompleted) {
                    session.setAttribute("dangerAlert", "none");
                    session.setAttribute("errorMessage", "");

                    session.setAttribute("successAlert", "block");
                    session.setAttribute("successMessage", "Password successfully changed.");
                    session.setAttribute("show", "show");
                } else {
                    session.setAttribute("successAlert", "none");
                    session.setAttribute("successMessage", "");

                    session.setAttribute("dangerAlert", "block");
                    session.setAttribute("errorMessage", "change password failed");
                    session.setAttribute("show", "show");
                }
            } else {
                session.setAttribute("successAlert", "none");
                session.setAttribute("successMessage", "");

                session.setAttribute("dangerAlert", "block");
                session.setAttribute("errorMessage", "wrong info provided");
                session.setAttribute("show", "show");
            }
        } catch (IndexOutOfBoundsException ex) {
            session.setAttribute("successAlert", "none");
            session.setAttribute("successMessage", "");

            session.setAttribute("dangerAlert", "block");
            session.setAttribute("errorMessage", "username does not exist");
            session.setAttribute("show", "show");
        }
        
        RequestDispatcher rd = context.getRequestDispatcher(path);
        rd.forward(request, response);
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
