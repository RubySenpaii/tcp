/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package servlet.admin;

import dao.EmployeeDAO;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import object.Employee;
import object.Product;
import servlet.BaseServlet;

/**
 *
 * @author RubySenpaii
 */
public class AdminLogin extends BaseServlet {
    
    @Override
    protected String servletAction(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        HttpSession session = request.getSession();
        String path = "/modules/admin/login.jsp";

        String username = request.getParameter("username");
        String password = request.getParameter("password");
        
        boolean doesEmployeeExist = new EmployeeDAO().doesEmployeeExist(username, password);
        System.out.println(username + " exist: " + doesEmployeeExist);
        if (doesEmployeeExist) {
            Employee employee = new EmployeeDAO().getEmployeeInfo(username);
            System.out.println("user info: " + employee.getUsername());
            session.setAttribute("userType", "employee");
            session.setAttribute("userLogged", employee);
            session.setAttribute("show", "nope");
            path = "/AdminHomepage?action=goHome";
        } else  {
            session.setAttribute("dangerAlert", "block");
            session.setAttribute("errorMessage", "username/password does not exist. please try again");
            session.setAttribute("show", "show");
            System.out.println("no user found...");
        }
        
        return path;
    }
}
