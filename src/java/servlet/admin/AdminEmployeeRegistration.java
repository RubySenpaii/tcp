/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package servlet.admin;

import dao.EmployeeDAO;
import java.io.IOException;
import java.io.PrintWriter;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import object.Employee;
import servlet.BaseServlet;

/**
 *
 * @author RubySenpaii
 */
public class AdminEmployeeRegistration extends BaseServlet {

    @Override
    protected String servletAction(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        HttpSession session = request.getSession();

        String lastName = request.getParameter("lastName");
        String firstName = request.getParameter("firstName");
        String middleName = request.getParameter("middleName");
        int userLevel = Integer.parseInt(request.getParameter("userLevel"));
        String position = request.getParameter("position");
        String address = request.getParameter("address");
        String contactNo = request.getParameter("contactNumber");
        String email = request.getParameter("email");
        String username = request.getParameter("username");
        String password = request.getParameter("password");
        String birthday = request.getParameter("birthday");
        String motherMaiden = request.getParameter("motherMaiden");

        Employee employee = new Employee();
        employee.setLastName(lastName);
        employee.setFirstName(firstName);
        employee.setMiddleName(middleName);
        employee.setUserLevel(userLevel);
        employee.setPosition(position);
        employee.setAddress(address);
        employee.setContactNo(contactNo);
        employee.setEmail(email);
        employee.setUsername(username);
        employee.setPassword(password);
        employee.setBirthday(birthday);
        employee.setMotherMaiden(motherMaiden);
        employee.setEmployeeID(new EmployeeDAO().getListOfEmployees().size() + 1);

        //registering new employee
        if (!new EmployeeDAO().checkEmployeeName(lastName, firstName, middleName)) {
            boolean employeeRegistered = new EmployeeDAO().registerEmployee(employee);
            session.setAttribute("successAlert", "block");
            session.setAttribute("successMessage", "Employee registration successful.");
            System.out.println("employee registered: " + employeeRegistered);
        } else {
            session.setAttribute("dangerAlert", "block");
            session.setAttribute("errorMessage", "Employee registration NOT successful.");
        }
        session.setAttribute("show", "show");
        return "/AdminEmployeeList?action=view_employee_list";
    }
}
