/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package servlet.admin;

import dao.CustomerDAO;
import dao.EmployeeDAO;
import dao.ProductDAO;
import dao.SalesOrderDAO;
import dao.SalesOrderItemDAO;
import dao.SalesPaymentDAO;
import dao.TruckReferenceDAO;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import object.Customer;
import object.Employee;
import object.Product;
import object.SalesOrder;
import object.SalesOrderItem;
import object.SalesPayment;
import object.TruckReference;
import servlet.BaseServlet;

/**
 *
 * @author RubySenpaii
 */
public class AdminPaymentVerification extends BaseServlet {

    @Override
    protected String servletAction(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        HttpSession session = request.getSession();
        String action = request.getParameter("action");
        Employee employee = (Employee) session.getAttribute("userLogged");

        if (action.equals("approve") || action.equals("reject")) {
            int paymentID = Integer.parseInt(request.getParameter("paymentID"));
            SalesPayment salesPayment = new SalesPaymentDAO().getSalesPaymentInfo(paymentID);
            if (action.equals("approve")) {
                System.out.println("approving payment...");
                salesPayment.setApprovedBy(employee.getEmployeeID());
            } else {
                System.out.println("rejecting payment...");
                salesPayment.setApprovedBy(0);
            }
            boolean updateSuccess = new SalesPaymentDAO().updateSalesPaymentInfo(salesPayment);
            System.out.println("payment update status: " + updateSuccess);

            if (updateSuccess) {
                session.setAttribute("successAlert", "block");
                session.setAttribute("successMessage", "Payment has been " + action.toUpperCase() + ".");
            } else {
                session.setAttribute("dangerAlert", "block");
                session.setAttribute("errorMessage", "Payment is not updated. Please try again.");
            }
            session.setAttribute("show", "show");

            if (updateSuccess && action.equals("approve")) {
                SalesOrder salesOrder = new SalesOrderDAO().getSalesOrderDetails(salesPayment.getSalesOrderNumber());
                salesOrder.setAssignedTruck(Integer.parseInt(request.getParameter("truck")));
                salesOrder.setDeliveredBy(Integer.parseInt(request.getParameter("delivery")));
                salesOrder.setOrderStatus("Delivery");
                boolean updatedSalesOrderStatus = new SalesOrderDAO().updateSalesOrderInfo(salesOrder);
                System.out.println("sales order status updated: " + updatedSalesOrderStatus + "...");
                return "/AdminHomepage?action=homepage";
            } else {
                return "/AdminHomepage?action=homepage";
            }
        } else {
            //retrieve info from db to be displayed on web
            ArrayList<TruckReference> trucks = new TruckReferenceDAO().getListOfTrucks();
            ArrayList<Employee> employees = new EmployeeDAO().getListOfDrivers();
            int salesOrderNumber = Integer.parseInt(request.getParameter("salesOrderNumber"));
            int paymentID = Integer.parseInt(request.getParameter("paymentID"));

            SalesOrder salesOrder = new SalesOrderDAO().getSalesOrderDetails(salesOrderNumber);
            ArrayList<SalesOrderItem> salesOrderItems = new SalesOrderItemDAO().getListOfSalesOrderItems(salesOrderNumber);
            Customer customerInfo = new CustomerDAO().getCustomerInfo(salesOrder.getCustomerID());
            ArrayList<Product> orderItems = new ArrayList<>();
            for (int a = 0; a < salesOrderItems.size(); a++) {
                Product product = new ProductDAO().getProductDetail(salesOrderItems.get(a).getProductID());
                product.setUnitPrice(salesOrderItems.get(a).getUnitPrice());
                product.setOrderQuantity(salesOrderItems.get(a).getQuantity());
                orderItems.add(product);
            }

            SalesPayment salesPayment = new SalesPaymentDAO().getSalesPaymentInfo(paymentID);

            session.setAttribute("employees", employees);
            session.setAttribute("trucks", trucks);
            session.setAttribute("salesPayment", salesPayment);
            session.setAttribute("salesOrder", salesOrder);
            session.setAttribute("orderItems", orderItems);
            session.setAttribute("customer", customerInfo);
            session.setAttribute("show", "nope");
            return "/modules/admin/payment_review.jsp";
        }
    }
}
