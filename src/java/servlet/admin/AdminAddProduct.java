/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package servlet.admin;

import dao.InventoryDAO;
import dao.ProductDAO;
import extra.Reference;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.nio.file.Paths;
import java.util.Calendar;
import javax.servlet.ServletException;
import javax.servlet.annotation.MultipartConfig;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import javax.servlet.http.Part;
import object.Inventory;
import object.Product;
import servlet.BaseServlet;

/**
 *
 * @author RubySenpaii
 */
@MultipartConfig
public class AdminAddProduct extends BaseServlet {

    @Override
    protected String servletAction(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        HttpSession session = request.getSession();
        String action = request.getParameter("action");

        if (action.equals("add_product_page")) {
            System.out.println("directing inventory_add.jsp...");
            session.setAttribute("show", "nope");
            return "/modules/admin/inventory_add.jsp";
        } else if (action.equals("add_product")) {
            System.out.println("adding product to db...");
            return addProduct(request, response);
        } else {
            System.out.println("incorrect parameters...");
            session.setAttribute("show", "nope");
            return "/AdminHomepage?action=homepage";
        }
    }

    private String addProduct(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        HttpSession session = request.getSession();
        //retrieve input from web
        Product product = new Product();
        product.setCategory(request.getParameter("category"));
        product.setCriticalLevel(Double.parseDouble(request.getParameter("critical_level")));
        product.setDescription(request.getParameter("description"));
        Part file = request.getPart("image");
        product.setImage(Paths.get(file.getSubmittedFileName()).getFileName().toString());
        product.setProductID(new ProductDAO().getListOfProducts().size() + 1);
        product.setProductName(request.getParameter("product_name"));
        product.setUnit(request.getParameter("unit"));
        product.setUnitPrice(Double.parseDouble(request.getParameter("unit_price")));
        product.setCostPrice(Double.parseDouble(request.getParameter("cost_price")));
        product.setFlag(Reference.ACTIVE);

        //connect to db and check if adding is succesful
        boolean isProductAdded = new ProductDAO().addProduct(product);
        if (isProductAdded) {
            System.out.println(product.getProductName() + " successfully added...");
            Inventory inventory = new Inventory();
            inventory.setDate(Reference.DATABASE_DATE_FORMAT.format(Calendar.getInstance().getTime()));
            inventory.setDefect(0);
            inventory.setIn(0);
            inventory.setOut(0);
            inventory.setProductID(product.getProductID());
            inventory.setReference("initial entry");
            inventory.setTotal(0);
            boolean startInventory = new InventoryDAO().recordInventory(inventory);
            System.out.println("inventory count added: "  + startInventory);

            System.out.println("uploading image to server...");
            String appPath = getServletContext().getRealPath("/assets/img/products");
            String savePath = appPath ;
            File fileDir = new File(savePath);
            if (!fileDir.exists()) {
                System.out.println(savePath + " created: " + fileDir.mkdir());
            }
            System.out.println(savePath);
            InputStream fileContent = null;
            OutputStream out = null;
            out = new FileOutputStream(new File(savePath + File.separator + product.getImage()));
            fileContent = file.getInputStream();
            int read = 0;
            final byte[] bytes = new byte[1024];
            while ((read = fileContent.read(bytes)) != -1) {
                out.write(bytes, 0, read);
            }
            out.flush();
            out.close();
            System.out.println("image upload success...");
            session.setAttribute("show", "show");
            session.setAttribute("successAlert", "block");
            session.setAttribute("successMessage", product.getProductName() + " successfully added.");
            return "/AdminHomepage?action=homepage";
        } else {
            System.out.println(product.getProductName() + " not successfully added...");
            session.setAttribute("show", "show");
            session.setAttribute("dangerAlert", "block");
            session.setAttribute("errorMessage", product.getProductID() + " NOT added.");
            return "/TCP/modules/admin/inventory_add.jsp";
        }
    }
}
