/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package servlet.admin;

import dao.ProductDAO;
import extra.Reference;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.Calendar;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import object.Product;
import servlet.BaseServlet;

/**
 *
 * @author RubySenpaii
 */
public class AdminViewInventoryList extends BaseServlet {

    @Override
    protected String servletAction(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        HttpSession session = request.getSession();
        String action = request.getParameter("action");
        String timeNow = Reference.DATABASE_DATETIME_FORMAT.format(Calendar.getInstance().getTime());
        ArrayList<Product> products = new ArrayList<>();
        if (action.equals("activeProducts")) {
            products = new ProductDAO().getListOfActiveProducts();
        } else {
            products = new ProductDAO().getListOfArchivedProducts();
        }
        session.setAttribute("show", "nope");
        session.setAttribute("timeNow", timeNow);
        session.setAttribute("products", products);
        return "/modules/admin/inventory_list.jsp";
    }
}
