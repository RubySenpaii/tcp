/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package servlet.admin;

import dao.InventoryDAO;
import dao.ProductDAO;
import extra.Reference;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.io.PrintWriter;
import java.nio.file.Paths;
import java.util.ArrayList;
import javax.servlet.ServletException;
import javax.servlet.annotation.MultipartConfig;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import javax.servlet.http.Part;
import object.Inventory;
import object.Product;
import servlet.BaseServlet;

/**
 *
 * @author RubySenpaii
 */
@MultipartConfig
public class AdminInventoryDetail extends BaseServlet {

    @Override
    protected String servletAction(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        HttpSession session = request.getSession();
        String action = request.getParameter("action");

        if (action.equals("edit") || action.equals("view")) {
            int productID = Integer.parseInt(request.getParameter("productID"));
            Product product = new ProductDAO().getProductDetail(productID);
            String enabled = "enabled";
            if (action.equals("view")) {
                enabled = "disabled";
            }
            ArrayList<Inventory> inventories = new InventoryDAO().getInventoryHistoryForProduct(productID);

            session.setAttribute("productID", productID);
            session.setAttribute("inventories", inventories);
            session.setAttribute("product", product);
            session.setAttribute("enabled", enabled);
            session.setAttribute("show", "nope");
            return "/modules/admin/inventory_detail.jsp";
        } else if (action.equals("back")) {
            int productID = (Integer) session.getAttribute("productID");
            Product product = new ProductDAO().getProductDetail(productID);
            String path = "/AdminViewInventoryList?action=view";
            if (product.getFlag() == Reference.ACTIVE) {
                path = "/AdminViewInventoryList?action=activeProducts";
            }
            session.setAttribute("show", "nope");
            return path;
        } else if (action.equals("flag")) {
            int productID = Integer.parseInt(request.getParameter("productID"));
            Product product = new ProductDAO().getProductDetail(productID);
            String path = "/AdminViewInventoryList?action=view";
            String productFlag = "ARCHIVED";
            if (product.getFlag() == Reference.ARCHIVED) {
                product.setFlag(Reference.ACTIVE);
                productFlag = "ACTIVE";
                path = "/AdminViewInventoryList?action=activeProducts";
            } else {
                product.setFlag(Reference.ARCHIVED);
            }
            boolean isFlagged = new ProductDAO().flagProduct(product);
            if (isFlagged) {
                session.setAttribute("successAlert", "block");
                session.setAttribute("successMessage", product.getProductName() + " flag changed successfully to " + productFlag);
            } else {
                session.setAttribute("dangerAlert", "block");
                session.setAttribute("errorMessage", product.getProductName() + " flag NOT changed.");
            }
            session.setAttribute("show", "show");
            System.out.println("product archived: " + isFlagged);
            return path;
        } else {
            //retrieving info from html to update product details
            Product product = (Product) session.getAttribute("product");
            int productID = product.getProductID();
            product.setCategory(request.getParameter("category"));
            product.setCriticalLevel(Double.parseDouble(request.getParameter("critical_level")));
            product.setDescription(request.getParameter("description"));
            product.setProductName(request.getParameter("product_name"));
            product.setUnit(request.getParameter("unit"));
            product.setUnitPrice(Double.parseDouble(request.getParameter("unit_price")));
            product.setCostPrice(Double.parseDouble(request.getParameter("cost_price")));

            //updating info on db
            boolean isProductUpdate = new ProductDAO().updateProductInfo(product);
            System.out.println(product.getProductID() + " info updated: " + isProductUpdate);

            if (isProductUpdate) {
                System.out.println(product.getProductName() + " successfully added...");
//
//                System.out.println("uploading image to server...");
//                String appPath = getServletContext().getRealPath("assets/img/products");
//                String savePath = appPath;
//                File fileDir = new File(savePath);
//                if (!fileDir.exists()) {
//                    System.out.println(savePath + " created: " + fileDir.mkdir());
//                }
//                System.out.println(savePath);
//                InputStream fileContent = null;
//                OutputStream out = null;
//                out = new FileOutputStream(new File(savePath + File.separator + product.getImage()));
//                fileContent = file.getInputStream();
//                int read = 0;
//                final byte[] bytes = new byte[1024];
//                while ((read = fileContent.read(bytes)) != -1) {
//                    out.write(bytes, 0, read);
//                }
//                out.flush();
//                out.close();
                session.setAttribute("successAlert", "block");
                session.setAttribute("successMessage", productID + " info successfully changed.");
                session.setAttribute("show", "show");
                return "/AdminInventoryDetail?action=view&productID=" + productID;
            } else {
                session.setAttribute("dangerAlert", "block");
                session.setAttribute("errorMessage", productID + " info NOT changed.");
                System.out.println(product.getProductName() + " not successfully added...");
                session.setAttribute("show", "showF");
                return "/AdminInventoryDetail?action=view&productID=" + productID;
            }
        }
    }
}
