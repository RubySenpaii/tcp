/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package servlet.admin;

import dao.EmployeeDAO;
import dao.MessageDAO;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import object.Employee;
import object.Message;
import servlet.BaseServlet;

/**
 *
 * @author RubySenpaii
 */
public class AdminContact extends BaseServlet {

    @Override
    protected String servletAction(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        HttpSession session = request.getSession();
        String path = "/modules/admin/message_list.jsp";
        
        //retrieve list of messages
        ArrayList<Message> messages = new MessageDAO().getMessages();
        session.setAttribute("messages", messages);
        session.setAttribute("show", "nope");
        return path;
    }
}
