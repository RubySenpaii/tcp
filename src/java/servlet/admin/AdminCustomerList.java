/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package servlet.admin;

import dao.CustomerDAO;
import dao.EmployeeDAO;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import object.Customer;
import object.Employee;
import servlet.BaseServlet;

/**
 *
 * @author RubySenpaii
 */
public class AdminCustomerList extends BaseServlet {

    @Override
    protected String servletAction(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        HttpSession session = request.getSession();
        //retrieve customer list from db
        ArrayList<Customer> customers = new CustomerDAO().getListOfCustomers();
        session.setAttribute("customers", customers);
        session.setAttribute("show", "nope");
        return "/modules/admin/customer_list.jsp";
    }
}
