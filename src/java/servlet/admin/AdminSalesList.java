/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package servlet.admin;

import dao.CustomerDAO;
import dao.EmployeeDAO;
import dao.SalesOrderDAO;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import object.Customer;
import object.Employee;
import object.SalesOrder;
import servlet.BaseServlet;

/**
 *
 * @author RubySenpaii
 */
public class AdminSalesList extends BaseServlet {

    @Override
    protected String servletAction(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        HttpSession session = request.getSession();
        String action = request.getParameter("action");
        ArrayList<SalesOrder> salesOrders = new ArrayList<>();
        if (action.equals("approved_sales")) {
            salesOrders = new SalesOrderDAO().getApprovedOrders();
        } else {
            salesOrders = new SalesOrderDAO().getListOfSalesOrdersWithStatus("Rejected");
        }
        for (int a = 0; a < salesOrders.size(); a++) {
            Employee approverInfo = new EmployeeDAO().getEmployeeInfo(salesOrders.get(a).getApprovedBy());
            Customer customerInfo = new CustomerDAO().getCustomerInfo(salesOrders.get(a).getCustomerID());
            salesOrders.get(a).setApprover(approverInfo.getLastName() + ", " + approverInfo.getFirstName());
            salesOrders.get(a).setCustomerName(customerInfo.getLastName() + ", " + customerInfo.getFirstName());
        }
        session.setAttribute("salesOrders", salesOrders);
        session.setAttribute("show", "nope");
        return "/modules/admin/sales_list.jsp";
    }
}
