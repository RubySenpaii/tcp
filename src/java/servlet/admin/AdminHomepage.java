/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package servlet.admin;

import dao.EmployeeDAO;
import dao.ProductDAO;
import dao.PurchaseOrderDAO;
import dao.SalesOrderDAO;
import dao.SalesPaymentDAO;
import dao.TruckReferenceDAO;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import object.Employee;
import object.Product;
import object.PurchaseOrder;
import object.SalesOrder;
import object.SalesPayment;
import object.TruckReference;
import servlet.BaseServlet;

/**
 *
 * @author RubySenpaii
 */
public class AdminHomepage extends BaseServlet {

    @Override
    protected String servletAction(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        HttpSession session = request.getSession();
        String path = "/modules/admin/homepage.jsp";

        ArrayList<PurchaseOrder> purchaseOrders = new PurchaseOrderDAO().getListOfPurchaseOrdersDueForDelivery();
        session.setAttribute("show", "nope");
        session.setAttribute("purchaseOrders", purchaseOrders);
        session.setAttribute("focus", "ex");
        try {
            String focus = request.getParameter("focus");
            if (focus != null) {
                session.setAttribute("focus", focus);
            }
        } catch (NullPointerException ex) {
            session.setAttribute("focus", "ex");
        }
        return path;
    }
}
