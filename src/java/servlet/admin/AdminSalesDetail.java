/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package servlet.admin;

import dao.CustomerDAO;
import dao.ProductDAO;
import dao.SalesDeliveryDAO;
import dao.SalesOrderDAO;
import dao.SalesOrderItemDAO;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import object.Customer;
import object.Product;
import object.SalesDelivery;
import object.SalesOrder;
import object.SalesOrderItem;
import servlet.BaseServlet;

/**
 *
 * @author RubySenpaii
 */
public class AdminSalesDetail extends BaseServlet {

    @Override
    protected String servletAction(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        HttpSession session = request.getSession();
        String action = request.getParameter("action");

        //retrieving of sales order details including sales order items
        int salesOrderNumber = Integer.parseInt(request.getParameter("salesOrderNumber"));
        SalesOrder salesOrder = new SalesOrderDAO().getSalesOrderDetails(salesOrderNumber);
        if (salesOrder.getOrderStatus().equals("Delivery")) {
            try {
                SalesDelivery salesDelivery = new SalesDeliveryDAO().getCurrentSalesDeliveryStatus(salesOrderNumber);
                salesOrder.setDeliveryStatus(salesDelivery.getDeliveryStatus());
            } catch (IndexOutOfBoundsException ex) {
                System.out.println("index out of bounds on line 42-43 of AdminSalesDetail");
                salesOrder.setDeliveryStatus("N/A");
            }
        } else {
            salesOrder.setDeliveryStatus("N/A");
        }
        ArrayList<SalesOrderItem> salesOrderItems = new SalesOrderItemDAO().getListOfSalesOrderItems(salesOrderNumber);
        Customer customerInfo = new CustomerDAO().getCustomerInfo(salesOrder.getCustomerID());
        ArrayList<Product> orderItems = new ArrayList<>();
        for (int a = 0; a < salesOrderItems.size(); a++) {
            Product product = new ProductDAO().getProductDetail(salesOrderItems.get(a).getProductID());
            product.setUnitPrice(salesOrderItems.get(a).getUnitPrice());
            product.setOrderQuantity(salesOrderItems.get(a).getQuantity());
            orderItems.add(product);
        }

        boolean isReview = false;
        if (action.equals("review_sales")) {
            isReview = true;
        }
        session.setAttribute("salesOrder", salesOrder);
        session.setAttribute("orderItems", orderItems);
        session.setAttribute("customer", customerInfo);
        session.setAttribute("isReview", isReview);
        session.setAttribute("show", "nope");
        return "/modules/admin/sales_detail.jsp";
    }
}
