/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package servlet.admin;

import dao.EmployeeDAO;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import object.Employee;
import servlet.BaseServlet;

/**
 *
 * @author RubySenpaii
 */
public class AdminEmployeeList extends BaseServlet {

    @Override
    protected String servletAction(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        HttpSession session = request.getSession();
        ArrayList<Employee> employees = new EmployeeDAO().getListOfEmployees();
        session.setAttribute("employees", employees);
            session.setAttribute("show", "nope");
        return "/modules/admin/employee_list.jsp";
    }
}
