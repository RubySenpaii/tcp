/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package servlet.admin;

import dao.ProductDAO;
import dao.SupplierItemDAO;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import object.Product;
import object.SupplierItem;
import servlet.BaseServlet;

/**
 *
 * @author RubySenpaii
 */
public class AdminSupplierItem extends BaseServlet {

    @Override
    protected String servletAction(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        HttpSession session = request.getSession();
        String action = request.getParameter("action");

        if (action.equals("viewItems")) {
            int supplierID = Integer.parseInt(request.getParameter("supplierID"));
            ArrayList<SupplierItem> supplierItems = new SupplierItemDAO().getSupplierItemsFromSupplier(supplierID);
            ArrayList<Product> products = new ProductDAO().getListOfActiveProducts();
            session.setAttribute("supplierItems", supplierItems);
            session.setAttribute("products", products);
            session.setAttribute("supplierID", supplierID);
            session.setAttribute("show", "nope");
            return "/modules/admin/supplier_item.jsp";
        } else if (action.equals("update")) {
            int supplierID = (Integer) session.getAttribute("supplierID");
            SupplierItemDAO sidao = new SupplierItemDAO();
            System.out.println("cleared supplier items: " + sidao.emptySupplierItem(supplierID));
            String[] productName = request.getParameterValues("productName");
            String[] unitPrice = request.getParameterValues("unitPrice");
            for (int a = 0; a < productName.length; a++) {
                SupplierItem supplierItem = new SupplierItem();
                supplierItem.setSupplierID(supplierID);
                supplierItem.setProductName(productName[a]);
                supplierItem.setUnitPrice(Double.parseDouble(unitPrice[a]));
                System.out.println(a + " addded: " + sidao.addSupplierItem(supplierItem));
            }
            session.setAttribute("show", "show");
            session.setAttribute("successAlert", "block");
            session.setAttribute("successMessage", "Supplier items updated.");
            return "/AdminSupplierDetail?action=view&supplierID=" + supplierID;
        }
        session.setAttribute("show", "nope");
        return "/AdminSupplierList?action=view_supplier_list";
    }
}
