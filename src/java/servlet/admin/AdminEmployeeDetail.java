/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package servlet.admin;

import dao.EmployeeDAO;
import extra.Reference;
import java.io.IOException;
import java.io.PrintWriter;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import object.Employee;
import servlet.BaseServlet;

/**
 *
 * @author RubySenpaii
 */
public class AdminEmployeeDetail extends BaseServlet {

    @Override
    protected String servletAction(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        HttpSession session = request.getSession();
        String action = request.getParameter("action");

        if (action.equals("edit") || action.equals("view")) {
            int employeeID = Integer.parseInt(request.getParameter("employeeID"));
            Employee employee = new EmployeeDAO().getEmployeeInfo(employeeID);
            String enabled = "enabled";
            if (action.equals("view")) {
                enabled = "disabled";
            }

            session.setAttribute("employee", employee);
            session.setAttribute("enabled", enabled);
            session.setAttribute("show", "nope");
            return "/modules/admin/employee_detail.jsp";
        } else if (action.equals("back")) {
            session.setAttribute("show", "nope");
            return "/AdminEmployeeList?action=view_employee_list";
        } else if (action.equals("flag")) {
            int employeeID = Integer.parseInt(request.getParameter("employeeID"));
            Employee employee = new EmployeeDAO().getEmployeeInfo(employeeID);
            if (employee.getFlag() == Reference.ACTIVE) {
                employee.setFlag(Reference.ARCHIVED);
            } else {
                employee.setFlag(Reference.ACTIVE);
            }
            boolean updated = new EmployeeDAO().updateEmployeeFlag(employee);
            System.out.println("employee flag updated: " + updated);

            if (updated) {
                session.setAttribute("successAlert", "block");
                session.setAttribute("successMessage", employee.getLastName() + ", " + employee.getFirstName() + " successfully updated flag.");
            } else {
                session.setAttribute("dangerAlert", "block");
                session.setAttribute("errorMessage", employee.getLastName() + ", " + employee.getFirstName() + " flag NOT updated.");
            }
            session.setAttribute("show", "show");
            return "/AdminEmployeeList?action=view_employee_list";
        } else {
            //updating of employee information
            String address = request.getParameter("address");
            String contactNo = request.getParameter("contactNumber");
            String lastName = request.getParameter("lastName");
            String middleName = request.getParameter("middleName");

            Employee employee = (Employee) session.getAttribute("employee");
            employee.setAddress(address);
            employee.setContactNo(contactNo);
            employee.setLastName(lastName);
            employee.setMiddleName(middleName);
            //update db info for employee
            boolean updatedEmployeeInfo = new EmployeeDAO().updateEmployeeInfo(employee);
            System.out.println(employee.getLastName() + ", " + employee.getFirstName() + " info updated: " + updatedEmployeeInfo);

            if (updatedEmployeeInfo) {
                session.setAttribute("successAlert", "block");
                session.setAttribute("successMessage", employee.getLastName() + ", " + employee.getFirstName() + " successfully employee info.");
            } else {
                session.setAttribute("dangerAlert", "block");
                session.setAttribute("errorMessage", employee.getLastName() + ", " + employee.getFirstName() + " employee info NOT updated.");
            }
            session.setAttribute("show", "show");
            return "/AdminEmployeeDetail?action=view&employeeID=" + employee.getEmployeeID();
        }
    }
}
