/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package servlet.admin;

import dao.CustomerDAO;
import dao.EmployeeDAO;
import dao.ProductDAO;
import dao.PurchaseOrderDAO;
import dao.SalesOrderDAO;
import dao.SalesOrderItemDAO;
import dao.SupplierDAO;
import extra.Reference;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.PrintWriter;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import net.sf.jasperreports.engine.JRException;
import object.Customer;
import object.Employee;
import object.Product;
import object.PurchaseOrder;
import object.SalesOrder;
import object.SalesOrderItem;
import object.Supplier;
import report.ReportingModule;
import servlet.BaseServlet;

/**
 *
 * @author RubySenpaii
 */
public class AdminReport extends BaseServlet {

    private String imgPath = "";
    private String jasperPath = "";
    private String pdfReportPath = "";
    private String pdfOthersPath = "";
    private ReportingModule reports;

    @Override
    protected String servletAction(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException, FileNotFoundException {
        HttpSession session = request.getSession();
        String action = request.getParameter("action");
        Employee userLogged = (Employee) session.getAttribute("userLogged");

        imgPath = getServletContext().getRealPath("/assets/img/resource");
        jasperPath = getServletContext().getRealPath("/report");
        pdfReportPath = getServletContext().getRealPath("/assets/pdf/report");
        pdfOthersPath = getServletContext().getRealPath("/assets/pdf/others");
        reports = new ReportingModule();

        if (action.equals("createReport")) {
            return createReport(request, session);
        } else if (action.equals("createReceipt")) {
            int salesOrderNumber = Integer.parseInt(request.getParameter("salesOrderNumber"));
            String preparedBy = userLogged.getLastName() + ", " + userLogged.getFirstName();
            SalesOrder salesOrder = new SalesOrderDAO().getSalesOrderDetails(salesOrderNumber);
            ArrayList<SalesOrderItem> salesItems = new SalesOrderItemDAO().getListOfSalesOrderItems(salesOrderNumber);
            ArrayList<Product> products = new ArrayList<>();
            for (int a = 0; a < salesItems.size(); a++) {
                Product product = new ProductDAO().getProductDetail(salesItems.get(a).getProductID());
                product.setOrderQuantity(salesItems.get(a).getQuantity());
                product.setUnitPrice(salesItems.get(a).getUnitPrice());
                product.setTotalPrice(salesItems.get(a).getQuantity() * salesItems.get(a).getUnitPrice());
                products.add(product);
            }

            String jasperFile = jasperPath + File.separator + "receipt.jasper";
            String pdfFile = pdfOthersPath + File.separator + "ReceiptForSalesOrder" + salesOrderNumber + ".pdf";
            try {
                reports.createReceipt(imgPath, jasperFile, pdfFile, preparedBy, salesOrder, products);
            } catch (JRException | SQLException ex) {
                Logger.getLogger(AdminReport.class.getName()).log(Level.SEVERE, null, ex);
            }

            return "/assets/pdf/others/" + "ReceiptForSalesOrder" + salesOrderNumber + ".pdf";
        } else if (action.equals("createCustomerSales")) {
            int customerId = Integer.parseInt(request.getParameter("customerID"));
            Customer customer = new CustomerDAO().getCustomerInfo(customerId);
            String startDate = request.getParameter("startDate");
            String endDate = request.getParameter("endDate");
            String dateRange = startDate + " to " + endDate;
            String preparedBy = userLogged.getLastName() + ", " + userLogged.getFirstName();
            ArrayList<SalesOrder> salesOrders = new SalesOrderDAO().getSalesOrderListForCustomerSalesReportFrom(customerId, startDate, endDate);
            for (int a = 0; a < salesOrders.size(); a++) {
                ArrayList<SalesOrderItem> salesItems = new SalesOrderItemDAO().getListOfSalesOrderItems(salesOrders.get(a).getSalesOrderNumber());
                String description = "";
                for (int b = 0; b < salesItems.size(); b++) {
                    description += salesItems.get(b).getProductName() + " - " + salesItems.get(b).getQuantity() + "@ Php " + salesItems.get(b).getUnitPrice();
                    if (b < salesItems.size() - 1) {
                        description += "\n";
                    }
                }
                salesOrders.get(a).setDescription(description);
            }

            String jasperFile = jasperPath + File.separator + "customerSalesList.jasper";
            String pdfFile = pdfOthersPath + File.separator + "CustomerSalesOrderList" + customer.getCustomerID() + ".pdf";
            try {
                reports.createCustomerSalesReport(imgPath, jasperFile, pdfFile, preparedBy, dateRange, customer, salesOrders);
            } catch (JRException | SQLException ex) {
                Logger.getLogger(AdminReport.class.getName()).log(Level.SEVERE, null, ex);
            }

            return "/assets/pdf/others/" + "CustomerSalesOrderList" + customer.getCustomerID() + ".pdf";
        } else if (action.equals("createList")) {
            return createList(request, session);
        } else if (action.equals("sales")) {
            File file = new File(pdfReportPath);
            String fileNames[] = file.list();
            ArrayList<String> filtered = new ArrayList<>();
            for (int a = 0; a < fileNames.length; a++) {
                if (fileNames[a].toLowerCase().contains("sales")) {
                    filtered.add(fileNames[a]);
                }
            }
            session.setAttribute("fileList", filtered);
        } else if (action.equals("purchase")) {
            File file = new File(pdfReportPath);
            String fileNames[] = file.list();
            ArrayList<String> filtered = new ArrayList<>();
            for (int a = 0; a < fileNames.length; a++) {
                if (fileNames[a].toLowerCase().contains("purchase")) {
                    filtered.add(fileNames[a]);
                }
            }
            session.setAttribute("fileList", filtered);
        } else if (action.equals("inventory")) {
            File file = new File(pdfReportPath);
            String fileNames[] = file.list();
            ArrayList<String> filtered = new ArrayList<>();
            for (int a = 0; a < fileNames.length; a++) {
                if (fileNames[a].toLowerCase().contains("inventory")) {
                    filtered.add(fileNames[a]);
                }
            }
            session.setAttribute("fileList", filtered);
        } else {
            File file = new File(pdfReportPath);
            String fileNames[] = file.list();
            ArrayList<String> filtered = new ArrayList<>();
            for (int a = 0; a < fileNames.length; a++) {
                filtered.add(fileNames[a]);
            }
            session.setAttribute("fileList", filtered);
        }

        return "/modules/admin/report.jsp";
    }

    private String createReport(HttpServletRequest request, HttpSession session) throws FileNotFoundException {
        String reportType = request.getParameter("reportType");
        Employee userLogged = (Employee) session.getAttribute("userLogged");
        if (reportType.equals("Sales Report")) {
            String startDate = request.getParameter("startDate");
            String endDate = request.getParameter("endDate");
            String dateRange = startDate + " to " + endDate;
            String preparedBy = userLogged.getLastName() + ", " + userLogged.getFirstName();
            ArrayList<SalesOrder> salesOrders = new SalesOrderDAO().getListOfOrdersForReport(startDate, endDate);

            String jasperFile = jasperPath + File.separator + "salesReport.jasper";
            String pdfFile = pdfReportPath + File.separator + "SalesReportFor" + dateRange.replaceAll(" ", "") + ".pdf";
            try {
                reports.createSalesReport(imgPath, jasperFile, pdfFile, preparedBy, dateRange, salesOrders);
            } catch (JRException | SQLException ex) {
                Logger.getLogger(AdminReport.class.getName()).log(Level.SEVERE, null, ex);
            }
        } else if (reportType.equals("Purchase Report")) {
            String startDate = request.getParameter("startDate");
            String endDate = request.getParameter("endDate");
            String dateRange = startDate + " to " + endDate;
            String preparedBy = userLogged.getLastName() + ", " + userLogged.getFirstName();
            ArrayList<PurchaseOrder> purchaseOrders = new PurchaseOrderDAO().getListOfOrdersForReport(startDate, endDate);

            String jasperFile = jasperPath + File.separator + "purchaseReport.jasper";
            String pdfFile = pdfReportPath + File.separator + "PurchaseReportFor" + dateRange.replaceAll(" ", "") + ".pdf";
            System.out.println(imgPath + jasperFile + pdfFile);
            try {
                reports.createPurchaseReport(imgPath, jasperFile, pdfFile, preparedBy, dateRange, purchaseOrders);
            } catch (JRException | SQLException ex) {
                Logger.getLogger(AdminReport.class.getName()).log(Level.SEVERE, null, ex);
            }
        } else if (reportType.equals("Top Product")) {
            String startDate = request.getParameter("startDate");
            String endDate = request.getParameter("endDate");
            String dateRange = startDate + " to " + endDate;
            String preparedBy = userLogged.getLastName() + ", " + userLogged.getFirstName();
            ArrayList<Product> products = new ProductDAO().getTop10ProductsWithDateRange(startDate, endDate);

            String jasperFile = jasperPath + File.separator + "inventoryReport.jasper";
            String pdfFile = pdfReportPath + File.separator + "InventoryReportFor" + dateRange.replaceAll(" ", "") + ".pdf";
            try {
                reports.createInventoryReport(imgPath, jasperFile, pdfFile, preparedBy, dateRange, products);
            } catch (JRException | SQLException ex) {
                Logger.getLogger(AdminReport.class.getName()).log(Level.SEVERE, null, ex);
            }
        } else if (reportType.equals("Delivery Report")) {
            String startDate = request.getParameter("startDate");
            String endDate = request.getParameter("endDate");
            String dateRange = startDate + " to " + endDate;
            String preparedBy = userLogged.getLastName() + ", " + userLogged.getFirstName();
            ArrayList<SalesOrder> salesOrders = new SalesOrderDAO().getInfoForDeliver(startDate, endDate);

            String jasperFile = jasperPath + File.separator + "deliveryReport.jasper";
            String pdfFile = pdfReportPath + File.separator + "DeliveryReportFor" + dateRange.replaceAll(" ", "") + ".pdf";
            try {
                reports.createSalesReport(imgPath, jasperFile, pdfFile, preparedBy, dateRange, salesOrders);
            } catch (JRException | SQLException ex) {
                Logger.getLogger(AdminReport.class.getName()).log(Level.SEVERE, null, ex);
            }
        } else if (reportType.equals("Sales Return Report")) {
            String startDate = request.getParameter("startDate");
            String endDate = request.getParameter("endDate");
            String dateRange = startDate + " to " + endDate;
            String preparedBy = userLogged.getLastName() + ", " + userLogged.getFirstName();
            ArrayList<Product> products = new ProductDAO().getListOfReturnsFrom(startDate, endDate);

            String jasperFile = jasperPath + File.separator + "salesReturnReport.jasper";
            String pdfFile = pdfReportPath + File.separator + "SalesReturnReportFor" + dateRange.replaceAll(" ", "") + ".pdf";
            try {
                reports.createInventoryReport(imgPath, jasperFile, pdfFile, preparedBy, dateRange, products);
            } catch (JRException | SQLException ex) {
                Logger.getLogger(AdminReport.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
        return "/AdminReport?action=reports";
    }

    private String createList(HttpServletRequest request, HttpSession session) throws FileNotFoundException {
        String userList = request.getParameter("userList");
        Employee userLogged = (Employee) session.getAttribute("userLogged");
        String preparedBy = userLogged.getLastName() + ", " + userLogged.getFirstName();
        String date = Reference.DATABASE_DATE_FORMAT.format(Calendar.getInstance().getTime());
        String fileName = "";
        if (userList.equals("customer")) {
            ArrayList<Customer> customers = new CustomerDAO().getListOfCustomers();
            
            String jasperFile = jasperPath + File.separator + "customerList.jasper";
            String pdfFile = pdfOthersPath + File.separator + "CustomerListFor" + date + ".pdf";
            fileName = "CustomerListFor" + date + ".pdf";
            try {
                reports.createCustomerList(imgPath, jasperFile, pdfFile, preparedBy, date, customers);
            } catch (JRException | SQLException ex) {
                Logger.getLogger(AdminReport.class.getName()).log(Level.SEVERE, null, ex);
            }
        } else if (userList.equals("supplier")) {
            ArrayList<Supplier> suppliers = new SupplierDAO().getListOfSuppliers();
            
            String jasperFile = jasperPath + File.separator + "supplierList.jasper";
            String pdfFile = pdfOthersPath + File.separator + "SupplierListFor" + date + ".pdf";
            fileName = "SupplierListFor" + date + ".pdf";
            try {
                reports.createSupplierList(imgPath, jasperFile, pdfFile, preparedBy, date, suppliers);
            } catch (JRException | SQLException ex) {
                Logger.getLogger(AdminReport.class.getName()).log(Level.SEVERE, null, ex);
            }
        } else if (userList.equals("employee")) {
            ArrayList<Employee> employees = new EmployeeDAO().getListOfEmployees();
            
            String jasperFile = jasperPath + File.separator + "employeeList.jasper";
            String pdfFile = pdfOthersPath + File.separator + "EmployeeListFor" + date + ".pdf";
            fileName = "EmployeeListFor" + date + ".pdf";
            try {
                reports.createEmployeeList(imgPath, jasperFile, pdfFile, preparedBy, date, employees);
                System.out.println("done creating employee list");
            } catch (JRException | SQLException ex) {
                Logger.getLogger(AdminReport.class.getName()).log(Level.SEVERE, null, ex);
            }
        }  else if (userList.equals("inventory")) {
            ArrayList<Product> products = new ProductDAO().getListOfActiveProducts();
            
            String jasperFile = jasperPath + File.separator + "inventoryList.jasper";
            String pdfFile = pdfOthersPath + File.separator + "InventoryListFor" + date + ".pdf";
            fileName = "InventoryListFor" + date + ".pdf";
            try {
                reports.createInventoryList(imgPath, jasperFile, pdfFile, preparedBy, date, products);
                System.out.println("done creating inventory list");
            } catch (JRException | SQLException ex) {
                Logger.getLogger(AdminReport.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
        return "/assets/pdf/others/" + fileName;
    }
}
