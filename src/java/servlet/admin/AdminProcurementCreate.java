/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package servlet.admin;

import dao.ProductDAO;
import dao.PurchaseOrderDAO;
import dao.PurchaseOrderItemDAO;
import dao.SupplierDAO;
import extra.Reference;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.Calendar;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import object.Employee;
import object.Product;
import object.PurchaseOrder;
import object.PurchaseOrderItem;
import object.Supplier;
import servlet.BaseServlet;

/**
 *
 * @author RubySenpaii
 */
public class AdminProcurementCreate extends BaseServlet {

    @Override
    protected String servletAction(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        HttpSession session = request.getSession();
        String action = request.getParameter("action");
        if (action.equals("procure_page")) {
            System.out.println("directing to procurement_create.jsp...");
            ArrayList<Supplier> suppliers = new SupplierDAO().getListOfSuppliers();
            int procurementNumber = new PurchaseOrderDAO().getListOfPurchaseOrders().size() + 1;
            ArrayList<Product> products = new ProductDAO().getListOfActiveProducts();
            session.setAttribute("suppliers", suppliers);
            session.setAttribute("productName", "");
            session.setAttribute("purchaseOrderNumber", procurementNumber);
            session.setAttribute("products", products);
            session.setAttribute("show", "nope");
            return "/modules/admin/procurement_create.jsp";
        } else if (action.equals("criticalProcurePage")) {
            System.out.println("directing to procurement_create.jsp with a product name...");
            String productName = request.getParameter("productName");
            ArrayList<Supplier> suppliers = new SupplierDAO().getListOfSuppliers();
            int procurementNumber = new PurchaseOrderDAO().getListOfPurchaseOrders().size() + 1;
            ArrayList<Product> products = new ProductDAO().getListOfActiveProducts();
            session.setAttribute("suppliers", suppliers);
            session.setAttribute("productName", productName);
            session.setAttribute("purchaseOrderNumber", procurementNumber);
            session.setAttribute("products", products);
            session.setAttribute("show", "nope");
            return "/modules/admin/procurement_create.jsp";
        } else if (action.equals("submit")) {
            System.out.println("submitting purchase order...");
            Employee userLogged = (Employee) session.getAttribute("userLogged");
            Supplier supplier = new SupplierDAO().getSupplierInfo(request.getParameter("supplierName"));
            PurchaseOrder purchaseOrder = new PurchaseOrder();
            purchaseOrder.setAddress(request.getParameter("address"));
            purchaseOrder.setApprovedBy(Reference.UNASSIGNED);
            purchaseOrder.setContactNo(request.getParameter("contactNo"));
            purchaseOrder.setContactPerson(request.getParameter("contactPerson"));
            purchaseOrder.setRequestedDelivery(request.getParameter("expectedDelivery"));
            purchaseOrder.setDeliveryDate("9999-12-31");
            purchaseOrder.setOrderDate(Reference.DATABASE_DATETIME_FORMAT.format(Calendar.getInstance().getTime()));
            purchaseOrder.setOrderStatus("Pending");
            purchaseOrder.setPreparedBy(userLogged.getEmployeeID());
            purchaseOrder.setPaymentOption(request.getParameter("payment"));
            purchaseOrder.setPurchaseOrderNumber(Integer.parseInt(request.getParameter("purchaseOrderNumber")));
            purchaseOrder.setRemarks("");
            purchaseOrder.setSupplierID(supplier.getSupplierID());

            ArrayList<PurchaseOrderItem> purchaseOrderItems = new ArrayList<>();
            String[] productName = request.getParameterValues("productName");
            String[] quantity = request.getParameterValues("quantity");
            String[] price = request.getParameterValues("unitPrice");
            double orderTotal = 0;
            for (int a = 0; a < productName.length; a++) {
                Product product = new ProductDAO().getProductDetail(productName[a]);
                PurchaseOrderItem purchaseOrderItem = new PurchaseOrderItem();
                purchaseOrderItem.setPurchaseOrderNumber(purchaseOrder.getPurchaseOrderNumber());
                purchaseOrderItem.setProductID(product.getProductID());
                purchaseOrderItem.setQuantity(Integer.parseInt(quantity[a]));
                purchaseOrderItem.setUnitPrice(Double.parseDouble(price[a]));
                orderTotal += (Integer.parseInt(quantity[a]) * Double.parseDouble(price[a]));
                purchaseOrderItems.add(purchaseOrderItem);
            }
            purchaseOrder.setOrderTotal(orderTotal);

            if (orderTotal > 0) {
                if (new PurchaseOrderDAO().addPurchaseOrder(purchaseOrder)) {
                    System.out.println("added purchase order...");
                    if (new PurchaseOrderItemDAO().addPurchaseOrderItems(purchaseOrderItems)) {
                        System.out.println("successfully created purchase order...");
                        session.setAttribute("successAlert", "block");
                        session.setAttribute("successMessage", "Purchase order has been submitted. Subject for approval.");
                        session.setAttribute("show", "show");
                        return "/AdminHomepage?action=goHomePage";
                    } else {
                        System.out.println("items not successfully added to db...");
                    }
                } else {
                    System.out.println("purchase order not created...");
                }
            }

            session.setAttribute("show", "show");
            session.setAttribute("dangerAlert", "block");
            session.setAttribute("errorMessage", "Purchase order not created. Please try again.");
            return "/AdminProcurementCreate?action=procure_page";
        } else {
            System.out.println("invalid action...");
            session.setAttribute("show", "nope");
            return "/AdminHomepage";
        }
    }
}
