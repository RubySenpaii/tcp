/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package servlet.admin;

import dao.PurchaseOrderDAO;
import dao.PurchaseOrderItemDAO;
import dao.SupplierDAO;
import extra.EmailSender;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import object.Employee;
import object.PurchaseOrder;
import object.PurchaseOrderItem;
import object.Supplier;
import servlet.BaseServlet;

/**
 *
 * @author RubySenpaii
 */
public class AdminProcurementReview extends BaseServlet {

    @Override
    protected String servletAction(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        HttpSession session = request.getSession();
        Employee userLogged = (Employee) session.getAttribute("userLogged");
        String action = request.getParameter("action");

        int purchaseOrderNumber = Integer.parseInt(request.getParameter("purchaseOrderNumber"));
        if (action.equals("approveOrder")) {
            PurchaseOrderDAO podao = new PurchaseOrderDAO();
            PurchaseOrder purchaseOrder = podao.getPurchaseOrderDetail(purchaseOrderNumber);
            Supplier supplier = new SupplierDAO().getSupplierInfo(purchaseOrder.getSupplierID());
            PurchaseOrder newPurchaseOrder = podao.getPurchaseOrderDetail(purchaseOrderNumber);
            newPurchaseOrder.setPurchaseOrderNumber(new PurchaseOrderDAO().getListOfPurchaseOrders().size() + 1);
            boolean createdNew = false;

            double total = 0, newTotal = 0;
            String[] index = request.getParameterValues("index");
            ArrayList<PurchaseOrderItem> purchaseItems = new PurchaseOrderItemDAO().getListOfPurchaseOrderItems(purchaseOrderNumber);
            for (int a = 0; a < purchaseItems.size(); a++) {
                for (int b = 0; b < index.length; b++) {
                    if (a == Integer.parseInt(index[b])) {
                        total += (purchaseItems.get(a).getQuantity() * purchaseItems.get(a).getUnitPrice());
                        break;
                    } else if (b == index.length - 1) {
                        if (!createdNew) {
                            createdNew = new PurchaseOrderDAO().addPurchaseOrder(newPurchaseOrder);
                        }
                        newTotal += (purchaseItems.get(a).getQuantity() * purchaseItems.get(a).getUnitPrice());
                        purchaseItems.get(a).setPurchaseOrderNumber(newPurchaseOrder.getPurchaseOrderNumber());
                        boolean isUpdated = new PurchaseOrderItemDAO().updatePurchaseOrderNumber(purchaseItems.get(a), purchaseOrderNumber);
                        System.out.println("updated: " + isUpdated);
                    }
                }
            }

            purchaseOrder.setPurchaseOrderNumber(purchaseOrderNumber);
            purchaseOrder.setApprovedBy(userLogged.getEmployeeID());
            purchaseOrder.setOrderStatus("Approved");
            purchaseOrder.setRemarks(request.getParameter("remarks"));
            purchaseOrder.setRequestedDelivery(request.getParameter("expectedDelivery"));
            purchaseOrder.setAddress(request.getParameter("address"));
            purchaseOrder.setContactNo(request.getParameter("contactNo"));
            purchaseOrder.setContactPerson(request.getParameter("contactPerson"));
            purchaseOrder.setPaymentOption(request.getParameter("payment"));
            purchaseOrder.setOrderTotal(total);
            System.out.println(purchaseOrder.getContactPerson());
            boolean updated = podao.updatePurchaseOrderInfo(purchaseOrder);

            newPurchaseOrder.setOrderTotal(newTotal);
            System.out.println("po order " + newPurchaseOrder.getPurchaseOrderNumber() + "  total update: " + podao.updatePurchaseOrderInfo(newPurchaseOrder));
            System.out.println("purchase order " + purchaseOrder.getPurchaseOrderNumber() + " approved: " + updated);
            session.setAttribute("show", "show");
            session.setAttribute("successAlert", "block");
            session.setAttribute("successMessage", "Purchase Order#" + purchaseOrder.getPurchaseOrderNumber() + " successfully " + purchaseOrder.getOrderStatus().toUpperCase() + ".");
            return "/AdminProcurementDetail?action=view_detail&purchaseOrderNumber=" + purchaseOrder.getPurchaseOrderNumber();
        } else if (action.equals("sendEmail")) {
            Supplier supplier = (Supplier) session.getAttribute("supplier");
            PurchaseOrder purchaseOrder = (PurchaseOrder) session.getAttribute("purchaseOrder");
            ArrayList<PurchaseOrderItem> purchaseItems = (ArrayList<PurchaseOrderItem>) session.getAttribute("purchaseItems");

            EmailSender sender = new EmailSender();
            boolean emailSent = sender.sendPurchaseOrderDetails(supplier, purchaseOrder, purchaseItems);

            if (emailSent) {
                session.setAttribute("show", "show");
                session.setAttribute("successAlert", "block");
                session.setAttribute("successMessage", "Email sent");
            } else {
                session.setAttribute("dangerAlert", "block");
                session.setAttribute("errorMessage", "Email not sent.");
                session.setAttribute("show", "show");
            }
            return "/AdminProcurementDetail?action=view_detail&purchaseOrderNumber=" + purchaseOrder.getPurchaseOrderNumber();
        } else {
            //rejecting of purchase order
            PurchaseOrder purchaseOrder = new PurchaseOrderDAO().getPurchaseOrderDetail(purchaseOrderNumber);
            purchaseOrder.setApprovedBy(userLogged.getEmployeeID());
            purchaseOrder.setOrderStatus("Rejected");
            purchaseOrder.setRemarks(request.getParameter("remarks"));
            boolean updated = new PurchaseOrderDAO().updatePurchaseOrderInfo(purchaseOrder);
            System.out.println("purchase order " + purchaseOrderNumber + " rejected: " + updated);
            session.setAttribute("show", "show");
            session.setAttribute("successAlert", "block");
            session.setAttribute("successMessage", "Purchase Order#" + purchaseOrder.getPurchaseOrderNumber() + " successfully " + purchaseOrder.getOrderStatus().toUpperCase() + ".");
        }
        return "/AdminHomepage";
    }
}
