/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package servlet.admin;

import dao.InventoryDAO;
import dao.ProductDAO;
import dao.PurchaseOrderDAO;
import dao.PurchaseOrderItemDAO;
import dao.PurchaseReturnDAO;
import extra.Reference;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.Calendar;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import object.Inventory;
import object.Product;
import object.PurchaseOrder;
import object.PurchaseOrderItem;
import object.PurchaseReturn;
import servlet.BaseServlet;

/**
 *
 * @author RubySenpaii
 */
public class AdminPurchaseDefective extends BaseServlet {

    @Override
    protected String servletAction(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        HttpSession session = request.getSession();
        String action = request.getParameter("action");
        
        if (action.equals("goPage")) {
            ArrayList<PurchaseOrder> purchaseOrders = new PurchaseOrderDAO().getListOfApprovedPurchaseOrders();
            ArrayList<PurchaseOrderItem> purchaseItems = new ArrayList<>();
            session.setAttribute("show", "nope");
            session.setAttribute("purchaseItems", purchaseItems);
            session.setAttribute("purchaseOrders", purchaseOrders);
            return "/modules/admin/procurement_return.jsp";
        } else if (action.equals("view")) {
            int purchaseOrderNumber = Integer.parseInt(request.getParameter("purchaseOrderNumber"));
            PurchaseOrder purchaseOrder = new PurchaseOrderDAO().getPurchaseOrderDetail(purchaseOrderNumber);
            ArrayList<PurchaseOrderItem> purchaseItems = new PurchaseOrderItemDAO().getListOfPurchaseOrderItems(purchaseOrderNumber);
            
            session.setAttribute("show", "nope");
            session.setAttribute("purchaseOrder", purchaseOrder);
            session.setAttribute("purchaseItems", purchaseItems);
            return "/modules/admin/procurement_return.jsp";
        } else {
            //recording of defective products
            String[] defective = request.getParameterValues("defective");
            String remarks = request.getParameter("remarks");
            ArrayList<PurchaseOrderItem> purchaseItems = (ArrayList<PurchaseOrderItem>) session.getAttribute("purchaseItems");
            for (int a = 0; a < defective.length; a++) {
                int defectiveQty = Integer.parseInt(defective[a]);
                if (defectiveQty > 0) {
                    Inventory inventory = new Inventory();
                    inventory.setDate(Reference.DATABASE_DATETIME_FORMAT.format(Calendar.getInstance().getTime()));
                    inventory.setDefect(defectiveQty);
                    inventory.setIn(-defectiveQty);
                    inventory.setOut(0);
                    inventory.setProductID(purchaseItems.get(a).getProductID());
                    inventory.setReference("DefectivePurchase-" + purchaseItems.get(a).getPurchaseOrderNumber() + " Remarks: " + remarks);
                    inventory.setTotal(new InventoryDAO().getLatestCountForProduct(purchaseItems.get(a).getProductID()).getTotal() - defectiveQty);
                    boolean isRecorded = new InventoryDAO().recordInventory(inventory);
                    System.out.println("inventory updated " + isRecorded);
                    
                    Product product = new ProductDAO().getProductDetail(purchaseItems.get(a).getProductID());
                    PurchaseReturn purchaseReturn = new PurchaseReturn();
                    purchaseReturn.setPurchaseOrderNumber(purchaseItems.get(a).getPurchaseOrderNumber());
                    purchaseReturn.setProductName(product.getProductName());
                    purchaseReturn.setQuantity(defectiveQty);
                    purchaseReturn.setReturnStatus("Returned");
                    boolean isAdded = new PurchaseReturnDAO().addPurchaseReturn(purchaseReturn);
                    System.out.println("return added: " + isAdded);
                }
            }
            session.setAttribute("show", "nope");
            return "/AdminProcurementList?action=procure_list";
        }
    }
}
