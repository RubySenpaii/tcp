/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package servlet.admin;

import dao.CustomerDAO;
import extra.Reference;
import java.io.IOException;
import java.io.PrintWriter;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import object.Customer;
import servlet.BaseServlet;

/**
 *
 * @author RubySenpaii
 */
public class AdminCustomerDetail extends BaseServlet {

    @Override
    protected String servletAction(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        HttpSession session = request.getSession();
        String action = request.getParameter("action");

        if (action.equals("edit") || action.equals("view")) {
            //if user wants to access details of a customer
            int customerID = Integer.parseInt(request.getParameter("customerID"));
            Customer customer = new CustomerDAO().getCustomerInfo(customerID);
            String enabled = "enabled";
            if (action.equals("view")) {
                //if user is only viewing.. inputs will be disabled
                enabled = "disabled";
            }

            session.setAttribute("show", "nope");
            session.setAttribute("customer", customer);
            session.setAttribute("enabled", enabled);
            return "/modules/admin/customer_detail.jsp";
        } else if (action.equals("back")) {
            //return to list
            session.setAttribute("show", "nope");
            return "/AdminCustomerList?action=view_customer_list";
        } else if (action.equals("flag")) {
            //flagging of cust from active to inactive and inactive to active
            int customerID = Integer.parseInt(request.getParameter("customerID"));
            Customer customer = new CustomerDAO().getCustomerInfo(customerID);
            if (customer.getFlag() == Reference.ACTIVE) {
                customer.setFlag(Reference.ARCHIVED);
            } else {
                customer.setFlag(Reference.ACTIVE);
            }
            boolean updated = new CustomerDAO().updateCustomerFlag(customer);
            System.out.println("customer flag updated: " + updated);
            if (updated) {
                session.setAttribute("successAlert", "block");
                session.setAttribute("successMessage", customer.getLastName() + ", " + customer.getFirstName() + " flag successfully changed.");
            } else {
                session.setAttribute("dangerAlert", "block");
                session.setAttribute("errorMessage", customer.getLastName() + ", " + customer.getFirstName() + " flag is NOT changed.");
            }
            session.setAttribute("show", "show");
            return "/AdminCustomerList?action=view_customer_list";
        } else {
            //retrieve info from html
            String address = request.getParameter("address");
            String contactNo = request.getParameter("contactNumber");
            String email = request.getParameter("email");
            String birthday = request.getParameter("birthday");

            Customer customer = (Customer) session.getAttribute("customer");
            customer.setAddress(address);
            customer.setContactNo(contactNo);
            customer.setEmail(email);
            customer.setBirthday(birthday);
            //update db with info provided above
            boolean updatedCustInfo = new CustomerDAO().updateCustomerInfo(customer);
            System.out.println(customer.getLastName() + ", " + customer.getFirstName() + " info updated: " + updatedCustInfo);

            if (updatedCustInfo) {
                session.setAttribute("successAlert", "block");
                session.setAttribute("successMessage", customer.getLastName() + ", " + customer.getFirstName() + " info successfully updated.");
            } else {
                session.setAttribute("dangerAlert", "block");
                session.setAttribute("errorMessage", customer.getLastName() + ", " + customer.getFirstName() + " info NOT updated.");
            }

            session.setAttribute("show", "show");
            return "/AdminCustomerDetail?action=view&customerID=" + customer.getCustomerID();
        }
    }
}
