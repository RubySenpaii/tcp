/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package servlet.highcharts;

import dao.ProductDAO;
import dao.SalesOrderDAO;
import java.io.IOException;
import java.io.PrintWriter;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import object.Employee;
import object.Product;
import object.SalesOrder;
import org.json.JSONException;
import org.json.JSONObject;

/**
 *
 * @author RubySenpaii
 */
public class AdminHomepageCharts extends HttpServlet {

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        HttpSession session = request.getSession();

        JSONObject jsonObjects = new JSONObject();
        try {
            SimpleDateFormat sdf1 = new SimpleDateFormat("yyyyy");
            int year = Integer.parseInt(sdf1.format(Calendar.getInstance().getTime()));
            ArrayList<SalesOrder> resultSales = new SalesOrderDAO().getMonthlySalesForYear(year);
            ArrayList<Double> monthlySales = new ArrayList<>();
            for (int a = 1; a < 13; a++) {
                for (int b = 0; b < resultSales.size(); b++) {
                    if (resultSales.get(b).getMonth().split("-")[0].equals(String.valueOf(a))) {
                        monthlySales.add(resultSales.get(b).getTotalSales());
                        break;
                    } else if (b == resultSales.size() - 1) {
                        monthlySales.add(0.0);
                    }
                }
            }
            jsonObjects.put("monthlySales", monthlySales);
            
            SimpleDateFormat sdf = new SimpleDateFormat("MM-yyyy");
            ArrayList<Product> topProducts = new ProductDAO().getTop10Products(sdf.format(Calendar.getInstance().getTime()));
            jsonObjects.put("topProducts", topProducts);

            response.setContentType("application/json");
            response.setCharacterEncoding("utf-8");
            System.out.println("data written for transfer to admin/homepage.jsp with monthlysales size: " + monthlySales.size() + " | topproducts size: " + topProducts);
            response.getWriter().write("[" + jsonObjects.toString() + "]");
        } catch (JSONException ex) {
            Logger.getLogger(AdminHomepageCharts.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
