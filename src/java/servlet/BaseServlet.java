/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package servlet;

import dao.ProductDAO;
import dao.PurchaseOrderDAO;
import dao.SalesOrderDAO;
import dao.SalesPaymentDAO;
import extra.Reference;
import extra.TaskSchedulingModule;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import object.Customer;
import object.Employee;
import object.Product;
import object.PurchaseOrder;
import object.SalesOrder;
import object.SalesPayment;
import servlet.admin.AdminReport;

/**
 *
 * @author RubySenpaii
 */
public abstract class BaseServlet extends HttpServlet {

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        HttpSession session = request.getSession();
        ServletContext context = getServletContext();
        String path = "/modules/customer/login.jsp";

        try {
            if (session.getAttribute("automaticBackup") == null) {
                TaskSchedulingModule tsm = new TaskSchedulingModule();
                String backupPath = getServletContext().getRealPath("/assets/backup");
                tsm.startTask(backupPath);
                session.setAttribute("automaticBackup", Reference.ACTIVE);
            }
            
            if (request.getParameter("action") != null) {
                String action = request.getParameter("action");
                System.out.println("attempting to " + action + "...");

                if (action.equals("login") || action.equals("new_customer") || action.equals("guest") || action.equals("forgotPassword")) {
                    session.setAttribute("dangerAlert", "none");
                    session.setAttribute("errorMessage", "");
                    session.setAttribute("successAlert", "none");
                    session.setAttribute("successMessage", "");
                    path = servletAction(request, response);
                } else {
                    String show = (String) session.getAttribute("show");
                    if (!show.equals("show")) {
                        session.setAttribute("dangerAlert", "none");
                        session.setAttribute("errorMessage", "");
                        session.setAttribute("successAlert", "none");
                        session.setAttribute("successMessage", "");
                    }
                    String userType = (String) session.getAttribute("userType");
                    if (userType.equals("customer")) {
                        System.out.println("user is customer...");
                        Customer userLogged = (Customer) session.getAttribute("userLogged");
                        if (userLogged != null) {
                            ArrayList<SalesOrder> approved = new SalesOrderDAO().getListOfSalesOrdersWithStatusFrom("Approved", userLogged.getCustomerID());
                            ArrayList<SalesOrder> delivery = new SalesOrderDAO().getListOfSalesOrdersWithStatusFrom("Delivery", userLogged.getCustomerID());
                            session.setAttribute("approved", approved.size());
                            session.setAttribute("delivery", delivery.size());
                            path = servletAction(request, response);
                        } else {
                            System.out.println("user no longer logged in...");
                            System.out.println("directing to portal...");
                        }
                    } else if (userType.equals("employee")) {
                        System.out.println("user is employee...");
                        Employee userLogged = (Employee) session.getAttribute("userLogged");
                        if (userLogged != null) {
                            //filter access here
                            ArrayList<SalesOrder> pendingSalesOrder = new SalesOrderDAO().getListOfSalesOrdersWithStatus("Pending");
                            ArrayList<SalesPayment> pendingSalesPayments = new SalesPaymentDAO().getListOfPendingSalesPayment();
                            ArrayList<PurchaseOrder> pendingPurchaseOrders = new PurchaseOrderDAO().getListOfPendingPurchaseOrders();
                            ArrayList<Product> criticalProducts = new ArrayList<>();
                            ArrayList<Product> products = new ProductDAO().getListOfProducts();
                            for (int a = 0; a < products.size(); a++) {
                                if (products.get(a).getStockQuantity() < products.get(a).getCriticalLevel()) {
                                    criticalProducts.add(products.get(a));
                                }
                            }

                            session.setAttribute("pendingSales", pendingSalesOrder);
                            session.setAttribute("pendingSalesPayments", pendingSalesPayments);
                            session.setAttribute("criticalProducts", criticalProducts);
                            session.setAttribute("pendingPurchases", pendingPurchaseOrders);
                            System.out.println("done");
                            path = servletAction(request, response);
                        } else {
                            System.out.println("user no longer logged in...");
                            System.out.println("directing to portal...");
                        }
                    } else if (userType.equals("guest")) {
                        System.out.println("user is a guest...");
                        path = servletAction(request, response);
                    } else {
                        System.out.println("unknown user type..");
                        System.out.println("directing to portal...");
                    }
                }
            } else {
                System.out.println("no action sent...");
                System.out.println("directing to portal...");
            }
            session.setAttribute("prevPath", path);
        } catch (NullPointerException ex) {
            Logger.getLogger(BaseServlet.class.getName()).log(Level.SEVERE, null, ex);
            path = (String) session.getAttribute("prevPath");
            System.out.println("prev path " + path);
            if (path.contains("TCP")) {
                path = path.substring(4);
            }
        }

        RequestDispatcher rd = context.getRequestDispatcher(path);
        rd.forward(request, response);
    }

    /**
     * Abstract method will be used in child class for logic. Extension of
     * processRequest method.
     *
     * @param request servlet request
     * @param response servlet response
     * @return String the path to direct to
     * @throws javax.servlet.ServletException
     * @throws java.io.IOException
     */
    protected abstract String servletAction(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException;

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
