/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package delete;

import dao.EmployeeDAO;
import dao.ProductDAO;
import dao.PurchaseOrderDAO;
import dao.PurchaseOrderItemDAO;
import dao.SalesOrderDAO;
import dao.SalesPaymentDAO;
import dao.SupplierDAO;
import db.Backups;
import extra.EmailSender;
import extra.Reference;
import java.io.File;
import java.io.IOException;
import java.net.URISyntaxException;
import java.security.CodeSource;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.logging.Level;
import java.util.logging.Logger;
import object.Customer;
import object.Product;
import object.PurchaseOrder;
import object.PurchaseOrderItem;
import object.SalesPayment;
import object.Supplier;

/**
 *
 * @author RubySenpaii
 */
public class Delete {

    public static void main(String[] args) throws IOException {
        System.out.println(new EmployeeDAO().getListOfDrivers());
        System.out.println(new SalesOrderDAO().getListOfOrdersForReport("2017-10-01", "2017-11-04").size());
        System.out.println(new ProductDAO().getTop10ProductsWithDateRange("2017-09-30", "2017-11-04").size());
        String dateTime = "backupAsOf20171108190017".split("f")[1];
        String date = dateTime.substring(0, 4) + "-" + dateTime.substring(4, 6) + "-" + dateTime.substring(6, 8);
        String time = dateTime.substring(8, 10) + ":" + dateTime.substring(10, 12);
        System.out.println(date + " " + time);
    }
}
