/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package object;

/**
 *
 * @author RubySenpaii
 */
public class Customer {
    public static final String TABLE_NAME = "Customer";
    public static final String COLUMN_CUSTOMER_ID = "CustomerID";
    public static final String COLUMN_LAST_NAME = "LastName";
    public static final String COLUMN_FIRST_NAME = "FirstName";
    public static final String COLUMN_MIDDLE_NAME = "MiddleName";
    public static final String COLUMN_ADDRESS = "Address";
    public static final String COLUMN_CONTACT_NO = "ContactNo";
    public static final String COLUMN_EMAIL = "Email";
    public static final String COLUMN_BIRTHDAY = "Birthday";
    public static final String COLUMN_USERNAME = "Username";
    public static final String COLUMN_PASSWORD = "Password";
    public static final String COLUMN_FLAG = "Flag";
    public static final String COLUMN_MOTHER_MAIDEN = "MotherMaiden";
    
    private int customerID;
    private String lastName;
    private String firstName;
    private String middleName;
    private String address;
    private String contactNo;
    private String email;
    private String birthday;
    private String username;
    private String password;
    private int flag;
    private String motherMaiden;
    
    public Customer() {
        
    }

    /**
     * @return the customerID
     */
    public int getCustomerID() {
        return customerID;
    }

    /**
     * @param customerID the customerID to set
     */
    public void setCustomerID(int customerID) {
        this.customerID = customerID;
    }

    /**
     * @return the lastName
     */
    public String getLastName() {
        return lastName;
    }

    /**
     * @param lastName the lastName to set
     */
    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    /**
     * @return the firstName
     */
    public String getFirstName() {
        return firstName;
    }

    /**
     * @param firstName the firstName to set
     */
    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    /**
     * @return the middleName
     */
    public String getMiddleName() {
        return middleName;
    }

    /**
     * @param middleName the middleName to set
     */
    public void setMiddleName(String middleName) {
        this.middleName = middleName;
    }

    /**
     * @return the address
     */
    public String getAddress() {
        return address;
    }

    /**
     * @param address the address to set
     */
    public void setAddress(String address) {
        this.address = address;
    }

    /**
     * @return the contactNo
     */
    public String getContactNo() {
        return contactNo;
    }

    /**
     * @param contactNo the contactNo to set
     */
    public void setContactNo(String contactNo) {
        this.contactNo = contactNo;
    }

    /**
     * @return the email
     */
    public String getEmail() {
        return email;
    }

    /**
     * @param email the email to set
     */
    public void setEmail(String email) {
        this.email = email;
    }

    /**
     * @return the username
     */
    public String getUsername() {
        return username;
    }

    /**
     * @param username the username to set
     */
    public void setUsername(String username) {
        this.username = username;
    }

    /**
     * @return the password
     */
    public String getPassword() {
        return password;
    }

    /**
     * @param password the password to set
     */
    public void setPassword(String password) {
        this.password = password;
    }

    /**
     * @return the flag
     */
    public int getFlag() {
        return flag;
    }

    /**
     * @param flag the flag to set
     */
    public void setFlag(int flag) {
        this.flag = flag;
    }

    /**
     * @return the birthday
     */
    public String getBirthday() {
        return birthday;
    }

    /**
     * @param birthday the birthday to set
     */
    public void setBirthday(String birthday) {
        this.birthday = birthday;
    }

    /**
     * @return the motherMaiden
     */
    public String getMotherMaiden() {
        return motherMaiden;
    }

    /**
     * @param motherMaiden the motherMaiden to set
     */
    public void setMotherMaiden(String motherMaiden) {
        this.motherMaiden = motherMaiden;
    }
}
