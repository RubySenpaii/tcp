/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package object;

/**
 *
 * @author RubySenpaii
 */
public class Product {
    public static final String TABLE_NAME = "Product";
    public static final String COLUMN_PRODUCT_ID = "ProductID";
    public static final String COLUMN_PRODUCT_NAME = "ProductName";
    public static final String COLUMN_CATEGORY = "Category";
    public static final String COLUMN_TYPE = "Type";
    public static final String COLUMN_DESCRIPTION = "Description";
    public static final String COLUMN_UNIT_PRICE = "UnitPrice";
    public static final String COLUMN_UNIT = "Unit";
    public static final String COLUMN_IMAGE = "Image";
    public static final String COLUMN_CRITICAL_LEVEL = "CriticalLevel";
    public static final String COLUMN_FLAG = "Flag";
    public static final String COLUMN_COST_PRICE = "CostPrice";
    
    private int productID;
    private String productName;
    private String category;
    private String description;
    private double unitPrice;
    private String unit;
    private String image;
    private double criticalLevel;
    private int flag;
    private double costPrice;
    
    //additional attribute
    private int orderQuantity;
    private int stockQuantity;
    private int deliveryQuantity;
    private int receivedQuantity;
    
    private double totalPrice;
    private int salesOrderNumber;
    private String customerName;
    private String dateReturned;
    
    public Product() {
        
    }

    /**
     * @return the productID
     */
    public int getProductID() {
        return productID;
    }

    /**
     * @param productID the productID to set
     */
    public void setProductID(int productID) {
        this.productID = productID;
    }

    /**
     * @return the productName
     */
    public String getProductName() {
        return productName;
    }

    /**
     * @param productName the productName to set
     */
    public void setProductName(String productName) {
        this.productName = productName;
    }

    /**
     * @return the category
     */
    public String getCategory() {
        return category;
    }

    /**
     * @param category the category to set
     */
    public void setCategory(String category) {
        this.category = category;
    }

    /**
     * @return the description
     */
    public String getDescription() {
        return description;
    }

    /**
     * @param description the description to set
     */
    public void setDescription(String description) {
        this.description = description;
    }

    /**
     * @return the unitPrice
     */
    public double getUnitPrice() {
        return unitPrice;
    }

    /**
     * @param unitPrice the unitPrice to set
     */
    public void setUnitPrice(double unitPrice) {
        this.unitPrice = unitPrice;
    }

    /**
     * @return the unit
     */
    public String getUnit() {
        return unit;
    }

    /**
     * @param unit the unit to set
     */
    public void setUnit(String unit) {
        this.unit = unit;
    }

    /**
     * @return the image
     */
    public String getImage() {
        return image;
    }

    /**
     * @param image the image to set
     */
    public void setImage(String image) {
        this.image = image;
    }

    /**
     * @return the criticalLevel
     */
    public double getCriticalLevel() {
        return criticalLevel;
    }

    /**
     * @param criticalLevel the criticalLevel to set
     */
    public void setCriticalLevel(double criticalLevel) {
        this.criticalLevel = criticalLevel;
    }

    /**
     * @return the orderQuantity
     */
    public int getOrderQuantity() {
        return orderQuantity;
    }

    /**
     * @param orderQuantity the orderQuantity to set
     */
    public void setOrderQuantity(int orderQuantity) {
        this.orderQuantity = orderQuantity;
    }

    /**
     * @return the stockQuantity
     */
    public int getStockQuantity() {
        return stockQuantity;
    }

    /**
     * @param stockQuantity the stockQuantity to set
     */
    public void setStockQuantity(int stockQuantity) {
        this.stockQuantity = stockQuantity;
    }

    /**
     * @return the flag
     */
    public int getFlag() {
        return flag;
    }

    /**
     * @param flag the flag to set
     */
    public void setFlag(int flag) {
        this.flag = flag;
    }

    /**
     * @return the deliveryQuantity
     */
    public int getDeliveryQuantity() {
        return deliveryQuantity;
    }

    /**
     * @param deliveryQuantity the deliveryQuantity to set
     */
    public void setDeliveryQuantity(int deliveryQuantity) {
        this.deliveryQuantity = deliveryQuantity;
    }

    /**
     * @return the costPrice
     */
    public double getCostPrice() {
        return costPrice;
    }

    /**
     * @param costPrice the costPrice to set
     */
    public void setCostPrice(double costPrice) {
        this.costPrice = costPrice;
    }

    /**
     * @return the receivedQuantity
     */
    public int getReceivedQuantity() {
        return receivedQuantity;
    }

    /**
     * @param receivedQuantity the receivedQuantity to set
     */
    public void setReceivedQuantity(int receivedQuantity) {
        this.receivedQuantity = receivedQuantity;
    }

    /**
     * @return the totalPrice
     */
    public double getTotalPrice() {
        return totalPrice;
    }

    /**
     * @param totalPrice the totalPrice to set
     */
    public void setTotalPrice(double totalPrice) {
        this.totalPrice = totalPrice;
    }

    /**
     * @return the salesOrderNumber
     */
    public int getSalesOrderNumber() {
        return salesOrderNumber;
    }

    /**
     * @param salesOrderNumber the salesOrderNumber to set
     */
    public void setSalesOrderNumber(int salesOrderNumber) {
        this.salesOrderNumber = salesOrderNumber;
    }

    /**
     * @return the customerName
     */
    public String getCustomerName() {
        return customerName;
    }

    /**
     * @param customerName the customerName to set
     */
    public void setCustomerName(String customerName) {
        this.customerName = customerName;
    }

    /**
     * @return the dateReturned
     */
    public String getDateReturned() {
        return dateReturned;
    }

    /**
     * @param dateReturned the dateReturned to set
     */
    public void setDateReturned(String dateReturned) {
        this.dateReturned = dateReturned;
    }
}
