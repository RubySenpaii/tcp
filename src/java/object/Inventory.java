/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package object;

/**
 *
 * @author RubySenpaii
 */
public class Inventory {
    public static final String TABLE_NAME = "Inventory";
    public static final String COLUMN_PRODUCT_ID = "ProductID";
    public static final String COLUMN_IN = "InvIn";
    public static final String COLUMN_OUT = "InvOut";
    public static final String COLUMN_DEFECT = "InvDefect";
    public static final String COLUMN_TOTAL = "InvTotal";
    public static final String COLUMN_REFERENCE = "Reference";
    public static final String COLUMN_DATE = "DateUpdated";
    
    private int productID;
    private int in;
    private int out;
    private int defect;
    private int total;
    private String reference;
    private String date;
    
    public Inventory() {
        
    }

    /**
     * @return the productID
     */
    public int getProductID() {
        return productID;
    }

    /**
     * @param productID the productID to set
     */
    public void setProductID(int productID) {
        this.productID = productID;
    }

    /**
     * @return the in
     */
    public int getIn() {
        return in;
    }

    /**
     * @param in the in to set
     */
    public void setIn(int in) {
        this.in = in;
    }

    /**
     * @return the out
     */
    public int getOut() {
        return out;
    }

    /**
     * @param out the out to set
     */
    public void setOut(int out) {
        this.out = out;
    }

    /**
     * @return the defect
     */
    public int getDefect() {
        return defect;
    }

    /**
     * @param defect the defect to set
     */
    public void setDefect(int defect) {
        this.defect = defect;
    }

    /**
     * @return the total
     */
    public int getTotal() {
        return total;
    }

    /**
     * @param total the total to set
     */
    public void setTotal(int total) {
        this.total = total;
    }

    /**
     * @return the reference
     */
    public String getReference() {
        return reference;
    }

    /**
     * @param reference the reference to set
     */
    public void setReference(String reference) {
        this.reference = reference;
    }

    /**
     * @return the date
     */
    public String getDate() {
        return date;
    }

    /**
     * @param date the date to set
     */
    public void setDate(String date) {
        this.date = date;
    }
}
