/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package object;

/**
 *
 * @author RubySenpaii
 */
public class SalesPayment {
    public static final String TABLE_NAME = "SalesPayment";
    public static final String COLUMN_PAYMENT_ID = "PaymentID";
    public static final String COLUMN_SALES_ORDER_NUMBER = "SalesOrderNumber";
    public static final String COLUMN_AMOUNT_PAID = "AmountPaid";
    public static final String COLUMN_DATE_PAID = "DatePaid";
    public static final String COLUMN_PROOF = "Proof";
    public static final String COLUMN_APPROVED_BY = "ApprovedBy";
    
    private int paymentID;
    private int salesOrderNumber;
    private double amountPaid;
    private String datePaid;
    private String proof;
    private int approvedBy;
    
    public SalesPayment() {
        
    }

    /**
     * @return the salesOrderNumber
     */
    public int getSalesOrderNumber() {
        return salesOrderNumber;
    }

    /**
     * @param salesOrderNumber the salesOrderNumber to set
     */
    public void setSalesOrderNumber(int salesOrderNumber) {
        this.salesOrderNumber = salesOrderNumber;
    }

    /**
     * @return the amountPaid
     */
    public double getAmountPaid() {
        return amountPaid;
    }

    /**
     * @param amountPaid the amountPaid to set
     */
    public void setAmountPaid(double amountPaid) {
        this.amountPaid = amountPaid;
    }

    /**
     * @return the datePaid
     */
    public String getDatePaid() {
        return datePaid;
    }

    /**
     * @param datePaid the datePaid to set
     */
    public void setDatePaid(String datePaid) {
        this.datePaid = datePaid;
    }

    /**
     * @return the proof
     */
    public String getProof() {
        return proof;
    }

    /**
     * @param proof the proof to set
     */
    public void setProof(String proof) {
        this.proof = proof;
    }

    /**
     * @return the approvedBy
     */
    public int getApprovedBy() {
        return approvedBy;
    }

    /**
     * @param approvedBy the approvedBy to set
     */
    public void setApprovedBy(int approvedBy) {
        this.approvedBy = approvedBy;
    }

    /**
     * @return the paymentID
     */
    public int getPaymentID() {
        return paymentID;
    }

    /**
     * @param paymentID the paymentID to set
     */
    public void setPaymentID(int paymentID) {
        this.paymentID = paymentID;
    }
}
