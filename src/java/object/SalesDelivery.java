/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package object;

/**
 *
 * @author RubySenpaii
 */
public class SalesDelivery {
    public static final String TABLE_NAME = "SalesDelivery";
    public static final String COLUMN_SALES_ORDER_NUMBER =  "SalesOrderNumber";
    public static final String COLUMN_UPDATED_BY = "UpdatedBy";
    public static final String COLUMN_DATE_UPDATED = "DateUpdated";
    public static final String COLUMN_DELIVERY_STATUS = "DeliveryStatus";
    public static final String COLUMN_REMARKS = "Remarks";

    private int salesOrderNumber;
    private int updatedBy;
    private String dateUpdated;
    private String deliveryStatus;
    private String remarks;
    
    private String updatedName;
    
    public SalesDelivery() {
        
    }

    /**
     * @return the salesOrderNumber
     */
    public int getSalesOrderNumber() {
        return salesOrderNumber;
    }

    /**
     * @param salesOrderNumber the salesOrderNumber to set
     */
    public void setSalesOrderNumber(int salesOrderNumber) {
        this.salesOrderNumber = salesOrderNumber;
    }

    /**
     * @return the updatedBy
     */
    public int getUpdatedBy() {
        return updatedBy;
    }

    /**
     * @param updatedBy the updatedBy to set
     */
    public void setUpdatedBy(int updatedBy) {
        this.updatedBy = updatedBy;
    }

    /**
     * @return the dateUpdated
     */
    public String getDateUpdated() {
        return dateUpdated;
    }

    /**
     * @param dateUpdated the dateUpdated to set
     */
    public void setDateUpdated(String dateUpdated) {
        this.dateUpdated = dateUpdated;
    }

    /**
     * @return the deliveryStatus
     */
    public String getDeliveryStatus() {
        return deliveryStatus;
    }

    /**
     * @param deliveryStatus the deliveryStatus to set
     */
    public void setDeliveryStatus(String deliveryStatus) {
        this.deliveryStatus = deliveryStatus;
    }

    /**
     * @return the updatedName
     */
    public String getUpdatedName() {
        return updatedName;
    }

    /**
     * @param updatedName the updatedName to set
     */
    public void setUpdatedName(String updatedName) {
        this.updatedName = updatedName;
    }

    /**
     * @return the remarks
     */
    public String getRemarks() {
        return remarks;
    }

    /**
     * @param remarks the remarks to set
     */
    public void setRemarks(String remarks) {
        this.remarks = remarks;
    }
}
