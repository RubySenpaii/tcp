/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package object;

/**
 *
 * @author RubySenpaii
 */
public class PurchaseReturn {
    public static final String TABLE_NAME = "PurchaseReturn";
    public static final String COLUMN_PURCHASE_ORDER_NUMBER = "PurchaseOrderNumber";
    public static final String COLUMN_PRODUCT_NAME = "ProductName";
    public static final String COLUMN_QUANTITY = "Quantity";
    public static final String COLUMN_RETURN_STATUS = "ReturnStatus";
    
    private int purchaseOrderNumber;
    private String productName;
    private int quantity;
    private String returnStatus;
    
    public PurchaseReturn() {
        
    }

    /**
     * @return the purchaseOrderNumber
     */
    public int getPurchaseOrderNumber() {
        return purchaseOrderNumber;
    }

    /**
     * @param purchaseOrderNumber the purchaseOrderNumber to set
     */
    public void setPurchaseOrderNumber(int purchaseOrderNumber) {
        this.purchaseOrderNumber = purchaseOrderNumber;
    }

    /**
     * @return the productName
     */
    public String getProductName() {
        return productName;
    }

    /**
     * @param productName the productName to set
     */
    public void setProductName(String productName) {
        this.productName = productName;
    }

    /**
     * @return the quantity
     */
    public int getQuantity() {
        return quantity;
    }

    /**
     * @param quantity the quantity to set
     */
    public void setQuantity(int quantity) {
        this.quantity = quantity;
    }

    /**
     * @return the returnStatus
     */
    public String getReturnStatus() {
        return returnStatus;
    }

    /**
     * @param returnStatus the returnStatus to set
     */
    public void setReturnStatus(String returnStatus) {
        this.returnStatus = returnStatus;
    }
}
