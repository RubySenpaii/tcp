/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package object;

/**
 *
 * @author RubySenpaii
 */
public class PurchaseOrder {
    public static final String TABLE_NAME = "PurchaseOrder";
    public static final String COLUMN_PURCHASE_ORDER_NUMBER = "PurchaseOrderNumber";
    public static final String COLUMN_ORDER_DATE = "OrderDate";
    public static final String COLUMN_SUPPLIER_ID = "SupplierID";
    public static final String COLUMN_PREPARED_BY = "PreparedBy";
    public static final String COLUMN_APPROVED_BY = "ApprovedBy";
    public static final String COLUMN_CONTACT_NO = "ContactNo";
    public static final String COLUMN_CONTACT_PERSON = "ContactPerson";
    public static final String COLUMN_ADDRESS = "Address";
    public static final String COLUMN_ORDER_STATUS = "OrderStatus";
    public static final String COLUMN_REQUESTED_DELIVERY = "RequestedDelivery";
    public static final String COLUMN_DELIVERY_DATE = "DeliveryDate";
    public static final String COLUMN_ORDER_TOTAL = "OrderTotal";
    public static final String COLUMN_PAYMENT_OPTION = "PaymentOption";
    public static final String COLUMN_REMARKS = "Remarks";
    
    private int purchaseOrderNumber;
    private String orderDate;
    private int supplierID;
    private int preparedBy;
    private int approvedBy;
    private String contactNo;
    private String contactPerson;
    private String address;
    private String orderStatus;
    private String requestedDelivery;
    private String deliveryDate;
    private String paymentOption;
    private double orderTotal;
    private String remarks;
    
    private String supplierName;
    private String preparerName;
    private String approverName;
    
    public PurchaseOrder() {
        
    }

    /**
     * @return the purchaseOrderNumber
     */
    public int getPurchaseOrderNumber() {
        return purchaseOrderNumber;
    }

    /**
     * @param purchaseOrderNumber the purchaseOrderNumber to set
     */
    public void setPurchaseOrderNumber(int purchaseOrderNumber) {
        this.purchaseOrderNumber = purchaseOrderNumber;
    }

    /**
     * @return the orderDate
     */
    public String getOrderDate() {
        return orderDate;
    }

    /**
     * @param orderDate the orderDate to set
     */
    public void setOrderDate(String orderDate) {
        this.orderDate = orderDate;
    }

    /**
     * @return the supplierID
     */
    public int getSupplierID() {
        return supplierID;
    }

    /**
     * @param supplierID the supplierID to set
     */
    public void setSupplierID(int supplierID) {
        this.supplierID = supplierID;
    }

    /**
     * @return the preparedBy
     */
    public int getPreparedBy() {
        return preparedBy;
    }

    /**
     * @param preparedBy the preparedBy to set
     */
    public void setPreparedBy(int preparedBy) {
        this.preparedBy = preparedBy;
    }

    /**
     * @return the address
     */
    public String getAddress() {
        return address;
    }

    /**
     * @param address the address to set
     */
    public void setAddress(String address) {
        this.address = address;
    }

    /**
     * @return the deliveryDate
     */
    public String getDeliveryDate() {
        return deliveryDate;
    }

    /**
     * @param deliveryDate the deliveryDate to set
     */
    public void setDeliveryDate(String deliveryDate) {
        this.deliveryDate = deliveryDate;
    }

    /**
     * @return the orderTotal
     */
    public double getOrderTotal() {
        return orderTotal;
    }

    /**
     * @param orderTotal the orderTotal to set
     */
    public void setOrderTotal(double orderTotal) {
        this.orderTotal = orderTotal;
    }

    /**
     * @return the remarks
     */
    public String getRemarks() {
        return remarks;
    }

    /**
     * @param remarks the remarks to set
     */
    public void setRemarks(String remarks) {
        this.remarks = remarks;
    }

    /**
     * @return the supplierName
     */
    public String getSupplierName() {
        return supplierName;
    }

    /**
     * @param supplierName the supplierName to set
     */
    public void setSupplierName(String supplierName) {
        this.supplierName = supplierName;
    }

    /**
     * @return the preparerName
     */
    public String getPreparerName() {
        return preparerName;
    }

    /**
     * @param preparerName the preparerName to set
     */
    public void setPreparerName(String preparerName) {
        this.preparerName = preparerName;
    }

    /**
     * @return the requestedDelivery
     */
    public String getRequestedDelivery() {
        return requestedDelivery;
    }

    /**
     * @param requestedDelivery the requestedDelivery to set
     */
    public void setRequestedDelivery(String requestedDelivery) {
        this.requestedDelivery = requestedDelivery;
    }

    /**
     * @return the approvedBy
     */
    public int getApprovedBy() {
        return approvedBy;
    }

    /**
     * @param approvedBy the approvedBy to set
     */
    public void setApprovedBy(int approvedBy) {
        this.approvedBy = approvedBy;
    }

    /**
     * @return the orderStatus
     */
    public String getOrderStatus() {
        return orderStatus;
    }

    /**
     * @param orderStatus the orderStatus to set
     */
    public void setOrderStatus(String orderStatus) {
        this.orderStatus = orderStatus;
    }

    /**
     * @return the contactNo
     */
    public String getContactNo() {
        return contactNo;
    }

    /**
     * @param contactNo the contactNo to set
     */
    public void setContactNo(String contactNo) {
        this.contactNo = contactNo;
    }

    /**
     * @return the contactPerson
     */
    public String getContactPerson() {
        return contactPerson;
    }

    /**
     * @param contactPerson the contactPerson to set
     */
    public void setContactPerson(String contactPerson) {
        this.contactPerson = contactPerson;
    }

    /**
     * @return the paymentOption
     */
    public String getPaymentOption() {
        return paymentOption;
    }

    /**
     * @param paymentOption the paymentOption to set
     */
    public void setPaymentOption(String paymentOption) {
        this.paymentOption = paymentOption;
    }

    /**
     * @return the approverName
     */
    public String getApproverName() {
        return approverName;
    }

    /**
     * @param approverName the approverName to set
     */
    public void setApproverName(String approverName) {
        this.approverName = approverName;
    }
}
