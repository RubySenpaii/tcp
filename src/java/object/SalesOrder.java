/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package object;

/**
 *
 * @author RubySenpaii
 */
public class SalesOrder {
    public static final String TABLE_NAME = "SalesOrder";
    public static final String COLUMN_SALES_ORDER_NUMBER = "SalesOrderNumber";
    public static final String COLUMN_ORDER_DATE = "OrderDate";
    public static final String COLUMN_CUSTOMER_ID = "CustomerID";
    public static final String COLUMN_APPROVED_BY = "ApprovedBy";
    public static final String COLUMN_CONTACT_PERSON = "ContactPerson";
    public static final String COLUMN_CONTACT_NO = "ContactNo";
    public static final String COLUMN_ADDRESS = "Address";
    public static final String COLUMN_ORDER_STATUS = "OrderStatus";
    public static final String COLUMN_ORDER_TOTAL = "OrderTotal";
    public static final String COLUMN_DELIVERED_BY = "DeliveredBy";
    public static final String COLUMN_ASSIGNED_TRUCK = "AssignedTruck";
    public static final String COLUMN_REQUEST_DELIVERY = "RequestDelivery";
    public static final String COLUMN_REMARKS = "Remarks";
    public static final String COLUMN_RECEIVED_BY = "ReceivedBy";
    public static final String COLUMN_RECEIVED_DATE = "ReceivedDate";
    public static final String COLUMN_PAYMENT_OPTION = "PaymentOption";
    
    private int salesOrderNumber;
    private String orderDate;
    private int customerID;
    private int approvedBy;
    private String contactPerson;
    private String contactNo;
    private String address;
    private String orderStatus;
    private double orderTotal;
    private int deliveredBy;
    private int assignedTruck;
    private String requestDelivery;
    private String remarks;
    private String receivedBy;
    private String receivedDate;
    private String paymentOption;
    
    private String approver;
    private String customerName;
    private String deliveryStatus;
    private String deliveryUpdate;
    
    private String month;
    private String deliverer;
    private double totalSales;
    
    private String description;
    
    public SalesOrder() {
        
    }

    /**
     * @return the salesOrderNumber
     */
    public int getSalesOrderNumber() {
        return salesOrderNumber;
    }

    /**
     * @param salesOrderNumber the salesOrderNumber to set
     */
    public void setSalesOrderNumber(int salesOrderNumber) {
        this.salesOrderNumber = salesOrderNumber;
    }

    /**
     * @return the orderDate
     */
    public String getOrderDate() {
        return orderDate;
    }

    /**
     * @param orderDate the orderDate to set
     */
    public void setOrderDate(String orderDate) {
        this.orderDate = orderDate;
    }

    /**
     * @return the customerID
     */
    public int getCustomerID() {
        return customerID;
    }

    /**
     * @param customerID the customerID to set
     */
    public void setCustomerID(int customerID) {
        this.customerID = customerID;
    }

    /**
     * @return the approvedBy
     */
    public int getApprovedBy() {
        return approvedBy;
    }

    /**
     * @param approvedBy the approvedBy to set
     */
    public void setApprovedBy(int approvedBy) {
        this.approvedBy = approvedBy;
    }

    /**
     * @return the address
     */
    public String getAddress() {
        return address;
    }

    /**
     * @param address the address to set
     */
    public void setAddress(String address) {
        this.address = address;
    }

    /**
     * @return the orderStatus
     */
    public String getOrderStatus() {
        return orderStatus;
    }

    /**
     * @param orderStatus the orderStatus to set
     */
    public void setOrderStatus(String orderStatus) {
        this.orderStatus = orderStatus;
    }

    /**
     * @return the orderTotal
     */
    public double getOrderTotal() {
        return orderTotal;
    }

    /**
     * @param orderTotal the orderTotal to set
     */
    public void setOrderTotal(double orderTotal) {
        this.orderTotal = orderTotal;
    }

    /**
     * @return the deliveredBy
     */
    public int getDeliveredBy() {
        return deliveredBy;
    }

    /**
     * @param deliveredBy the deliveredBy to set
     */
    public void setDeliveredBy(int deliveredBy) {
        this.deliveredBy = deliveredBy;
    }

    /**
     * @return the remarks
     */
    public String getRemarks() {
        return remarks;
    }

    /**
     * @param remarks the remarks to set
     */
    public void setRemarks(String remarks) {
        this.remarks = remarks;
    }

    /**
     * @return the approver
     */
    public String getApprover() {
        return approver;
    }

    /**
     * @return the contactPerson
     */
    public String getContactPerson() {
        return contactPerson;
    }

    /**
     * @param contactPerson the contactPerson to set
     */
    public void setContactPerson(String contactPerson) {
        this.contactPerson = contactPerson;
    }

    /**
     * @return the contactNo
     */
    public String getContactNo() {
        return contactNo;
    }

    /**
     * @param contactNo the contactNo to set
     */
    public void setContactNo(String contactNo) {
        this.contactNo = contactNo;
    }

    /**
     * @param approver the approver to set
     */
    public void setApprover(String approver) {
        this.approver = approver;
    }

    /**
     * @return the customerName
     */
    public String getCustomerName() {
        return customerName;
    }

    /**
     * @param customerName the customerName to set
     */
    public void setCustomerName(String customerName) {
        this.customerName = customerName;
    }

    /**
     * @return the requestDelivery
     */
    public String getRequestDelivery() {
        return requestDelivery;
    }

    /**
     * @param requestDelivery the requestDelivery to set
     */
    public void setRequestDelivery(String requestDelivery) {
        this.requestDelivery = requestDelivery;
    }

    /**
     * @return the deliveryStatus
     */
    public String getDeliveryStatus() {
        return deliveryStatus;
    }

    /**
     * @param deliveryStatus the deliveryStatus to set
     */
    public void setDeliveryStatus(String deliveryStatus) {
        this.deliveryStatus = deliveryStatus;
    }

    /**
     * @return the deliveryUpdate
     */
    public String getDeliveryUpdate() {
        return deliveryUpdate;
    }

    /**
     * @param deliveryUpdate the deliveryUpdate to set
     */
    public void setDeliveryUpdate(String deliveryUpdate) {
        this.deliveryUpdate = deliveryUpdate;
    }

    /**
     * @return the assignedTruck
     */
    public int getAssignedTruck() {
        return assignedTruck;
    }

    /**
     * @param assignedTruck the assignedTruck to set
     */
    public void setAssignedTruck(int assignedTruck) {
        this.assignedTruck = assignedTruck;
    }

    /**
     * @return the receivedBy
     */
    public String getReceivedBy() {
        return receivedBy;
    }

    /**
     * @param receivedBy the receivedBy to set
     */
    public void setReceivedBy(String receivedBy) {
        this.receivedBy = receivedBy;
    }

    /**
     * @return the receivedDate
     */
    public String getReceivedDate() {
        return receivedDate;
    }

    /**
     * @param receivedDate the receivedDate to set
     */
    public void setReceivedDate(String receivedDate) {
        this.receivedDate = receivedDate;
    }

    /**
     * @return the month
     */
    public String getMonth() {
        return month;
    }

    /**
     * @param month the month to set
     */
    public void setMonth(String month) {
        this.month = month;
    }

    /**
     * @return the totalSales
     */
    public double getTotalSales() {
        return totalSales;
    }

    /**
     * @param totalSales the totalSales to set
     */
    public void setTotalSales(double totalSales) {
        this.totalSales = totalSales;
    }

    /**
     * @return the paymentOption
     */
    public String getPaymentOption() {
        return paymentOption;
    }

    /**
     * @param paymentOption the paymentOption to set
     */
    public void setPaymentOption(String paymentOption) {
        this.paymentOption = paymentOption;
    }

    /**
     * @return the deliverer
     */
    public String getDeliverer() {
        return deliverer;
    }

    /**
     * @param deliverer the deliverer to set
     */
    public void setDeliverer(String deliverer) {
        this.deliverer = deliverer;
    }

    /**
     * @return the description
     */
    public String getDescription() {
        return description;
    }

    /**
     * @param description the description to set
     */
    public void setDescription(String description) {
        this.description = description;
    }
}
