/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package object;

/**
 *
 * @author RubySenpaii
 */
public class SupplierItem {
    public static final String TABLE_NAME = "SupplierItem";
    public static final String COLUMN_SUPPLIER_ID = "SupplierID";
    public static final String COLUMN_PRODUCT_NAME = "ProductName";
    public static final String COLUMN_UNIT_PRICE = "UnitPrice";
    
    private int supplierID;
    private String productName;
    private double unitPrice;
    
    public SupplierItem() {
        
    }

    /**
     * @return the supplierID
     */
    public int getSupplierID() {
        return supplierID;
    }

    /**
     * @param supplierID the supplierID to set
     */
    public void setSupplierID(int supplierID) {
        this.supplierID = supplierID;
    }

    /**
     * @return the productName
     */
    public String getProductName() {
        return productName;
    }

    /**
     * @param productName the productName to set
     */
    public void setProductName(String productName) {
        this.productName = productName;
    }

    /**
     * @return the unitPrice
     */
    public double getUnitPrice() {
        return unitPrice;
    }

    /**
     * @param unitPrice the unitPrice to set
     */
    public void setUnitPrice(double unitPrice) {
        this.unitPrice = unitPrice;
    }
}
