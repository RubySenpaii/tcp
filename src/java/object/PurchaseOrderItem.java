/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package object;

/**
 *
 * @author RubySenpaii
 */
public class PurchaseOrderItem {
    public static final String TABLE_NAME = "PurchaseOrderItem";
    public static final String COLUMN_PURCHASE_ORDER_NUMBER = "PurchaseOrderNumber";
    public static final String COLUMN_PRODUCT_ID = "ProductID";
    public static final String COLUMN_UNIT_PRICE = "UnitPrice";
    public static final String COLUMN_QUANTITY = "Quantity";
    
    private int purchaseOrderNumber;
    private int productID;
    private double unitPrice;
    private int quantity;
    
    private String productName;
    
    public PurchaseOrderItem() {
        
    }

    /**
     * @return the purchaseOrderNumber
     */
    public int getPurchaseOrderNumber() {
        return purchaseOrderNumber;
    }

    /**
     * @param purchaseOrderNumber the purchaseOrderNumber to set
     */
    public void setPurchaseOrderNumber(int purchaseOrderNumber) {
        this.purchaseOrderNumber = purchaseOrderNumber;
    }

    /**
     * @return the productID
     */
    public int getProductID() {
        return productID;
    }

    /**
     * @param productID the productID to set
     */
    public void setProductID(int productID) {
        this.productID = productID;
    }

    /**
     * @return the unitPrice
     */
    public double getUnitPrice() {
        return unitPrice;
    }

    /**
     * @param unitPrice the unitPrice to set
     */
    public void setUnitPrice(double unitPrice) {
        this.unitPrice = unitPrice;
    }

    /**
     * @return the quantity
     */
    public int getQuantity() {
        return quantity;
    }

    /**
     * @param quantity the quantity to set
     */
    public void setQuantity(int quantity) {
        this.quantity = quantity;
    }

    /**
     * @return the productName
     */
    public String getProductName() {
        return productName;
    }

    /**
     * @param productName the productName to set
     */
    public void setProductName(String productName) {
        this.productName = productName;
    }
}
