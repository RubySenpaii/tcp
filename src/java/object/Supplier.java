/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package object;

/**
 *
 * @author RubySenpaii
 */
public class Supplier {
    public static final String TABLE_NAME = "Supplier";
    public static final String COLUMN_SUPPLIER_ID = "SupplierID";
    public static final String COLUMN_SUPPLIER_NAME = "SupplierName";
    public static final String COLUMN_ADDRESS = "Address";
    public static final String COLUMN_CONTACT_PERSON = "ContactPerson";
    public static final String COLUMN_CONTACT_NO = "ContactNo";
    public static final String COLUMN_EMAIL = "Email";
    public static final String COLUMN_FLAG = "Flag";
    public static final String COLUMN_PUNCTUALITY_RATING = "PunctualityRating";
    public static final String COLUMN_VOLUME_RATING = "VolumeRating";
    public static final String COLUMN_FRIENDLY_RATING = "FriendlyRating";
    public static final String COLUMN_PAYMENT_RATING = "PaymentRating";
    public static final String COLUMN_REMARKS = "Remarks";
    
    private int supplierID;
    private String supplierName;
    private String address;
    private String contactPerson;
    private String contactNo;
    private String email;
    private int flag;
    private int punctualityRating;
    private int volumeRating;
    private int friendlyRating;
    private int paymentRating;
    private String remarks;
    
    private int averageRating;
    
    public Supplier() {
        
    }

    /**
     * @return the supplierID
     */
    public int getSupplierID() {
        return supplierID;
    }

    /**
     * @param supplierID the supplierID to set
     */
    public void setSupplierID(int supplierID) {
        this.supplierID = supplierID;
    }

    /**
     * @return the supplierName
     */
    public String getSupplierName() {
        return supplierName;
    }

    /**
     * @param supplierName the supplierName to set
     */
    public void setSupplierName(String supplierName) {
        this.supplierName = supplierName;
    }

    /**
     * @return the address
     */
    public String getAddress() {
        return address;
    }

    /**
     * @param address the address to set
     */
    public void setAddress(String address) {
        this.address = address;
    }

    /**
     * @return the contactPerson
     */
    public String getContactPerson() {
        return contactPerson;
    }

    /**
     * @param contactPerson the contactPerson to set
     */
    public void setContactPerson(String contactPerson) {
        this.contactPerson = contactPerson;
    }

    /**
     * @return the contactNo
     */
    public String getContactNo() {
        return contactNo;
    }

    /**
     * @param contactNo the contactNo to set
     */
    public void setContactNo(String contactNo) {
        this.contactNo = contactNo;
    }

    /**
     * @return the email
     */
    public String getEmail() {
        return email;
    }

    /**
     * @param email the email to set
     */
    public void setEmail(String email) {
        this.email = email;
    }

    /**
     * @return the flag
     */
    public int getFlag() {
        return flag;
    }

    /**
     * @param flag the flag to set
     */
    public void setFlag(int flag) {
        this.flag = flag;
    }

    /**
     * @return the punctualityRating
     */
    public int getPunctualityRating() {
        return punctualityRating;
    }

    /**
     * @param punctualityRating the punctualityRating to set
     */
    public void setPunctualityRating(int punctualityRating) {
        this.punctualityRating = punctualityRating;
    }

    /**
     * @return the volumeRating
     */
    public int getVolumeRating() {
        return volumeRating;
    }

    /**
     * @param volumeRating the volumeRating to set
     */
    public void setVolumeRating(int volumeRating) {
        this.volumeRating = volumeRating;
    }

    /**
     * @return the friendlyRating
     */
    public int getFriendlyRating() {
        return friendlyRating;
    }

    /**
     * @param friendlyRating the friendlyRating to set
     */
    public void setFriendlyRating(int friendlyRating) {
        this.friendlyRating = friendlyRating;
    }

    /**
     * @return the paymentRating
     */
    public int getPaymentRating() {
        return paymentRating;
    }

    /**
     * @param paymentRating the paymentRating to set
     */
    public void setPaymentRating(int paymentRating) {
        this.paymentRating = paymentRating;
    }

    /**
     * @return the remarks
     */
    public String getRemarks() {
        return remarks;
    }

    /**
     * @param remarks the remarks to set
     */
    public void setRemarks(String remarks) {
        this.remarks = remarks;
    }

    /**
     * @return the averageRating
     */
    public int getAverageRating() {
        return averageRating;
    }

    /**
     * @param averageRating the averageRating to set
     */
    public void setAverageRating(int averageRating) {
        this.averageRating = averageRating;
    }
}
