/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package object;

/**
 *
 * @author RubySenpaii
 */
public class TruckReference {
    public static final String TABLE_NAME = "TruckReference";
    public static final String COLUMN_TRUCK_ID = "TruckID";
    public static final String COLUMN_PLATE_NUMBER = "PlateNumber";
    public static final String COLUMN_CAPACITY = "Capacity";
    
    private int truckID;
    private String plateNumber;
    private int capacity;
    
    //additional info
    private int salesOrderNumber;
    private String productName;
    private int quantity;
    private int totalQuantity;
    private String address;
    
    private String details;
    private String customerName;
    private String truckDelivery;
    private String deliverDate;
    
    public TruckReference() {
        
    }

    /**
     * @return the truckID
     */
    public int getTruckID() {
        return truckID;
    }

    /**
     * @param truckID the truckID to set
     */
    public void setTruckID(int truckID) {
        this.truckID = truckID;
    }

    /**
     * @return the plateNumber
     */
    public String getPlateNumber() {
        return plateNumber;
    }

    /**
     * @param plateNumber the plateNumber to set
     */
    public void setPlateNumber(String plateNumber) {
        this.plateNumber = plateNumber;
    }

    /**
     * @return the capacity
     */
    public int getCapacity() {
        return capacity;
    }

    /**
     * @param capacity the capacity to set
     */
    public void setCapacity(int capacity) {
        this.capacity = capacity;
    }

    /**
     * @return the salesOrderNumber
     */
    public int getSalesOrderNumber() {
        return salesOrderNumber;
    }

    /**
     * @param salesOrderNumber the salesOrderNumber to set
     */
    public void setSalesOrderNumber(int salesOrderNumber) {
        this.salesOrderNumber = salesOrderNumber;
    }

    /**
     * @return the productName
     */
    public String getProductName() {
        return productName;
    }

    /**
     * @param productName the productName to set
     */
    public void setProductName(String productName) {
        this.productName = productName;
    }

    /**
     * @return the quantity
     */
    public int getQuantity() {
        return quantity;
    }

    /**
     * @param quantity the quantity to set
     */
    public void setQuantity(int quantity) {
        this.quantity = quantity;
    }

    /**
     * @return the address
     */
    public String getAddress() {
        return address;
    }

    /**
     * @param address the address to set
     */
    public void setAddress(String address) {
        this.address = address;
    }

    /**
     * @return the details
     */
    public String getDetails() {
        return details;
    }

    /**
     * @param details the details to set
     */
    public void setDetails(String details) {
        this.details = details;
    }

    /**
     * @return the totalQuantity
     */
    public int getTotalQuantity() {
        return totalQuantity;
    }

    /**
     * @param totalQuantity the totalQuantity to set
     */
    public void setTotalQuantity(int totalQuantity) {
        this.totalQuantity = totalQuantity;
    }

    /**
     * @return the customerName
     */
    public String getCustomerName() {
        return customerName;
    }

    /**
     * @param customerName the customerName to set
     */
    public void setCustomerName(String customerName) {
        this.customerName = customerName;
    }

    /**
     * @return the truckDelivery
     */
    public String getTruckDelivery() {
        return truckDelivery;
    }

    /**
     * @param truckDelivery the truckDelivery to set
     */
    public void setTruckDelivery(String truckDelivery) {
        this.truckDelivery = truckDelivery;
    }

    /**
     * @return the deliverDate
     */
    public String getDeliverDate() {
        return deliverDate;
    }

    /**
     * @param deliverDate the deliverDate to set
     */
    public void setDeliverDate(String deliverDate) {
        this.deliverDate = deliverDate;
    }
}
