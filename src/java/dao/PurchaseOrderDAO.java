/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package dao;

import db.DBConnectionFactory;
import extra.Reference;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.logging.Level;
import java.util.logging.Logger;
import object.Employee;
import object.PurchaseOrder;

/**
 *
 * @author RubySenpaii
 */
public class PurchaseOrderDAO {

    public boolean addPurchaseOrder(PurchaseOrder purchaseOrder) {
        try {
            DBConnectionFactory myFactory = DBConnectionFactory.getInstance();
            Connection conn = myFactory.getConnection();

            PreparedStatement ps = conn.prepareStatement("INSERT INTO " + PurchaseOrder.TABLE_NAME + " "
                    + "(" + PurchaseOrder.COLUMN_ADDRESS + ", " + PurchaseOrder.COLUMN_DELIVERY_DATE + ", " + PurchaseOrder.COLUMN_ORDER_DATE
                    + ", " + PurchaseOrder.COLUMN_ORDER_TOTAL + ", " + PurchaseOrder.COLUMN_PREPARED_BY + ", " + PurchaseOrder.COLUMN_APPROVED_BY
                    + ", " + PurchaseOrder.COLUMN_PURCHASE_ORDER_NUMBER + ", " + PurchaseOrder.COLUMN_REMARKS + ", " + PurchaseOrder.COLUMN_ORDER_STATUS
                    + ", " + PurchaseOrder.COLUMN_CONTACT_NO + ", " + PurchaseOrder.COLUMN_CONTACT_PERSON + ", " + PurchaseOrder.COLUMN_PAYMENT_OPTION
                    + ", " + PurchaseOrder.COLUMN_REQUESTED_DELIVERY + ", " + PurchaseOrder.COLUMN_SUPPLIER_ID + ") "
                    + "VALUES(?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)");
            ps.setString(1, purchaseOrder.getAddress());
            ps.setString(2, purchaseOrder.getDeliveryDate());
            ps.setString(3, purchaseOrder.getOrderDate());
            ps.setDouble(4, purchaseOrder.getOrderTotal());
            ps.setInt(5, purchaseOrder.getPreparedBy());
            ps.setInt(6, purchaseOrder.getApprovedBy());
            ps.setInt(7, purchaseOrder.getPurchaseOrderNumber());
            ps.setString(8, purchaseOrder.getRemarks());
            ps.setString(9, purchaseOrder.getOrderStatus());
            ps.setString(10, purchaseOrder.getContactNo());
            ps.setString(11, purchaseOrder.getContactPerson());
            ps.setString(12, purchaseOrder.getPaymentOption());
            ps.setString(13, purchaseOrder.getRequestedDelivery());
            ps.setInt(14, purchaseOrder.getSupplierID());

            ps.executeUpdate();
            ps.close();
            conn.close();
        } catch (SQLException x) {
            Logger.getLogger(PurchaseOrderDAO.class.getName()).log(Level.SEVERE, null, x);
            return false;
        }
        return true;
    }

    public boolean updatePurchaseOrderInfo(PurchaseOrder purchaseOrder) {
        try {
            DBConnectionFactory myFactory = DBConnectionFactory.getInstance();
            Connection conn = myFactory.getConnection();

            PreparedStatement ps = conn.prepareStatement("UPDATE " + PurchaseOrder.TABLE_NAME
                    + " SET " + PurchaseOrder.COLUMN_DELIVERY_DATE + " = ?, " + PurchaseOrder.COLUMN_APPROVED_BY + " = ?, "
                    + PurchaseOrder.COLUMN_ORDER_STATUS + " = ?, " + PurchaseOrder.COLUMN_ADDRESS + " = ?, " + PurchaseOrder.COLUMN_CONTACT_PERSON + " = ?, "
                    + PurchaseOrder.COLUMN_CONTACT_NO + " = ?, " + PurchaseOrder.COLUMN_PAYMENT_OPTION + " = ?, " + PurchaseOrder.COLUMN_REQUESTED_DELIVERY + " = ?, "
                    + PurchaseOrder.COLUMN_ORDER_TOTAL + " = ?, " + PurchaseOrder.COLUMN_REMARKS + " = ? "
                    + "WHERE " + PurchaseOrder.COLUMN_PURCHASE_ORDER_NUMBER + " = ?");
            ps.setString(1, purchaseOrder.getDeliveryDate());
            ps.setInt(2, purchaseOrder.getApprovedBy());
            ps.setString(3, purchaseOrder.getOrderStatus());
            ps.setString(4, purchaseOrder.getAddress());
            ps.setString(5, purchaseOrder.getContactPerson());
            ps.setString(6, purchaseOrder.getContactNo());
            ps.setString(7, purchaseOrder.getPaymentOption());
            ps.setString(8, purchaseOrder.getRequestedDelivery());
            ps.setDouble(9, purchaseOrder.getOrderTotal());
            ps.setString(10, purchaseOrder.getRemarks());
            ps.setInt(11, purchaseOrder.getPurchaseOrderNumber());
            ps.executeUpdate();
            ps.close();
            conn.close();
        } catch (SQLException ex) {
            Logger.getLogger(PurchaseOrderDAO.class.getName()).log(Level.SEVERE, null, ex);
            return false;
        }
        return true;
    }

    public ArrayList<PurchaseOrder> getListOfApprovedPurchaseOrders() {
        ArrayList<PurchaseOrder> purchaseOrderItems = new ArrayList<>();
        try {
            String status = "Approved";
            DBConnectionFactory myFactory = DBConnectionFactory.getInstance();
            Connection conn = myFactory.getConnection();

            PreparedStatement ps = conn.prepareStatement("SELECT * FROM " + PurchaseOrder.TABLE_NAME
                    + " WHERE " + PurchaseOrder.COLUMN_ORDER_STATUS + " = ? "
                    + " ORDER BY STR_TO_DATE(" + PurchaseOrder.COLUMN_ORDER_DATE + ", '%Y-%m-%d %H:%i:%s') DESC");
            ps.setString(1, status);

            ResultSet rs = ps.executeQuery();
            purchaseOrderItems = getDataFromResultSet(rs);

            rs.close();
            ps.close();
            conn.close();
        } catch (SQLException x) {
            Logger.getLogger(PurchaseOrderDAO.class.getName()).log(Level.SEVERE, null, x);
        }
        return purchaseOrderItems;
    }

    public ArrayList<PurchaseOrder> getListOfPendingPurchaseOrders() {
        ArrayList<PurchaseOrder> purchaseOrderItems = new ArrayList<>();
        try {
            DBConnectionFactory myFactory = DBConnectionFactory.getInstance();
            Connection conn = myFactory.getConnection();

            PreparedStatement ps = conn.prepareStatement("SELECT * FROM " + PurchaseOrder.TABLE_NAME + " WHERE " + PurchaseOrder.COLUMN_ORDER_STATUS + " = ?");
            ps.setString(1, "Pending");

            ResultSet rs = ps.executeQuery();
            purchaseOrderItems = getDataFromResultSet(rs);

            rs.close();
            ps.close();
            conn.close();
        } catch (SQLException x) {
            Logger.getLogger(PurchaseOrderDAO.class.getName()).log(Level.SEVERE, null, x);
        }
        return purchaseOrderItems;
    }

    public ArrayList<PurchaseOrder> getListOfPurchaseOrders() {
        ArrayList<PurchaseOrder> purchaseOrderItems = new ArrayList<>();
        try {
            DBConnectionFactory myFactory = DBConnectionFactory.getInstance();
            Connection conn = myFactory.getConnection();

            PreparedStatement ps = conn.prepareStatement("SELECT * FROM " + PurchaseOrder.TABLE_NAME);

            ResultSet rs = ps.executeQuery();
            purchaseOrderItems = getDataFromResultSet(rs);

            rs.close();
            ps.close();
            conn.close();
        } catch (SQLException x) {
            Logger.getLogger(PurchaseOrderDAO.class.getName()).log(Level.SEVERE, null, x);
        }
        return purchaseOrderItems;
    }
    
    public ArrayList<PurchaseOrder> getListOfOrdersForReport(String startDate, String endDate) {
        ArrayList<PurchaseOrder> purchaseOrders = new ArrayList<>();
        try {
            DBConnectionFactory myFactory = DBConnectionFactory.getInstance();
            Connection conn = myFactory.getConnection();

            PreparedStatement ps = conn.prepareStatement("SELECT * FROM " + PurchaseOrder.TABLE_NAME + " WHERE " + PurchaseOrder.COLUMN_ORDER_STATUS + " != ? "
                    + "AND " + PurchaseOrder.COLUMN_ORDER_STATUS + " != ? AND STR_TO_DATE(" + PurchaseOrder.COLUMN_ORDER_DATE + ", '%Y-%m-%d') >= STR_TO_DATE(?, '%Y-%m-%d') "
                    + " AND STR_TO_DATE(" + PurchaseOrder.COLUMN_ORDER_DATE + ", '%Y-%m-%d') <= STR_TO_DATE(?, '%Y-%m-%d')");
            ps.setString(1, "Rejected");
            ps.setString(2, "Pending");
            ps.setString(3, startDate);
            ps.setString(4, endDate);

            ResultSet rs = ps.executeQuery();
            purchaseOrders = getDataFromResultSet(rs);

            rs.close();
            ps.close();
            conn.close();
        } catch (SQLException x) {
            Logger.getLogger(PurchaseOrderDAO.class.getName()).log(Level.SEVERE, null, x);
        }
        return purchaseOrders;
    }

    public ArrayList<PurchaseOrder> getListOfPurchaseOrdersDueForDelivery() {
        ArrayList<PurchaseOrder> purchaseOrderItems = new ArrayList<>();
        try {
            DBConnectionFactory myFactory = DBConnectionFactory.getInstance();
            Connection conn = myFactory.getConnection();

            PreparedStatement ps = conn.prepareStatement("select * from purchaseorder "
                    + "where deliverydate = \"9999-12-31\" and str_to_date(?, '%Y-%m-%d') >= str_to_date(requesteddelivery, '%Y-%m-%d') and orderstatus = 'Approved';");
            ps.setString(1, Reference.DATABASE_DATE_FORMAT.format(Calendar.getInstance().getTime()));

            ResultSet rs = ps.executeQuery();
            purchaseOrderItems = getDataFromResultSet(rs);

            rs.close();
            ps.close();
            conn.close();
        } catch (SQLException x) {
            Logger.getLogger(PurchaseOrderDAO.class.getName()).log(Level.SEVERE, null, x);
        }
        return purchaseOrderItems;
    }

    public PurchaseOrder getPurchaseOrderDetail(int purchaseOrderNumber) {
        ArrayList<PurchaseOrder> purchaseOrderItems = new ArrayList<>();
        try {
            DBConnectionFactory myFactory = DBConnectionFactory.getInstance();
            Connection conn = myFactory.getConnection();

            PreparedStatement ps = conn.prepareStatement("SELECT * FROM " + PurchaseOrder.TABLE_NAME + " WHERE " + PurchaseOrder.COLUMN_PURCHASE_ORDER_NUMBER + " = ?");
            ps.setInt(1, purchaseOrderNumber);

            ResultSet rs = ps.executeQuery();
            purchaseOrderItems = getDataFromResultSet(rs);

            rs.close();
            ps.close();
            conn.close();
        } catch (SQLException x) {
            Logger.getLogger(PurchaseOrderDAO.class.getName()).log(Level.SEVERE, null, x);
        }
        return purchaseOrderItems.get(0);
    }

    private ArrayList<PurchaseOrder> getDataFromResultSet(ResultSet rs) throws SQLException {
        ArrayList<PurchaseOrder> purchaseOrders = new ArrayList<>();
        while (rs.next()) {
            PurchaseOrder purchaseOrder = new PurchaseOrder();
            purchaseOrder.setAddress(rs.getString(PurchaseOrder.COLUMN_ADDRESS));
            purchaseOrder.setApprovedBy(rs.getInt(PurchaseOrder.COLUMN_APPROVED_BY));
            purchaseOrder.setDeliveryDate(rs.getString(PurchaseOrder.COLUMN_DELIVERY_DATE));
            purchaseOrder.setOrderDate(rs.getString(PurchaseOrder.COLUMN_ORDER_DATE));
            purchaseOrder.setOrderStatus(rs.getString(PurchaseOrder.COLUMN_ORDER_STATUS));
            purchaseOrder.setOrderTotal(rs.getDouble(PurchaseOrder.COLUMN_ORDER_TOTAL));
            purchaseOrder.setPreparedBy(rs.getInt(PurchaseOrder.COLUMN_PREPARED_BY));
            purchaseOrder.setPurchaseOrderNumber(rs.getInt(PurchaseOrder.COLUMN_PURCHASE_ORDER_NUMBER));
            purchaseOrder.setRemarks(rs.getString(PurchaseOrder.COLUMN_REMARKS));
            purchaseOrder.setRequestedDelivery(rs.getString(PurchaseOrder.COLUMN_REQUESTED_DELIVERY));
            purchaseOrder.setSupplierID(rs.getInt(PurchaseOrder.COLUMN_SUPPLIER_ID));
            purchaseOrder.setContactNo(rs.getString(PurchaseOrder.COLUMN_CONTACT_NO));
            purchaseOrder.setContactPerson(rs.getString(PurchaseOrder.COLUMN_CONTACT_PERSON));
            purchaseOrder.setPaymentOption(rs.getString(PurchaseOrder.COLUMN_PAYMENT_OPTION));
            purchaseOrder.setSupplierName(new SupplierDAO().getSupplierInfo(purchaseOrder.getSupplierID()).getSupplierName());
            Employee approver = new EmployeeDAO().getEmployeeInfo(purchaseOrder.getApprovedBy());
            purchaseOrder.setApproverName(approver.getLastName() + ", " + approver.getFirstName());
            purchaseOrders.add(purchaseOrder);
        }
        return purchaseOrders;
    }
}
