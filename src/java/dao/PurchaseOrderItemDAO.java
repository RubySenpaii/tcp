/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package dao;

import db.DBConnectionFactory;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;
import object.Product;
import object.PurchaseOrderItem;

/**
 *
 * @author RubySenpaii
 */
public class PurchaseOrderItemDAO {

    public boolean addPurchaseOrderItem(PurchaseOrderItem salesOrder) {
        try {
            DBConnectionFactory myFactory = DBConnectionFactory.getInstance();
            Connection conn = myFactory.getConnection();

            PreparedStatement ps = conn.prepareStatement("INSERT INTO " + PurchaseOrderItem.TABLE_NAME + " "
                    + "(" + PurchaseOrderItem.COLUMN_PRODUCT_ID + ", " + PurchaseOrderItem.COLUMN_PURCHASE_ORDER_NUMBER + ", " + PurchaseOrderItem.COLUMN_QUANTITY
                    + ", " + PurchaseOrderItem.COLUMN_UNIT_PRICE + ") "
                    + "VALUES(?, ?, ?, ?)");
            ps.setInt(1, salesOrder.getProductID());
            ps.setInt(2, salesOrder.getPurchaseOrderNumber());
            ps.setInt(3, salesOrder.getQuantity());
            ps.setDouble(4, salesOrder.getUnitPrice());

            ps.executeUpdate();
            ps.close();
            conn.close();
        } catch (SQLException x) {
            Logger.getLogger(PurchaseOrderItemDAO.class.getName()).log(Level.SEVERE, null, x);
            return false;
        }
        return true;
    }

    public boolean addPurchaseOrderItems(ArrayList<PurchaseOrderItem> purchaseOrderItems) {
        try {
            DBConnectionFactory myFactory = DBConnectionFactory.getInstance();
            Connection conn = myFactory.getConnection();
            conn.setAutoCommit(false);

            PreparedStatement ps = conn.prepareStatement("INSERT INTO " + PurchaseOrderItem.TABLE_NAME + " "
                    + "(" + PurchaseOrderItem.COLUMN_PRODUCT_ID + ", " + PurchaseOrderItem.COLUMN_PURCHASE_ORDER_NUMBER + ", " + PurchaseOrderItem.COLUMN_QUANTITY
                    + ", " + PurchaseOrderItem.COLUMN_UNIT_PRICE + ") "
                    + "VALUES(?, ?, ?, ?)");
            for (int a = 0; a < purchaseOrderItems.size(); a++) {
                ps.setInt(1, purchaseOrderItems.get(a).getProductID());
                ps.setInt(2, purchaseOrderItems.get(a).getPurchaseOrderNumber());
                ps.setInt(3, purchaseOrderItems.get(a).getQuantity());
                ps.setDouble(4, purchaseOrderItems.get(a).getUnitPrice());
                ps.addBatch();
            }

            int[] itemsAdded = ps.executeBatch();
            System.out.println("purchase order items recorded: " + itemsAdded.length);
            conn.commit();
            ps.close();
            conn.close();
        } catch (SQLException x) {
            Logger.getLogger(PurchaseOrderItemDAO.class.getName()).log(Level.SEVERE, null, x);
            return false;
        }
        return true;
    }
    
    public boolean updatePurchaseOrderItemQuantity(PurchaseOrderItem purchaseOrderItem) {
        try {
            DBConnectionFactory myFactory = DBConnectionFactory.getInstance();
            Connection conn = myFactory.getConnection();

            PreparedStatement ps = conn.prepareStatement("UPDATE " + PurchaseOrderItem.TABLE_NAME
                    + " SET " + PurchaseOrderItem.COLUMN_QUANTITY + " = ? " 
                    + "WHERE " + PurchaseOrderItem.COLUMN_PURCHASE_ORDER_NUMBER + " = ? AND " + PurchaseOrderItem.COLUMN_PRODUCT_ID + " = ?");
            ps.setInt(1, purchaseOrderItem.getQuantity());
            ps.setInt(2, purchaseOrderItem.getPurchaseOrderNumber());
            ps.setInt(3, purchaseOrderItem.getProductID());
            ps.executeUpdate();
            ps.close();
            conn.close();
        } catch (SQLException ex) {
            Logger.getLogger(PurchaseOrderItemDAO.class.getName()).log(Level.SEVERE, null, ex);
            return false;
        }
        return true;
    }
    
    public boolean updatePurchaseOrderNumber(PurchaseOrderItem purchaseOrderItem, int purchaseOrderNumber) {
        try {
            DBConnectionFactory myFactory = DBConnectionFactory.getInstance();
            Connection conn = myFactory.getConnection();

            PreparedStatement ps = conn.prepareStatement("UPDATE " + PurchaseOrderItem.TABLE_NAME
                    + " SET " + PurchaseOrderItem.COLUMN_PURCHASE_ORDER_NUMBER + " = ? " 
                    + "WHERE " + PurchaseOrderItem.COLUMN_PURCHASE_ORDER_NUMBER + " = ? AND " + PurchaseOrderItem.COLUMN_PRODUCT_ID + " = ?");
            ps.setInt(1, purchaseOrderItem.getPurchaseOrderNumber());
            ps.setInt(2, purchaseOrderNumber);
            ps.setInt(3, purchaseOrderItem.getProductID());
            ps.executeUpdate();
            ps.close();
            conn.close();
        } catch (SQLException ex) {
            Logger.getLogger(PurchaseOrderItemDAO.class.getName()).log(Level.SEVERE, null, ex);
            return false;
        }
        return true;
    }

    public ArrayList<PurchaseOrderItem> getListOfPurchaseOrderItems() {
        ArrayList<PurchaseOrderItem> salesOrders = new ArrayList<>();
        try {
            DBConnectionFactory myFactory = DBConnectionFactory.getInstance();
            Connection conn = myFactory.getConnection();

            PreparedStatement ps = conn.prepareStatement("SELECT * FROM " + PurchaseOrderItem.TABLE_NAME);

            ResultSet rs = ps.executeQuery();
            salesOrders = getDataFromResultSet(rs);

            rs.close();
            ps.close();
            conn.close();
        } catch (SQLException x) {
            Logger.getLogger(PurchaseOrderItemDAO.class.getName()).log(Level.SEVERE, null, x);
        }
        return salesOrders;
    }
    
    public ArrayList<PurchaseOrderItem> getListOfPurchaseOrderItems(int purchaseOrderNumber) {
        ArrayList<PurchaseOrderItem> salesOrders = new ArrayList<>();
        try {
            DBConnectionFactory myFactory = DBConnectionFactory.getInstance();
            Connection conn = myFactory.getConnection();

            PreparedStatement ps = conn.prepareStatement("SELECT * FROM " + PurchaseOrderItem.TABLE_NAME 
                    + " WHERE " + PurchaseOrderItem.COLUMN_PURCHASE_ORDER_NUMBER + " = ? AND " + PurchaseOrderItem.COLUMN_QUANTITY + " > 0");
            ps.setInt(1, purchaseOrderNumber);

            ResultSet rs = ps.executeQuery();
            salesOrders = getDataFromResultSet(rs);

            rs.close();
            ps.close();
            conn.close();
        } catch (SQLException x) {
            Logger.getLogger(PurchaseOrderItemDAO.class.getName()).log(Level.SEVERE, null, x);
        }
        return salesOrders;
    }

    private ArrayList<PurchaseOrderItem> getDataFromResultSet(ResultSet rs) throws SQLException {
        ArrayList<PurchaseOrderItem> purchaseItems = new ArrayList<>();
        while (rs.next()) {
            PurchaseOrderItem purchaseItem = new PurchaseOrderItem();
            purchaseItem.setProductID(rs.getInt(PurchaseOrderItem.COLUMN_PRODUCT_ID));
            purchaseItem.setPurchaseOrderNumber(rs.getInt(PurchaseOrderItem.COLUMN_PURCHASE_ORDER_NUMBER));
            purchaseItem.setQuantity(rs.getInt(PurchaseOrderItem.COLUMN_QUANTITY));
            purchaseItem.setUnitPrice(rs.getDouble(PurchaseOrderItem.COLUMN_UNIT_PRICE));
            Product product = new ProductDAO().getProductDetail(purchaseItem.getProductID());
            purchaseItem.setProductName(product.getProductName());
            purchaseItems.add(purchaseItem);
        }
        return purchaseItems;
    }
}
