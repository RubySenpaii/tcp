/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package dao;

import db.DBConnectionFactory;
import extra.Reference;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;
import object.Inventory;
import object.Product;
import object.SalesOrder;

/**
 *
 * @author RubySenpaii
 */
public class ProductDAO {

    public boolean addProduct(Product product) {
        try {
            DBConnectionFactory myFactory = DBConnectionFactory.getInstance();
            Connection conn = myFactory.getConnection();

            PreparedStatement ps = conn.prepareStatement("INSERT INTO " + Product.TABLE_NAME + " "
                    + "(" + Product.COLUMN_CATEGORY + ", " + Product.COLUMN_CRITICAL_LEVEL + ", " + Product.COLUMN_DESCRIPTION
                    + ", " + Product.COLUMN_IMAGE + ", " + Product.COLUMN_PRODUCT_ID + ", " + Product.COLUMN_FLAG + ", " + Product.COLUMN_PRODUCT_NAME
                    + ", " + Product.COLUMN_UNIT + ", " + Product.COLUMN_UNIT_PRICE + ", " + Product.COLUMN_COST_PRICE + ") "
                    + "VALUES(?, ?, ?, ?, ?, ?, ?, ?, ?, ?)");
            ps.setString(1, product.getCategory());
            ps.setDouble(2, product.getCriticalLevel());
            ps.setString(3, product.getDescription());
            ps.setString(4, product.getImage());
            ps.setInt(5, product.getProductID());
            ps.setInt(6, product.getFlag());
            ps.setString(7, product.getProductName());
            ps.setString(8, product.getUnit());
            ps.setDouble(9, product.getUnitPrice());
            ps.setDouble(10, product.getCostPrice());

            ps.executeUpdate();
            ps.close();
            conn.close();
        } catch (SQLException x) {
            Logger.getLogger(ProductDAO.class.getName()).log(Level.SEVERE, null, x);
            return false;
        }
        return true;
    }

    public boolean updateProductInfo(Product product) {
        try {
            DBConnectionFactory myFactory = DBConnectionFactory.getInstance();
            Connection conn = myFactory.getConnection();

            PreparedStatement ps = conn.prepareStatement("UPDATE " + Product.TABLE_NAME
                    + " SET " + Product.COLUMN_CATEGORY + " = ?, " + Product.COLUMN_CRITICAL_LEVEL + " = ?, " + Product.COLUMN_DESCRIPTION + " = ?, "
                    + Product.COLUMN_IMAGE + " = ?, " + Product.COLUMN_PRODUCT_NAME + " = ?, " + Product.COLUMN_COST_PRICE + " = ?, "
                    + Product.COLUMN_UNIT + " = ?, " + Product.COLUMN_UNIT_PRICE + " = ? "
                    + "WHERE " + Product.COLUMN_PRODUCT_ID + " = ?");
            ps.setString(1, product.getCategory());
            ps.setDouble(2, product.getCriticalLevel());
            ps.setString(3, product.getDescription());
            ps.setString(4, product.getImage());
            ps.setString(5, product.getProductName());
            ps.setDouble(6, product.getCostPrice());
            ps.setString(7, product.getUnit());
            ps.setDouble(8, product.getUnitPrice());
            ps.setInt(9, product.getProductID());

            ps.executeUpdate();
            ps.close();
            conn.close();
        } catch (SQLException ex) {
            Logger.getLogger(ProductDAO.class.getName()).log(Level.SEVERE, null, ex);
            return false;
        }
        return true;
    }

    public boolean flagProduct(Product product) {
        try {
            DBConnectionFactory myFactory = DBConnectionFactory.getInstance();
            Connection conn = myFactory.getConnection();

            PreparedStatement ps = conn.prepareStatement("UPDATE " + Product.TABLE_NAME
                    + " SET " + Product.COLUMN_FLAG + " = ? "
                    + "WHERE " + Product.COLUMN_PRODUCT_ID + " = ?");
            ps.setInt(1, product.getFlag());
            ps.setInt(2, product.getProductID());

            ps.executeUpdate();
            ps.close();
            conn.close();
        } catch (SQLException ex) {
            Logger.getLogger(ProductDAO.class.getName()).log(Level.SEVERE, null, ex);
            return false;
        }
        return true;
    }

    public ArrayList<String> getProductCategories() {
        ArrayList<String> categories = new ArrayList<>();
        try {
            DBConnectionFactory myFactory = DBConnectionFactory.getInstance();
            Connection conn = myFactory.getConnection();

            PreparedStatement ps = conn.prepareStatement("SELECT " + Product.COLUMN_CATEGORY + " FROM " + Product.TABLE_NAME + " GROUP BY " + Product.COLUMN_CATEGORY);

            ResultSet rs = ps.executeQuery();
            while (rs.next()) {
                categories.add(rs.getString(Product.COLUMN_CATEGORY));
            }

            rs.close();
            ps.close();
            conn.close();
        } catch (SQLException x) {
            Logger.getLogger(ProductDAO.class.getName()).log(Level.SEVERE, null, x);
        }
        return categories;
    }

    public Product getProductDetail(int productID) {
        ArrayList<Product> products = new ArrayList<>();
        try {
            DBConnectionFactory myFactory = DBConnectionFactory.getInstance();
            Connection conn = myFactory.getConnection();

            PreparedStatement ps = conn.prepareStatement("SELECT * FROM " + Product.TABLE_NAME + " WHERE " + Product.COLUMN_PRODUCT_ID + " = ?");
            ps.setInt(1, productID);

            ResultSet rs = ps.executeQuery();
            products = getDataFromResultSet(rs);

            rs.close();
            ps.close();
            conn.close();
        } catch (SQLException x) {
            Logger.getLogger(ProductDAO.class.getName()).log(Level.SEVERE, null, x);
        }
        return products.get(0);
    }

    public Product getProductDetail(String productName) {
        ArrayList<Product> products = new ArrayList<>();
        try {
            DBConnectionFactory myFactory = DBConnectionFactory.getInstance();
            Connection conn = myFactory.getConnection();

            PreparedStatement ps = conn.prepareStatement("SELECT * FROM " + Product.TABLE_NAME + " WHERE " + Product.COLUMN_PRODUCT_NAME + " = ?");
            ps.setString(1, productName);

            ResultSet rs = ps.executeQuery();
            products = getDataFromResultSet(rs);

            rs.close();
            ps.close();
            conn.close();
        } catch (SQLException x) {
            Logger.getLogger(ProductDAO.class.getName()).log(Level.SEVERE, null, x);
        }
        return products.get(0);
    }

    public ArrayList<Product> getListOfProductsForCategory(String category) {
        ArrayList<Product> products = new ArrayList<>();
        try {
            DBConnectionFactory myFactory = DBConnectionFactory.getInstance();
            Connection conn = myFactory.getConnection();

            PreparedStatement ps = conn.prepareStatement("SELECT * FROM " + Product.TABLE_NAME + " WHERE " + Product.COLUMN_CATEGORY + " = ?");
            ps.setString(1, category);

            ResultSet rs = ps.executeQuery();
            products = getDataFromResultSet(rs);

            rs.close();
            ps.close();
            conn.close();
        } catch (SQLException x) {
            Logger.getLogger(ProductDAO.class.getName()).log(Level.SEVERE, null, x);
        }
        return products;
    }

    public ArrayList<Product> getListOfProductsWithKeyword(String keyword) {
        ArrayList<Product> products = new ArrayList<>();
        try {
            DBConnectionFactory myFactory = DBConnectionFactory.getInstance();
            Connection conn = myFactory.getConnection();

            PreparedStatement ps = conn.prepareStatement("SELECT * FROM " + Product.TABLE_NAME + " WHERE " + Product.COLUMN_PRODUCT_NAME + " LIKE ?");
            ps.setString(1, "%" + keyword + "%");

            ResultSet rs = ps.executeQuery();
            products = getDataFromResultSet(rs);

            rs.close();
            ps.close();
            conn.close();
        } catch (SQLException x) {
            Logger.getLogger(ProductDAO.class.getName()).log(Level.SEVERE, null, x);
        }
        return products;
    }

    public ArrayList<Product> getListOfProducts() {
        ArrayList<Product> products = new ArrayList<>();
        try {
            DBConnectionFactory myFactory = DBConnectionFactory.getInstance();
            Connection conn = myFactory.getConnection();

            PreparedStatement ps = conn.prepareStatement("SELECT * FROM " + Product.TABLE_NAME);

            ResultSet rs = ps.executeQuery();
            products = getDataFromResultSet(rs);

            rs.close();
            ps.close();
            conn.close();
        } catch (SQLException x) {
            Logger.getLogger(ProductDAO.class.getName()).log(Level.SEVERE, null, x);
        }
        return products;
    }

    public ArrayList<Product> getListOfActiveProducts() {
        ArrayList<Product> products = new ArrayList<>();
        try {
            DBConnectionFactory myFactory = DBConnectionFactory.getInstance();
            Connection conn = myFactory.getConnection();

            PreparedStatement ps = conn.prepareStatement("SELECT * FROM " + Product.TABLE_NAME + " WHERE " + Product.COLUMN_FLAG + " = ?");
            ps.setInt(1, Reference.ACTIVE);

            ResultSet rs = ps.executeQuery();
            products = getDataFromResultSet(rs);

            rs.close();
            ps.close();
            conn.close();
        } catch (SQLException x) {
            Logger.getLogger(ProductDAO.class.getName()).log(Level.SEVERE, null, x);
        }
        return products;
    }

    public ArrayList<Product> getListOfArchivedProducts() {
        ArrayList<Product> products = new ArrayList<>();
        try {
            DBConnectionFactory myFactory = DBConnectionFactory.getInstance();
            Connection conn = myFactory.getConnection();

            PreparedStatement ps = conn.prepareStatement("SELECT * FROM " + Product.TABLE_NAME + " WHERE " + Product.COLUMN_FLAG + " = ?");
            ps.setInt(1, Reference.ARCHIVED);

            ResultSet rs = ps.executeQuery();
            products = getDataFromResultSet(rs);

            rs.close();
            ps.close();
            conn.close();
        } catch (SQLException x) {
            Logger.getLogger(ProductDAO.class.getName()).log(Level.SEVERE, null, x);
        }
        return products;
    }

    public ArrayList<Product> getTop10Products(String month) {
        ArrayList<Product> products = new ArrayList<>();
        try {
            DBConnectionFactory myFactory = DBConnectionFactory.getInstance();
            Connection conn = myFactory.getConnection();

            PreparedStatement ps = conn.prepareStatement("SELECT P.ProductName, SUM(SOI.Quantity) AS 'QuantitySold'\n"
                    + "FROM Product P JOIN SalesOrderItem SOI ON P.ProductID = SOI.ProductID\n"
                    + "			   JOIN SalesOrder SO ON SOI.SalesOrderNumber = SO.SalesOrderNumber\n"
                    + "WHERE DATE_FORMAT(SO.OrderDate, \"%m-%Y\") = ? AND (SO.OrderStatus = 'Approved' OR SO.OrderStatus = 'Delivery' OR SO.OrderStatus = 'Completed')\n"
                    + "GROUP BY P.ProductName\n"
                    + "ORDER BY QuantitySold DESC LIMIT 10");
            ps.setString(1, month);

            ResultSet rs = ps.executeQuery();
            while (rs.next()) {
                Product product = new Product();
                product.setProductName(rs.getString(Product.COLUMN_PRODUCT_NAME));
                product.setOrderQuantity(rs.getInt("QuantitySold"));
                products.add(product);
            }

            rs.close();
            ps.close();
            conn.close();
        } catch (SQLException x) {
            Logger.getLogger(ProductDAO.class.getName()).log(Level.SEVERE, null, x);
        }
        return products;
    }

    public ArrayList<Product> getTop10ProductsWithDateRange(String startDate, String endDate) {
        ArrayList<Product> products = new ArrayList<>();
        try {
            DBConnectionFactory myFactory = DBConnectionFactory.getInstance();
            Connection conn = myFactory.getConnection();

            PreparedStatement ps = conn.prepareStatement("SELECT P.ProductName, SUM(SOI.Quantity) AS 'QuantitySold'\n"
                    + "FROM Product P JOIN SalesOrderItem SOI ON P.ProductID = SOI.ProductID\n"
                    + "			   JOIN SalesOrder SO ON SOI.SalesOrderNumber = SO.SalesOrderNumber\n"
                    + "WHERE SO.OrderStatus != 'Rejected' AND SO.OrderStatus != 'Pending' "
                    + "     AND STR_TO_DATE(SO.OrderDate, '%Y-%m-%d') >= STR_TO_DATE(?, '%Y-%m-%d') AND STR_TO_DATE(SO.OrderDate, '%Y-%m-%d') <= STR_TO_DATE(?, '%Y-%m-%d')\n"
                    + "GROUP BY P.ProductName\n"
                    + "ORDER BY QuantitySold DESC LIMIT 10");
            ps.setString(1, startDate);
            ps.setString(2, endDate);

            ResultSet rs = ps.executeQuery();
            while (rs.next()) {
                Product product = new Product();
                product.setProductName(rs.getString(Product.COLUMN_PRODUCT_NAME));
                product.setOrderQuantity(rs.getInt("QuantitySold"));
                products.add(product);
            }

            rs.close();
            ps.close();
            conn.close();
        } catch (SQLException x) {
            Logger.getLogger(ProductDAO.class.getName()).log(Level.SEVERE, null, x);
        }
        return products;
    }

    public ArrayList<Product> getListOfReturnsFrom(String startDate, String endDate) {
        ArrayList<Product> products = new ArrayList<>();
        try {
            DBConnectionFactory myFactory = DBConnectionFactory.getInstance();
            Connection conn = myFactory.getConnection();

            PreparedStatement ps = conn.prepareStatement("select *\n"
                    + "from product p join inventory i on p.productid = i.productid\n"
                    + "where i.reference like '%DefectiveSales%' and STR_TO_DATE(i.dateupdated, '%Y-%m-%d') >= STR_TO_DATE(?, '%Y-%m-%d') AND STR_TO_DATE(i.dateupdated, '%Y-%m-%d') <= STR_TO_DATE(?, '%Y-%m-%d')");
            ps.setString(1, startDate);
            ps.setString(2, endDate);

            ResultSet rs = ps.executeQuery();
            while (rs.next()) {
                Product product = new Product();
                product.setProductName(rs.getString("ProductName"));
                product.setDateReturned(rs.getString("DateUpdated"));
                product.setOrderQuantity(rs.getInt("InvDefect"));
                String remarks = rs.getString("Reference");
                int salesOrderNumber = Integer.parseInt(remarks.split("-")[1].split(" ")[0]);
                product.setSalesOrderNumber(salesOrderNumber);
                SalesOrder salesOrder = new SalesOrderDAO().getSalesOrderDetails(salesOrderNumber);
                product.setCustomerName(salesOrder.getCustomerName());
                products.add(product);
            }

            rs.close();
            ps.close();
            conn.close();
        } catch (SQLException x) {
            Logger.getLogger(ProductDAO.class.getName()).log(Level.SEVERE, null, x);
        }
        return products;
    }

    private ArrayList<Product> getDataFromResultSet(ResultSet rs) throws SQLException {
        ArrayList<Product> products = new ArrayList<>();
        while (rs.next()) {
            Product product = new Product();
            product.setCategory(rs.getString(Product.COLUMN_CATEGORY));
            product.setCriticalLevel(rs.getDouble(Product.COLUMN_CRITICAL_LEVEL));
            product.setDescription(rs.getString(Product.COLUMN_DESCRIPTION));
            product.setImage(rs.getString(Product.COLUMN_IMAGE));
            product.setProductID(rs.getInt(Product.COLUMN_PRODUCT_ID));
            product.setProductName(rs.getString(Product.COLUMN_PRODUCT_NAME));
            product.setUnit(rs.getString(Product.COLUMN_UNIT));
            product.setUnitPrice(rs.getDouble(Product.COLUMN_UNIT_PRICE));
            product.setCostPrice(rs.getDouble(Product.COLUMN_COST_PRICE));
            Inventory inventory = new InventoryDAO().getLatestCountForProduct(product.getProductID());
            product.setStockQuantity(inventory.getTotal());
            product.setFlag(rs.getInt(Product.COLUMN_FLAG));
            product.setDeliveryQuantity(new InventoryDAO().getQuantityForDeliveryOfProduct(product.getProductID()));
            products.add(product);
        }
        return products;
    }
}
