/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package dao;

import db.DBConnectionFactory;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;
import object.SupplierItem;

/**
 *
 * @author RubySenpaii
 */
public class SupplierItemDAO {
    
    public boolean addSupplierItem(SupplierItem supplierItem) {
        try {
            DBConnectionFactory myFactory = DBConnectionFactory.getInstance();
            Connection conn = myFactory.getConnection();

            PreparedStatement ps = conn.prepareStatement("INSERT INTO " + SupplierItem.TABLE_NAME + " "
                    + "(" + SupplierItem.COLUMN_PRODUCT_NAME + ", " + SupplierItem.COLUMN_SUPPLIER_ID + ", " + SupplierItem.COLUMN_UNIT_PRICE + ") "
                    + "VALUES(?, ?, ?)");
            ps.setString(1, supplierItem.getProductName());
            ps.setInt(2, supplierItem.getSupplierID());
            ps.setDouble(3, supplierItem.getUnitPrice());

            ps.executeUpdate();
            ps.close();
            conn.close();
        } catch (SQLException x) {
            Logger.getLogger(SupplierItemDAO.class.getName()).log(Level.SEVERE, null, x);
            return false;
        }
        return true;
    }
    
    public boolean emptySupplierItem(int supplierID) {
        try {
            DBConnectionFactory myFactory = DBConnectionFactory.getInstance();
            Connection conn = myFactory.getConnection();

            PreparedStatement ps = conn.prepareStatement("DELETE FROM " + SupplierItem.TABLE_NAME + " WHERE " + SupplierItem.COLUMN_SUPPLIER_ID + " = ?");
            ps.setInt(1, supplierID);

            ps.executeUpdate();
            ps.close();
            conn.close();
        } catch (SQLException x) {
            Logger.getLogger(SupplierItemDAO.class.getName()).log(Level.SEVERE, null, x);
            return false;
        }
        return true;
    }
    
    public ArrayList<SupplierItem> getSupplierItemsFromSupplier(int supplierID) {
        ArrayList<SupplierItem> supplierItems = new ArrayList<>();
        try {
            DBConnectionFactory myFactory = DBConnectionFactory.getInstance();
            Connection conn = myFactory.getConnection();

            PreparedStatement ps = conn.prepareStatement("SELECT * FROM " + SupplierItem.TABLE_NAME + " WHERE " + SupplierItem.COLUMN_SUPPLIER_ID + " = ?");
            ps.setInt(1, supplierID);

            ResultSet rs = ps.executeQuery();
            supplierItems = getDataFromResultSet(rs);

            rs.close();
            ps.close();
            conn.close();
        } catch (SQLException x) {
            Logger.getLogger(SupplierDAO.class.getName()).log(Level.SEVERE, null, x);
        }
        return supplierItems;
    }
    
    private ArrayList<SupplierItem> getDataFromResultSet(ResultSet rs) throws SQLException {
        ArrayList<SupplierItem> supplierItems = new ArrayList<>();
        while (rs.next()) {
            SupplierItem supplierItem = new SupplierItem();
            supplierItem.setProductName(rs.getString(SupplierItem.COLUMN_PRODUCT_NAME));
            supplierItem.setSupplierID(rs.getInt(SupplierItem.COLUMN_SUPPLIER_ID));
            supplierItem.setUnitPrice(rs.getDouble(SupplierItem.COLUMN_UNIT_PRICE));
            supplierItems.add(supplierItem);
        }
        return supplierItems;
    }
}
