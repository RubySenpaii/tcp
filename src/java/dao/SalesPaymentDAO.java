/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package dao;

import db.DBConnectionFactory;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;
import object.SalesPayment;

/**
 *
 * @author RubySenpaii
 */
public class SalesPaymentDAO {
    
    public boolean addSalesPayment(SalesPayment salesPayment) {
        try {
            DBConnectionFactory myFactory = DBConnectionFactory.getInstance();
            Connection conn = myFactory.getConnection();

            PreparedStatement ps = conn.prepareStatement("INSERT INTO " + SalesPayment.TABLE_NAME + " "
                    + "(" + SalesPayment.COLUMN_AMOUNT_PAID + ", " + SalesPayment.COLUMN_APPROVED_BY + ", " + SalesPayment.COLUMN_DATE_PAID 
                    + ", " + SalesPayment.COLUMN_PROOF + ", " + SalesPayment.COLUMN_SALES_ORDER_NUMBER + ", " + SalesPayment.COLUMN_PAYMENT_ID + ") "
                    + "VALUES(?, ?, ?, ?, ?, ?)");
            ps.setDouble(1, salesPayment.getAmountPaid());
            ps.setInt(2, salesPayment.getApprovedBy());
            ps.setString(3, salesPayment.getDatePaid());
            ps.setString(4, salesPayment.getProof());
            ps.setInt(5, salesPayment.getSalesOrderNumber());
            ps.setInt(6, salesPayment.getPaymentID());

            ps.executeUpdate();
            ps.close();
            conn.close();
        } catch (SQLException x) {
            Logger.getLogger(SalesPaymentDAO.class.getName()).log(Level.SEVERE, null, x);
            return false;
        }
        return true;
    }
    
    public boolean updateSalesPaymentInfo(SalesPayment salesPayment) {
        try {
            DBConnectionFactory myFactory = DBConnectionFactory.getInstance();
            Connection conn = myFactory.getConnection();

            PreparedStatement ps = conn.prepareStatement("UPDATE " + SalesPayment.TABLE_NAME
                    + " SET " + SalesPayment.COLUMN_APPROVED_BY + " = ? "
                    + "WHERE " + SalesPayment.COLUMN_SALES_ORDER_NUMBER + " = ? AND " + SalesPayment.COLUMN_DATE_PAID + " = ? AND " 
                    + SalesPayment.COLUMN_AMOUNT_PAID + " = ?");
            ps.setInt(1, salesPayment.getApprovedBy());
            ps.setInt(2, salesPayment.getSalesOrderNumber());
            ps.setString(3, salesPayment.getDatePaid());
            ps.setDouble(4, salesPayment.getAmountPaid());
            ps.executeUpdate();
            ps.close();
            conn.close();
        } catch (SQLException ex) {
            Logger.getLogger(SalesPaymentDAO.class.getName()).log(Level.SEVERE, null, ex);
            return false;
        }
        return true;
    }

    public ArrayList<SalesPayment> getListOfSalesPayments() {
        ArrayList<SalesPayment> salesPayments = new ArrayList<>();
        try {
            DBConnectionFactory myFactory = DBConnectionFactory.getInstance();
            Connection conn = myFactory.getConnection();

            PreparedStatement ps = conn.prepareStatement("SELECT * FROM " + SalesPayment.TABLE_NAME);

            ResultSet rs = ps.executeQuery();
            salesPayments = getDataFromResultSet(rs);

            rs.close();
            ps.close();
            conn.close();
        } catch (SQLException x) {
            Logger.getLogger(SalesPaymentDAO.class.getName()).log(Level.SEVERE, null, x);
        }
        return salesPayments;
    }
    
    public SalesPayment getSalesPaymentInfo(int paymentID) {
        ArrayList<SalesPayment> salesPayments = new ArrayList<>();
        try {
            DBConnectionFactory myFactory = DBConnectionFactory.getInstance();
            Connection conn = myFactory.getConnection();

            PreparedStatement ps = conn.prepareStatement("SELECT * FROM " + SalesPayment.TABLE_NAME + " WHERE " + SalesPayment.COLUMN_PAYMENT_ID + " = ?");
            ps.setInt(1, paymentID);

            ResultSet rs = ps.executeQuery();
            salesPayments = getDataFromResultSet(rs);

            rs.close();
            ps.close();
            conn.close();
        } catch (SQLException x) {
            Logger.getLogger(SalesPaymentDAO.class.getName()).log(Level.SEVERE, null, x);
        }
        return salesPayments.get(0);
    }
    
    public ArrayList<SalesPayment> getListOfPendingSalesPayment() {
        ArrayList<SalesPayment> salesPayments = new ArrayList<>();
        try {
            DBConnectionFactory myFactory = DBConnectionFactory.getInstance();
            Connection conn = myFactory.getConnection();

            PreparedStatement ps = conn.prepareStatement("SELECT * FROM " + SalesPayment.TABLE_NAME + " WHERE " + SalesPayment.COLUMN_APPROVED_BY + " = ?");
            ps.setInt(1, -1);

            ResultSet rs = ps.executeQuery();
            salesPayments = getDataFromResultSet(rs);

            rs.close();
            ps.close();
            conn.close();
        } catch (SQLException x) {
            Logger.getLogger(SalesPaymentDAO.class.getName()).log(Level.SEVERE, null, x);
        }
        return salesPayments;
    }
    
    public ArrayList<SalesPayment> getListOfApprovedSalesPayment() {
        ArrayList<SalesPayment> salesPayments = new ArrayList<>();
        try {
            DBConnectionFactory myFactory = DBConnectionFactory.getInstance();
            Connection conn = myFactory.getConnection();

            PreparedStatement ps = conn.prepareStatement("SELECT * FROM " + SalesPayment.TABLE_NAME + " WHERE " + SalesPayment.COLUMN_APPROVED_BY + " > ?");
            ps.setInt(1, 0);

            ResultSet rs = ps.executeQuery();
            salesPayments = getDataFromResultSet(rs);

            rs.close();
            ps.close();
            conn.close();
        } catch (SQLException x) {
            Logger.getLogger(SalesPaymentDAO.class.getName()).log(Level.SEVERE, null, x);
        }
        return salesPayments;
    }
    
    public ArrayList<SalesPayment> getApprovedPaymentsOnSalesOrder(int salesOrderNumber) {
        ArrayList<SalesPayment> salesPayments = new ArrayList<>();
        try {
            DBConnectionFactory myFactory = DBConnectionFactory.getInstance();
            Connection conn = myFactory.getConnection();

            PreparedStatement ps = conn.prepareStatement("SELECT * FROM " + SalesPayment.TABLE_NAME + " WHERE " + SalesPayment.COLUMN_APPROVED_BY + " > ? AND " + SalesPayment.COLUMN_SALES_ORDER_NUMBER + " = ?");
            ps.setInt(1, 0);
            ps.setInt(2, salesOrderNumber);

            ResultSet rs = ps.executeQuery();
            salesPayments = getDataFromResultSet(rs);

            rs.close();
            ps.close();
            conn.close();
        } catch (SQLException x) {
            Logger.getLogger(SalesPaymentDAO.class.getName()).log(Level.SEVERE, null, x);
        }
        return salesPayments;
    }
    
    private ArrayList<SalesPayment> getDataFromResultSet(ResultSet rs) throws SQLException {
        ArrayList<SalesPayment> salesPayments = new ArrayList<>();
        while (rs.next()) {
            SalesPayment salesPayment = new SalesPayment();
            salesPayment.setAmountPaid(rs.getDouble(SalesPayment.COLUMN_AMOUNT_PAID));
            salesPayment.setApprovedBy(rs.getInt(SalesPayment.COLUMN_APPROVED_BY));
            salesPayment.setDatePaid(rs.getString(SalesPayment.COLUMN_DATE_PAID));
            salesPayment.setProof(rs.getString(SalesPayment.COLUMN_PROOF));
            salesPayment.setSalesOrderNumber(rs.getInt(SalesPayment.COLUMN_SALES_ORDER_NUMBER));
            salesPayment.setPaymentID(rs.getInt(SalesPayment.COLUMN_PAYMENT_ID));
            salesPayments.add(salesPayment);
        }
        return salesPayments;
    }
}
