/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package dao;

import db.DBConnectionFactory;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;
import object.Customer;
import object.Employee;
import object.SalesOrder;

/**
 *
 * @author RubySenpaii
 */
public class SalesOrderDAO {

    public boolean addSalesOrder(SalesOrder salesOrder) {
        try {
            DBConnectionFactory myFactory = DBConnectionFactory.getInstance();
            Connection conn = myFactory.getConnection();

            PreparedStatement ps = conn.prepareStatement("INSERT INTO " + SalesOrder.TABLE_NAME + " "
                    + "(" + SalesOrder.COLUMN_ADDRESS + ", " + SalesOrder.COLUMN_APPROVED_BY + ", " + SalesOrder.COLUMN_CONTACT_NO
                    + ", " + SalesOrder.COLUMN_CONTACT_PERSON + ", " + SalesOrder.COLUMN_CUSTOMER_ID
                    + ", " + SalesOrder.COLUMN_DELIVERED_BY + ", " + SalesOrder.COLUMN_ORDER_DATE + ", " + SalesOrder.COLUMN_ORDER_STATUS
                    + ", " + SalesOrder.COLUMN_ORDER_TOTAL + ", " + SalesOrder.COLUMN_REMARKS + ", " + SalesOrder.COLUMN_REQUEST_DELIVERY
                    + ", " + SalesOrder.COLUMN_SALES_ORDER_NUMBER + ", " + SalesOrder.COLUMN_RECEIVED_BY + ", "
                    + SalesOrder.COLUMN_RECEIVED_DATE + ", " + SalesOrder.COLUMN_ASSIGNED_TRUCK + ", " + SalesOrder.COLUMN_PAYMENT_OPTION + ") "
                    + "VALUES(?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)");
            ps.setString(1, salesOrder.getAddress());
            ps.setInt(2, salesOrder.getApprovedBy());
            ps.setString(3, salesOrder.getContactNo());
            ps.setString(4, salesOrder.getContactPerson());
            ps.setInt(5, salesOrder.getCustomerID());
            ps.setInt(6, salesOrder.getDeliveredBy());
            ps.setString(7, salesOrder.getOrderDate());
            ps.setString(8, salesOrder.getOrderStatus());
            ps.setDouble(9, salesOrder.getOrderTotal());
            ps.setString(10, salesOrder.getRemarks());
            ps.setString(11, salesOrder.getRequestDelivery());
            ps.setInt(12, salesOrder.getSalesOrderNumber());
            ps.setString(13, salesOrder.getReceivedBy());
            ps.setString(14, salesOrder.getReceivedDate());
            ps.setInt(15, salesOrder.getAssignedTruck());
            ps.setString(16, salesOrder.getPaymentOption());

            ps.executeUpdate();
            ps.close();
            conn.close();
        } catch (SQLException x) {
            Logger.getLogger(SalesOrderDAO.class.getName()).log(Level.SEVERE, null, x);
            return false;
        }
        return true;
    }

    public boolean updateSalesOrderInfo(SalesOrder salesOrder) {
        try {
            DBConnectionFactory myFactory = DBConnectionFactory.getInstance();
            Connection conn = myFactory.getConnection();

            PreparedStatement ps = conn.prepareStatement("UPDATE " + SalesOrder.TABLE_NAME
                    + " SET " + SalesOrder.COLUMN_ADDRESS + " = ?, " + SalesOrder.COLUMN_APPROVED_BY + " = ?, " + SalesOrder.COLUMN_CUSTOMER_ID + " = ?, "
                    + SalesOrder.COLUMN_DELIVERED_BY + " = ?, " + SalesOrder.COLUMN_ORDER_DATE + " = ?, " + SalesOrder.COLUMN_ORDER_STATUS + " = ?, "
                    + SalesOrder.COLUMN_ORDER_TOTAL + " = ?, " + SalesOrder.COLUMN_REMARKS + " = ?, " + SalesOrder.COLUMN_ASSIGNED_TRUCK + " = ?, "
                    + SalesOrder.COLUMN_CONTACT_NO + " = ?, " + SalesOrder.COLUMN_CONTACT_PERSON + " = ?, " + SalesOrder.COLUMN_RECEIVED_BY + " = ?, "
                    + SalesOrder.COLUMN_RECEIVED_DATE + " = ?, " + SalesOrder.COLUMN_REQUEST_DELIVERY + " = ? "
                    + "WHERE " + SalesOrder.COLUMN_SALES_ORDER_NUMBER + " = ?");
            ps.setString(1, salesOrder.getAddress());
            ps.setInt(2, salesOrder.getApprovedBy());
            ps.setInt(3, salesOrder.getCustomerID());
            ps.setInt(4, salesOrder.getDeliveredBy());
            ps.setString(5, salesOrder.getOrderDate());
            ps.setString(6, salesOrder.getOrderStatus());
            ps.setDouble(7, salesOrder.getOrderTotal());
            ps.setString(8, salesOrder.getRemarks());
            ps.setInt(9, salesOrder.getAssignedTruck());
            ps.setString(10, salesOrder.getContactNo());
            ps.setString(11, salesOrder.getContactPerson());
            ps.setString(12, salesOrder.getReceivedBy());
            ps.setString(13, salesOrder.getReceivedDate());
            ps.setString(14, salesOrder.getRequestDelivery());
            ps.setInt(15, salesOrder.getSalesOrderNumber());
            ps.executeUpdate();
            ps.close();
            conn.close();
        } catch (SQLException ex) {
            Logger.getLogger(SalesOrderDAO.class.getName()).log(Level.SEVERE, null, ex);
            return false;
        }
        return true;
    }

    public SalesOrder getSalesOrderDetails(int salesOrderNumber) {
        ArrayList<SalesOrder> salesOrders = new ArrayList<>();
        try {
            DBConnectionFactory myFactory = DBConnectionFactory.getInstance();
            Connection conn = myFactory.getConnection();

            PreparedStatement ps = conn.prepareStatement("SELECT * FROM " + SalesOrder.TABLE_NAME + " WHERE " + SalesOrder.COLUMN_SALES_ORDER_NUMBER + " = ?");
            ps.setInt(1, salesOrderNumber);

            ResultSet rs = ps.executeQuery();
            salesOrders = getDataFromResultSet(rs);

            rs.close();
            ps.close();
            conn.close();
        } catch (SQLException x) {
            Logger.getLogger(SalesOrderDAO.class.getName()).log(Level.SEVERE, null, x);
        }
        return salesOrders.get(0);
    }

    public ArrayList<SalesOrder> getListOfSalesOrders() {
        ArrayList<SalesOrder> salesOrders = new ArrayList<>();
        try {
            DBConnectionFactory myFactory = DBConnectionFactory.getInstance();
            Connection conn = myFactory.getConnection();

            PreparedStatement ps = conn.prepareStatement("SELECT * FROM " + SalesOrder.TABLE_NAME);

            ResultSet rs = ps.executeQuery();
            salesOrders = getDataFromResultSet(rs);

            rs.close();
            ps.close();
            conn.close();
        } catch (SQLException x) {
            Logger.getLogger(SalesOrderDAO.class.getName()).log(Level.SEVERE, null, x);
        }
        return salesOrders;
    }

    public ArrayList<SalesOrder> getMonthlySalesForYear(int year) {
        ArrayList<SalesOrder> salesOrders = new ArrayList<>();
        try {
            DBConnectionFactory myFactory = DBConnectionFactory.getInstance();
            Connection conn = myFactory.getConnection();

            PreparedStatement ps = conn.prepareStatement("SELECT DATE_FORMAT(orderdate, \"%m-%Y\") AS Month, SUM(ordertotal) AS 'TotalSales'\n"
                    + "FROM salesorder\n"
                    + "WHERE (orderstatus = 'Approved' OR orderstatus = 'Delivery' OR orderstatus = 'Completed') AND DATE_FORMAT(orderdate, \"%Y\") = ?\n"
                    + "GROUP BY DATE_FORMAT(orderdate, \"%m-%Y\")");
            ps.setInt(1, year);

            ResultSet rs = ps.executeQuery();
            while (rs.next()) {
                SalesOrder salesOrder = new SalesOrder();
                salesOrder.setMonth(rs.getString("Month"));
                salesOrder.setTotalSales(rs.getDouble("TotalSales"));
                salesOrders.add(salesOrder);
            }

            rs.close();
            ps.close();
            conn.close();
        } catch (SQLException x) {
            Logger.getLogger(SalesOrderDAO.class.getName()).log(Level.SEVERE, null, x);
        }
        return salesOrders;
    }

    public ArrayList<SalesOrder> getApprovedOrders() {
        ArrayList<SalesOrder> salesOrders = new ArrayList<>();
        try {
            DBConnectionFactory myFactory = DBConnectionFactory.getInstance();
            Connection conn = myFactory.getConnection();

            PreparedStatement ps = conn.prepareStatement("SELECT * FROM " + SalesOrder.TABLE_NAME + " WHERE " + SalesOrder.COLUMN_ORDER_STATUS + " != ? "
                    + "AND " + SalesOrder.COLUMN_ORDER_STATUS + " != ? " + " ORDER BY STR_TO_DATE(" + SalesOrder.COLUMN_ORDER_DATE + ", '%Y-%m-%d %H:%i:%s') DESC");
            ps.setString(1, "Rejected");
            ps.setString(2, "Pending");

            ResultSet rs = ps.executeQuery();
            salesOrders = getDataFromResultSet(rs);

            rs.close();
            ps.close();
            conn.close();
        } catch (SQLException x) {
            Logger.getLogger(SalesOrderDAO.class.getName()).log(Level.SEVERE, null, x);
        }
        return salesOrders;
    }

    public ArrayList<SalesOrder> getListOfOrdersForReport(String startDate, String endDate) {
        ArrayList<SalesOrder> salesOrders = new ArrayList<>();
        try {
            DBConnectionFactory myFactory = DBConnectionFactory.getInstance();
            Connection conn = myFactory.getConnection();

            PreparedStatement ps = conn.prepareStatement("SELECT * FROM " + SalesOrder.TABLE_NAME + " WHERE " + SalesOrder.COLUMN_ORDER_STATUS + " != ? "
                    + "AND " + SalesOrder.COLUMN_ORDER_STATUS + " != ? AND STR_TO_DATE(" + SalesOrder.COLUMN_ORDER_DATE + ", '%Y-%m-%d') >= STR_TO_DATE(?, '%Y-%m-%d') "
                    + " AND STR_TO_DATE(" + SalesOrder.COLUMN_ORDER_DATE + ", '%Y-%m-%d') <= STR_TO_DATE(?, '%Y-%m-%d')");
            ps.setString(1, "Rejected");
            ps.setString(2, "Pending");
            ps.setString(3, startDate);
            ps.setString(4, endDate);

            ResultSet rs = ps.executeQuery();
            salesOrders = getDataFromResultSet(rs);

            rs.close();
            ps.close();
            conn.close();
        } catch (SQLException x) {
            Logger.getLogger(SalesOrderDAO.class.getName()).log(Level.SEVERE, null, x);
        }
        return salesOrders;
    }

    public ArrayList<SalesOrder> getListOfDeliveriesForReport(String startDate, String endDate) {
        ArrayList<SalesOrder> salesOrders = new ArrayList<>();
        try {
            DBConnectionFactory myFactory = DBConnectionFactory.getInstance();
            Connection conn = myFactory.getConnection();

            PreparedStatement ps = conn.prepareStatement("SELECT * FROM " + SalesOrder.TABLE_NAME + " WHERE " + SalesOrder.COLUMN_ORDER_STATUS + " != ? "
                    + "AND " + SalesOrder.COLUMN_ORDER_STATUS + " != ? AND STR_TO_DATE(" + SalesOrder.COLUMN_ORDER_DATE + ", '%Y-%m-%d') >= STR_TO_DATE(?, '%Y-%m-%d') "
                    + " AND STR_TO_DATE(" + SalesOrder.COLUMN_ORDER_DATE + ", '%Y-%m-%d') <= STR_TO_DATE(?, '%Y-%m-%d')");
            ps.setString(1, "Rejected");
            ps.setString(2, "Pending");
            ps.setString(3, startDate);
            ps.setString(4, endDate);

            ResultSet rs = ps.executeQuery();
            salesOrders = getDataFromResultSet(rs);

            rs.close();
            ps.close();
            conn.close();
        } catch (SQLException x) {
            Logger.getLogger(SalesOrderDAO.class.getName()).log(Level.SEVERE, null, x);
        }
        return salesOrders;
    }

    public ArrayList<SalesOrder> getListOfSalesOrdersWithStatus(String orderStatus) {
        ArrayList<SalesOrder> salesOrders = new ArrayList<>();
        try {
            DBConnectionFactory myFactory = DBConnectionFactory.getInstance();
            Connection conn = myFactory.getConnection();

            PreparedStatement ps = conn.prepareStatement("SELECT * FROM " + SalesOrder.TABLE_NAME
                    + " WHERE " + SalesOrder.COLUMN_ORDER_STATUS + " = ? "
                    + "ORDER BY STR_TO_DATE(" + SalesOrder.COLUMN_ORDER_DATE + ", '%Y-%m-%d %H:%i:%s') DESC");
            ps.setString(1, orderStatus);

            ResultSet rs = ps.executeQuery();
            salesOrders = getDataFromResultSet(rs);

            rs.close();
            ps.close();
            conn.close();
        } catch (SQLException x) {
            Logger.getLogger(SalesOrderDAO.class.getName()).log(Level.SEVERE, null, x);
        }
        return salesOrders;
    }

    public ArrayList<SalesOrder> getListOfSalesOrdersWithFrom(int customerID) {
        ArrayList<SalesOrder> salesOrders = new ArrayList<>();
        try {
            DBConnectionFactory myFactory = DBConnectionFactory.getInstance();
            Connection conn = myFactory.getConnection();

            PreparedStatement ps = conn.prepareStatement("SELECT * FROM " + SalesOrder.TABLE_NAME
                    + " WHERE " + SalesOrder.COLUMN_CUSTOMER_ID + " = ? ORDER BY STR_TO_DATE(" + SalesOrder.COLUMN_ORDER_DATE + ", '%Y-%m-%d %H:%i:%s') DESC");
            ps.setInt(1, customerID);

            ResultSet rs = ps.executeQuery();
            salesOrders = getDataFromResultSet(rs);

            rs.close();
            ps.close();
            conn.close();
        } catch (SQLException x) {
            Logger.getLogger(SalesOrderDAO.class.getName()).log(Level.SEVERE, null, x);
        }
        return salesOrders;
    }

    public ArrayList<SalesOrder> getListOfSalesOrdersWithStatusFrom(String orderStatus, int customerID) {
        ArrayList<SalesOrder> salesOrders = new ArrayList<>();
        try {
            DBConnectionFactory myFactory = DBConnectionFactory.getInstance();
            Connection conn = myFactory.getConnection();

            PreparedStatement ps = conn.prepareStatement("SELECT * FROM " + SalesOrder.TABLE_NAME
                    + " WHERE " + SalesOrder.COLUMN_ORDER_STATUS + " = ? AND " + SalesOrder.COLUMN_CUSTOMER_ID + " = ?");
            ps.setString(1, orderStatus);
            ps.setInt(2, customerID);

            ResultSet rs = ps.executeQuery();
            salesOrders = getDataFromResultSet(rs);

            rs.close();
            ps.close();
            conn.close();
        } catch (SQLException x) {
            Logger.getLogger(SalesOrderDAO.class.getName()).log(Level.SEVERE, null, x);
        }
        return salesOrders;
    }

    public ArrayList<SalesOrder> getSalesOrderListForCustomerSalesReportFrom(int customerID, String startDate, String endDate) {
        ArrayList<SalesOrder> salesOrders = new ArrayList<>();
        try {
            DBConnectionFactory myFactory = DBConnectionFactory.getInstance();
            Connection conn = myFactory.getConnection();

            PreparedStatement ps = conn.prepareStatement("SELECT * FROM " + SalesOrder.TABLE_NAME + " WHERE " + SalesOrder.COLUMN_ORDER_STATUS + " != ? "
                    + "AND " + SalesOrder.COLUMN_ORDER_STATUS + " != ? AND STR_TO_DATE(" + SalesOrder.COLUMN_ORDER_DATE + ", '%Y-%m-%d') >= STR_TO_DATE(?, '%Y-%m-%d') "
                    + " AND STR_TO_DATE(" + SalesOrder.COLUMN_ORDER_DATE + ", '%Y-%m-%d') <= STR_TO_DATE(?, '%Y-%m-%d') AND " + SalesOrder.COLUMN_CUSTOMER_ID + " = ?");
            ps.setString(1, "Rejected");
            ps.setString(2, "Pending");
            ps.setString(3, startDate);
            ps.setString(4, endDate);
            ps.setInt(5, customerID);

            ResultSet rs = ps.executeQuery();
            salesOrders = getDataFromResultSet(rs);

            rs.close();
            ps.close();
            conn.close();
        } catch (SQLException x) {
            Logger.getLogger(SalesOrderDAO.class.getName()).log(Level.SEVERE, null, x);
        }
        return salesOrders;
    }

    public ArrayList<SalesOrder> getInfoForDeliver(String startDate, String endDate) {
        ArrayList<SalesOrder> salesOrders = new ArrayList<>();
        try {
            DBConnectionFactory myFactory = DBConnectionFactory.getInstance();
            Connection conn = myFactory.getConnection();

            PreparedStatement ps = conn.prepareStatement("SELECT SO.SalesOrderNumber, C.LastName AS 'CLastName', C.FirstName AS 'CFirstName', C.MiddleName AS 'CMiddleName', SO.Address, T1.DateUpdated, SUM(SOI.Quantity) AS 'OrderQuantity', E.LastName AS 'ELastName', E.FirstName AS 'EFirstName', E.MiddleName AS 'EMiddleName'\n"
                    + "FROM SalesOrder SO JOIN Customer C ON SO.CustomerID = C.CustomerID\n"
                    + "			  JOIN SalesOrderItem SOI ON SO.SalesOrderNumber = SOI.SalesOrderNumber\n"
                    + "                    JOIN (select sd1.*\n"
                    + "				from \n"
                    + "				salesdelivery sd1 JOIN\n"
                    + "				(select max(str_to_date(sd.DateUpdated, '%Y-%m-%d %H:%i:%s')) AS 'currentstatus', sd.salesordernumber\n"
                    + "				from salesdelivery sd\n"
                    + "				group by sd.salesordernumber) T1 on sd1.salesordernumber = T1.salesordernumber AND T1.currentstatus = sd1.dateupdated)  t1  ON SO.SalesOrderNumber = T1.SalesOrderNumber\n"
                    + "			JOIN Employee E ON SO.DeliveredBy = E.EmployeeID\n"
                    + "WHERE STR_TO_DATE(t1.dateupdated, '%Y-%m-%d') >= STR_TO_DATE(?, '%Y-%m-%d') AND STR_TO_DATE(t1.dateupdated, '%Y-%m-%d') <= STR_TO_DATE(?, '%Y-%m-%d') AND SO.OrderStatus = 'Completed'\n"
                    + "GROUP BY SO.SalesOrderNumber");
            ps.setString(1, startDate);
            ps.setString(2, endDate);

            ResultSet rs = ps.executeQuery();
            while (rs.next()) {
                SalesOrder salesOrder = new SalesOrder();
                salesOrder.setSalesOrderNumber(rs.getInt("SalesOrderNumber"));
                salesOrder.setCustomerName(rs.getString("CLastName") + ", " + rs.getString("CFirstName") + " " + rs.getString("CMiddleName"));
                salesOrder.setAddress(rs.getString("Address"));
                salesOrder.setRequestDelivery(rs.getString("DateUpdated"));
                salesOrder.setOrderTotal(rs.getInt("OrderQuantity"));
                salesOrder.setDeliverer(rs.getString("ELastName") + ", " + rs.getString("EFirstName") + " " + rs.getString("EMiddleName"));
                salesOrders.add(salesOrder);
            }

            rs.close();
            ps.close();
            conn.close();
        } catch (SQLException x) {
            Logger.getLogger(SalesOrderDAO.class.getName()).log(Level.SEVERE, null, x);
        }
        return salesOrders;
    }

    private ArrayList<SalesOrder> getDataFromResultSet(ResultSet rs) throws SQLException {
        ArrayList<SalesOrder> salesOrders = new ArrayList<>();
        while (rs.next()) {
            SalesOrder salesOrder = new SalesOrder();
            salesOrder.setAddress(rs.getString(SalesOrder.COLUMN_ADDRESS));
            salesOrder.setApprovedBy(rs.getInt(SalesOrder.COLUMN_APPROVED_BY));
            salesOrder.setContactNo(rs.getString(SalesOrder.COLUMN_CONTACT_NO));
            salesOrder.setContactPerson(rs.getString(SalesOrder.COLUMN_CONTACT_PERSON));
            salesOrder.setCustomerID(rs.getInt(SalesOrder.COLUMN_CUSTOMER_ID));
            salesOrder.setDeliveredBy(rs.getInt(SalesOrder.COLUMN_DELIVERED_BY));
            salesOrder.setOrderDate(rs.getString(SalesOrder.COLUMN_ORDER_DATE));
            salesOrder.setOrderStatus(rs.getString(SalesOrder.COLUMN_ORDER_STATUS));
            salesOrder.setOrderTotal(rs.getDouble(SalesOrder.COLUMN_ORDER_TOTAL));
            salesOrder.setRemarks(rs.getString(SalesOrder.COLUMN_REMARKS));
            salesOrder.setRequestDelivery(rs.getString(SalesOrder.COLUMN_REQUEST_DELIVERY));
            salesOrder.setSalesOrderNumber(rs.getInt(SalesOrder.COLUMN_SALES_ORDER_NUMBER));
            salesOrder.setAssignedTruck(rs.getInt(SalesOrder.COLUMN_ASSIGNED_TRUCK));
            salesOrder.setReceivedBy(rs.getString(SalesOrder.COLUMN_RECEIVED_BY));
            salesOrder.setReceivedDate(rs.getString(SalesOrder.COLUMN_RECEIVED_DATE));
            Customer customer = new CustomerDAO().getCustomerInfo(salesOrder.getCustomerID());
            salesOrder.setCustomerName(customer.getLastName() + ", " + customer.getFirstName());
            salesOrder.setPaymentOption(rs.getString(SalesOrder.COLUMN_PAYMENT_OPTION));
            Employee employee = new EmployeeDAO().getEmployeeInfo(salesOrder.getDeliveredBy());
            salesOrder.setDeliverer(employee.getLastName() + ", " + employee.getFirstName());
            salesOrders.add(salesOrder);
        }
        return salesOrders;
    }
}
