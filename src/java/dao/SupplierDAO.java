/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package dao;

import db.DBConnectionFactory;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;
import object.Supplier;

/**
 *
 * @author RubySenpaii
 */
public class SupplierDAO {
     
    public boolean registerSupplier(Supplier supplier) {
        try {
            DBConnectionFactory myFactory = DBConnectionFactory.getInstance();
            Connection conn = myFactory.getConnection();

            PreparedStatement ps = conn.prepareStatement("INSERT INTO " + Supplier.TABLE_NAME + " "
                    + "(" + Supplier.COLUMN_ADDRESS + ", " + Supplier.COLUMN_CONTACT_NO + ", " + Supplier.COLUMN_CONTACT_PERSON 
                    + ", " + Supplier.COLUMN_EMAIL + ", " + Supplier.COLUMN_FLAG + ", " + Supplier.COLUMN_SUPPLIER_ID
                    + ", " + Supplier.COLUMN_SUPPLIER_NAME + ", " + Supplier.COLUMN_FRIENDLY_RATING + ", " + Supplier.COLUMN_PAYMENT_RATING 
                    + ", " + Supplier.COLUMN_PUNCTUALITY_RATING + ", " + Supplier.COLUMN_VOLUME_RATING + ", " + Supplier.COLUMN_REMARKS + ") "
                    + "VALUES(?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)");
            ps.setString(1, supplier.getAddress());
            ps.setString(2, supplier.getContactNo());
            ps.setString(3, supplier.getContactPerson());
            ps.setString(4, supplier.getEmail());
            ps.setInt(5, supplier.getFlag());
            ps.setInt(6, supplier.getSupplierID());
            ps.setString(7, supplier.getSupplierName());
            ps.setInt(8, supplier.getFriendlyRating());
            ps.setInt(9, supplier.getPaymentRating());
            ps.setInt(10, supplier.getPunctualityRating());
            ps.setInt(11, supplier.getVolumeRating());
            ps.setString(12, supplier.getRemarks());

            ps.executeUpdate();
            ps.close();
            conn.close();
        } catch (SQLException x) {
            Logger.getLogger(SupplierDAO.class.getName()).log(Level.SEVERE, null, x);
            return false;
        }
        return true;
    }

    public boolean updateSupplierInfo(Supplier supplier) {
        try {
            DBConnectionFactory myFactory = DBConnectionFactory.getInstance();
            Connection conn = myFactory.getConnection();

            PreparedStatement ps = conn.prepareStatement("UPDATE " + Supplier.TABLE_NAME
                    + " SET " + Supplier.COLUMN_ADDRESS + " = ?, " + Supplier.COLUMN_CONTACT_NO + " = ?, " + Supplier.COLUMN_CONTACT_PERSON + " = ?, "
                    + Supplier.COLUMN_EMAIL + " = ?, " + Supplier.COLUMN_REMARKS + " = ?, " + Supplier.COLUMN_FRIENDLY_RATING + " = ?, " 
                    + Supplier.COLUMN_PAYMENT_RATING + " = ?, " 
                    + Supplier.COLUMN_PUNCTUALITY_RATING + " = ?, " + Supplier.COLUMN_VOLUME_RATING + " = ?, " + Supplier.COLUMN_SUPPLIER_NAME + " = ? "
                    + "WHERE " + Supplier.COLUMN_SUPPLIER_ID + " = ?");
            ps.setString(1, supplier.getAddress());
            ps.setString(2, supplier.getContactNo());
            ps.setString(3, supplier.getContactPerson());
            ps.setString(4, supplier.getEmail());
            ps.setString(5, supplier.getRemarks());
            ps.setInt(6, supplier.getFriendlyRating());
            ps.setInt(7, supplier.getPaymentRating());
            ps.setInt(8, supplier.getPunctualityRating());
            ps.setInt(9, supplier.getVolumeRating());
            ps.setString(10, supplier.getSupplierName());
            ps.setInt(11, supplier.getSupplierID());

            ps.executeUpdate();
            ps.close();
            conn.close();
        } catch (SQLException ex) {
            Logger.getLogger(SupplierDAO.class.getName()).log(Level.SEVERE, null, ex);
            return false;
        }
        return true;
    }
    
    public boolean updateSupplierFlag(Supplier supplier) {
        try {
            DBConnectionFactory myFactory = DBConnectionFactory.getInstance();
            Connection conn = myFactory.getConnection();

            PreparedStatement ps = conn.prepareStatement("UPDATE " + Supplier.TABLE_NAME
                    + " SET " + Supplier.COLUMN_FLAG + " = ? "
                    + "WHERE " + Supplier.COLUMN_SUPPLIER_ID + " = ?");
            ps.setInt(1, supplier.getFlag());
            ps.setInt(2, supplier.getSupplierID());

            ps.executeUpdate();
            ps.close();
            conn.close();
        } catch (SQLException ex) {
            Logger.getLogger(SupplierDAO.class.getName()).log(Level.SEVERE, null, ex);
            return false;
        }
        return true;
    }

    public ArrayList<Supplier> getListOfSuppliers() {
        ArrayList<Supplier> suppliers = new ArrayList<>();
        try {
            DBConnectionFactory myFactory = DBConnectionFactory.getInstance();
            Connection conn = myFactory.getConnection();

            PreparedStatement ps = conn.prepareStatement("SELECT * FROM " + Supplier.TABLE_NAME + " WHERE " + Supplier.COLUMN_FLAG + " >= 0");

            ResultSet rs = ps.executeQuery();
            suppliers = getDataFromResultSet(rs);

            rs.close();
            ps.close();
            conn.close();
        } catch (SQLException x) {
            Logger.getLogger(SupplierDAO.class.getName()).log(Level.SEVERE, null, x);
        }
        return suppliers;
    }
    
    public ArrayList<Supplier> getListOfSuppliersForProduct(String product) {
        ArrayList<Supplier> suppliers = new ArrayList<>();
        try {
            DBConnectionFactory myFactory = DBConnectionFactory.getInstance();
            Connection conn = myFactory.getConnection();

            PreparedStatement ps = conn.prepareStatement("SELECT S.* FROM Supplier S JOIN SupplierItem SI ON S.SupplierID = SI.SupplierID WHERE SI.ProductName = ?");
            ps.setString(1, product);

            ResultSet rs = ps.executeQuery();
            suppliers = getDataFromResultSet(rs);

            rs.close();
            ps.close();
            conn.close();
        } catch (SQLException x) {
            Logger.getLogger(SupplierDAO.class.getName()).log(Level.SEVERE, null, x);
        }
        return suppliers;
    }
    
    public Supplier getSupplierInfo(int supplierID) {
        ArrayList<Supplier> suppliers = new ArrayList<>();
        try {
            DBConnectionFactory myFactory = DBConnectionFactory.getInstance();
            Connection conn = myFactory.getConnection();

            PreparedStatement ps = conn.prepareStatement("SELECT * FROM " + Supplier.TABLE_NAME + " WHERE " + Supplier.COLUMN_SUPPLIER_ID + " = ?");
            ps.setInt(1, supplierID);

            ResultSet rs = ps.executeQuery();
            suppliers = getDataFromResultSet(rs);

            rs.close();
            ps.close();
            conn.close();
        } catch (SQLException x) {
            Logger.getLogger(SupplierDAO.class.getName()).log(Level.SEVERE, null, x);
        }
        return suppliers.get(0);
    }
    
    public Supplier getSupplierInfo(String supplierName) {
        ArrayList<Supplier> suppliers = new ArrayList<>();
        try {
            DBConnectionFactory myFactory = DBConnectionFactory.getInstance();
            Connection conn = myFactory.getConnection();

            PreparedStatement ps = conn.prepareStatement("SELECT * FROM " + Supplier.TABLE_NAME + " WHERE " + Supplier.COLUMN_SUPPLIER_NAME + " = ?");
            ps.setString(1, supplierName);

            ResultSet rs = ps.executeQuery();
            suppliers = getDataFromResultSet(rs);

            rs.close();
            ps.close();
            conn.close();
        } catch (SQLException x) {
            Logger.getLogger(SupplierDAO.class.getName()).log(Level.SEVERE, null, x);
        }
        return suppliers.get(0);
    }
    
    private ArrayList<Supplier> getDataFromResultSet(ResultSet rs) throws SQLException {
        ArrayList<Supplier> suppliers = new ArrayList<>();
        while (rs.next()) {
            Supplier supplier = new Supplier();
            supplier.setAddress(rs.getString(Supplier.COLUMN_ADDRESS));
            supplier.setContactNo(rs.getString(Supplier.COLUMN_CONTACT_NO));
            supplier.setContactPerson(rs.getString(Supplier.COLUMN_CONTACT_PERSON));
            supplier.setEmail(rs.getString(Supplier.COLUMN_EMAIL));
            supplier.setFlag(rs.getInt(Supplier.COLUMN_FLAG));
            supplier.setSupplierID(rs.getInt(Supplier.COLUMN_SUPPLIER_ID));
            supplier.setSupplierName(rs.getString(Supplier.COLUMN_SUPPLIER_NAME));
            supplier.setFriendlyRating(rs.getInt(Supplier.COLUMN_FRIENDLY_RATING));
            supplier.setPaymentRating(rs.getInt(Supplier.COLUMN_PAYMENT_RATING));
            supplier.setPunctualityRating(rs.getInt(Supplier.COLUMN_PUNCTUALITY_RATING));
            supplier.setVolumeRating(rs.getInt(Supplier.COLUMN_VOLUME_RATING));
            supplier.setRemarks(rs.getString(Supplier.COLUMN_REMARKS));
            suppliers.add(supplier);
        }
        return suppliers;
    }
}
