/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package dao;

import db.DBConnectionFactory;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;
import object.Message;

/**
 *
 * @author RubySenpaii
 */
public class MessageDAO {
    
    public boolean addMessage(Message message) {
        try {
            DBConnectionFactory myFactory = DBConnectionFactory.getInstance();
            Connection conn = myFactory.getConnection();

            PreparedStatement ps = conn.prepareStatement("INSERT INTO " + Message.TABLE_NAME + " "
                    + "(" + Message.COLUMN_CONTACT_NO + ", " + Message.COLUMN_EMAIL + ", " + Message.COLUMN_MESSAGE
                    + ", " + Message.COLUMN_MESSAGE_ID + ", " + Message.COLUMN_NAME + ", " + Message.COLUMN_MESSAGE_DATE + ") "
                    + "VALUES(?, ?, ?, ?, ?, ?)");
            ps.setString(1, message.getContactNo());
            ps.setString(2, message.getEmail());
            ps.setString(3, message.getMessage());
            ps.setInt(4, message.getMessageID());
            ps.setString(5, message.getName());
            ps.setString(6, message.getMessageDate());

            ps.executeUpdate();
            ps.close();
            conn.close();
        } catch (SQLException x) {
            Logger.getLogger(CustomerDAO.class.getName()).log(Level.SEVERE, null, x);
            return false;
        }
        return true;
    }
    
    public ArrayList<Message> getMessages() {
        ArrayList<Message> messages = new ArrayList<>();
        try {
            DBConnectionFactory myFactory = DBConnectionFactory.getInstance();
            Connection conn = myFactory.getConnection();

            PreparedStatement ps = conn.prepareStatement("SELECT * FROM " + Message.TABLE_NAME);

            ResultSet rs = ps.executeQuery();
            messages = getDataFromResultSet(rs);

            rs.close();
            ps.close();
            conn.close();
        } catch (SQLException x) {
            Logger.getLogger(CustomerDAO.class.getName()).log(Level.SEVERE, null, x);
        }
        return messages;
    }
    
    private ArrayList<Message> getDataFromResultSet(ResultSet rs) throws SQLException {
        ArrayList<Message> messages = new ArrayList<>();
        while (rs.next()) {
            Message message = new Message();
            message.setContactNo(rs.getString(Message.COLUMN_CONTACT_NO));
            message.setEmail(rs.getString(Message.COLUMN_EMAIL));
            message.setMessage(rs.getString(Message.COLUMN_MESSAGE));
            message.setMessageID(rs.getInt(Message.COLUMN_MESSAGE_ID));
            message.setName(rs.getString(Message.COLUMN_NAME));
            message.setMessageDate(rs.getString(Message.COLUMN_MESSAGE_DATE));
            messages.add(message);
        }
        return messages;
    }
}
