/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package dao;

import db.DBConnectionFactory;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;
import object.Product;
import object.SalesOrderItem;

/**
 *
 * @author RubySenpaii
 */
public class SalesOrderItemDAO {

    public boolean addSalesOrderItem(SalesOrderItem salesOrder) {
        try {
            DBConnectionFactory myFactory = DBConnectionFactory.getInstance();
            Connection conn = myFactory.getConnection();

            PreparedStatement ps = conn.prepareStatement("INSERT INTO " + SalesOrderItem.TABLE_NAME + " "
                    + "(" + SalesOrderItem.COLUMN_PRODUCT_ID + ", " + SalesOrderItem.COLUMN_SALES_ORDER_NUMBER + ", " + SalesOrderItem.COLUMN_QUANTITY
                    + ", " + SalesOrderItem.COLUMN_UNIT_PRICE + ") "
                    + "VALUES(?, ?, ?, ?)");
            ps.setInt(1, salesOrder.getProductID());
            ps.setInt(2, salesOrder.getSalesOrderNumber());
            ps.setInt(3, salesOrder.getQuantity());
            ps.setDouble(4, salesOrder.getUnitPrice());

            ps.executeUpdate();
            ps.close();
            conn.close();
        } catch (SQLException x) {
            Logger.getLogger(SalesOrderItemDAO.class.getName()).log(Level.SEVERE, null, x);
            return false;
        }
        return true;
    }

    public boolean addSalesOrderItems(ArrayList<SalesOrderItem> salesOrders) {
        try {
            DBConnectionFactory myFactory = DBConnectionFactory.getInstance();
            Connection conn = myFactory.getConnection();
            conn.setAutoCommit(false);

            PreparedStatement ps = conn.prepareStatement("INSERT INTO " + SalesOrderItem.TABLE_NAME + " "
                    + "(" + SalesOrderItem.COLUMN_PRODUCT_ID + ", " + SalesOrderItem.COLUMN_SALES_ORDER_NUMBER + ", " + SalesOrderItem.COLUMN_QUANTITY
                    + ", " + SalesOrderItem.COLUMN_UNIT_PRICE + ") "
                    + "VALUES(?, ?, ?, ?)");
            for (int a = 0; a < salesOrders.size(); a++) {
                ps.setInt(1, salesOrders.get(a).getProductID());
                ps.setInt(2, salesOrders.get(a).getSalesOrderNumber());
                ps.setInt(3, salesOrders.get(a).getQuantity());
                ps.setDouble(4, salesOrders.get(a).getUnitPrice());
                ps.addBatch();
            }

            int[] itemsAdded = ps.executeBatch();
            System.out.println("sales order items recorded: " + itemsAdded.length);
            conn.commit();
            ps.close();
            conn.close();
        } catch (SQLException x) {
            Logger.getLogger(SalesOrderItemDAO.class.getName()).log(Level.SEVERE, null, x);
            return false;
        }
        return true;
    }
    
    public boolean updateSalesOrderItemQuantity(SalesOrderItem salesOrderItem) {
        try {
            DBConnectionFactory myFactory = DBConnectionFactory.getInstance();
            Connection conn = myFactory.getConnection();

            PreparedStatement ps = conn.prepareStatement("UPDATE " + SalesOrderItem.TABLE_NAME
                    + " SET " + SalesOrderItem.COLUMN_QUANTITY + " = ? " 
                    + "WHERE " + SalesOrderItem.COLUMN_SALES_ORDER_NUMBER + " = ? AND " + SalesOrderItem.COLUMN_PRODUCT_ID + " = ?");
            ps.setInt(1, salesOrderItem.getQuantity());
            ps.setInt(2, salesOrderItem.getSalesOrderNumber());
            ps.setInt(3, salesOrderItem.getProductID());
            ps.executeUpdate();
            ps.close();
            conn.close();
        } catch (SQLException ex) {
            Logger.getLogger(SalesOrderItemDAO.class.getName()).log(Level.SEVERE, null, ex);
            return false;
        }
        return true;
    }
    
    public boolean updateSalesOrderNumber(SalesOrderItem salesOrderItem, int salesOrderNumber) {
        try {
            DBConnectionFactory myFactory = DBConnectionFactory.getInstance();
            Connection conn = myFactory.getConnection();

            PreparedStatement ps = conn.prepareStatement("UPDATE " + SalesOrderItem.TABLE_NAME
                    + " SET " + SalesOrderItem.COLUMN_SALES_ORDER_NUMBER + " = ? "
                    + "WHERE " + SalesOrderItem.COLUMN_SALES_ORDER_NUMBER + " = ? AND " + SalesOrderItem.COLUMN_PRODUCT_ID + " = ?");
            ps.setInt(1, salesOrderItem.getSalesOrderNumber());
            ps.setInt(2, salesOrderNumber);
            ps.setInt(3, salesOrderItem.getProductID());
            ps.executeUpdate();
            ps.close();
            conn.close();
        } catch (SQLException ex) {
            Logger.getLogger(SalesOrderItemDAO.class.getName()).log(Level.SEVERE, null, ex);
            return false;
        }
        return true;
    }
    
    
    public ArrayList<SalesOrderItem> getListOfSalesOrderItems() {
        ArrayList<SalesOrderItem> salesOrderItems = new ArrayList<>();
        try {
            DBConnectionFactory myFactory = DBConnectionFactory.getInstance();
            Connection conn = myFactory.getConnection();

            PreparedStatement ps = conn.prepareStatement("SELECT * FROM " + SalesOrderItem.TABLE_NAME);

            ResultSet rs = ps.executeQuery();
            salesOrderItems = getDataFromResultSet(rs);

            rs.close();
            ps.close();
            conn.close();
        } catch (SQLException x) {
            Logger.getLogger(SalesOrderItemDAO.class.getName()).log(Level.SEVERE, null, x);
        }
        return salesOrderItems;
    }
    
    public ArrayList<SalesOrderItem> getListOfSalesOrderItems(int salesOrderNumber) {
        ArrayList<SalesOrderItem> salesOrderItems = new ArrayList<>();
        try {
            DBConnectionFactory myFactory = DBConnectionFactory.getInstance();
            Connection conn = myFactory.getConnection();

            PreparedStatement ps = conn.prepareStatement("SELECT * FROM " + SalesOrderItem.TABLE_NAME 
                    + " WHERE " + SalesOrderItem.COLUMN_SALES_ORDER_NUMBER + " = ? AND " + SalesOrderItem.COLUMN_QUANTITY + " > 0");
            ps.setInt(1, salesOrderNumber);

            ResultSet rs = ps.executeQuery();
            salesOrderItems = getDataFromResultSet(rs);

            rs.close();
            ps.close();
            conn.close();
        } catch (SQLException x) {
            Logger.getLogger(SalesOrderItemDAO.class.getName()).log(Level.SEVERE, null, x);
        }
        return salesOrderItems;
    }

    private ArrayList<SalesOrderItem> getDataFromResultSet(ResultSet rs) throws SQLException {
        ArrayList<SalesOrderItem> salesOrderItems = new ArrayList<>();
        while (rs.next()) {
            SalesOrderItem salesOrderItem = new SalesOrderItem();
            salesOrderItem.setProductID(rs.getInt(SalesOrderItem.COLUMN_PRODUCT_ID));
            salesOrderItem.setSalesOrderNumber(rs.getInt(SalesOrderItem.COLUMN_SALES_ORDER_NUMBER));
            salesOrderItem.setQuantity(rs.getInt(SalesOrderItem.COLUMN_QUANTITY));
            salesOrderItem.setUnitPrice(rs.getDouble(SalesOrderItem.COLUMN_UNIT_PRICE));
            Product product = new ProductDAO().getProductDetail(salesOrderItem.getProductID());
            salesOrderItem.setProductName(product.getProductName());
            salesOrderItems.add(salesOrderItem);
        }
        return salesOrderItems;
    }
}
