/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package dao;

import db.DBConnectionFactory;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;
import object.TruckReference;

/**
 *
 * @author RubySenpaii
 */
public class TruckReferenceDAO {

    public boolean registerTruck(TruckReference truck) {
        try {
            DBConnectionFactory myFactory = DBConnectionFactory.getInstance();
            Connection conn = myFactory.getConnection();

            PreparedStatement ps = conn.prepareStatement("INSERT INTO " + TruckReference.TABLE_NAME + " "
                    + "(" + TruckReference.COLUMN_CAPACITY + ", " + TruckReference.COLUMN_PLATE_NUMBER + ", " + TruckReference.COLUMN_TRUCK_ID + ") "
                    + "VALUES(?, ?, ?)");
            ps.setInt(1, truck.getCapacity());
            ps.setString(2, truck.getPlateNumber());
            ps.setInt(3, truck.getTruckID());

            ps.executeUpdate();
            ps.close();
            conn.close();
        } catch (SQLException x) {
            Logger.getLogger(TruckReferenceDAO.class.getName()).log(Level.SEVERE, null, x);
            return false;
        }
        return true;
    }

    public ArrayList<TruckReference> getListOfTrucks() {
        ArrayList<TruckReference> trucks = new ArrayList<>();
        try {
            DBConnectionFactory myFactory = DBConnectionFactory.getInstance();
            Connection conn = myFactory.getConnection();

            PreparedStatement ps = conn.prepareStatement("SELECT * FROM " + TruckReference.TABLE_NAME + " WHERE " + TruckReference.COLUMN_TRUCK_ID + " > 0");

            ResultSet rs = ps.executeQuery();
            trucks = getDataFromResultSet(rs);

            rs.close();
            ps.close();
            conn.close();
        } catch (SQLException x) {
            Logger.getLogger(TruckReferenceDAO.class.getName()).log(Level.SEVERE, null, x);
        }
        return trucks;
    }

    public ArrayList<TruckReference> getListOfTrucksWithDeliveryDetails() {
        ArrayList<TruckReference> trucks = new ArrayList<>();
        try {
            DBConnectionFactory myFactory = DBConnectionFactory.getInstance();
            Connection conn = myFactory.getConnection();

            PreparedStatement ps = conn.prepareStatement("SELECT TR.*, SOI.*, SO.Address, P.ProductName "
                    + "FROM TruckReference TR JOIN SalesOrder SO ON TR.TruckID = SO.AssignedTruck "
                    + "                       JOIN SalesOrderItem SOI ON SO.SalesOrderNumber = SOI.SalesOrderNumber "
                    + "                       JOIN Product P ON P.ProductID = SOI.ProductID "
                    + "WHERE SO.OrderStatus = 'Delivery'");

            ResultSet rs = ps.executeQuery();
            while (rs.next()) {
                TruckReference truck = new TruckReference();
                truck.setPlateNumber(rs.getString(TruckReference.COLUMN_PLATE_NUMBER));
                truck.setTruckID(rs.getInt(TruckReference.COLUMN_TRUCK_ID));
                truck.setCapacity(rs.getInt(TruckReference.COLUMN_CAPACITY));
                truck.setProductName(rs.getString("ProductName"));
                truck.setQuantity(rs.getInt("Quantity"));
                truck.setSalesOrderNumber(rs.getInt("SalesOrderNumber"));
                truck.setAddress(rs.getString("Address"));
                trucks.add(truck);
            }

            rs.close();
            ps.close();
            conn.close();
        } catch (SQLException x) {
            Logger.getLogger(TruckReferenceDAO.class.getName()).log(Level.SEVERE, null, x);
        }
        return trucks;
    }

    public ArrayList<TruckReference> getListOfTrucksWithFormattedDeliveryDetails() {
        ArrayList<TruckReference> trucks = new ArrayList<>();
        try {
            DBConnectionFactory myFactory = DBConnectionFactory.getInstance();
            Connection conn = myFactory.getConnection();

            PreparedStatement ps = conn.prepareStatement("SELECT TR.*, SOI.*, SO.Address, P.ProductName "
                    + "FROM TruckReference TR JOIN SalesOrder SO ON TR.TruckID = SO.AssignedTruck "
                    + "                       JOIN SalesOrderItem SOI ON SO.SalesOrderNumber = SOI.SalesOrderNumber "
                    + "                       JOIN Product P ON P.ProductID = SOI.ProductID "
                    + "WHERE SO.OrderStatus = 'Delivery'");

            ResultSet rs = ps.executeQuery();
            int totalQty = 0;
            while (rs.next()) {
                TruckReference truck = new TruckReference();
                truck.setPlateNumber(rs.getString(TruckReference.COLUMN_PLATE_NUMBER));
                truck.setTruckID(rs.getInt(TruckReference.COLUMN_TRUCK_ID));
                truck.setCapacity(rs.getInt(TruckReference.COLUMN_CAPACITY));
                String productName = rs.getString("ProductName");
                int quantity = rs.getInt("Quantity");
                int salesOrderNumber = rs.getInt("SalesOrderNumber");
                String address = rs.getString("Address");
                String detail = salesOrderNumber + " - " + productName + " - " + quantity + " - " + address;
                totalQty += quantity;
                if (trucks.isEmpty()) {
                    truck.setDetails(detail);
                    truck.setTotalQuantity(totalQty);
                    trucks.add(truck);
                } else if (truck.getTruckID() == trucks.get(trucks.size() - 1).getTruckID()) {
                    String prevDetails = trucks.get(trucks.size() - 1).getDetails();
                    trucks.get(trucks.size() - 1).setDetails(prevDetails + "<br>" + detail);
                    trucks.get(trucks.size() - 1).setTotalQuantity(totalQty);
                } else {
                    truck.setDetails(detail);
                    truck.setTotalQuantity(totalQty);
                    trucks.add(truck);
                }
            }

            rs.close();
            ps.close();
            conn.close();
        } catch (SQLException x) {
            Logger.getLogger(TruckReferenceDAO.class.getName()).log(Level.SEVERE, null, x);
        }
        return trucks;
    }
    
    public ArrayList<TruckReference> getListOfTrucksWithFormattedDeliveryDetails(int truckID) {
        ArrayList<TruckReference> trucks = new ArrayList<>();
        try {
            DBConnectionFactory myFactory = DBConnectionFactory.getInstance();
            Connection conn = myFactory.getConnection();

            PreparedStatement ps = conn.prepareStatement("SELECT TR.*, SOI.*, SO.Address, P.ProductName "
                    + "FROM TruckReference TR JOIN SalesOrder SO ON TR.TruckID = SO.AssignedTruck "
                    + "                       JOIN SalesOrderItem SOI ON SO.SalesOrderNumber = SOI.SalesOrderNumber "
                    + "                       JOIN Product P ON P.ProductID = SOI.ProductID "
                    + "WHERE SO.OrderStatus = 'Delivery' AND TR.TruckID = ?");
            ps.setInt(1, truckID);

            ResultSet rs = ps.executeQuery();
            int totalQty = 0;
            while (rs.next()) {
                TruckReference truck = new TruckReference();
                truck.setPlateNumber(rs.getString(TruckReference.COLUMN_PLATE_NUMBER));
                truck.setTruckID(rs.getInt(TruckReference.COLUMN_TRUCK_ID));
                truck.setCapacity(rs.getInt(TruckReference.COLUMN_CAPACITY));
                int quantity = rs.getInt("Quantity");
                int salesOrderNumber = rs.getInt("SalesOrderNumber");
                String address = rs.getString("Address");
                truck.setSalesOrderNumber(salesOrderNumber);
                truck.setQuantity(quantity);
                truck.setAddress(address);
                trucks.add(truck);
            }

            rs.close();
            ps.close();
            conn.close();
        } catch (SQLException x) {
            Logger.getLogger(TruckReferenceDAO.class.getName()).log(Level.SEVERE, null, x);
        }
        return trucks;
    }

    public ArrayList<TruckReference> getListOfTrucksWithFormattedDeliveryDetails(String plateNumber) {
        ArrayList<TruckReference> trucks = new ArrayList<>();
        try {
            DBConnectionFactory myFactory = DBConnectionFactory.getInstance();
            Connection conn = myFactory.getConnection();

            PreparedStatement ps = conn.prepareStatement("SELECT TR.*, SOI.*, SO.Address, P.ProductName "
                    + "FROM TruckReference TR JOIN SalesOrder SO ON TR.TruckID = SO.AssignedTruck "
                    + "                       JOIN SalesOrderItem SOI ON SO.SalesOrderNumber = SOI.SalesOrderNumber "
                    + "                       JOIN Product P ON P.ProductID = SOI.ProductID "
                    + "WHERE SO.OrderStatus = 'Delivery' AND TR.PlateNumber = ?");
            ps.setString(1, plateNumber);

            ResultSet rs = ps.executeQuery();
            int totalQty = 0;
            while (rs.next()) {
                TruckReference truck = new TruckReference();
                truck.setPlateNumber(rs.getString(TruckReference.COLUMN_PLATE_NUMBER));
                truck.setTruckID(rs.getInt(TruckReference.COLUMN_TRUCK_ID));
                truck.setCapacity(rs.getInt(TruckReference.COLUMN_CAPACITY));
                int quantity = rs.getInt("Quantity");
                int salesOrderNumber = rs.getInt("SalesOrderNumber");
                String address = rs.getString("Address");
                truck.setSalesOrderNumber(salesOrderNumber);
                truck.setQuantity(quantity);
                truck.setAddress(address);
                trucks.add(truck);
            }

            rs.close();
            ps.close();
            conn.close();
        } catch (SQLException x) {
            Logger.getLogger(TruckReferenceDAO.class.getName()).log(Level.SEVERE, null, x);
        }
        return trucks;
    }

    private ArrayList<TruckReference> getDataFromResultSet(ResultSet rs) throws SQLException {
        ArrayList<TruckReference> trucks = new ArrayList<>();
        while (rs.next()) {
            TruckReference truck = new TruckReference();
            truck.setPlateNumber(rs.getString(TruckReference.COLUMN_PLATE_NUMBER));
            truck.setTruckID(rs.getInt(TruckReference.COLUMN_TRUCK_ID));
            truck.setCapacity(rs.getInt(TruckReference.COLUMN_CAPACITY));
            trucks.add(truck);
        }
        return trucks;
    }
}
