/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package dao;

import db.DBConnectionFactory;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;
import object.Employee;

/**
 *
 * @author RubySenpaii
 */
public class EmployeeDAO {

    public boolean registerEmployee(Employee employee) {
        try {
            DBConnectionFactory myFactory = DBConnectionFactory.getInstance();
            Connection conn = myFactory.getConnection();

            PreparedStatement ps = conn.prepareStatement("INSERT INTO " + Employee.TABLE_NAME + " "
                    + "(" + Employee.COLUMN_ADDRESS + ", " + Employee.COLUMN_CONTACT_NO + ", " + Employee.COLUMN_EMAIL
                    + ", " + Employee.COLUMN_EMPLOYEE_ID + ", " + Employee.COLUMN_FIRST_NAME + ", " + Employee.COLUMN_FLAG
                    + ", " + Employee.COLUMN_LAST_NAME + ", " + Employee.COLUMN_MIDDLE_NAME + ", " + Employee.COLUMN_PASSWORD
                    + ", " + Employee.COLUMN_POSITION + ", " + Employee.COLUMN_USERNAME + ", " + Employee.COLUMN_USER_LEVEL
                    + ", " + Employee.COLUMN_BIRTHDAY + ", " + Employee.COLUMN_MOTHER_MAIDEN + ") "
                    + "VALUES(?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)");
            ps.setString(1, employee.getAddress());
            ps.setString(2, employee.getContactNo());
            ps.setString(3, employee.getEmail());
            ps.setInt(4, employee.getEmployeeID());
            ps.setString(5, employee.getFirstName());
            ps.setInt(6, employee.getFlag());
            ps.setString(7, employee.getLastName());
            ps.setString(8, employee.getMiddleName());
            ps.setString(9, employee.getPassword());
            ps.setString(10, employee.getPosition());
            ps.setString(11, employee.getUsername());
            ps.setInt(12, employee.getUserLevel());
            ps.setString(13, employee.getBirthday());
            ps.setString(14, employee.getMotherMaiden());

            ps.executeUpdate();
            ps.close();
            conn.close();
        } catch (SQLException x) {
            Logger.getLogger(EmployeeDAO.class.getName()).log(Level.SEVERE, null, x);
            return false;
        }
        return true;
    }

    public boolean updateEmployeeInfo(Employee employee) {
        try {
            DBConnectionFactory myFactory = DBConnectionFactory.getInstance();
            Connection conn = myFactory.getConnection();

            PreparedStatement ps = conn.prepareStatement("UPDATE " + Employee.TABLE_NAME
                    + " SET " + Employee.COLUMN_ADDRESS + " = ?, " + Employee.COLUMN_CONTACT_NO + " = ?, " + Employee.COLUMN_EMAIL + " = ?, "
                    + Employee.COLUMN_FIRST_NAME + " = ?, " + Employee.COLUMN_LAST_NAME + " = ?, " + Employee.COLUMN_MIDDLE_NAME + " = ?, "
                    + Employee.COLUMN_PASSWORD + " = ? "
                    + "WHERE " + Employee.COLUMN_EMPLOYEE_ID + " = ?");
            ps.setString(1, employee.getAddress());
            ps.setString(2, employee.getContactNo());
            ps.setString(3, employee.getEmail());
            ps.setString(4, employee.getFirstName());
            ps.setString(5, employee.getLastName());
            ps.setString(6, employee.getMiddleName());
            ps.setString(7, employee.getPassword());
            ps.setInt(8, employee.getEmployeeID());

            ps.executeUpdate();
            ps.close();
            conn.close();
        } catch (SQLException ex) {
            Logger.getLogger(EmployeeDAO.class.getName()).log(Level.SEVERE, null, ex);
            return false;
        }
        return true;
    }

    public boolean updateEmployeeFlag(Employee employee) {
        try {
            DBConnectionFactory myFactory = DBConnectionFactory.getInstance();
            Connection conn = myFactory.getConnection();

            PreparedStatement ps = conn.prepareStatement("UPDATE " + Employee.TABLE_NAME
                    + " SET " + Employee.COLUMN_FLAG + " = ? "
                    + "WHERE " + Employee.COLUMN_EMPLOYEE_ID + " = ?");
            ps.setInt(1, employee.getFlag());
            ps.setInt(2, employee.getEmployeeID());

            ps.executeUpdate();
            ps.close();
            conn.close();
        } catch (SQLException ex) {
            Logger.getLogger(EmployeeDAO.class.getName()).log(Level.SEVERE, null, ex);
            return false;
        }
        return true;
    }

    public boolean checkEmployeeName(String lastName, String firstName, String middleName) {
        ArrayList<Employee> employees = new ArrayList<>();
        try {
            DBConnectionFactory myFactory = DBConnectionFactory.getInstance();
            Connection conn = myFactory.getConnection();

            PreparedStatement ps = conn.prepareStatement("SELECT * FROM " + Employee.TABLE_NAME
                    + " WHERE " + Employee.COLUMN_LAST_NAME + " = ? AND " + Employee.COLUMN_FIRST_NAME + " = ? AND " + Employee.COLUMN_MIDDLE_NAME + " = ?");
            ps.setString(1, lastName);
            ps.setString(2, firstName);
            ps.setString(3, middleName);

            ResultSet rs = ps.executeQuery();
            employees = getDataFromResultSet(rs);

            rs.close();
            ps.close();
            conn.close();
        } catch (SQLException x) {
            Logger.getLogger(EmployeeDAO.class.getName()).log(Level.SEVERE, null, x);
        }
        if (employees.size() > 0) {
            return true;
        }
        return false;
    }

    public ArrayList<Employee> getListOfEmployees() {
        ArrayList<Employee> employees = new ArrayList<>();
        try {
            DBConnectionFactory myFactory = DBConnectionFactory.getInstance();
            Connection conn = myFactory.getConnection();

            PreparedStatement ps = conn.prepareStatement("SELECT * FROM " + Employee.TABLE_NAME + " WHERE " + Employee.COLUMN_FLAG + " >= 0");

            ResultSet rs = ps.executeQuery();
            employees = getDataFromResultSet(rs);

            rs.close();
            ps.close();
            conn.close();
        } catch (SQLException x) {
            Logger.getLogger(EmployeeDAO.class.getName()).log(Level.SEVERE, null, x);
        }
        return employees;
    }

    public ArrayList<Employee> getListOfDrivers() {
        ArrayList<Employee> employees = new ArrayList<>();
        try {
            DBConnectionFactory myFactory = DBConnectionFactory.getInstance();
            Connection conn = myFactory.getConnection();

            PreparedStatement ps = conn.prepareStatement("SELECT * FROM " + Employee.TABLE_NAME + " WHERE " + Employee.COLUMN_FLAG + " >= 0" + " AND " + Employee.COLUMN_POSITION + " = ?");
            ps.setString(1, "Truck Driver");

            ResultSet rs = ps.executeQuery();
            employees = getDataFromResultSet(rs);

            rs.close();
            ps.close();
            conn.close();
        } catch (SQLException x) {
            Logger.getLogger(EmployeeDAO.class.getName()).log(Level.SEVERE, null, x);
        }
        return employees;
    }

    public boolean doesEmployeeExist(String username, String password) {
        boolean doesExist = false;
        try {
            DBConnectionFactory myFactory = DBConnectionFactory.getInstance();
            Connection conn = myFactory.getConnection();

            PreparedStatement ps = conn.prepareStatement("SELECT * FROM " + Employee.TABLE_NAME + " WHERE " + Employee.COLUMN_USERNAME + " = ? AND " + Employee.COLUMN_PASSWORD + " = ?");
            ps.setString(1, username);
            ps.setString(2, password);

            ResultSet rs = ps.executeQuery();
            if (getDataFromResultSet(rs).size() > 0) {
                doesExist = true;
            }

            rs.close();
            ps.close();
            conn.close();
        } catch (SQLException x) {
            Logger.getLogger(EmployeeDAO.class.getName()).log(Level.SEVERE, null, x);
        }
        return doesExist;
    }

    public Employee getEmployeeInfo(String username) {
        Employee employee = new Employee();
        try {
            DBConnectionFactory myFactory = DBConnectionFactory.getInstance();
            Connection conn = myFactory.getConnection();

            PreparedStatement ps = conn.prepareStatement("SELECT * FROM " + Employee.TABLE_NAME + " WHERE " + Employee.COLUMN_USERNAME + " = ?");
            ps.setString(1, username);

            ResultSet rs = ps.executeQuery();
            employee = getDataFromResultSet(rs).get(0);

            rs.close();
            ps.close();
            conn.close();
        } catch (SQLException x) {
            Logger.getLogger(EmployeeDAO.class.getName()).log(Level.SEVERE, null, x);
        }
        return employee;
    }

    public Employee getEmployeeInfo(int employeeID) {
        Employee employee = new Employee();
        try {
            DBConnectionFactory myFactory = DBConnectionFactory.getInstance();
            Connection conn = myFactory.getConnection();

            PreparedStatement ps = conn.prepareStatement("SELECT * FROM " + Employee.TABLE_NAME + " WHERE " + Employee.COLUMN_EMPLOYEE_ID + " = ?");
            ps.setInt(1, employeeID);

            ResultSet rs = ps.executeQuery();
            employee = getDataFromResultSet(rs).get(0);

            rs.close();
            ps.close();
            conn.close();
        } catch (SQLException x) {
            Logger.getLogger(EmployeeDAO.class.getName()).log(Level.SEVERE, null, x);
        }
        return employee;
    }

    private ArrayList<Employee> getDataFromResultSet(ResultSet rs) throws SQLException {
        ArrayList<Employee> employees = new ArrayList<>();
        while (rs.next()) {
            Employee employee = new Employee();
            employee.setAddress(rs.getString(Employee.COLUMN_ADDRESS));
            employee.setContactNo(rs.getString(Employee.COLUMN_CONTACT_NO));
            employee.setEmail(rs.getString(Employee.COLUMN_EMAIL));
            employee.setEmployeeID(rs.getInt(Employee.COLUMN_EMPLOYEE_ID));
            employee.setFirstName(rs.getString(Employee.COLUMN_FIRST_NAME));
            employee.setFlag(rs.getInt(Employee.COLUMN_FLAG));
            employee.setLastName(rs.getString(Employee.COLUMN_LAST_NAME));
            employee.setMiddleName(rs.getString(Employee.COLUMN_MIDDLE_NAME));
            employee.setPassword(rs.getString(Employee.COLUMN_PASSWORD));
            employee.setPosition(rs.getString(Employee.COLUMN_POSITION));
            employee.setUsername(rs.getString(Employee.COLUMN_USERNAME));
            employee.setUserLevel(rs.getInt(Employee.COLUMN_USER_LEVEL));
            employee.setBirthday(rs.getString(Employee.COLUMN_BIRTHDAY));
            employee.setMotherMaiden(rs.getString(Employee.COLUMN_MOTHER_MAIDEN));
            employees.add(employee);
        }
        return employees;
    }
}
