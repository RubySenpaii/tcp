/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package dao;

import db.DBConnectionFactory;
import extra.Reference;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;
import object.Customer;

/**
 *
 * @author RubySenpaii
 */
public class CustomerDAO {

    public boolean registerCustomer(Customer customer) {
        try {
            DBConnectionFactory myFactory = DBConnectionFactory.getInstance();
            Connection conn = myFactory.getConnection();

            PreparedStatement ps = conn.prepareStatement("INSERT INTO " + Customer.TABLE_NAME + " "
                    + "(" + Customer.COLUMN_ADDRESS + ", " + Customer.COLUMN_CONTACT_NO + ", " + Customer.COLUMN_CUSTOMER_ID
                    + ", " + Customer.COLUMN_EMAIL + ", " + Customer.COLUMN_FIRST_NAME + ", " + Customer.COLUMN_FLAG
                    + ", " + Customer.COLUMN_LAST_NAME + ", " + Customer.COLUMN_MIDDLE_NAME + ", " + Customer.COLUMN_PASSWORD
                    + ", " + Customer.COLUMN_USERNAME + ", " + Customer.COLUMN_BIRTHDAY + ", " + Customer.COLUMN_MOTHER_MAIDEN + ") "
                    + "VALUES(?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)");
            ps.setString(1, customer.getAddress());
            ps.setString(2, customer.getContactNo());
            ps.setInt(3, customer.getCustomerID());
            ps.setString(4, customer.getEmail());
            ps.setString(5, customer.getFirstName());
            ps.setInt(6, customer.getFlag());
            ps.setString(7, customer.getLastName());
            ps.setString(8, customer.getMiddleName());
            ps.setString(9, customer.getPassword());
            ps.setString(10, customer.getUsername());
            ps.setString(11, customer.getBirthday());
            ps.setString(12, customer.getMotherMaiden());

            ps.executeUpdate();
            ps.close();
            conn.close();
        } catch (SQLException x) {
            Logger.getLogger(CustomerDAO.class.getName()).log(Level.SEVERE, null, x);
            return false;
        }
        return true;
    }

    public boolean doesCustomerExist(String username, String password) {
        boolean doesExist = false;
        try {
            DBConnectionFactory myFactory = DBConnectionFactory.getInstance();
            Connection conn = myFactory.getConnection();

            PreparedStatement ps = conn.prepareStatement("SELECT * FROM " + Customer.TABLE_NAME + " WHERE " + Customer.COLUMN_USERNAME + " = ? AND " + Customer.COLUMN_PASSWORD + " = ?");
            ps.setString(1, username);
            ps.setString(2, password);

            ResultSet rs = ps.executeQuery();
            if (getDataFromResultSet(rs).size() > 0) {
                doesExist = true;
            }

            rs.close();
            ps.close();
            conn.close();
        } catch (SQLException x) {
            Logger.getLogger(CustomerDAO.class.getName()).log(Level.SEVERE, null, x);
        }
        return doesExist;
    }

    public boolean updateCustomerInfo(Customer customer) {
        try {
            DBConnectionFactory myFactory = DBConnectionFactory.getInstance();
            Connection conn = myFactory.getConnection();

            PreparedStatement ps = conn.prepareStatement("UPDATE " + Customer.TABLE_NAME
                    + " SET " + Customer.COLUMN_ADDRESS + " = ?, " + Customer.COLUMN_CONTACT_NO + " = ?, " + Customer.COLUMN_EMAIL + " = ?, "
                    + Customer.COLUMN_FIRST_NAME + " = ?, " + Customer.COLUMN_LAST_NAME + " = ?, " + Customer.COLUMN_MIDDLE_NAME + " = ?, "
                    + Customer.COLUMN_PASSWORD + " = ? "
                    + "WHERE " + Customer.COLUMN_CUSTOMER_ID + " = ?");
            ps.setString(1, customer.getAddress());
            ps.setString(2, customer.getContactNo());
            ps.setString(3, customer.getEmail());
            ps.setString(4, customer.getFirstName());
            ps.setString(5, customer.getLastName());
            ps.setString(6, customer.getMiddleName());
            ps.setString(7, customer.getPassword());
            ps.setInt(8, customer.getCustomerID());

            ps.executeUpdate();
            ps.close();
            conn.close();
        } catch (SQLException ex) {
            Logger.getLogger(CustomerDAO.class.getName()).log(Level.SEVERE, null, ex);
            return false;
        }
        return true;
    }

    public boolean updateCustomerFlag(Customer customer) {
        try {
            DBConnectionFactory myFactory = DBConnectionFactory.getInstance();
            Connection conn = myFactory.getConnection();

            PreparedStatement ps = conn.prepareStatement("UPDATE " + Customer.TABLE_NAME
                    + " SET " + Customer.COLUMN_FLAG + " = ? "
                    + "WHERE " + Customer.COLUMN_CUSTOMER_ID + " = ?");
            ps.setInt(1, customer.getFlag());
            ps.setInt(2, customer.getCustomerID());

            ps.executeUpdate();
            ps.close();
            conn.close();
        } catch (SQLException ex) {
            Logger.getLogger(CustomerDAO.class.getName()).log(Level.SEVERE, null, ex);
            return false;
        }
        return true;
    }

    public boolean checkCustomerName(String lastName, String firstName, String middleName) {
        Customer customer = new Customer();
        try {
            DBConnectionFactory myFactory = DBConnectionFactory.getInstance();
            Connection conn = myFactory.getConnection();

            PreparedStatement ps = conn.prepareStatement("SELECT * FROM " + Customer.TABLE_NAME
                    + " WHERE " + Customer.COLUMN_LAST_NAME + " = ? AND " + Customer.COLUMN_FIRST_NAME + " = ? AND " + Customer.COLUMN_MIDDLE_NAME + " = ?");
            ps.setString(1, lastName);
            ps.setString(2, firstName);
            ps.setString(3, middleName);

            ResultSet rs = ps.executeQuery();
            customer = getDataFromResultSet(rs).get(0);

            rs.close();
            ps.close();
            conn.close();
        } catch (SQLException x) {
            Logger.getLogger(CustomerDAO.class.getName()).log(Level.SEVERE, null, x);
        } catch (IndexOutOfBoundsException x) {
            System.out.println(x);
            return true;
        }
        return false;
    }

    public boolean checkCustomerContactNo(String contactNo) {
        try {
            Customer customer = new Customer();
            DBConnectionFactory myFactory = DBConnectionFactory.getInstance();
            Connection conn = myFactory.getConnection();

            PreparedStatement ps = conn.prepareStatement("SELECT * FROM " + Customer.TABLE_NAME + " WHERE " + Customer.COLUMN_CONTACT_NO + " = ?");
            ps.setString(1, contactNo);

            ResultSet rs = ps.executeQuery();
            customer = getDataFromResultSet(rs).get(0);

            rs.close();
            ps.close();
            conn.close();
        } catch (SQLException x) {
            Logger.getLogger(CustomerDAO.class.getName()).log(Level.SEVERE, null, x);
        } catch (IndexOutOfBoundsException x) {
            System.out.println(x);
            return true;
        }
        return false;
    }
    
    public boolean checkCustomerEmail(String email) {
        try {
            Customer customer = new Customer();
            DBConnectionFactory myFactory = DBConnectionFactory.getInstance();
            Connection conn = myFactory.getConnection();

            PreparedStatement ps = conn.prepareStatement("SELECT * FROM " + Customer.TABLE_NAME + " WHERE " + Customer.COLUMN_EMAIL + " = ?");
            ps.setString(1, email);

            ResultSet rs = ps.executeQuery();
            customer = getDataFromResultSet(rs).get(0);

            rs.close();
            ps.close();
            conn.close();
        } catch (SQLException x) {
            Logger.getLogger(CustomerDAO.class.getName()).log(Level.SEVERE, null, x);
        } catch (IndexOutOfBoundsException x) {
            System.out.println(x);
            return true;
        }
        return false;
    }
    
    public boolean checkCustomerUsername(String username) {
        try {
            Customer customer = new Customer();
            DBConnectionFactory myFactory = DBConnectionFactory.getInstance();
            Connection conn = myFactory.getConnection();

            PreparedStatement ps = conn.prepareStatement("SELECT * FROM " + Customer.TABLE_NAME + " WHERE " + Customer.COLUMN_USERNAME + " = ?");
            ps.setString(1, username);

            ResultSet rs = ps.executeQuery();
            customer = getDataFromResultSet(rs).get(0);

            rs.close();
            ps.close();
            conn.close();
        } catch (SQLException x) {
            Logger.getLogger(CustomerDAO.class.getName()).log(Level.SEVERE, null, x);
        } catch (IndexOutOfBoundsException x) {
            System.out.println(x);
            return true;
        }
        return false;
    }

    public Customer getCustomerInfo(String username) {
        Customer customer = new Customer();
        try {
            DBConnectionFactory myFactory = DBConnectionFactory.getInstance();
            Connection conn = myFactory.getConnection();

            PreparedStatement ps = conn.prepareStatement("SELECT * FROM " + Customer.TABLE_NAME + " WHERE " + Customer.COLUMN_USERNAME + " = ?");
            ps.setString(1, username);

            ResultSet rs = ps.executeQuery();
            customer = getDataFromResultSet(rs).get(0);

            rs.close();
            ps.close();
            conn.close();
        } catch (SQLException x) {
            Logger.getLogger(CustomerDAO.class.getName()).log(Level.SEVERE, null, x);
        }
        return customer;
    }

    public Customer getCustomerInfo(int customerID) {
        Customer customer = new Customer();
        try {
            DBConnectionFactory myFactory = DBConnectionFactory.getInstance();
            Connection conn = myFactory.getConnection();

            PreparedStatement ps = conn.prepareStatement("SELECT * FROM " + Customer.TABLE_NAME + " WHERE " + Customer.COLUMN_CUSTOMER_ID + " = ?");
            ps.setInt(1, customerID);

            ResultSet rs = ps.executeQuery();
            customer = getDataFromResultSet(rs).get(0);

            rs.close();
            ps.close();
            conn.close();
        } catch (SQLException x) {
            Logger.getLogger(CustomerDAO.class.getName()).log(Level.SEVERE, null, x);
        }
        return customer;
    }

    public ArrayList<Customer> getListOfCustomers() {
        ArrayList<Customer> customers = new ArrayList<>();
        try {
            DBConnectionFactory myFactory = DBConnectionFactory.getInstance();
            Connection conn = myFactory.getConnection();

            PreparedStatement ps = conn.prepareStatement("SELECT * FROM " + Customer.TABLE_NAME + " WHERE " + Customer.COLUMN_FLAG + " >= 0");

            ResultSet rs = ps.executeQuery();
            customers = getDataFromResultSet(rs);

            rs.close();
            ps.close();
            conn.close();
        } catch (SQLException x) {
            Logger.getLogger(CustomerDAO.class.getName()).log(Level.SEVERE, null, x);
        }
        return customers;
    }

    private ArrayList<Customer> getDataFromResultSet(ResultSet rs) throws SQLException {
        ArrayList<Customer> customers = new ArrayList<>();
        while (rs.next()) {
            Customer customer = new Customer();
            customer.setAddress(rs.getString(Customer.COLUMN_ADDRESS));
            customer.setContactNo(rs.getString(Customer.COLUMN_CONTACT_NO));
            customer.setCustomerID(rs.getInt(Customer.COLUMN_CUSTOMER_ID));
            customer.setEmail(rs.getString(Customer.COLUMN_EMAIL));
            customer.setFirstName(rs.getString(Customer.COLUMN_FIRST_NAME));
            customer.setFlag(rs.getInt(Customer.COLUMN_FLAG));
            customer.setLastName(rs.getString(Customer.COLUMN_LAST_NAME));
            customer.setMiddleName(rs.getString(Customer.COLUMN_MIDDLE_NAME));
            customer.setPassword(rs.getString(Customer.COLUMN_PASSWORD));
            customer.setUsername(rs.getString(Customer.COLUMN_USERNAME));
            customer.setBirthday(rs.getString(Customer.COLUMN_BIRTHDAY));
            customer.setMotherMaiden(rs.getString(Customer.COLUMN_MOTHER_MAIDEN));
            customers.add(customer);
        }
        return customers;
    }
}
