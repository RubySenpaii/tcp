/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package dao;

import db.DBConnectionFactory;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;
import object.Inventory;

/**
 *
 * @author RubySenpaii
 */
public class InventoryDAO {

    public boolean recordInventory(Inventory inventory) {
        try {
            DBConnectionFactory myFactory = DBConnectionFactory.getInstance();
            Connection conn = myFactory.getConnection();

            PreparedStatement ps = conn.prepareStatement("INSERT INTO " + Inventory.TABLE_NAME + " "
                    + "(" + Inventory.COLUMN_DATE + ", " + Inventory.COLUMN_DEFECT + ", " + Inventory.COLUMN_IN
                    + ", " + Inventory.COLUMN_OUT + ", " + Inventory.COLUMN_PRODUCT_ID + ", " + Inventory.COLUMN_REFERENCE
                    + ", " + Inventory.COLUMN_TOTAL + ") "
                    + "VALUES(?, ?, ?, ?, ?, ?, ?)");
            ps.setString(1, inventory.getDate());
            ps.setInt(2, inventory.getDefect());
            ps.setInt(3, inventory.getIn());
            ps.setInt(4, inventory.getOut());
            ps.setInt(5, inventory.getProductID());
            ps.setString(6, inventory.getReference());
            ps.setInt(7, inventory.getTotal());

            ps.executeUpdate();
            ps.close();
            conn.close();
        } catch (SQLException x) {
            Logger.getLogger(InventoryDAO.class.getName()).log(Level.SEVERE, null, x);
            return false;
        }
        return true;
    }

    public boolean updateInventoryRecord(Inventory inventory) {
        try {
            DBConnectionFactory myFactory = DBConnectionFactory.getInstance();
            Connection conn = myFactory.getConnection();

            PreparedStatement ps = conn.prepareStatement("UPDATE " + Inventory.TABLE_NAME
                    + " SET " + Inventory.COLUMN_DEFECT + " = ?, " + Inventory.COLUMN_IN + " = ?, " + Inventory.COLUMN_OUT + " = ?, "
                    + Inventory.COLUMN_TOTAL + " = ?, " + Inventory.COLUMN_REFERENCE + " = ? "
                    + "WHERE " + Inventory.COLUMN_PRODUCT_ID + " = ? AND " + Inventory.COLUMN_DATE + " = ?");
            ps.setInt(1, inventory.getDefect());
            ps.setInt(2, inventory.getIn());
            ps.setInt(3, inventory.getOut());
            ps.setInt(4, inventory.getTotal());
            ps.setString(5, inventory.getReference());
            ps.setInt(6, inventory.getProductID());
            ps.setString(7, inventory.getDate());

            ps.executeUpdate();
            ps.close();
            conn.close();
        } catch (SQLException ex) {
            Logger.getLogger(InventoryDAO.class.getName()).log(Level.SEVERE, null, ex);
            return false;
        }
        return true;
    }

    public Inventory getLatestCountForProduct(int productID) {
        ArrayList<Inventory> inventorys = new ArrayList<>();
        try {
            DBConnectionFactory myFactory = DBConnectionFactory.getInstance();
            Connection conn = myFactory.getConnection();

            PreparedStatement ps = conn.prepareStatement("select i.*\n"
                    + "from\n"
                    + "inventory i join\n"
                    + "(select max(str_to_date(i1.dateupdated, '%Y-%m-%d %H:%i:%s')) AS 'date', i1.productid\n"
                    + "from inventory i1\n"
                    + "where i1.productid = ?) t1 on i.productid = t1.productid and i.dateupdated = t1.date");
            ps.setInt(1, productID);

            ResultSet rs = ps.executeQuery();
            inventorys = getDataFromResultSet(rs);

            rs.close();
            ps.close();
            conn.close();
        } catch (SQLException x) {
            Logger.getLogger(InventoryDAO.class.getName()).log(Level.SEVERE, null, x);
        }
        return inventorys.get(0);
    }

    public int getQuantityForDeliveryOfProduct(int productID) {
        int quantity = 0;
        try {
            DBConnectionFactory myFactory = DBConnectionFactory.getInstance();
            Connection conn = myFactory.getConnection();

            PreparedStatement ps = conn.prepareStatement("SELECT P.ProductID, SUM(SOI.Quantity) AS 'QtyForDelivery'\n"
                    + "FROM Product P JOIN SalesOrderItem SOI ON P.ProductID = SOI.ProductID\n"
                    + "				JOIN SalesOrder SO ON SOI.SalesOrderNumber = SO.SalesOrderNumber\n"
                    + "WHERE SO.OrderStatus = \"Delivery\" AND P.ProductID = ?\n"
                    + "GROUP BY P.ProductID");
            ps.setInt(1, productID);

            ResultSet rs = ps.executeQuery();
            while (rs.next()) {
                quantity = rs.getInt("QtyForDelivery");
            }

            rs.close();
            ps.close();
            conn.close();
        } catch (SQLException x) {
            Logger.getLogger(InventoryDAO.class.getName()).log(Level.SEVERE, null, x);
        }
        return quantity;
    }

    public ArrayList<Inventory> getInventoryHistoryForProduct(int productID) {
        ArrayList<Inventory> inventorys = new ArrayList<>();
        try {
            DBConnectionFactory myFactory = DBConnectionFactory.getInstance();
            Connection conn = myFactory.getConnection();

            PreparedStatement ps = conn.prepareStatement("SELECT * FROM " + Inventory.TABLE_NAME + " WHERE " + Inventory.COLUMN_PRODUCT_ID + " = ?");
            ps.setInt(1, productID);

            ResultSet rs = ps.executeQuery();
            inventorys = getDataFromResultSet(rs);

            rs.close();
            ps.close();
            conn.close();
        } catch (SQLException x) {
            Logger.getLogger(InventoryDAO.class.getName()).log(Level.SEVERE, null, x);
        }
        return inventorys;
    }

    public ArrayList<Inventory> getListOfProductReferences(int productID, String reference) {
        ArrayList<Inventory> inventorys = new ArrayList<>();
        try {
            DBConnectionFactory myFactory = DBConnectionFactory.getInstance();
            Connection conn = myFactory.getConnection();

            PreparedStatement ps = conn.prepareStatement("SELECT * FROM " + Inventory.TABLE_NAME
                    + " WHERE " + Inventory.COLUMN_PRODUCT_ID + " = ? AND " + Inventory.COLUMN_REFERENCE + " = ?");
            ps.setInt(1, productID);
            ps.setString(2, reference);

            ResultSet rs = ps.executeQuery();
            inventorys = getDataFromResultSet(rs);

            rs.close();
            ps.close();
            conn.close();
        } catch (SQLException x) {
            Logger.getLogger(InventoryDAO.class.getName()).log(Level.SEVERE, null, x);
        }
        return inventorys;
    }

    public ArrayList<Inventory> getListOfInventories() {
        ArrayList<Inventory> inventorys = new ArrayList<>();
        try {
            DBConnectionFactory myFactory = DBConnectionFactory.getInstance();
            Connection conn = myFactory.getConnection();

            PreparedStatement ps = conn.prepareStatement("SELECT * FROM " + Inventory.TABLE_NAME);

            ResultSet rs = ps.executeQuery();
            inventorys = getDataFromResultSet(rs);

            rs.close();
            ps.close();
            conn.close();
        } catch (SQLException x) {
            Logger.getLogger(InventoryDAO.class.getName()).log(Level.SEVERE, null, x);
        }
        return inventorys;
    }

    private ArrayList<Inventory> getDataFromResultSet(ResultSet rs) throws SQLException {
        ArrayList<Inventory> inventorys = new ArrayList<>();
        while (rs.next()) {
            Inventory inventory = new Inventory();
            inventory.setDate(rs.getString(Inventory.COLUMN_DATE));
            inventory.setDefect(rs.getInt(Inventory.COLUMN_DEFECT));
            inventory.setIn(rs.getInt(Inventory.COLUMN_IN));
            inventory.setOut(rs.getInt(Inventory.COLUMN_OUT));
            inventory.setProductID(rs.getInt(Inventory.COLUMN_PRODUCT_ID));
            inventory.setReference(rs.getString(Inventory.COLUMN_REFERENCE));
            inventory.setTotal(rs.getInt(Inventory.COLUMN_TOTAL));
            inventorys.add(inventory);
        }
        return inventorys;
    }
}
