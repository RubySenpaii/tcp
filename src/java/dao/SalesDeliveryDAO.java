/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package dao;

import db.DBConnectionFactory;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;
import object.Employee;
import object.SalesDelivery;

/**
 *
 * @author RubySenpaii
 */
public class SalesDeliveryDAO {
    
    public boolean addSalesDelivery(SalesDelivery salesDelivery) {
        try {
            DBConnectionFactory myFactory = DBConnectionFactory.getInstance();
            Connection conn = myFactory.getConnection();
            
            PreparedStatement ps = conn.prepareStatement("INSERT INTO " + SalesDelivery.TABLE_NAME + " "
                    + "(" + SalesDelivery.COLUMN_DATE_UPDATED + ", " + SalesDelivery.COLUMN_DELIVERY_STATUS + ", " + SalesDelivery.COLUMN_SALES_ORDER_NUMBER 
                    + ", " + SalesDelivery.COLUMN_UPDATED_BY + ", " + SalesDelivery.COLUMN_REMARKS + ") "
                    + "VALUES(?, ?, ?, ?, ?)");
            ps.setString(1, salesDelivery.getDateUpdated());
            ps.setString(2, salesDelivery.getDeliveryStatus());
            ps.setInt(3, salesDelivery.getSalesOrderNumber());
            ps.setInt(4, salesDelivery.getUpdatedBy());
            ps.setString(5, salesDelivery.getRemarks());
            
            ps.executeUpdate();
            ps.close();
            conn.close();
        } catch (SQLException x) {
            Logger.getLogger(SalesDeliveryDAO.class.getName()).log(Level.SEVERE, null, x);
            return false;
        }
        return true;
    }
    
    public SalesDelivery getCurrentSalesDeliveryStatus(int salesOrderNumber) {
        ArrayList<SalesDelivery> salesDeliveries = new ArrayList<>();
        try {
            DBConnectionFactory myFactory = DBConnectionFactory.getInstance();
            Connection conn = myFactory.getConnection();
            System.out.println("getting delivery status of sales order" + salesOrderNumber);
            
            PreparedStatement ps = conn.prepareStatement("select sd1.* \n"
                    + "from \n"
                    + "salesdelivery sd1 JOIN \n"
                    + "(select max(str_to_date(sd.DateUpdated, '%Y-%m-%d %H:%i:%s')) AS 'currentstatus', sd.salesordernumber \n"
                    + "from salesdelivery sd \n"
                    + "where sd.salesordernumber = ? \n"
                    + "group by sd.salesordernumber) T1 on sd1.salesordernumber = T1.salesordernumber AND T1.currentstatus = sd1.dateupdated;");
            ps.setInt(1, salesOrderNumber);
            
            ResultSet rs = ps.executeQuery();
            salesDeliveries = getDataFromResultSet(rs);
            
            rs.close();
            ps.close();
            conn.close();
        } catch (SQLException x) {
            Logger.getLogger(SalesDeliveryDAO.class.getName()).log(Level.SEVERE, null, x);
        }
        return salesDeliveries.get(0);
    }
    
    public ArrayList<SalesDelivery> getListOfSalesDeliveries() {
        ArrayList<SalesDelivery> salesDeliveries = new ArrayList<>();
        try {
            DBConnectionFactory myFactory = DBConnectionFactory.getInstance();
            Connection conn = myFactory.getConnection();
            
            PreparedStatement ps = conn.prepareStatement("SELECT * FROM " + SalesDelivery.TABLE_NAME);
            
            ResultSet rs = ps.executeQuery();
            salesDeliveries = getDataFromResultSet(rs);
            
            rs.close();
            ps.close();
            conn.close();
        } catch (SQLException x) {
            Logger.getLogger(SalesDeliveryDAO.class.getName()).log(Level.SEVERE, null, x);
        }
        return salesDeliveries;
    }
    
    public ArrayList<SalesDelivery> getDeliveryUpdatesForSO(int salesOrderNumber) {
        ArrayList<SalesDelivery> salesDeliveries = new ArrayList<>();
        try {
            DBConnectionFactory myFactory = DBConnectionFactory.getInstance();
            Connection conn = myFactory.getConnection();
            
            PreparedStatement ps = conn.prepareStatement("select *\n"
                    + "from salesdelivery\n"
                    + "where salesordernumber = ?\n"
                    + "order by str_to_date(DateUpdated, '%Y-%m-%d %H:%i:%s') desc");
            ps.setInt(1, salesOrderNumber);
            
            ResultSet rs = ps.executeQuery();
            salesDeliveries = getDataFromResultSet(rs);
            
            rs.close();
            ps.close();
            conn.close();
        } catch (SQLException x) {
            Logger.getLogger(SalesDeliveryDAO.class.getName()).log(Level.SEVERE, null, x);
        }
        return salesDeliveries;
    }
    
    private ArrayList<SalesDelivery> getDataFromResultSet(ResultSet rs) throws SQLException {
        ArrayList<SalesDelivery> salesDeliveries = new ArrayList<>();
        while (rs.next()) {
            SalesDelivery salesDelivery = new SalesDelivery();
            salesDelivery.setDateUpdated(rs.getString(SalesDelivery.COLUMN_DATE_UPDATED));
            salesDelivery.setDeliveryStatus(rs.getString(SalesDelivery.COLUMN_DELIVERY_STATUS));
            salesDelivery.setRemarks(rs.getString(SalesDelivery.COLUMN_REMARKS));
            salesDelivery.setSalesOrderNumber(rs.getInt(SalesDelivery.COLUMN_SALES_ORDER_NUMBER));
            salesDelivery.setUpdatedBy(rs.getInt(SalesDelivery.COLUMN_UPDATED_BY));
            Employee employee = new EmployeeDAO().getEmployeeInfo(salesDelivery.getUpdatedBy());
            salesDelivery.setUpdatedName(employee.getLastName() + ", " + employee.getFirstName());
            salesDeliveries.add(salesDelivery);
        }
        return salesDeliveries;
    }
}
