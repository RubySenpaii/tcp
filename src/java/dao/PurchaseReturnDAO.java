/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package dao;

import db.DBConnectionFactory;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;
import object.PurchaseReturn;

/**
 *
 * @author RubySenpaii
 */
public class PurchaseReturnDAO {
    
    public boolean addPurchaseReturn(PurchaseReturn purchaseReturn) {
        try {
            DBConnectionFactory myFactory = DBConnectionFactory.getInstance();
            Connection conn = myFactory.getConnection();

            PreparedStatement ps = conn.prepareStatement("INSERT INTO " + PurchaseReturn.TABLE_NAME + " "
                    + "(" + PurchaseReturn.COLUMN_PRODUCT_NAME + ", " + PurchaseReturn.COLUMN_PURCHASE_ORDER_NUMBER + ", " + PurchaseReturn.COLUMN_QUANTITY
                    + ", " + PurchaseReturn.COLUMN_RETURN_STATUS + ") "
                    + "VALUES(?, ?, ?, ?)");
            ps.setString(1, purchaseReturn.getProductName());
            ps.setInt(2, purchaseReturn.getPurchaseOrderNumber());
            ps.setInt(3, purchaseReturn.getQuantity());
            ps.setString(4, purchaseReturn.getReturnStatus());

            ps.executeUpdate();
            ps.close();
            conn.close();
        } catch (SQLException x) {
            Logger.getLogger(PurchaseOrderDAO.class.getName()).log(Level.SEVERE, null, x);
            return false;
        }
        return true;
    }
    
    public boolean updatePurchaseReturn(PurchaseReturn purchaseReturn) {
        try {
            DBConnectionFactory myFactory = DBConnectionFactory.getInstance();
            Connection conn = myFactory.getConnection();

            PreparedStatement ps = conn.prepareStatement("UPDATE " + PurchaseReturn.TABLE_NAME
                    + " SET " + PurchaseReturn.COLUMN_RETURN_STATUS + " = ? "
                    + " WHERE " + PurchaseReturn.COLUMN_PRODUCT_NAME + " = ? AND " + PurchaseReturn.COLUMN_PURCHASE_ORDER_NUMBER + " = ? AND " 
                    + PurchaseReturn.COLUMN_QUANTITY + " = ?");
            ps.setString(1, purchaseReturn.getReturnStatus());
            ps.setString(2, purchaseReturn.getProductName());
            ps.setInt(3, purchaseReturn.getPurchaseOrderNumber());
            ps.setInt(4, purchaseReturn.getQuantity());

            ps.executeUpdate();
            ps.close();
            conn.close();
        } catch (SQLException ex) {
            Logger.getLogger(CustomerDAO.class.getName()).log(Level.SEVERE, null, ex);
            return false;
        }
        return true;
    }
    
    public ArrayList<PurchaseReturn> getListOfPurchaseReturns() {
        ArrayList<PurchaseReturn> purchaseReturns = new ArrayList<>();
        try {
            DBConnectionFactory myFactory = DBConnectionFactory.getInstance();
            Connection conn = myFactory.getConnection();

            PreparedStatement ps = conn.prepareStatement("SELECT * FROM " + PurchaseReturn.TABLE_NAME);

            ResultSet rs = ps.executeQuery();
            purchaseReturns = getDataFromResultSet(rs);

            rs.close();
            ps.close();
            conn.close();
        } catch (SQLException x) {
            Logger.getLogger(PurchaseOrderDAO.class.getName()).log(Level.SEVERE, null, x);
        }
        return purchaseReturns;
    }
    
    public ArrayList<PurchaseReturn> getListOfReturnedPurchaseReturns() {
        ArrayList<PurchaseReturn> purchaseReturns = new ArrayList<>();
        try {
            DBConnectionFactory myFactory = DBConnectionFactory.getInstance();
            Connection conn = myFactory.getConnection();

            PreparedStatement ps = conn.prepareStatement("SELECT * FROM " + PurchaseReturn.TABLE_NAME + " WHERE " + PurchaseReturn.COLUMN_RETURN_STATUS + " = ?");
            ps.setString(1, "Returned");

            ResultSet rs = ps.executeQuery();
            purchaseReturns = getDataFromResultSet(rs);

            rs.close();
            ps.close();
            conn.close();
        } catch (SQLException x) {
            Logger.getLogger(PurchaseOrderDAO.class.getName()).log(Level.SEVERE, null, x);
        }
        return purchaseReturns;
    }
    
    public ArrayList<PurchaseReturn> getListOfReceivedPurchaseReturns() {
        ArrayList<PurchaseReturn> purchaseReturns = new ArrayList<>();
        try {
            DBConnectionFactory myFactory = DBConnectionFactory.getInstance();
            Connection conn = myFactory.getConnection();

            PreparedStatement ps = conn.prepareStatement("SELECT * FROM " + PurchaseReturn.TABLE_NAME + " WHERE " + PurchaseReturn.COLUMN_RETURN_STATUS + " = ?");
            ps.setString(1, "Received");

            ResultSet rs = ps.executeQuery();
            purchaseReturns = getDataFromResultSet(rs);

            rs.close();
            ps.close();
            conn.close();
        } catch (SQLException x) {
            Logger.getLogger(PurchaseOrderDAO.class.getName()).log(Level.SEVERE, null, x);
        }
        return purchaseReturns;
    }
    
    private ArrayList<PurchaseReturn> getDataFromResultSet(ResultSet rs) throws SQLException {
        ArrayList<PurchaseReturn> purchaseReturns = new ArrayList<>();
        while (rs.next()) {
            PurchaseReturn purchaseReturn = new PurchaseReturn();
            purchaseReturn.setProductName(rs.getString(PurchaseReturn.COLUMN_PRODUCT_NAME));
            purchaseReturn.setPurchaseOrderNumber(rs.getInt(PurchaseReturn.COLUMN_PURCHASE_ORDER_NUMBER));
            purchaseReturn.setQuantity(rs.getInt(PurchaseReturn.COLUMN_QUANTITY));
            purchaseReturn.setReturnStatus(rs.getString(PurchaseReturn.COLUMN_RETURN_STATUS));
            purchaseReturns.add(purchaseReturn);
        }
        return purchaseReturns;
    }
}
