/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package report;

import java.io.File;
import java.io.FileNotFoundException;
import java.sql.SQLException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashMap;
import java.util.Map;
import net.sf.jasperreports.engine.JRException;
import net.sf.jasperreports.engine.JasperExportManager;
import net.sf.jasperreports.engine.JasperFillManager;
import net.sf.jasperreports.engine.JasperPrint;
import net.sf.jasperreports.engine.JasperReport;
import net.sf.jasperreports.engine.data.JRBeanCollectionDataSource;
import net.sf.jasperreports.engine.util.JRLoader;
import object.Customer;
import object.Employee;
import object.Product;
import object.PurchaseOrder;
import object.SalesOrder;
import object.Supplier;

/**
 *
 * @author RubySenpaii
 */
public class ReportingModule {
    
    public void createSalesReport(String imgPath, String source, String fileName, String preparedBy, String dateRange, ArrayList<SalesOrder> salesOrders) 
            throws JRException, FileNotFoundException, SQLException {
        File file = new File(source);
        Map parameters = new HashMap();
        parameters.put("dateRange", dateRange);
        parameters.put("preparedBy", preparedBy);
        parameters.put("imgPath", imgPath);
        JasperPrint jasperPrint;

        JRBeanCollectionDataSource dataSource = new JRBeanCollectionDataSource(salesOrders, false);
        JasperReport jasperPlantingReport = (JasperReport) JRLoader.loadObject(file);
        jasperPrint = JasperFillManager.fillReport(jasperPlantingReport, parameters, dataSource);
        JasperExportManager.exportReportToPdfFile(jasperPrint, fileName);
    }
    
    public void createPurchaseReport(String imgPath, String source, String fileName, String preparedBy, String dateRange, ArrayList<PurchaseOrder> purchaseOrders) 
            throws JRException, FileNotFoundException, SQLException {
        File file = new File(source);
        Map parameters = new HashMap();
        parameters.put("dateRange", dateRange);
        parameters.put("preparedBy", preparedBy);
        parameters.put("imgPath", imgPath);
        JasperPrint jasperPrint;

        JRBeanCollectionDataSource dataSource = new JRBeanCollectionDataSource(purchaseOrders, false);
        JasperReport jasperPlantingReport = (JasperReport) JRLoader.loadObject(file);
        jasperPrint = JasperFillManager.fillReport(jasperPlantingReport, parameters, dataSource);
        JasperExportManager.exportReportToPdfFile(jasperPrint, fileName);
    }
    
    public void createInventoryReport(String imgPath, String source, String fileName, String preparedBy, String dateRange, ArrayList<Product> products) 
            throws JRException, FileNotFoundException, SQLException {
        File file = new File(source);
        Map parameters = new HashMap();
        parameters.put("dateRange", dateRange);
        parameters.put("preparedBy", preparedBy);
        parameters.put("imgPath", imgPath);
        JasperPrint jasperPrint;

        System.out.println(products.size());
        JRBeanCollectionDataSource dataSource = new JRBeanCollectionDataSource(products, false);
        JasperReport jasperPlantingReport = (JasperReport) JRLoader.loadObject(file);
        jasperPrint = JasperFillManager.fillReport(jasperPlantingReport, parameters, dataSource);
        JasperExportManager.exportReportToPdfFile(jasperPrint, fileName);
    }
    
    public void createReceipt(String imgPath, String source, String fileName, String preparedBy, SalesOrder salesOrder, ArrayList<Product> products) 
            throws JRException, FileNotFoundException, SQLException {
        File file = new File(source);
        Map parameters = new HashMap();
        parameters.put("customerName", salesOrder.getCustomerName());
        parameters.put("requestedDate", salesOrder.getRequestDelivery());
        parameters.put("preparedBy", preparedBy);
        parameters.put("salesOrderNumber", salesOrder.getSalesOrderNumber());
        parameters.put("orderDate", salesOrder.getOrderDate());
        parameters.put("incharge", salesOrder.getDeliverer());
        parameters.put("imgPath", imgPath);
        JasperPrint jasperPrint;

        JRBeanCollectionDataSource dataSource = new JRBeanCollectionDataSource(products, false);
        JasperReport jasperPlantingReport = (JasperReport) JRLoader.loadObject(file);
        jasperPrint = JasperFillManager.fillReport(jasperPlantingReport, parameters, dataSource);
        JasperExportManager.exportReportToPdfFile(jasperPrint, fileName);
    }
    
    public void createCustomerSalesReport(String imgPath, String source, String fileName, String preparedBy, String dateRange, Customer customer, ArrayList<SalesOrder> salesOrders) 
            throws JRException, FileNotFoundException, SQLException {
        File file = new File(source);
        Map parameters = new HashMap();
        parameters.put("dateRange", dateRange);
        parameters.put("preparedBy", preparedBy);
        parameters.put("customerName", customer.getLastName() + ", " + customer.getFirstName());
        parameters.put("address", customer.getAddress());
        parameters.put("email", customer.getEmail());
        parameters.put("contactNo", customer.getContactNo());
        parameters.put("imgPath", imgPath);
        JasperPrint jasperPrint;

        JRBeanCollectionDataSource dataSource = new JRBeanCollectionDataSource(salesOrders, false);
        JasperReport jasperPlantingReport = (JasperReport) JRLoader.loadObject(file);
        jasperPrint = JasperFillManager.fillReport(jasperPlantingReport, parameters, dataSource);
        JasperExportManager.exportReportToPdfFile(jasperPrint, fileName);
    }
    
    public void createCustomerList(String imgPath, String source, String fileName, String preparedBy, String date, ArrayList<Customer> customers) 
            throws JRException, FileNotFoundException, SQLException {
        File file = new File(source);
        Map parameters = new HashMap();
        parameters.put("date", date);
        parameters.put("preparedBy", preparedBy);
        parameters.put("imgPath", imgPath);
        JasperPrint jasperPrint;

        JRBeanCollectionDataSource dataSource = new JRBeanCollectionDataSource(customers, false);
        JasperReport jasperPlantingReport = (JasperReport) JRLoader.loadObject(file);
        jasperPrint = JasperFillManager.fillReport(jasperPlantingReport, parameters, dataSource);
        JasperExportManager.exportReportToPdfFile(jasperPrint, fileName);
    }
    
    public void createEmployeeList(String imgPath, String source, String fileName, String preparedBy, String date, ArrayList<Employee> employees) 
            throws JRException, FileNotFoundException, SQLException {
        File file = new File(source);
        Map parameters = new HashMap();
        parameters.put("date", date);
        parameters.put("preparedBy", preparedBy);
        parameters.put("imgPath", imgPath);
        JasperPrint jasperPrint;

        JRBeanCollectionDataSource dataSource = new JRBeanCollectionDataSource(employees, false);
        JasperReport jasperPlantingReport = (JasperReport) JRLoader.loadObject(file);
        jasperPrint = JasperFillManager.fillReport(jasperPlantingReport, parameters, dataSource);
        JasperExportManager.exportReportToPdfFile(jasperPrint, fileName);
    }
    
    public void createSupplierList(String imgPath, String source, String fileName, String preparedBy, String date, ArrayList<Supplier> suppliers) 
            throws JRException, FileNotFoundException, SQLException {
        File file = new File(source);
        Map parameters = new HashMap();
        parameters.put("date", date);
        parameters.put("preparedBy", preparedBy);
        parameters.put("imgPath", imgPath);
        JasperPrint jasperPrint;

        JRBeanCollectionDataSource dataSource = new JRBeanCollectionDataSource(suppliers, false);
        JasperReport jasperPlantingReport = (JasperReport) JRLoader.loadObject(file);
        jasperPrint = JasperFillManager.fillReport(jasperPlantingReport, parameters, dataSource);
        JasperExportManager.exportReportToPdfFile(jasperPrint, fileName);
    }
    
    public void createInventoryList(String imgPath, String source, String fileName, String preparedBy, String date, ArrayList<Product> products) 
            throws JRException, FileNotFoundException, SQLException {
        File file = new File(source);
        Map parameters = new HashMap();
        parameters.put("date", date);
        parameters.put("preparedBy", preparedBy);
        parameters.put("imgPath", imgPath);
        JasperPrint jasperPrint;

        JRBeanCollectionDataSource dataSource = new JRBeanCollectionDataSource(products, false);
        JasperReport jasperPlantingReport = (JasperReport) JRLoader.loadObject(file);
        jasperPrint = JasperFillManager.fillReport(jasperPlantingReport, parameters, dataSource);
        JasperExportManager.exportReportToPdfFile(jasperPrint, fileName);
    }
}
