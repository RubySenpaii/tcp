/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package db;

import extra.Reference;
import java.awt.HeadlessException;
import java.io.File;
import java.io.IOException;
import java.net.URISyntaxException;
import java.security.CodeSource;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.util.Calendar;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author RubySenpaii
 */
public class Backups {

    public boolean createBackup(String filePath) {
        try {
            /*NOTE: Creating Database Constraints*/
            String dbName = "tcpdb";
            String dbUser = "root";
            String dbPass = "1234";

            String dateTimeNow = Reference.DATETIME_NOSECONDS_FORMAT.format(Calendar.getInstance().getTime());
            String fileName = "backupAsOf" + dateTimeNow;
            fileName = fileName.replace(" ", "");
            fileName = fileName.replace("-", "");
            fileName = fileName.replace(":", "");

            /*NOTE: Creating Path Constraints for backup saving*/
 /*NOTE: Here the backup is saved in a folder called backup with the name backup.sql*/
            String savePath = filePath + File.separator + fileName;

            System.out.println(savePath);
            /*NOTE: Used to create a cmd command*/
            String executeCmd = "mysqldump -u" + dbUser + " -p" + dbPass + " " + dbName + " -r \"" + savePath + "\"";

            /*NOTE: Executing the command here*/
            Process runtimeProcess = Runtime.getRuntime().exec(executeCmd);
            int processComplete = runtimeProcess.waitFor();

            /*NOTE: processComplete=0 if correctly executed, will contain other values if not*/
            if (processComplete == 0) {
                System.out.println("Backup Complete");
                return true;
            } else {
                System.out.println("Backup Failure");
                return false;
            }
        } catch (IOException | InterruptedException ex) {
            System.out.println(ex);
            return false;
        }
    }

    public boolean restoreBackup(String backupFile) {
        try {
            /*NOTE: Creating Database Constraints*/
            String dbName = "tcpdb";
            String dbUser = "root";
            String dbPass = "1234";

            /*NOTE: Used to create a cmd command*/
            /*NOTE: Do not create a single large string, this will cause buffer locking, use string array*/
            String[] executeCmd = new String[]{"mysql", dbName, "-u" + dbUser, "-p" + dbPass, "-e", " source " + backupFile};

            /*NOTE: processComplete=0 if correctly executed, will contain other values if not*/
            Process runtimeProcess = Runtime.getRuntime().exec(executeCmd);
            int processComplete = runtimeProcess.waitFor();

            /*NOTE: processComplete=0 if correctly executed, will contain other values if not*/
            if (processComplete == 0) {
                System.out.println("Restore Complete");
                return true;
            } else {
                System.out.println("Restore Failure");
                return false;
            }
        } catch (IOException | InterruptedException | HeadlessException ex) {
            System.out.println("ex");
            return false;
        }
    }
}
