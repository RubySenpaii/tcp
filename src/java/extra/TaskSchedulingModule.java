/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package extra;

import db.Backups;
import java.util.Calendar;
import java.util.Date;
import java.util.Timer;
import java.util.TimerTask;

/**
 *
 * @author RubySenpaii
 */
public class TaskSchedulingModule extends TimerTask {

    private final static long ONCE_PER_DAY = 1000 * 60 * 60 * 24;

    private final static int BACKUP_HOUR = 19;
    private final static int BACKUP_MINUTES = 00;
    private static String filePath = "";

    @Override
    public void run() {
        System.out.println("beginning automatic backup at: " + Reference.DATABASE_DATETIME_FORMAT.format(Calendar.getInstance().getTime()));
        Backups backups = new Backups();
        boolean created = backups.createBackup(filePath);
        System.out.println("automatic backup created: " + created);
        System.out.println("ending automatic backup at: " + Reference.DATABASE_DATETIME_FORMAT.format(Calendar.getInstance().getTime()));
    }

    private Date getBackupTime() {
        Date date2am = new java.util.Date();
        date2am.setHours(BACKUP_HOUR);
        date2am.setMinutes(BACKUP_MINUTES);
        return date2am;
    }

    //call this method from your servlet init method
    public void startTask(String filePath) {
        System.out.println("task initiated");
        TaskSchedulingModule.filePath = filePath;
        TaskSchedulingModule task = new TaskSchedulingModule();
        Timer timer = new Timer();
        timer.schedule(task, getBackupTime(), ONCE_PER_DAY);
    }
}
