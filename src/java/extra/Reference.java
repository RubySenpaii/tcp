/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package extra;

import java.text.DecimalFormat;
import java.text.SimpleDateFormat;

/**
 *
 * @author RubySenpaii
 */
public class Reference {
    public static final SimpleDateFormat DATABASE_DATE_FORMAT = new SimpleDateFormat("yyyy-MM-dd");
    public static final SimpleDateFormat DATABASE_DATETIME_FORMAT = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
    public static final SimpleDateFormat DATETIME_NOSECONDS_FORMAT = new SimpleDateFormat("yyyy-MM-dd HH:mm");
    public static final SimpleDateFormat FULL_DATE_FORMAT = new SimpleDateFormat("MMMM dd, yyyy");
    public static final int UNASSIGNED = -1;
    public static final int TRUE = 1, ACTIVE = 1;
    public static final int FALSE = 0, ARCHIVED = 0;
    
    public static String doubleToString(double value) {
        DecimalFormat df = new DecimalFormat("#,###.00");
        if (value > 0 && value < 1) {
            return "0" + df.format(value);
        } else if (value != 0) {
            return df.format(value);
        } else {
            return "0.00";
        }
    }
}
