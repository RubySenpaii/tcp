/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package extra;

import java.util.*;
import javax.mail.*;
import javax.mail.internet.*;
import javax.activation.*;
import object.Product;
import object.PurchaseOrder;
import object.PurchaseOrderItem;
import object.Supplier;
import object.Customer;
import object.Employee;

/**
 *
 * @author RubySenpaii
 */
public class EmailSender {

    final String SSL_FACTORY = "javax.net.ssl.SSLSocketFactory";

    public boolean sendPurchaseOrderDetails(Supplier supplier, PurchaseOrder purchaseOrder, ArrayList<PurchaseOrderItem> items) {
        // Get system properties
        Properties props = System.getProperties();
        // Setup mail server
        props.setProperty("mail.smtp.host", "smtp.gmail.com");
        props.setProperty("mail.smtp.socketFactory.class", SSL_FACTORY);
        props.setProperty("mail.smtp.socketFactory.fallback", "true");
        props.setProperty("mail.smtp.port", "587");
        props.setProperty("mail.smtp.socketFactory.port", "587");
        props.put("mail.smtp.auth", "true");
        props.put("mail.debug", "true");
        props.put("mail.store.protocol", "pop3");
        props.put("mail.transport.protocol", "smtp");
        props.put("mail.smtp.starttls.enable", "true");

        final String username = "tcp.testserver@gmail.com";//
        final String password = "tcpserver1";
        try {
            Session session = Session.getInstance(props,
                    new Authenticator() {
                protected PasswordAuthentication getPasswordAuthentication() {
                    return new PasswordAuthentication(username, password);
                }
            });

            // -- Create a new message --
            Message msg = new MimeMessage(session);

            StringBuilder sb = new StringBuilder();
            sb.append(purchaseOrderMessage(supplier, purchaseOrder, items));
            // -- Set the FROM and TO fields --
            msg.setFrom(new InternetAddress("tcp.testserver@gmail.com"));
            msg.setRecipients(Message.RecipientType.TO,
                    InternetAddress.parse(supplier.getEmail(), false));
            msg.setSubject("Purchase Order#");
            msg.setContent(sb.toString(), "text/html");
            msg.setSentDate(new Date());
            Transport.send(msg);
            System.out.println("Message sent.");
            return true;
        } catch (MessagingException e) {
            System.out.println("Error: " + e);
            return false;
        }
    }
    
    public void sendCustomerPassword(Customer customer) {
        // Get system properties
        Properties props = System.getProperties();
        // Setup mail server
        props.setProperty("mail.smtp.host", "smtp.gmail.com");
        props.setProperty("mail.smtp.socketFactory.class", SSL_FACTORY);
        props.setProperty("mail.smtp.socketFactory.fallback", "true");
        props.setProperty("mail.smtp.port", "587");
        props.setProperty("mail.smtp.socketFactory.port", "587");
        props.put("mail.smtp.auth", "true");
        props.put("mail.debug", "true");
        props.put("mail.store.protocol", "pop3");
        props.put("mail.transport.protocol", "smtp");
        props.put("mail.smtp.starttls.enable", "true");

        final String username = "tcp.testserver@gmail.com";//
        final String password = "tcpserver1";
        try {
            Session session = Session.getInstance(props,
                    new Authenticator() {
                protected PasswordAuthentication getPasswordAuthentication() {
                    return new PasswordAuthentication(username, password);
                }
            });

            // -- Create a new message --
            Message msg = new MimeMessage(session);

            StringBuilder sb = new StringBuilder();
            // -- Set the FROM and TO fields --
            msg.setFrom(new InternetAddress("tcp.testserver@gmail.com"));
            msg.setRecipients(Message.RecipientType.TO,
                    InternetAddress.parse(customer.getEmail(), false));
            msg.setSubject("Password Reset");
            msg.setText("Your password is: " + customer.getPassword());
            msg.setSentDate(new Date());
            Transport.send(msg);
            System.out.println("Message sent.");
        } catch (MessagingException e) {
            System.out.println("Error: " + e);
        }
    }
    
    public void sendEmployeePassword(Employee employee) {
        // Get system properties
        Properties props = System.getProperties();
        // Setup mail server
        props.setProperty("mail.smtp.host", "smtp.gmail.com");
        props.setProperty("mail.smtp.socketFactory.class", SSL_FACTORY);
        props.setProperty("mail.smtp.socketFactory.fallback", "true");
        props.setProperty("mail.smtp.port", "587");
        props.setProperty("mail.smtp.socketFactory.port", "587");
        props.put("mail.smtp.auth", "true");
        props.put("mail.debug", "true");
        props.put("mail.store.protocol", "pop3");
        props.put("mail.transport.protocol", "smtp");
        props.put("mail.smtp.starttls.enable", "true");

        final String username = "tcp.testserver@gmail.com";//
        final String password = "tcpserver1";
        try {
            Session session = Session.getInstance(props,
                    new Authenticator() {
                protected PasswordAuthentication getPasswordAuthentication() {
                    return new PasswordAuthentication(username, password);
                }
            });

            // -- Create a new message --
            Message msg = new MimeMessage(session);

            StringBuilder sb = new StringBuilder();
            // -- Set the FROM and TO fields --
            msg.setFrom(new InternetAddress("tcp.testserver@gmail.com"));
            msg.setRecipients(Message.RecipientType.TO,
                    InternetAddress.parse(employee.getEmail(), false));
            msg.setSubject("Password Reset");
            msg.setText("Your password is: " + employee.getPassword());
            msg.setSentDate(new Date());
            Transport.send(msg);
            System.out.println("Message sent.");
        } catch (MessagingException e) {
            System.out.println("Error: " + e);
        }
    }

    private String purchaseOrderMessage(Supplier supplier, PurchaseOrder purchaseOrder, ArrayList<PurchaseOrderItem> items) {
        String itemMessage = "";
        for (int a = 0; a < items.size(); a++) {
            double price = items.get(a).getQuantity()* items.get(a).getUnitPrice();
            itemMessage += "<tr>";
            itemMessage += "<td>" + items.get(a).getProductName() + "</td>";
            itemMessage += "<td>" + items.get(a).getQuantity() + "</td>";
            itemMessage += "<td>Php " + items.get(a).getUnitPrice()+ "</td>";
            itemMessage += "<td>Php " + price + "</td>";
            itemMessage += "</tr>";
        }


        String message = "<div class=\"row\">\n"
                + "	<div class=\"col-md-12\">\n"
                + "		<div class=\"col-md-4\">\n"
                + "			<h4>Supplier Details</h4>\n"
                + "			<address>\n"
                + "				<strong>" + supplier.getSupplierName() + "</strong>\n"
                + "				<br>" + supplier.getAddress() + "\n"
                + "				<br>\n"
                + "				<abbr title=\"Contact Number\">CN:</abbr> " + supplier.getContactNo() + "\n"
                + "			</address>\n"
                + "		</div>\n"
                + "\n"
                + "		<div class=\"col-md-4\">\n"
                + "			<h4>Billed To:</h4>\n"
                + "			<address>\n"
                + "				<strong>" + purchaseOrder.getContactPerson() + "</strong>\n"
                + "				<br>" + purchaseOrder.getAddress() + "\n"
                + "				<br>\n"
                + "				Payment: " + purchaseOrder.getPaymentOption() + "\n"
                + "				<br>\n"
                + "				<abbr title=\"Contact Number\">CN:</abbr> " + purchaseOrder.getContactNo() + "\n"
                + "				<br>\n"
                + "				<abbr title=\"Requested Date\">RD:</abbr> " + purchaseOrder.getRequestedDelivery() + "\n"
                + "			</address>\n"
                + "		</div>\n"
                + "	</div>\n<br><br>"
                + "	<div class=\"col-md-12\">\n"
                + "		<table class=\"table table-bordered table-hover table-review\">\n"
                + "			<thead>\n"
                + "				<tr>\n"
                + "					<th style=\"width: 10%\">Product Name</th>\n"
                + "					<th style=\"width: 10%\">Quantity</th>\n"
                + "					<th style=\"width: 10%\">Unit Price</th>\n"
                + "					<th style=\"width: 10%\">Total Price</th>\n"
                + "				</tr>\n"
                + "			</thead>\n"
                + "			<tbody>\n" + itemMessage
                + "			</tbody>\n"
                + "		</table>\n"
                + "	</div>\n<br><br><br>"
                + "	<div class=\"col-md-12\">\n"
                + "		<div class=\"form-group\">\n"
                + "			<h4>Remarks:</h4> \n" + purchaseOrder.getRemarks()
                + "		</div>\n"
                + "	</div>\n"
                + "</div>";
        return message;
    }
}
